=====
DataG
=====






Input Data Generator for TeamPlay Security Quantifier



Features
--------
Data Generator: Used to create a Security Exchange file in HDF5 format to be used by the Security Quantifier as input.
The Data Generator requires parameters such as :
 - a HDF5 input file
 - a output file path, the generated Security Exchange file
 - Parameters for the noise function : mean and standard deviation
 - the variables tht will be generated and their numbers of occurrences
 - the lower and upper bound for the generated values

 Binomial Distribution: Used to generate a binomial distribution JSON file. It can be used to generate distribution for different variables (cf EnumDistributionTypes for the list).
 Needed parameters:
 - A number of tuples, which will generate an equal number of element for the distribution
 - The path to the output JSON file that will containt the generated distribution.

Credits
-------
