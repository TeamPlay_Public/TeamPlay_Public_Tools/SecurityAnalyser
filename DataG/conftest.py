"""
    conftest.py gathers methods that may be used by the tests cases
"""
import os
import sys
from pathlib import Path

import pytest

from pandas import DataFrame
from sqcommon.adapter.File.JSON.Distribution.json_distribution_store_repository import JSONDistributionStoreRepository
from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict
from sqcommon.domain.Output.JSONFile.json_file_storage import JSONFileStorage
from sqcommon.domain.Variable.variable import Variable
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples

from datag.domain.Config.config_cli_args import ConfigCliArgs
from datag.domain.Config.config_data import ConfigData
from datag.use_case.RawBinomialDistribution.raw_binomial_distribution_generate_uc import CSVRawBinomialDistributionGetUC

sys.path.append('path')


# ---------------------------------------------------------------------------------
# Fixtures for files
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_absolute_dir_name():
    """ Return the directory name of pathname __file__ """
    return os.path.dirname(__file__)


@pytest.fixture
def json_generated_binomial_distribution_file_init(return_absolute_dir_name):
    """
        Fixture method returning the json file that will contain the generated binomial distribution.
    """
    dirname = return_absolute_dir_name

    partial_dist_file = 'tests/test_data/output/json/generated_binomial_distribution.json'

    return JSONFileStorage(name=Path(dirname, partial_dist_file))


# ---------------------------------------------------------------------------------
# Fixtures for simple attributes values
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
# Fixtures for the Configuration Objects - ConfigData
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_tainted_vars_names_2g_equal_to_secret_as_list():
    """ Return a tainted_vars_names_2g equal to ['secret'] """
    return ['secret']


@pytest.fixture
def return_tainted_vars_occurrences_2g_equal_to_50_as_list():
    """ Return a tainted_vars_occurrences_2g equal to [50] """
    return ['50']


@pytest.fixture
def return_untainted_vars_names_2g_equal_to_a_n_as_list():
    """ Return a untainted_vars_names_2g equal to ['a', 'n'] """
    return ['a', 'n']


@pytest.fixture
def return_untainted_vars_occurrences_2g_equal_to_40_40_as_list():
    """ Return a untainted_vars_occurrences_2g equal to [40,40] """
    return ['40', '40']


@pytest.fixture
def return_random_lower_bound_equal_to_1():
    """ Return a random_lower_bound equal to 1 """
    return 1


@pytest.fixture
def return_random_upper_bound_equal_to_100000():
    """ Return a random_upper_bound equal to 100000 """
    return 100000


@pytest.fixture
def return_mean_equal_to_0():
    """ Return a mean equal to 0 """
    return 0


@pytest.fixture
def return_standard_deviation_equal_to_01():
    """ Return a standard_deviation equal to 0.1 """
    return 0.1


@pytest.fixture
def return_is_data_generator_true():
    """ Return True for is_data_generator """
    return True


@pytest.fixture
def return_is_data_generator_false():
    """ Return False for is_data_generator """
    return False


# ---------------------------------------------------------------------------------
# Fixtures for the CSVRawBinomialDistributionGetUC Objects - CSVRawBinomialDistributionGetUC
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_nb_tuples_4():
    """ Return a number of tuples equal to 4 """
    return 4


@pytest.fixture
def return_distribution_type_secret_public():
    """ Return a EnumDistributionTuples SECRET_PUBLIC"""
    return EnumDistributionTuples.SECRET_PUBLIC


# ---------------------------------------------------------------------------------
# Fixtures for the Configuration Objects - Files
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_absolute_dir_name():
    """ Return the directory name of pathname __file__ """
    return os.path.dirname(__file__)


@pytest.fixture
def return_conf_file(return_absolute_dir_name):
    """ Return a configuration file"""
    dirname = return_absolute_dir_name

    input_test_data = "tests/test_config/datag.ini"

    return Path(dirname, input_test_data)


@pytest.fixture
def return_hdf5_input_file_dg(return_absolute_dir_name):
    """ Return an input file """
    dirname = return_absolute_dir_name

    input_test_data = "tests/test_data/input/WCET.h5"

    return Path(dirname, input_test_data)


@pytest.fixture
def return_hdf5_output_file_for_dg(return_absolute_dir_name):
    """ Return a HDF5 output_file path for the Data Generator """
    dirname = return_absolute_dir_name

    input_test_data = "tests/test_data/output/hdf5/WCET.h5"

    return Path(dirname, input_test_data)


@pytest.fixture
def return_json_output_file_for_bd_secret_public(return_absolute_dir_name):
    """ Return a JSON output_file path for the Binomial Distribution """
    dirname = return_absolute_dir_name

    input_test_data = "tests/test_data/output/json/secret_public.json"

    return Path(dirname, input_test_data)


# ---------------------------------------------------------------------------------
# Fixtures for the Boolean switching to DataGenerator or BinomialDistribution
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_is_binomial_distribution_true():
    """ Return True for is_binomial_distribution """
    return True


@pytest.fixture
def return_is_binomial_distribution_false():
    """ Return False for is_binomial_distribution """
    return False


# ---------------------------------------------------------------------------------
# Fixtures for the Configuration Objects
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
# Fixtures for the Configuration Objects - ConfigData
# ---------------------------------------------------------------------------------
@pytest.fixture
def configdata_model_init_with_nb_tuples_equal_4_bd(return_hdf5_input_file_dg,
                                                    return_hdf5_output_file_for_dg,
                                                    return_is_data_generator_false,
                                                    return_is_binomial_distribution_true,
                                                    return_tainted_vars_names_2g_equal_to_secret_as_list,
                                                    return_tainted_vars_occurrences_2g_equal_to_50_as_list,
                                                    return_untainted_vars_names_2g_equal_to_a_n_as_list,
                                                    return_untainted_vars_occurrences_2g_equal_to_40_40_as_list,
                                                    return_random_lower_bound_equal_to_1,
                                                    return_random_upper_bound_equal_to_100000,
                                                    return_mean_equal_to_0,
                                                    return_standard_deviation_equal_to_01,
                                                    return_nb_tuples_4,
                                                    return_json_output_file_for_bd_secret_public):
    """ Generate an instance of ConfigData """
    configdata = ConfigData(
        input_hdf5file=return_hdf5_input_file_dg,
        hdf5_output_file_dg=return_hdf5_output_file_for_dg,
        is_data_generator=return_is_data_generator_false,
        is_binomial_distribution=return_is_binomial_distribution_true,
        tainted_vars_names_2g=return_tainted_vars_names_2g_equal_to_secret_as_list,
        tainted_vars_occurrences_2g=return_tainted_vars_occurrences_2g_equal_to_50_as_list,
        untainted_vars_names_2g=return_untainted_vars_names_2g_equal_to_a_n_as_list,
        untainted_vars_occurrences_2g=return_untainted_vars_occurrences_2g_equal_to_40_40_as_list,
        random_lower_bound=return_random_lower_bound_equal_to_1,
        random_upper_bound=return_random_upper_bound_equal_to_100000,
        mean=return_mean_equal_to_0,
        standard_deviation=return_standard_deviation_equal_to_01,
        binomial_distribution_nb_tuples=return_nb_tuples_4,
        json_output_file_bd=return_json_output_file_for_bd_secret_public
    )

    return configdata


# ---------------------------------------------------------------------------------
# Fixtures for the Configuration Objects - ConfigCliArgs
# ---------------------------------------------------------------------------------
@pytest.fixture
def conf_cli_args_model_init_nb_tuples_equal_1000_bd(return_conf_file,
                                                     return_hdf5_input_file_dg,
                                                     return_hdf5_output_file_for_dg,
                                                     return_is_data_generator_true,
                                                     return_is_binomial_distribution_false,
                                                     return_nb_tuples_1000,
                                                     return_json_output_file_for_bd_secret_public):
    """ Generate an instance of ConfigCliArgs """
    config_cli_args = ConfigCliArgs(config_file=return_conf_file,
                                    input_f=return_hdf5_input_file_dg,
                                    hdf5_output_dg=return_hdf5_output_file_for_dg,
                                    is_data_generator=return_is_data_generator_true,
                                    is_binomial_distribution=return_is_binomial_distribution_false,
                                    binomial_distribution_nb_tuples=return_nb_tuples_1000,
                                    json_output_bd=return_json_output_file_for_bd_secret_public
                                    )

    return config_cli_args


# ---------------------------------------------------------------------------------
# Fixtures for the CSVRawBinomialDistributionGetUC
# ---------------------------------------------------------------------------------
@pytest.fixture
def csv_raw_binomial_distrib_uc_init(json_generated_binomial_distribution_file_init,
                                     return_nb_tuples_4,
                                     return_distribution_type_secret_public,
                                     return_json_distribution_store_repository):
    """ Generate an instance of CSVRawBinomialDistributionGetUC """
    csv_raw_binomial_distrib = CSVRawBinomialDistributionGetUC(
        output_storage_repo=return_json_distribution_store_repository,
        distribution_type=return_distribution_type_secret_public,
        nb_elements=return_nb_tuples_4,
        output_path=json_generated_binomial_distribution_file_init)

    return csv_raw_binomial_distrib

# ---------------------------------------------------------------------------------
# Fixtures for the Distribution - distribution
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_distribution_as_dict_secret_indiscernability_v2():
    """ Return distribution as a dict for testing the CSVRawBinomialDistributionGetUC"""
    dist_dict = {
        "secret_public":
            [0.05, 0.15, 0.05,
             0.10, 0.05, 0.10,
             0.15, 0.05, 0.10,
             0.05, 0.10, 0.05]
    }

    return dist_dict


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution Objects - DistributionAsDict
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_distributionasdict_secret_public(return_distribution_as_dict_secret_indiscernability_v2):
    """
        Return a DistributionAsDict with one entry: secret and its related values
    """

    distributionasdict = DistributionAsDict(return_distribution_as_dict_secret_indiscernability_v2)

    return distributionasdict


# ---------------------------------------------------------------------------------
# Fixtures for the Dataframe and associated
# ---------------------------------------------------------------------------------
@pytest.fixture
def dataframe_extrema_2064_and_2124():
    """
        Fixture method to generate a Dataframe with extrema 2064 and 2124
    """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "10100001000001111000101011000111",
                               "10111111111100101001011110110001"],
                    'a': [2,
                          2,
                          2],
                    'n': [10,
                          10,
                          10],
                    'time_worst': [2064,
                                   2124,
                                   2064
                                   ]
                    })

    return df


@pytest.fixture
def data_as_dataframe_extrema_2064_and_2124(dataframe_extrema_2064_and_2124):
    """
         Fixture method to generate a DataAsDataframe from a DataFrame with extrema 2064 and 2124

        :param dataframe_extrema_2064_and_2124:
            pytest.fixture returning a DataFrame
        :return:
            DataAsDataFrame: DataFrame nested in a DataAsDataFrame
    """
    data_as_df = DataAsDataFrame(dataframe_extrema_2064_and_2124)

    return data_as_df


# ---------------------------------------------------------------------------------
# Fixtures for Object instantiation
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_variable_with_value_a_10():
    """ Return a Variable with cardinality=10 and value=a """

    return Variable(value="a",
                    cardinality=10)


@pytest.fixture
def return_variable_with_value_n_20():
    """ Return a Variable with cardinality=20 and value=n """

    return Variable(value="n",
                    cardinality=20)


@pytest.fixture
def return_variable_with_value_secret_5():
    """ Return a Variable with cardinality=5 and value=secret """

    return Variable(value="secret",
                    cardinality=5)


@pytest.fixture
def return_variables_list_a10_n20_secret5(return_variable_with_value_a_10,
                                          return_variable_with_value_n_20,
                                          return_variable_with_value_secret_5):
    """
        Return a VariableList with three variables:
        a=10
        n=20,
        secret=5
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_a_10)
    vars_list.lst.append(return_variable_with_value_n_20)
    vars_list.lst.append(return_variable_with_value_secret_5)

    return vars_list


# ---------------------------------------------------------------------------------
# Fixtures for the Storage and Repository Objects
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_json_file_storage(json_generated_binomial_distribution_file_init):
    """ Return a JSONFileStorage with value json_generated_binomial_distribution_file_init """
    json_file_storage = JSONFileStorage(name=json_generated_binomial_distribution_file_init)

    return json_file_storage


@pytest.fixture
def return_json_distribution_store_repository():
    """ Return a JSONDistributionStoreRepository class """
    return JSONDistributionStoreRepository
