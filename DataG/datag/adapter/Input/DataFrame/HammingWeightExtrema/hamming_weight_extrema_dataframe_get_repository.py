"""
    Module defining the Repository used to get the HammingWeightExtrema from a DataFrame
"""
import sys

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame

from sqcommon.domain.Variable.variables_list import VariablesList

from sqcommon.helper.variables_helper import VariableHelper

from datag.domain.HammingWeight.hamming_weight import HammingWeight
from datag.domain.HammingWeight.hamming_weight_extrema import HammingWeightExtrema
from datag.domain.Tuple.input_tuples_list import InputTuplesList

from datag.interface.adapter.HammingWeight.hamming_weight_extrema_repository_interface import \
    HammingWeightExtremaRepositoryInterface
from datag.interface.domain.HammingWeight.hamming_weight_extrema_interface import HammingWeightExtremaInterface


class HammingWeightExtremaDataframeGetRepository(HammingWeightExtremaRepositoryInterface):
    """
        HammingWeightExtremaHDF5Repository class defines the Repository
        to get the two Hamming Weight Extrema values and their related tuples found in a DataFrame
    """

    @staticmethod
    def find_extrema(data: DataAsDataFrame,
                     nb_tainted_vars: int,
                     variables_card: VariablesList
                     ) -> HammingWeightExtremaInterface:
        """
            Extract the Hamming Weight extrema and their related tuples from a DataFrame

            Parameters
            ----------
            data: DataAsDataFrame
                the pandas DataFrame to extract the HammingWeightExtrema from
            nb_tainted_vars: int,
                number of tainted variables in the dataframe
            variables_card: VariablesList
                list of cardinality for each variable

            Returns
            -------
            HammingWeightExtremaInterface
                the two Hamming Weight Extrema values and their related tuples found in a DataFrame
                and the average values
         """
        # Gathering needed information,
        # Cardinality for the tainted variables and the untainted_variables
        tainted_vars_cards, untainted_vars_cards = VariableHelper.list_card_per_var_type(
            data.data.columns.tolist(),
            nb_tainted_vars,
            variables_card)
        # Number of tuples
        nb_tuples = VariableHelper.nb_of_rows(tainted_vars_cards, untainted_vars_cards)
        # Number of untainted tuples
        nb_untainted_tuples = VariableHelper.nb_tuples(untainted_vars_cards)

        # Instantiate HammingWeight and HammingWeightExtrema
        hw = HammingWeight(0)
        hwe = HammingWeightExtrema(0, 0, InputTuplesList([]), InputTuplesList([]), 0, 0)

        # Initializing HammingWeightExtrema.minima to a very big value
        hwe.minima = sys.maxsize
        # and HammingWeightExtrema.maxima to a very small value
        hwe.maxima = -sys.maxsize - 1

        for index1 in range(0, nb_tuples - 1, nb_untainted_tuples):
            # Take column 0 from row number index1, that is the value of the first tainted variable
            # for the first tuple. This is because at each loop iteration we get "nb_untainted_tuples" tuples
            # which are all the tuples related to the current tainted variable.
            # From it we set the HammingWeight object attribute named value and extract its hamming weight.
            hw.value = data.data.iloc[index1, 0]
            current_hw = hw.weight()

            # Now we get the related tuples, data.data.iloc[index1:index1 + nb_untainted_tuples, 0:],
            # turn it into a list and update the HammingWeightExtrema with the new values
            related_tuples = data.data.iloc[index1:index1 + nb_untainted_tuples, 0:].values.tolist()
            if current_hw < hwe.minima:
                hwe.minima = current_hw
                hwe.minima_tuples = InputTuplesList(related_tuples)
            if current_hw > hwe.maxima:
                hwe.maxima = current_hw
                hwe.maxima_tuples = InputTuplesList(related_tuples)

        # Compute the average min and max measurements
        hwe.avg_minima()
        hwe.avg_maxima()

        return hwe
