"""
    Module defining the Repository used to store the generated Measurements into a HDF5 file
"""
from pathlib import Path
import tables as tb


from datag.domain.Measurements.measurements import Measurements
from datag.domain.Output.HDF5File.hdf5_file_storage import HDF5FileStorage
from datag.interface.adapter.Storage.measurements_store_repository_interface import MeasurementsStoreRepositoryInterface


class HDF5MeasurementsStoreRepository(MeasurementsStoreRepositoryInterface):
    """
        HDF5MeasurementsStoreRepository class defines the Repository
        to store the generated measurements values into a HDF5 file
    """

    @staticmethod
    def store(data: Measurements,
              storage: HDF5FileStorage):
        """
            Store the generated measurements into a HDF5 file

            Parameters
            ----------
            data: Measurements
                the generated Measurements to store
            storage: HDF5FileStorage
                the data needed to store the output of the program, here the path to the HDF5 file
         """
        h5_path = Path(storage.name)
        h5_path.touch()  # will create file, if it exists will do nothing

        # Generates HDF5
        try:
            with tb.open_file(h5_path, "w") as h5file:
                # Create the table storing the data
                table = h5file.create_table('/', data.dataset, data.dtype)

                # Adding the data
                table.append(data.data)

                # Adding User Attributes
                table.attrs["nbTainted"] = data.tainted_vars_card
                table.attrs["nbUntainted"] = data.untainted_vars_card
                table.attrs["a"] = int(data.untainted_vars_occurrences[0])
                table.attrs["n"] = int(data.untainted_vars_occurrences[1])
                table.attrs["secret"] = int(data.tainted_vars_occurrences[0])
        except FileNotFoundError as e:
            print(e.errno)
