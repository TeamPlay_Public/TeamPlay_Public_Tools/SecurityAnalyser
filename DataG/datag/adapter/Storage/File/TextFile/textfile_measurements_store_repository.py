"""
    Module defining the Repository used to store the generated Measurements into a text file
"""
from sqcommon.interface.domain.Storage.storage_interface import StorageInterface

from datag.interface.adapter.Storage.measurements_store_repository_interface import MeasurementsStoreRepositoryInterface
from datag.interface.domain.Measurements.measurements_interface import MeasurementsInterface


class TextFileMeasurementsStoreRepository(MeasurementsStoreRepositoryInterface):
    """
        TextFileMeasurementsStoreRepository class defines the Repository
        to store the generated Measurements into a text file
    """

    @staticmethod
    def store(data: MeasurementsInterface,
              storage: StorageInterface):
        """
            Store the generated Measurements into a text file

            Parameters
            ----------
            data: MeasurementsInterface
                the generated Measurements to store
            storage: StorageInterface
                the data needed to store the output of the program
         """
        try:
            with open(storage.name, 'w') as f:
                for item in data.data:
                    f.write("%s\n" % item.data)
        except FileNotFoundError as e:
            print(e.errno)
