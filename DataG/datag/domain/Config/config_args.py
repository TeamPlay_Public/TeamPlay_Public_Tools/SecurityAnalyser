"""
    ConfigArgs module gathers the arguments of the command line to run the DataGenerator
"""
import argparse


def _msg():
    """

    :return:
        Information message for the configuration parser for the commandline arguments
    """
    return "Data Generator mode: Generate Data inspired by Input data. Binomial Distribution mode: Generate a " \
           "distribution with n elements "


class ConfigArgs:
    """
        ConfigArgs class is a container gathering the arguments of the command line to run the DataGenerator

    """

    def __init__(
        self
    ):
        """ Constructor for the ConfigArgs class """
        self._msg = _msg()
        self._parser = argparse.ArgumentParser(description=self._msg)

        self._add_args()

    def _add_args(self):
        """
            Add the arguments for the configuration parser for the commandline arguments
        """

        # Adding optional argument
        self._parser.add_argument("-c", "--Config", help="Configuration file location")
        self._parser.add_argument("-i", "--Input", help="Input file location")
        self._parser.add_argument("-oh5", "--OHDF5", help="HDF5 file for the Data Generator")
        self._parser.add_argument("-dg", "--DG", action='store_true', help="Data Generator")
        self._parser.add_argument("-bd", "--BD", action='store_true', help="Binomial Distribution")
        self._parser.add_argument("-rbd", "--RBD", action='store_true', help="Generate a raw Binomial Distribution. "
                                                                             "Use together with -ORBD to specify the "
                                                                             "location of the file storing the "
                                                                             "generated raw binomial distribution and "
                                                                             "--NbTuples to specify the number of "
                                                                             "elements "
                                                                             "in the distribution")
        self._parser.add_argument("-nt", "--NbTuples", help="Number of tuples in the associated Security Exchange "
                                                            "file. Number of element to generate for the distribution")
        self._parser.add_argument("-ojson", "--OJSON", help="JSON file for the Gaussian Distribution")
        self._parser.add_argument("-orbd", "--ORBD", help="CSV file storing the generated raw binomial distribution. "
                                                          "Use together with --RBD and --NRBD")

    def parser(self):
        """

        :return:
            a configuration parser for the commandline arguments
        """
        return self._parser
