"""
    ConfigCliArgs module is a container for the name-value pairs from the cli arguments
"""
from pathlib import Path


class ConfigCliArgs:
    """
        ConfigCliArgs class is a container for the name-value pairs from the cli arguments

    """

    def __init__(
        self,
        config_file: Path,
        input_f: Path,
        hdf5_output_dg: Path,
        is_data_generator: bool,
        is_binomial_distribution: bool,
        is_generate_raw_binomial_distribution: bool,
        binomial_distribution_nb_tuples: int,
        json_output_bd: Path,
        generated_raw_binomial_distribution_output: Path
    ):
        """ Constructor for the ConfigCliArgs class """
        # Configuration file
        self._config_file = config_file
        # Input hdf5 file
        self._input_f = input_f
        # Output hdf5 file for the Data Generator
        self._hdf5_output_dg = hdf5_output_dg
        # DataGenerator algorithm will be run
        self._is_data_generator = is_data_generator
        # BinomialCoefficient algorithm will be run
        self._is_binomial_distribution = is_binomial_distribution
        # GenerateRawBinomialDistribution algorithm will be run.
        self._is_generate_raw_binomial_distribution = is_generate_raw_binomial_distribution
        # Number of tuples in the Security Exchange file. Number of elements in the generated binomial distribution
        self._binomial_distribution_nb_tuples = binomial_distribution_nb_tuples
        # Output JSON file for the Binomial Distribution
        self._json_output_bd = json_output_bd
        # Output CSV file for the Raw Binomial Distribution
        self._generated_raw_binomial_distribution_output = generated_raw_binomial_distribution_output

    @property
    def config_file(self) -> Path:
        """ Getter for the config_file attribute """
        return self._config_file

    @config_file.setter
    def config_file(self, config_file):
        """ Setter for the config_file attribute """
        self._config_file = config_file

    @property
    def input_f(self) -> Path:
        """ Getter for the input_f attribute """
        return self._input_f

    @input_f.setter
    def input_f(self, input_f):
        """ Setter for the input_f attribute """
        self._input_f = input_f

    @property
    def hdf5_output_dg(self) -> Path:
        """ Getter for the hdf5_output_dg attribute """
        return self._hdf5_output_dg

    @hdf5_output_dg.setter
    def hdf5_output_dg(self, hdf5_output_dg):
        """ Setter for the hdf5_output_dg attribute """
        self._hdf5_output_dg = hdf5_output_dg

    @property
    def is_data_generator(self) -> bool:
        """ Getter for the is_data_generator attribute """
        return self._is_data_generator

    @is_data_generator.setter
    def is_data_generator(self, is_data_generator):
        """ Setter for the is_data_generator attribute """
        self._is_data_generator = is_data_generator

    @property
    def is_binomial_distribution(self) -> bool:
        """ Getter for the is_binomial_distribution attribute """
        return self._is_binomial_distribution

    @is_binomial_distribution.setter
    def is_binomial_distribution(self, is_binomial_distribution):
        """ Setter for the is_binomial_distribution attribute """
        self._is_binomial_distribution = is_binomial_distribution

    @property
    def is_generate_raw_binomial_distribution(self) -> bool:
        """ Getter for the is_generate_raw_binomial_distribution attribute """
        return self._is_generate_raw_binomial_distribution

    @is_generate_raw_binomial_distribution.setter
    def is_generate_raw_binomial_distribution(self, is_generate_raw_binomial_distribution):
        """ Setter for the is_generate_raw_binomial_distribution attribute """
        self._is_generate_raw_binomial_distribution = is_generate_raw_binomial_distribution

    @property
    def binomial_distribution_nb_tuples(self) -> int:
        """ Getter for the binomial_distribution_nb_tuples attribute """
        return self._binomial_distribution_nb_tuples

    @binomial_distribution_nb_tuples.setter
    def binomial_distribution_nb_tuples(self, binomial_distribution_nb_tuples):
        """ Setter for the binomial_distribution_nb_tuples attribute """
        self._binomial_distribution_nb_tuples = binomial_distribution_nb_tuples

    @property
    def json_output_bd(self) -> Path:
        """ Getter for the json_output_bd attribute """
        return self._json_output_bd

    @json_output_bd.setter
    def json_output_bd(self, json_output_bd):
        """ Setter for the json_output_bd attribute """
        self._json_output_bd = json_output_bd

    @property
    def generated_raw_binomial_distribution_output(self) -> Path:
        """ Getter for the generated_raw_binomial_distribution_output attribute """
        return self._generated_raw_binomial_distribution_output

    @generated_raw_binomial_distribution_output.setter
    def generated_raw_binomial_distribution_output(self, generated_raw_binomial_distribution_output):
        """ Setter for the generated_raw_binomial_distribution_output attribute """
        self._generated_raw_binomial_distribution_output = generated_raw_binomial_distribution_output
