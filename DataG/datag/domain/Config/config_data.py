"""
    ConfigData module is a container for the name-value pairs for configuration data
"""
from pathlib import Path
from typing import List


class ConfigData:
    """
        ConfigData class is a container for the name-value pairs for configuration data

    """

    def __init__(
        self,
        input_hdf5file: Path,
        hdf5_output_file_dg: Path,
        tainted_vars_names_2g: List[str],
        tainted_vars_occurrences_2g: List[int],
        untainted_vars_names_2g: List[str],
        untainted_vars_occurrences_2g: List[int],
        random_lower_bound: int,
        random_upper_bound: int,
        mean: float,
        standard_deviation: float,
        is_data_generator: bool,
        is_binomial_distribution: bool,
        is_generate_raw_binomial_distribution: bool,
        binomial_distribution_nb_tuples: int,
        json_output_file_bd: Path,
        generated_raw_binomial_distribution_output: Path
    ):
        """ Constructor for the ConfigData class """
        # Input/output files
        self._input_hdf5file = input_hdf5file
        self._hdf5_output_file_dg = hdf5_output_file_dg
        # Variables-related values
        self._tainted_vars_names_2g = tainted_vars_names_2g
        self._tainted_vars_occurrences_2g = tainted_vars_occurrences_2g

        self._untainted_vars_names_2g = untainted_vars_names_2g
        self._untainted_vars_occurrences_2g = untainted_vars_occurrences_2g

        # Random function related parameters
        self._random_lower_bound = random_lower_bound
        self._random_upper_bound = random_upper_bound

        # Noise-related values
        self._mean = mean
        self._standard_deviation = standard_deviation

        # Mode
        self._is_data_generator = is_data_generator
        self._is_binomial_distribution = is_binomial_distribution
        self._is_generate_raw_binomial_distribution = is_generate_raw_binomial_distribution

        # Distribution-related parameters
        self._binomial_distribution_nb_tuples = binomial_distribution_nb_tuples
        self._json_output_file_bd = json_output_file_bd
        self._generated_raw_binomial_distribution_output = generated_raw_binomial_distribution_output

    @property
    def input_hdf5file(self) -> Path:
        """ Getter for the input_hdf5file attribute """
        return self._input_hdf5file

    @input_hdf5file.setter
    def input_hdf5file(self, input_hdf5file):
        """ Setter for the input_hdf5file attribute """
        self._input_hdf5file = input_hdf5file

    @property
    def hdf5_output_file_dg(self) -> Path:
        """ Getter for the hdf5_output_file_dg attribute """
        return self._hdf5_output_file_dg

    @hdf5_output_file_dg.setter
    def hdf5_output_file_dg(self, output_hdf5file):
        """ Setter for the hdf5_output_file_dg attribute """
        self._hdf5_output_file_dg = output_hdf5file

    @property
    def tainted_vars_names_2g(self) -> List[str]:
        """ Getter for the tainted_vars_names_2g attribute """
        return self._tainted_vars_names_2g

    @tainted_vars_names_2g.setter
    def tainted_vars_names_2g(self, tainted_vars_names_2g):
        """ Setter for the tainted_vars_names_2g attribute """
        self._tainted_vars_names_2g = tainted_vars_names_2g

    @property
    def tainted_vars_occurrences_2g(self) -> List[int]:
        """ Getter for the tainted_vars_occurrences_2g attribute """
        return self._tainted_vars_occurrences_2g

    @tainted_vars_occurrences_2g.setter
    def tainted_vars_occurrences_2g(self, tainted_vars_occurrences_2g):
        """ Setter for the tainted_vars_occurrences_2g attribute """
        self._tainted_vars_occurrences_2g = tainted_vars_occurrences_2g

    @property
    def untainted_vars_names_2g(self) -> List[str]:
        """ Getter for the untainted_vars_names_2g attribute """
        return self._untainted_vars_names_2g

    @untainted_vars_names_2g.setter
    def untainted_vars_names_2g(self, untainted_vars_names_2g):
        """ Setter for the untainted_vars_names_2g attribute """
        self._untainted_vars_names_2g = untainted_vars_names_2g

    @property
    def untainted_vars_occurrences_2g(self) -> List[int]:
        """ Getter for the untainted_vars_occurrences_2g attribute """
        return self._untainted_vars_occurrences_2g

    @untainted_vars_occurrences_2g.setter
    def untainted_vars_occurrences_2g(self, untainted_vars_occurrences_2g):
        """ Setter for the untainted_vars_occurrences_2g attribute """
        self._untainted_vars_occurrences_2g = untainted_vars_occurrences_2g

    @property
    def random_lower_bound(self) -> int:
        """ Getter for the random_lower_bound attribute """
        return self._random_lower_bound

    @random_lower_bound.setter
    def random_lower_bound(self, random_lower_bound):
        """ Setter for the random_lower_bound attribute """
        self._random_lower_bound = random_lower_bound

    @property
    def random_upper_bound(self) -> int:
        """ Getter for the random_upper_bound attribute """
        return self._random_upper_bound

    @random_upper_bound.setter
    def random_upper_bound(self, random_upper_bound):
        """ Setter for the random_upper_bound attribute """
        self._random_upper_bound = random_upper_bound

    @property
    def mean(self) -> float:
        """ Getter for the mean attribute """
        return self._mean

    @mean.setter
    def mean(self, mean):
        """ Setter for the mean attribute """
        self._mean = mean

    @property
    def standard_deviation(self) -> float:
        """ Getter for the standard_deviation attribute """
        return self._standard_deviation

    @standard_deviation.setter
    def standard_deviation(self, standard_deviation):
        """ Setter for the standard_deviation attribute """
        self._standard_deviation = standard_deviation

    @property
    def is_data_generator(self) -> bool:
        """ Getter for the is_data_generator attribute """
        return self._is_data_generator

    @is_data_generator.setter
    def is_data_generator(self, is_data_generator):
        """ Setter for the is_data_generator attribute """
        self._is_data_generator = is_data_generator

    @property
    def is_binomial_distribution(self) -> bool:
        """ Getter for the is_binomial_distribution attribute """
        return self._is_binomial_distribution

    @is_binomial_distribution.setter
    def is_binomial_distribution(self, is_binomial_distribution):
        """ Setter for the is_binomial_distribution attribute """
        self._is_binomial_distribution = is_binomial_distribution

    @property
    def is_generate_raw_binomial_distribution(self) -> bool:
        """ Getter for the is_generate_raw_binomial_distribution attribute """
        return self._is_generate_raw_binomial_distribution

    @is_generate_raw_binomial_distribution.setter
    def is_generate_raw_binomial_distribution(self, is_generate_raw_binomial_distribution):
        """ Setter for the is_generate_raw_binomial_distribution attribute """
        self._is_generate_raw_binomial_distribution = is_generate_raw_binomial_distribution

    @property
    def binomial_distribution_nb_tuples(self) -> int:
        """ Getter for the binomial_distribution_nb_tuples attribute """
        return self._binomial_distribution_nb_tuples

    @binomial_distribution_nb_tuples.setter
    def binomial_distribution_nb_tuples(self, output_hdf5file):
        """ Setter for the binomial_distribution_nb_tuples attribute """
        self._binomial_distribution_nb_tuples = output_hdf5file

    @property
    def json_output_file_bd(self) -> Path:
        """ Getter for the json_output_file_bd attribute """
        return self._json_output_file_bd

    @json_output_file_bd.setter
    def json_output_file_bd(self, output_file):
        """ Setter for the json_output_file_bd attribute """
        self._json_output_file_bd = output_file

    @property
    def generated_raw_binomial_distribution_output(self) -> Path:
        """ Getter for the generated_raw_binomial_distribution_output attribute """
        return self._generated_raw_binomial_distribution_output

    @generated_raw_binomial_distribution_output.setter
    def generated_raw_binomial_distribution_output(self, output_file):
        """ Setter for the generated_raw_binomial_distribution_output attribute """
        self._generated_raw_binomial_distribution_output = output_file
