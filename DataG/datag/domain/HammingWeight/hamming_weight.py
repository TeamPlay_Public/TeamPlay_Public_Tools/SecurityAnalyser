""" HammingWeight module specifies an implementation of HammingWeightInterface
    which computes the Hamming Weight of an integer
"""
from datag.interface.domain.HammingWeight.hamming_weight_interface import HammingWeightInterface


class HammingWeight(HammingWeightInterface):
    """
        HammingWeight class specifies the Hamming Weight of an integer

        Attribute
        ----------
        value: int
            the value for which we want to compute the Hamming Weight

    """

    def __init__(
        self,
        value: int
    ):
        """ Constructor for the HammingWeight class """
        self._value = value
        self._weight = ''

    @property
    def value(self) -> int:
        """ Getter for the value attribute """
        return self._value

    @value.setter
    def value(self, value):
        """ Setter for the value attribute """
        self._value = value

    def weight(self):
        """ Method computing the hamming weight of an integer """
        self._weight = bin(self._value).count('1')

        return self._weight
