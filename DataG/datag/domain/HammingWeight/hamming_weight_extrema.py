""" HammingWeightExtrema module specifies an implementation of HammingWeightInterface
    which stores the Hamming Weight extrema contained in a given input data
"""

from datag.domain.Tuple.input_tuples_list import InputTuplesList
from datag.helper.hammingweightextrema_helper import HammingWeightExtremaHelper
from datag.interface.domain.HammingWeight.hamming_weight_extrema_interface import HammingWeightExtremaInterface


class HammingWeightExtrema(HammingWeightExtremaInterface):
    """
        HammingWeightExtrema class specifies the Hamming Weight extrema contained in the given input data.

        Attributes
        ----------
        minima: int
            the smallest HammingWeight value for a tainted variable in a given input data
        maxima: int
            the highest HammingWeight value for a tainted variable in a given input data
        minima_tuples: InputTuplesList
            the tuples related to the minima
        maxima_tuples: InputTuplesList
            the tuples related to the maxima
        min_avg: float
            the average value of all the signal measurement included in minima_tuples
        max_avg: float
            the average value of all the signal measurement included in maxima_tuples
    """

    def __init__(
        self,
        minima: int,
        maxima: int,
        minima_tuples: InputTuplesList,
        maxima_tuples: InputTuplesList,
        min_avg: float,
        max_avg: float
    ):
        """ Constructor for the HammingWeightExtrema class """
        self._minima = minima
        self._maxima = maxima
        self._minima_tuples = minima_tuples
        self._maxima_tuples = maxima_tuples
        self._min_avg = min_avg
        self._max_avg = max_avg

    def avg_minima(self):
        """ Compute the average value of all the signal measurement included in minima_tuples """
        self._min_avg = HammingWeightExtremaHelper.compute_avg(self.minima_tuples)

    def avg_maxima(self):
        """ Compute the average value of all the signal measurement included in maxima_tuples """
        self._max_avg = HammingWeightExtremaHelper.compute_avg(self.maxima_tuples)

    @property
    def minima(self) -> int:
        """ Getter for the minima attribute """
        return self._minima

    @minima.setter
    def minima(self, minima):
        """ Setter for the minima attribute """
        self._minima = minima

    @property
    def maxima(self) -> int:
        """ Getter for the maxima attribute """
        return self._maxima

    @maxima.setter
    def maxima(self, maxima):
        """ Setter for the maxima attribute """
        self._maxima = maxima

    @property
    def minima_tuples(self) -> InputTuplesList:
        """ Getter for the minima_tuples attribute """
        return self._minima_tuples

    @minima_tuples.setter
    def minima_tuples(self, minima_tuples):
        """ Setter for the minima_tuples attribute """
        self._minima_tuples = minima_tuples

    @property
    def maxima_tuples(self) -> InputTuplesList:
        """ Getter for the maxima_tuples attribute """
        return self._maxima_tuples

    @maxima_tuples.setter
    def maxima_tuples(self, maxima_tuples):
        """ Setter for the maxima_tuples attribute """
        self._maxima_tuples = maxima_tuples

    @property
    def min_avg(self) -> float:
        """ Getter for the min_avg attribute """
        return self._min_avg

    @min_avg.setter
    def min_avg(self, min_avg):
        """ Setter for the min_avg attribute """
        self._min_avg = min_avg

    @property
    def max_avg(self) -> float:
        """ Getter for the max_avg attribute """
        return self._max_avg

    @max_avg.setter
    def max_avg(self, max_avg):
        """ Setter for the max_avg attribute """
        self._max_avg = max_avg
