"""
    LinearEquation module specifies an implementation of LinearEquationInterface
    which defines the Linear Equation attributes, ie slope and Y-intercept
"""

from datag.interface.domain.LinearEquation.linear_equation_interface import LinearEquationInterface
from datag.interface.domain.LinearEquation.slope_interface import SlopeInterface
from datag.interface.domain.LinearEquation.y_intercept_interface import YInterceptInterface


class LinearEquation(LinearEquationInterface):
    """
        LinearEquation class specifies a Linear Equation

        Attribute
        ----------
        slope: SlopeInterface
            the slope of the equation
        y_intercept: YInterceptInterface
            the y-intercept of the equation

    """

    def __init__(
        self,
        slope: SlopeInterface,
        y_intercept: YInterceptInterface
    ):
        """ Constructor for the LinearEquation class """
        self._slope = slope
        self._y_intercept = y_intercept

    @property
    def slope(self) -> SlopeInterface:
        """ Getter for the slope attribute """
        return self._slope

    @slope.setter
    def slope(self, slope):
        """ Setter for the slope attribute """
        self._slope = slope

    @property
    def y_intercept(self) -> YInterceptInterface:
        """ Getter for the y_intercept attribute """
        return self._y_intercept

    @y_intercept.setter
    def y_intercept(self, y_intercept):
        """ Setter for the y_intercept attribute """
        self._y_intercept = y_intercept
