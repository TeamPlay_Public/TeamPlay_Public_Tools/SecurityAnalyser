"""
    Slope module specifies an implementation of SlopeInterface which defines the Slope attribute of a Linear Equation
"""
from datag.interface.domain.LinearEquation.slope_interface import SlopeInterface


class Slope(SlopeInterface):
    """
        Slope class specifies the Slope attribute of a Linear Equation

        Attribute
        ----------
        value: float
            the slope of the equation
    """

    def __init__(
        self,
        value: float
    ):
        """ Constructor for the Slope class """
        self._value = value

    @property
    def value(self) -> float:
        """ Getter for the slope attribute """
        return self._value

    @value.setter
    def value(self, value):
        """ Setter for the slope attribute """
        self._value = value
