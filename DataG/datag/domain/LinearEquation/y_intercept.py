"""
    YIntercept module specifies an implementation of YInterceptInterface
    which defines the YIntercept attribute of a Linear Equation
"""
from datag.interface.domain.LinearEquation.y_intercept_interface import YInterceptInterface


class YIntercept(YInterceptInterface):
    """
        YIntercept class specifies the YIntercept attribute of a Linear Equation

        Attribute
        ----------
        value: float
            the y-intercept of the equation
    """

    def __init__(
        self,
        value: float
    ):
        """ Constructor for the YIntercept class """
        self._value = value

    @property
    def value(self) -> float:
        """ Getter for the y-value attribute """
        return self._value

    @value.setter
    def value(self, value):
        """ Setter for the y-value attribute """
        self._value = value
