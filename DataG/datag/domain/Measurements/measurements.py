"""
    Measurements module specifies an implementation of MeasurementsInterface
    which encapsulates the generated measurements
"""
import numpy as np

from typing import List

from datag.domain.OutputTuplesGenerationParams.output_tuples_generation_params import OutputTuplesGenerationParams
from datag.interface.domain.Measurements.measurements_interface import MeasurementsInterface


class Measurements(MeasurementsInterface):
    """
        Measurements class encapsulates the generated measurements

        Attribute
        ----------
        output_tuples_generation_params: OutputTuplesGenerationParams
            the parameters related to the output tuples generation
        vars_occurrences: List[int]
            Number of occurrences of each variable
        nb_tuples: int
            Number of generated tuples
        dataset: str
            the dataset name
        data: numpy array
            the entire set of generated tuples
    """

    def __init__(
        self,
        output_tuples_generation_params: OutputTuplesGenerationParams,
        vars_occurrences: List[int],
        nb_tuples: int,
        dataset: str
    ):
        """ Constructor for the Measurements class """
        self._output_tuples_generation_params = output_tuples_generation_params
        self._tainted_vars_card = len(self._output_tuples_generation_params.tainted_vars_names)
        self._untainted_vars_card = len(self._output_tuples_generation_params.untainted_vars_names)
        self._vars_occurrences = vars_occurrences
        self._nb_tuples = nb_tuples
        self._tainted_vars_occurrences = self._output_tuples_generation_params.tainted_vars_occurrences
        self._untainted_vars_occurrences = self._output_tuples_generation_params.untainted_vars_occurrences
        self._dataset = dataset
        self._dtype = np.dtype([
            (self._output_tuples_generation_params.tainted_vars_names[0], 'f8'),
            (self._output_tuples_generation_params.untainted_vars_names[0], 'f8'),
            (self._output_tuples_generation_params.untainted_vars_names[1], 'f8'),
            (self._output_tuples_generation_params.side_channel_class, 'f8')])
        self._data = np.empty((nb_tuples, len(vars_occurrences) + 1),
                              dtype=[(self._output_tuples_generation_params.tainted_vars_names[0], np.int32),
                                     (self._output_tuples_generation_params.untainted_vars_names[0], np.int32),
                                     (self._output_tuples_generation_params.untainted_vars_names[1], np.int32),
                                     (self._output_tuples_generation_params.side_channel_class, np.float32)])

    @property
    def data(self):
        """ Getter for the data attribute """
        return self._data

    @data.setter
    def data(self, data):
        """ Setter for the data attribute """
        self._data = data

    @property
    def tainted_vars_card(self) -> int:
        """ Getter for the tainted_vars_card attribute """
        return self._tainted_vars_card

    @tainted_vars_card.setter
    def tainted_vars_card(self, card):
        """ Setter for the tainted_vars_card attribute """
        self._tainted_vars_card = card

    @property
    def untainted_vars_card(self) -> int:
        """ Getter for the untainted_vars_card attribute """
        return self._untainted_vars_card

    @untainted_vars_card.setter
    def untainted_vars_card(self, card):
        """ Setter for the untainted_vars_card attribute """
        self._untainted_vars_card = card

    @property
    def tainted_vars_occurrences(self) -> List[int]:
        """ Getter for the tainted_vars_occurrences attribute """
        return self._tainted_vars_occurrences

    @tainted_vars_occurrences.setter
    def tainted_vars_occurrences(self, card):
        """ Setter for the tainted_vars_occurrences attribute """
        self._tainted_vars_occurrences = card

    @property
    def untainted_vars_occurrences(self) -> List[int]:
        """ Getter for the untainted_vars_occurrences attribute """
        return self._untainted_vars_occurrences

    @untainted_vars_occurrences.setter
    def untainted_vars_occurrences(self, card):
        """ Setter for the untainted_vars_occurrences attribute """
        self._untainted_vars_occurrences = card

    @property
    def vars_occurrences(self) -> List[int]:
        """ Getter for the vars_occurrences attribute """
        return self._vars_occurrences

    @vars_occurrences.setter
    def vars_occurrences(self, card):
        """ Setter for the vars_occurrences attribute """
        self._vars_occurrences = card

    @property
    def dataset(self) -> str:
        """ Getter for the dataset attribute """
        return self._dataset

    @dataset.setter
    def dataset(self, card):
        """ Setter for the dataset attribute """
        self._dataset = card

    @property
    def dtype(self) -> np.dtype:
        """ Getter for the dtype attribute """
        return self._dtype

    @dtype.setter
    def dtype(self, card):
        """ Setter for the dtype attribute """
        self._dtype = card
