"""
    Noise module specifies an implementation of the NoiseInterface
    for the noise function: https://numpy.org/doc/stable/reference/random/generated/numpy.random.normal.html
    used to produce noise during the generation of the Measurements
"""
from numpy import random

from datag.interface.domain.Noise.noise_interface import NoiseInterface


class NoiseNumpyNormal(NoiseInterface):
    """
        NoiseNumpyRandom class uses the function random.normal(...)
        and its needed parameters to generate noise during the generation of the Measurements

        Attribute
        ----------
        mean: float
           Mean ("center") of the distribution
        standard_deviation: float
            standard deviation (spread or “width”) of the distribution
    """

    def __init__(
        self,
        mean: float,
        standard_deviation: float
    ):
        """ Constructor for the NoiseNumpyRandom class """
        self._mean = mean
        self._standard_deviation = standard_deviation

    def noise(self) -> float:
        """
            Implement noise() from the interface with the selected function, random.normal(...)

        """
        return float(random.normal(size=1,
                                   loc=self._mean,
                                   scale=self._standard_deviation)[0])

    @property
    def mean(self) -> float:
        """ Getter for the mean attribute """
        return self._mean

    @mean.setter
    def mean(self, mean):
        """ Setter for the mean attribute """
        self._mean = mean

    @property
    def standard_deviation(self) -> float:
        """ Getter for the standard_deviation attribute """
        return self._standard_deviation

    @standard_deviation.setter
    def standard_deviation(self, standard_deviation):
        """ Setter for the standard_deviation attribute """
        self._standard_deviation = standard_deviation
