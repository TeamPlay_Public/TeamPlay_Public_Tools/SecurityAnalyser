"""
    HDF5FileStorage module specifies an implementation of StorageInterface
    which encapsulates data related to the storage of the output as a HDF5 file
"""
from pathlib import Path

from sqcommon.interface.domain.Storage.storage_interface import StorageInterface


class HDF5FileStorage(StorageInterface):
    """
        HDF5FileStorage class encapsulates data related to the storage of the output as a HDF5 file

        Attribute
        ----------
        name: Path
            name of the HDF5 file where to store the output of the program
    """

    def __init__(
        self,
        name: Path
    ):
        """ Constructor for the HDF5FileStorage class """
        self._name = name

    @property
    def name(self) -> Path:
        """ Getter for the name attribute """
        return self._name

    @name.setter
    def name(self, name):
        """ Setter for the name attribute """
        self._name = name
