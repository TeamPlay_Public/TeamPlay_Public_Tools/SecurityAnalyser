"""
    OutputTuplesGenerationParams module encapsulates the parameters needed to generate the output tuples
"""
from typing import List

from datag.interface.domain.OutputTuplesGenerationParams.output_tuples_generation_params_interface \
    import OutputTuplesGenerationParamsInterface


class OutputTuplesGenerationParams(OutputTuplesGenerationParamsInterface):
    """
        OutputTuplesGenerationParams class specifies the parameters needed to generate the output tuples

        Attribute
        ----------
        tainted_vars_names: List[str]
            the names of the tainted variables to generate
        tainted_vars_occurrences: List[int]
            the number of occurrences for each tainted variables to generate
        untainted_vars_names: List[str]
            the names of the untainted variables to generate
        untainted_vars_occurrences: List[int]
            the number of occurrences for each untainted variables to generate
        side_channel_class: str
            the side channel class value, used to name the column in the Measurements

    """

    def __init__(
        self,
        tainted_vars_names: List[str],
        tainted_vars_occurrences: List[int],
        untainted_vars_names: List[str],
        untainted_vars_occurrences: List[int],
        side_channel_class: str
    ):
        """ Constructor for the OutputTuplesGenerationParams class """
        self._tainted_vars_names = tainted_vars_names
        self._tainted_vars_occurrences = tainted_vars_occurrences
        self._untainted_vars_names = untainted_vars_names
        self._untainted_vars_occurrences = untainted_vars_occurrences
        self._side_channel_class = side_channel_class

    @property
    def data(self) -> List:
        """ Returns a List containing the class attributes """
        return [self._tainted_vars_names,
                self._tainted_vars_occurrences,
                self._untainted_vars_names,
                self._untainted_vars_occurrences,
                self.total_occurrences_product]

    @property
    def tainted_vars_names(self) -> List[str]:
        """ Getter for the tainted_vars_names attribute """
        return self._tainted_vars_names

    @tainted_vars_names.setter
    def tainted_vars_names(self, tainted_vars_names):
        """ Setter for the tainted_vars_names attribute """
        self._tainted_vars_names = tainted_vars_names

    @property
    def tainted_vars_occurrences(self) -> List[int]:
        """ Getter for the tainted_vars_occurrences attribute """
        return self._tainted_vars_occurrences

    @tainted_vars_occurrences.setter
    def tainted_vars_occurrences(self, tainted_vars_occurrences):
        """ Setter for the tainted_vars_occurrences attribute """
        self._tainted_vars_occurrences = tainted_vars_occurrences

    @property
    def untainted_vars_names(self) -> List[str]:
        """ Getter for the untainted_vars_names attribute """
        return self._untainted_vars_names

    @untainted_vars_names.setter
    def untainted_vars_names(self, untainted_vars_names):
        """ Setter for the untainted_vars_names attribute """
        self._untainted_vars_names = untainted_vars_names

    @property
    def untainted_vars_occurrences(self) -> List[int]:
        """ Getter for the untainted_vars_occurrences attribute """
        return self._untainted_vars_occurrences

    @untainted_vars_occurrences.setter
    def untainted_vars_occurrences(self, untainted_vars_occurrences):
        """ Setter for the untainted_vars_occurrences attribute """
        self._untainted_vars_occurrences = untainted_vars_occurrences

    @property
    def side_channel_class(self) -> str:
        """ Getter for the side_channel_class attribute """
        return self._side_channel_class

    @side_channel_class.setter
    def side_channel_class(self, side_channel_class):
        """ Setter for the side_channel_class attribute """
        self._side_channel_class = side_channel_class

    @property
    def total_occurrences_product(self) -> int:
        """
            Method computing the number of occurrences of the cartesian product
            between the three sets of variables
        """
        total = 0
        for t_var_occ in self._tainted_vars_occurrences:
            total += int(t_var_occ)
        for u_var_occ in self._untainted_vars_occurrences:
            total += int(u_var_occ)

        return total
