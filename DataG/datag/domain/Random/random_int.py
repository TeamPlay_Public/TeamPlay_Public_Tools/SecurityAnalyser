"""
    RandomInt module specifies an implementation of the RandomInterface
    for the random function random.randint(...) https://docs.python.org/3/library/random.html
    used to generate the Variables
"""
from random import randint

from datag.interface.domain.Random.random_interface import RandomInterface


class RandomInt(RandomInterface):
    """
        RandomInt class uses the function random.randint(...)
        and its needed parameters to generate the Variables

        Attribute
        ----------
        lower_bound: int
            Such as the randomly generated integer is >= lower_bound
        upper_bound: int
            Such as the randomly generated integer is <= lower_bound
    """

    def __init__(
        self,
        lower_bound: int,
        upper_bound: int
    ):
        """ Constructor for the RandomInt class """
        self._lower_bound = lower_bound
        self._upper_bound = upper_bound

    def rand(self) -> int:
        """
            Implementation of the rand() method of the related interface.
            Returns a random integer value using the function randint from the package random
        """
        return randint(self._lower_bound, self._upper_bound)

    @property
    def lower_bound(self) -> int:
        """ Getter for the lower_bound attribute """
        return self._lower_bound

    @lower_bound.setter
    def lower_bound(self, lower_bound):
        """ Setter for the lower_bound attribute """
        self._lower_bound = lower_bound

    @property
    def upper_bound(self) -> int:
        """ Getter for the upper_bound attribute """
        return self._upper_bound

    @upper_bound.setter
    def upper_bound(self, upper_bound):
        """ Setter for the upper_bound attribute """
        self._upper_bound = upper_bound
