"""
    Module defining the Raw Binomial Distribution domain that computes the binomial_distribution algorithm
"""
from typing import List

from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples
from sqcommon.exception.list_length_exception import ListLengthException
from sqcommon.exception.nb_tuples_lower_or_equal_to_one_exception import NbTuplesLowerOrEqualToOneException
from sqcommon.interface.domain.Shuffle.shuffle_interface import ShuffleInterface


class RawBinomialDistribution(object):
    """
        RawBinomialDistribution class holds the generated binomial distribution.
        Unlike SecurityAnalyser/SQ_Common/sqcommon/domain/BinomialDistribution/binomial_distribution.py
        it does not shuffle it, but is meant to be generated and stored in a file.

        Attribute
        ----------
        nb_tuples: int
            the number of elements the generated distribution will be made of.
            This number matches the number of tuples in the source file,
            ie the Security Exchange file with the public, secret and leakage tuples
        distribution_type: EnumDistributionTuples
            The type of distribution to generate
    """

    def __init__(
        self,
        nb_tuples: int,
        distribution_type: EnumDistributionTuples
    ):
        """ Constructor for the BinomialDistribution class """
        self._nb_tuples = nb_tuples
        self._distribution_type = distribution_type
        self._final_binomial_distribution_list = self.compute_binomial_distribution()
        self._data = self.convert_to_dict()

    def compute_binomial_distribution(self):
        """
            Given the nb_tuples, the compute() method will generate a distribution with nb_tuples elements,
            following the binomial distribution algorithm

            :return
                A list of int representing the binomial distribution with nb_tuples elements
        """

        initial_list = [1]

        if self._nb_tuples <= 1:
            raise NbTuplesLowerOrEqualToOneException(
                expression="The parameter nb_tuples for class BinomialDistribution "
                           "must be higher than 1",
                message='"The given nb_tuples parameter is equal to %d", '
                        'self._nb_tuples')
        for _ in range(1, self._nb_tuples):
            # Append a zero to the end of list
            list1 = initial_list + [0]

            # Append a zero to the beginning of list
            list2 = [0] + initial_list
            # Add the two lists elements
            initial_list = self._add_list(list1, list2)

        # Turns the list into a list of probability
        # by dividing each element of initial_list by 2^nb_tuples
        return list(map(lambda x: x / (2 ** (self._nb_tuples - 1)), initial_list))

    def convert_to_dict(self):
        """
            Embed the distribution into a Dictionary with key equal to distribution_type.value
        """
        # Turn the list of distribution into a Dict with key equal to distribution_type
        return {self._distribution_type.value: self._final_binomial_distribution_list}

    @staticmethod
    def _add_list(list1: List[int],
                  list2: List[int]):
        """
        Element_wise addition of two lists

            :param list1: A list of int
            :param list2: A list of int

            :return:
                A list where elements from list1 and list2 are added up.
        """
        if len(list1) != len(list2):
            raise ListLengthException(
                expression="The two lists given in parameter of the function add_list must have the "
                           "same length",
                message='"list1 length= %d , list2 length= %d", len(list1), len(list2)')

        return [a + b for a, b in zip(list1, list2)]

    @property
    def nb_tuples(self) -> List[float]:
        """ Getter for the nb_tuples attribute """
        return self._nb_tuples

    @nb_tuples.setter
    def nb_tuples(self, nb_tuples):
        """ Setter for the nb_tuples """
        self._nb_tuples = nb_tuples

    @property
    def data(self):
        """ Getter for the data attribute """
        return self._data

    @data.setter
    def data(self, data):
        """ Setter for the data attribute """
        self._data = data
