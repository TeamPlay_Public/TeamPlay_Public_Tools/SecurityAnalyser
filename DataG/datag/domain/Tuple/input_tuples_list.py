""" InputTuplesList module specifies a list of List[int] representing the input tuples """
from typing import List

from datag.interface.domain.Tuple.tuples_list_interface import TuplesListInterface


class InputTuplesList(TuplesListInterface):
    """ InputTuplesList class specifies attributes and methods for a list of List[int] representing the input tuples """

    def __init__(self,
                 tuples: List[List[int]]
                 ):
        """
            Constructor for the InputTuplesList class
        """
        self._tuples = tuples

    @property
    def data(self):
        """ Getter for the tuples attribute """
        return self._tuples

    def nb_tuples(self):
        """ Return the length of the tuples attributes """
        return len(self._tuples)
