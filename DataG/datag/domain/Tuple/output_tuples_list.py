""" OutputTuplesList module specifies a list of an implementation of TupleInterface """
from typing import List

from datag.interface.domain.Tuple.tuple_interface import TupleInterface
from datag.interface.domain.Tuple.tuples_list_interface import TuplesListInterface


class OutputTuplesList(TuplesListInterface):
    """ OutputTuplesList class specifies attributes and methods for a list of TupleInterface """

    def __init__(self,
                 tuples: List[TupleInterface]
                 ):
        """
            Constructor for the OutputTuplesList class
        """
        self._tuples = tuples

    @property
    def data(self):
        """ Getter for the tuples attribute """
        return self._tuples

    def nb_tuples(self):
        """ Returns the length of the tuples attribute """
        return len(self._tuples)

    def add(self, tuple_to_add):
        """ Method appending a tuple to the tuples attribute of type List[TupleInterface] """
        self._tuples.append(tuple_to_add)
