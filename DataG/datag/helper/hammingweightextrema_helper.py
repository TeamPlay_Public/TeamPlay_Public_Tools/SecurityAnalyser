""" HammingWeightExtremaHelper module defines tools helping with constructing a HammingWeightExtrema instance """
from datag.domain.Tuple.input_tuples_list import InputTuplesList


class HammingWeightExtremaHelper:
    """ HammingWeightExtremaHelper class defines tools helping with constructing a HammingWeightExtrema instance """

    @staticmethod
    def compute_avg(tuples: InputTuplesList):
        """
      Compute the average value for the measurements, ie the last element in TuplesList

        Parameters
        ----------
        tuples: InputTuplesList
            The list of measurements tuples

        Returns
        -------
        float
            The average value for all measurements
        """

        total = 0
        # For each element of the embedding list
        for curr_tuple in tuples.data:
            # The measurement we are interested in is the last element in the list
            total += curr_tuple[-1]

        return total / tuples.nb_tuples()
