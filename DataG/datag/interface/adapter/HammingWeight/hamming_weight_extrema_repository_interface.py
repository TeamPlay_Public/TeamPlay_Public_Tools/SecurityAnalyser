"""
    Module defining the interface for HammingWeightExtremaRepository.
    Interface to be implemented for a class to behave as a HammingWeightExtremaRepository
"""
import abc

from pandas import DataFrame

from sqcommon.domain.Variable.variables_list import VariablesList

from datag.interface.domain.HammingWeight.hamming_weight_extrema_interface import HammingWeightExtremaInterface


class HammingWeightExtremaRepositoryInterface(metaclass=abc.ABCMeta):
    """
        HammingWeightExtremaRepositoryInterface interface defines the behavior of a HammingWeightExtremaRepository
    """

    @staticmethod
    @abc.abstractmethod
    def find_extrema(data: DataFrame,
                     nb_tainted_vars: int,
                     variables_card: VariablesList
                     ) -> HammingWeightExtremaInterface:
        """
            find_extrema(...) is the method to implement to have a HammingWeightExtremaRepository behavior
        """
        pass
