"""
    Module defining the interface for MeasurementsStoreRepository.
    Interface to be implemented for a class to behave as an MeasurementsStoreRepository
"""
import abc

from sqcommon.interface.adapter.Storage.data_store_repository_interface import DataStoreRepositoryInterface
from sqcommon.interface.domain.Storage.storage_interface import StorageInterface

from datag.interface.domain.Measurements.measurements_interface import MeasurementsInterface


class MeasurementsStoreRepositoryInterface(DataStoreRepositoryInterface):
    """
        MeasurementsStoreRepositoryInterface interface defines the behavior of a MeasurementsStoreRepository
    """

    @staticmethod
    @abc.abstractmethod
    def store(data: MeasurementsInterface,
              storage: StorageInterface
              ):
        """
            store(...) is the method to implement to have a MeasurementsStoreRepository behavior
        """
        pass
