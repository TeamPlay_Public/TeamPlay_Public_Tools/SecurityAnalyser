"""
    Interface to be implemented by class to behave as HammingWeightExtrema
"""
import abc

from datag.domain.Tuple.input_tuples_list import InputTuplesList


class HammingWeightExtremaInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as HammingWeightExtrema
   """

    @property
    @abc.abstractmethod
    def minima(self) -> int:
        """ Getter for the minima attribute """
        pass

    @property
    @abc.abstractmethod
    def maxima(self) -> int:
        """ Getter for the maxima attribute """
        pass

    @property
    @abc.abstractmethod
    def minima_tuples(self) -> InputTuplesList:
        """ Getter for the minima_tuples attribute """
        pass

    @property
    @abc.abstractmethod
    def maxima_tuples(self) -> InputTuplesList:
        """ Getter for the maxima_tuples attribute """
        pass

    @property
    @abc.abstractmethod
    def min_avg(self) -> float:
        """ Getter for the min_avg attribute """
        pass

    @property
    @abc.abstractmethod
    def max_avg(self) -> float:
        """ Getter for the max_avg attribute """
        pass
