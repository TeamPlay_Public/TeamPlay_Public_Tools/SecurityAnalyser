"""
    Interface to be implemented by class to behave as HammingWeight
"""
import abc


class HammingWeightInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as HammingWeight
   """
    @property
    @abc.abstractmethod
    def value(self) -> str:
        """ Getter for the value attribute """
        pass
