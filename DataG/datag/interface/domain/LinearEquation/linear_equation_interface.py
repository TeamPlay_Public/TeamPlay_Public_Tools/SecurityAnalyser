"""
    Interface to be implemented by class to behave as LinearEquation
"""
import abc

from datag.interface.domain.LinearEquation.slope_interface import SlopeInterface
from datag.interface.domain.LinearEquation.y_intercept_interface import YInterceptInterface


class LinearEquationInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as LinearEquation
   """

    @property
    @abc.abstractmethod
    def slope(self) -> SlopeInterface:
        """ Getter for the slope attribute """
        pass

    @property
    @abc.abstractmethod
    def y_intercept(self) -> YInterceptInterface:
        """ Getter for the y_intercept attribute """
        pass
