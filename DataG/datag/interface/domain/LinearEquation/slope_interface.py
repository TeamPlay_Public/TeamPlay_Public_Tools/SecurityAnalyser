"""
    Interface to be implemented by class to behave as Slope
"""
import abc


class SlopeInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as Slope
   """

    @property
    @abc.abstractmethod
    def value(self) -> float:
        """
            Method to implement to return the slope value
        """
        pass
