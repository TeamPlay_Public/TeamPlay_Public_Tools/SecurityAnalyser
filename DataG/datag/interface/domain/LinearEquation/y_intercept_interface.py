"""
    Interface to be implemented by class to behave as YIntercept
"""
import abc


class YInterceptInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as YIntercept
   """

    @property
    @abc.abstractmethod
    def value(self) -> float:
        """
            Method to implement to behave as YIntercept.
            Returns the value of the y-intercept
        """
        pass
