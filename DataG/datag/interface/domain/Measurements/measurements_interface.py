"""
    Interface to be implemented by class having data of type Measurements
"""
import abc

from sqcommon.interface.domain.Data.data_interface import DataInterface

from datag.interface.domain.Tuple.tuples_list_interface import TuplesListInterface


class MeasurementsInterface(DataInterface):
    """
        Interface to be implemented by class having data of type Measurements
   """

    @property
    @abc.abstractmethod
    def data(self) -> TuplesListInterface:
        """ Getter for the data attribute """
        pass
