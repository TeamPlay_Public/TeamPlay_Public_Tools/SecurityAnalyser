"""
    Interface to be implemented by class to behave as Noise.
    Defines the noise method that returns a value to be considered as noise
"""
import abc


class NoiseInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as Noise.
        Defines the noise method that returns a value to be considered as noise
   """

    @property
    @abc.abstractmethod
    def noise(self) -> float:
        """ Getter for the noise attribute """
        pass
