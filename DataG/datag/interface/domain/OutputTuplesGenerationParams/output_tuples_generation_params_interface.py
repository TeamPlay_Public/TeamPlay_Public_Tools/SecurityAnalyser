"""
    Interface to be implemented by class to behave as OutputTuplesGenerationParams
"""
import abc
from typing import List

from sqcommon.interface.domain.Data.data_interface import DataInterface


class OutputTuplesGenerationParamsInterface(DataInterface):
    """
        Interface to be implemented by class to behave as OutputTuplesGenerationParams
   """
    @property
    @abc.abstractmethod
    def data(self) -> List:
        """ Getter for the data attribute """
        pass

    @property
    @abc.abstractmethod
    def tainted_vars_names(self) -> List[str]:
        """ Getter for the tainted_vars_names attribute """
        pass

    @property
    @abc.abstractmethod
    def tainted_vars_occurrences(self) -> List[int]:
        """ Getter for the tainted_vars_occurrences attribute """
        pass

    @property
    @abc.abstractmethod
    def untainted_vars_names(self) -> List[str]:
        """ Getter for the untainted_vars_names attribute """
        pass

    @property
    @abc.abstractmethod
    def untainted_vars_occurrences(self) -> List[int]:
        """ Getter for the untainted_vars_occurrences attribute """
        pass
