"""
    Interface to be implemented by class to behave as Random.
    Define the method rand(self) to generate random numbers
"""
import abc


class RandomInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as Random.
        Define the method rand(self) to generate random numbers
   """

    @property
    @abc.abstractmethod
    def rand(self):
        """
            Method to implement with the desired random generator
        """
        pass
