"""
    Interface to be implemented by class to behave as Tuple
"""
import abc

from sqcommon.interface.domain.Data.data_interface import DataInterface


class TupleInterface(DataInterface):
    """
        Interface to be implemented by class to behave as Tuple
   """

    @property
    @abc.abstractmethod
    def data(self):
        """ Setter for the data attribute """
        pass
