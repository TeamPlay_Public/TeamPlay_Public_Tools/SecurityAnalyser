"""
    Interface to be implemented by class to behave as TuplesList
"""
import abc

from sqcommon.interface.domain.Data.data_interface import DataInterface


class TuplesListInterface(DataInterface):
    """
        Interface to be implemented by class to behave as TuplesList
   """

    @property
    @abc.abstractmethod
    def data(self):
        """ Getter for the tuples attribute """
        pass

    @abc.abstractmethod
    def nb_tuples(self):
        """ Getter for the nb_tuples attribute """
        pass
