"""   Main module """

import time

from sqcommon.adapter.File.JSON.Distribution.json_distribution_store_repository import JSONDistributionStoreRepository
from sqcommon.adapter.File.HDF5.Data.hdf5_data_as_dataframe_repository import HDF5DataAsDataFrameGetRepository
from sqcommon.domain.Output.JSONFile.json_file_storage import JSONFileStorage
from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples
from sqcommon.use_case.Data.hdf5_data_as_dataframe_get import HDF5DataAsDataFrameGetUC

from datag.domain.Config.config_args import ConfigArgs
from datag.domain.Config.config_cli_args import ConfigCliArgs

from sqcommon.use_case.BinomialDistributionNoise.binomial_distribution_noise_uc import BinomialDistributionNoiseUC
from datag.use_case.Config.config_get_uc import ConfigurationGetUC
from datag.use_case.DataGenerating.data_generate_uc import DataGenerateUC
from datag.use_case.RawBinomialDistribution.raw_binomial_distribution_generate_uc import CSVRawBinomialDistributionGetUC


def main():
    """   Main function """

    start_time = time.time()

    # Initialize parser
    config_args = ConfigArgs()
    parser = config_args.parser()

    # Run parameters are: main.py -c ../config/datag.ini -i ../data/input/WCET.h5 -o ../data/output/hdf5/WCET.h5
    # with an additional parameter, either -dg for the DataGenerator or -bd for the Binomial Distribution
    # It is also possible to ask the DataG to generate a (raw) binomial distribution using argument

    # Read arguments from command line
    args = parser.parse_args()
    cli_args = ConfigCliArgs(
        config_file=args.Config,
        input_f=args.Input,
        hdf5_output_dg=args.OHDF5,
        is_data_generator=args.DG,
        is_binomial_distribution=args.BD,
        is_generate_raw_binomial_distribution=args.RBD,
        binomial_distribution_nb_tuples=args.NbTuples,
        json_output_bd=args.OJSON,
        generated_raw_binomial_distribution_output=args.RBD
    )

    # UC: Extracting information from the configuration file
    config_uc = ConfigurationGetUC(
        config_file=cli_args.config_file,
        hdf5_input_file_dg=cli_args.input_f,
        hdf5_output_file_dg=cli_args.hdf5_output_dg,
        is_data_generator=cli_args.is_data_generator,
        is_binomial_distribution=cli_args.is_binomial_distribution,
        is_generate_raw_binomial_distribution=cli_args.is_generate_raw_binomial_distribution,
        binomial_distribution_nb_tuples=args.NbTuples,
        json_output_file_bd=cli_args.json_output_bd,
        generated_raw_binomial_distribution_output=cli_args.generated_raw_binomial_distribution_output
    )
    config_data = config_uc.execute()

    step1_time = time.time()
    print("--- Config: %s seconds ---" % (step1_time - start_time))

    # If we are in the case where we want to generate a raw binomial distribution,
    # that is only a binomial distribution stored in a csv file.
    # This is different from the case is_binomial_distribution
    # for which we generate a raw binomial distribution, then we shuffle it, etc...
    if config_data.is_generate_raw_binomial_distribution:
        # UC: Generate a raw binomial distribution with "nb_tuples" elements.
        # The distribution is a joint distribution secret, public stored in a JSON file
        raw_binomial_dist_uc = CSVRawBinomialDistributionGetUC(
            output_storage_repo=JSONDistributionStoreRepository,
            distribution_type=EnumDistributionTuples.SECRET_PUBLIC,
            nb_elements=config_data.binomial_distribution_nb_tuples,
            output_path=config_data.generated_raw_binomial_distribution_output
        )
        raw_binomial_dist_uc.execute()
    else:
        # UC: Get the HDF5 file as a DataAsDataframe
        data_uc = HDF5DataAsDataFrameGetUC(
            data_repo=HDF5DataAsDataFrameGetRepository,
            input_path=config_data.input_hdf5file
        )
        data = data_uc.execute()

        if config_data.is_data_generator:
            dg_uc = DataGenerateUC(config_data=config_data,
                                   data=data)
            dg_uc.execute()
        elif config_data.is_binomial_distribution:
            # Generate the distribution
            ng_uc = BinomialDistributionNoiseUC(json_output_file=config_data.json_output_file_bd,
                                                nb_tuples=len(data.data),
                                                distribution_type=EnumDistributionTuples.SECRET_PUBLIC,
                                                storage_data=JSONFileStorage,
                                                storage_repo=JSONDistributionStoreRepository
                                                )
            ng_uc.execute()


if __name__ == '__main__':
    main()
