""" Module defining the uses cases related to the ConfigurationGetUC """
from configparser import ConfigParser
from pathlib import Path

from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from datag.domain.Config.config_data import ConfigData


class ConfigurationGetUC(UseCaseInterface):
    """ ConfigurationGetUC class defines the execute method that returns an ConfigData instance """

    def __init__(self,
                 config_file: Path,
                 hdf5_input_file_dg: Path,
                 hdf5_output_file_dg: Path,
                 is_data_generator: bool,
                 is_binomial_distribution: bool,
                 is_generate_raw_binomial_distribution: bool,
                 binomial_distribution_nb_tuples: int,
                 json_output_file_bd: Path,
                 generated_raw_binomial_distribution_output: Path
                 ):
        """ Constructor for the ConfigurationGetUC class """
        # Configuration file
        self._config_file = config_file
        # Input hdf5 file
        self._hdf5_input_file_dg = hdf5_input_file_dg
        # Output hdf5 file
        self._hdf5_output_file_dg = hdf5_output_file_dg
        # DataGenerator algorithm will be run
        self._is_data_generator = is_data_generator
        # BinomialCoefficient algorithm will be run
        self._is_binomial_distribution = is_binomial_distribution
        # GenerateRawBinomialDistribution algorithm will be run.
        self._is_generate_raw_binomial_distribution = is_generate_raw_binomial_distribution
        # Number of tuples in the Security Exchange file. Number of elements to generate for the distribution
        self._binomial_distribution_nb_tuples = binomial_distribution_nb_tuples
        # Output JSON file for the Binomial Distribution
        self._json_output_file_bd = json_output_file_bd
        # Output CSV file for the Raw Binomial Distribution
        self._generated_raw_binomial_distribution_output = generated_raw_binomial_distribution_output

    def execute(self):
        """
            The execute() method returns a ConfigData instance containing the information extracted
            from the configuration file and needed by the program to run
        """
        # Configuration file call
        conf_parser = ConfigParser()
        conf_parser.read(self._config_file)

        # Arguments for input/output files and is_data_generator, is_binomial_distribution can be given as argument
        # for the command line. In that case they preempt the information from the configuration file
        if not self._hdf5_input_file_dg:
            self._hdf5_input_file_dg = Path(conf_parser['input']['hdf5file'])
        if not self._hdf5_output_file_dg:
            self._hdf5_output_file_dg = Path(conf_parser['output']['hdf5_output_file_dg'])
        if self._is_data_generator == "":
            self._is_data_generator = conf_parser['mode'].getboolean('is_data_generator')
        if self._is_binomial_distribution == "":
            self._is_binomial_distribution = conf_parser['mode'].getboolean('is_binomial_distribution')
        if self._is_generate_raw_binomial_distribution == "":
            self._is_generate_raw_binomial_distribution = \
                conf_parser['mode'].getboolean('is_generate_raw_binomial_distribution')
        if not self._binomial_distribution_nb_tuples:
            self._binomial_distribution_nb_tuples = int(conf_parser['binomial_dist']['nb_tuples'])
        if not self._json_output_file_bd:
            self._json_output_file_bd = Path(conf_parser['output']['json_output_file_bd'])
        if not self._generated_raw_binomial_distribution_output:
            self._generated_raw_binomial_distribution_output = \
                Path(conf_parser['output']['csv_generated_raw_binomial_distribution_output'])

        # Configuration file call
        conf_parser = ConfigParser(converters={'list': lambda x: [i.strip() for i in x.split(',')]})
        conf_parser.read(self._config_file)

        config_data = ConfigData(
            input_hdf5file=self._hdf5_input_file_dg,
            hdf5_output_file_dg=self._hdf5_output_file_dg,
            tainted_vars_names_2g=conf_parser['variables'].getlist('tainted_vars_names'),
            tainted_vars_occurrences_2g=conf_parser['variables'].getlist('tainted_vars_occurrences'),
            untainted_vars_names_2g=conf_parser['variables'].getlist('untainted_vars_names'),
            untainted_vars_occurrences_2g=conf_parser['variables'].getlist('untainted_vars_occurrences'),
            random_lower_bound=int(conf_parser['random']['lower_bound']),
            random_upper_bound=int(conf_parser['random']['upper_bound']),
            mean=float(conf_parser['noise']['mean']),
            standard_deviation=float(conf_parser['noise']['standard_deviation']),
            is_data_generator=self._is_data_generator,
            is_binomial_distribution=self._is_binomial_distribution,
            is_generate_raw_binomial_distribution=self._is_generate_raw_binomial_distribution,
            binomial_distribution_nb_tuples=int(conf_parser['binomial_dist']['nb_tuples']),
            json_output_file_bd=self._json_output_file_bd,
            generated_raw_binomial_distribution_output=self._generated_raw_binomial_distribution_output
        )

        return config_data
