""" Module defining the uses cases related to the Data Generation """

import time

from sqcommon.adapter.File.HDF5.SideChannel.side_channel_class_get_hdf5_repository import \
    SideChannelClassGetHDF5Repository
from sqcommon.adapter.File.HDF5.Variable.variables_cardinality_repository import VariablesCardinalityHDF5Repository
from sqcommon.adapter.File.HDF5.Variable.variables_count_hdf5_repository import VariablesCountHDF5Repository
from sqcommon.adapter.File.HDF5.Variable.variables_names_list_hdf5_repository import VariablesNamesHDF5Repository

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame

from sqcommon.enums.enum_hdf5_attributes import HDF5Attributes

from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from sqcommon.use_case.SideChannel.side_channel_class_get import SideChannelClassGetUC
from sqcommon.use_case.Variable.variables_cardinality_hdf5 import VariablesCardHDF5UC
from sqcommon.use_case.Variable.variables_hdf5_count import VariablesHDF5CountUC
from sqcommon.use_case.Variable.variables_names_hdf5_list import VariablesNamesHDF5UC

from datag.adapter.Input.DataFrame.HammingWeightExtrema.hamming_weight_extrema_dataframe_get_repository import \
    HammingWeightExtremaDataframeGetRepository
from datag.adapter.Storage.File.HDF5.hdf5_measurements_store_repository import HDF5MeasurementsStoreRepository

from datag.domain.Config.config_data import ConfigData
from datag.domain.Noise.noise_numpy_normal import NoiseNumpyNormal
from datag.domain.Output.HDF5File.hdf5_file_storage import HDF5FileStorage
from datag.domain.OutputTuplesGenerationParams.output_tuples_generation_params import OutputTuplesGenerationParams
from datag.domain.Random.random_int import RandomInt


from datag.use_case.HammingWeight.hammingweight_extrema_get_uc import HammingWeightExtremaGetUC
from datag.use_case.LinearEquation.linear_equation_get_uc import LinearEquationGetUC
from datag.use_case.OutputTuples.output_tuples_with_measurement_generate_uc import OutputTuplesMeasurementGenerateUC
from datag.use_case.Storage.output_measurements_store_uc import OutputMeasurementsStoreUC


class DataGenerateUC(UseCaseInterface):
    """ DataGenerateUC class runs the DataGenerator algorithm which generates a HDF5 file  """

    def __init__(self,
                 config_data: ConfigData,
                 data: DataAsDataFrame
                 ):
        """ Constructor for the DataGenerateUC class """
        self._config_data = config_data
        self._data = data

    def execute(self):
        """
            The execute() method compute binomial coefficients for number len(input),
            then divide the number by the total cardinality, to have a distribution (Gaussian curve)
        """

        step1_time = time.time()

        # UC: Get Side Channel Class from the HDF5 file
        scc_uc = SideChannelClassGetUC(
            side_channel_class_repo=SideChannelClassGetHDF5Repository,
            input_path=self._config_data.input_hdf5file
        )
        scc = scc_uc.execute()

        # UC: Get number of tainted variables from the HDF5 file
        nb_tainted_vars_uc = VariablesHDF5CountUC(
            variables_count_repo=VariablesCountHDF5Repository,
            input_path=self._config_data.input_hdf5file,
            side_channel_class=scc,
            hdf5_attribute=HDF5Attributes.NB_TAINTED
        )
        nb_tainted_vars = nb_tainted_vars_uc.execute()

        # UC: Get the variables names from the HDF5 file
        vars_names_uc = VariablesNamesHDF5UC(
            variables_names_repo=VariablesNamesHDF5Repository,
            input_path=self._config_data.input_hdf5file
        )
        vars_names = vars_names_uc.execute()

        # UC: Get the cardinality of each variable
        vars_cardinality_uc = VariablesCardHDF5UC(
            variables_card_repo=VariablesCardinalityHDF5Repository,
            input_path=self._config_data.input_hdf5file,
            side_channel_class=scc,
            vars_list=vars_names
        )
        vars_cardinalities = vars_cardinality_uc.execute()

        step2_time = time.time()
        print("--- Extraction from input: %s seconds ---" % (step2_time - step1_time))

        step3_time = time.time()

        # Find lowest and highest Hamming weight among the ks
        # Compute the Average value for the measurements related to the lowest and highest ks
        hamming_extrema_uc = HammingWeightExtremaGetUC(
            hamming_weight_extrema_repo=HammingWeightExtremaDataframeGetRepository,
            data=self._data,
            nb_tainted_vars=nb_tainted_vars,
            variables_card=vars_cardinalities
        )
        hamming_extrema = hamming_extrema_uc.execute()

        print("--- Finding HW Extrema: %s seconds ---" % (step3_time - step2_time))

        linear_equation_uc = LinearEquationGetUC(
            hamming_extrema=hamming_extrema
        )
        linear_equation = linear_equation_uc.execute()

        step4_time = time.time()
        print("--- Linear Equation: %s seconds ---" % (step4_time - step3_time))

        # Object encapsulating the needed parameters to generate the variables
        output_tuples_generation_params = OutputTuplesGenerationParams(
            tainted_vars_names=self._config_data.tainted_vars_names_2g,
            tainted_vars_occurrences=self._config_data.tainted_vars_occurrences_2g,
            untainted_vars_names=self._config_data.untainted_vars_names_2g,
            untainted_vars_occurrences=self._config_data.untainted_vars_occurrences_2g,
            side_channel_class=scc.value
        )

        step5_time = time.time()
        print("--- Tuples Parameters Generation: %s seconds ---" % (step5_time - step4_time))
        # Object encapsulating the Random parameters needed to generate the variables
        random_function_name = RandomInt(
            lower_bound=self._config_data.random_lower_bound,
            upper_bound=self._config_data.random_upper_bound
        )
        # Noise function name and parameters encapsulation object
        noise_function = NoiseNumpyNormal(
            mean=self._config_data.mean,
            standard_deviation=self._config_data.standard_deviation
        )

        # UC: Generate the tainted and untainted variables in addition to the measurement
        # and return a Measurements container
        tuples_generate_uc = OutputTuplesMeasurementGenerateUC(
            output_tuples_generation_params=output_tuples_generation_params,
            rand_function=random_function_name,
            linear_equation=linear_equation,
            noise=noise_function
        )
        generated_measurements = tuples_generate_uc.execute()

        step6_time = time.time()
        print("--- Tuples Generation: %s seconds ---" % (step6_time - step5_time))

        # Write the results in a HDF5 file.
        hdf5_file_store_uc = OutputMeasurementsStoreUC(
            output_storage_repo=HDF5MeasurementsStoreRepository,
            data=generated_measurements,
            storage_data=HDF5FileStorage(self._config_data.hdf5_output_file_dg)
        )
        hdf5_file_store_uc.execute()

        step7_time = time.time()
        print("--- Storage writing: %s seconds ---" % (step7_time - step6_time))

        print("--- Total duration: %s seconds ---" % (step7_time - step2_time))
