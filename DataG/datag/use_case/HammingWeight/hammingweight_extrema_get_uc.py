""" Module defining the uses cases related to the HammingWeightExtrema """
from pandas import DataFrame
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from datag.interface.adapter.HammingWeight.hamming_weight_extrema_repository_interface import \
    HammingWeightExtremaRepositoryInterface


class HammingWeightExtremaGetUC(UseCaseInterface):
    """ HammingWeightExtremaGetUC class defines the execute method that returns an HammingWeightExtrema instance """

    def __init__(self,
                 hamming_weight_extrema_repo: HammingWeightExtremaRepositoryInterface,
                 data: DataFrame,
                 nb_tainted_vars: int,
                 variables_card:VariablesList
                 ):
        """ Constructor for the HammingWeightExtremaGetUC class """
        self._repository = hamming_weight_extrema_repo
        self._data = data
        self._nb_tainted_vars = nb_tainted_vars
        self._variables_card = variables_card

    def execute(self):
        """
            The execute() method calls the adapter to get the HammingWeightExtrema which contains
            the hamming weight extrema values and their related tuples
        """
        return self._repository.find_extrema(self._data, self._nb_tainted_vars, self._variables_card)
