""" Module defining the uses cases related to the LinearEquation """
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from datag.domain.LinearEquation.linear_equation import LinearEquation
from datag.domain.LinearEquation.slope import Slope
from datag.domain.LinearEquation.y_intercept import YIntercept

from datag.interface.domain.HammingWeight.hamming_weight_extrema_interface import HammingWeightExtremaInterface
from datag.interface.domain.LinearEquation.slope_interface import SlopeInterface
from datag.interface.domain.LinearEquation.y_intercept_interface import YInterceptInterface


class LinearEquationGetUC(UseCaseInterface):
    """ LinearEquationGetUC class defines the execute method that returns a LinearEquation instance """

    def __init__(self,
                 hamming_extrema: HammingWeightExtremaInterface
                 ):
        """ Constructor for the LinearEquationGetUC class """
        self._hamming_extrema = hamming_extrema

    def execute(self):
        """
            The execute() method calls two inner methods
            to get the Slope and the Y-intercept that define the Linear Equation
        """
        slope = self.__compute_slope()
        y_intercept = self.__compute_y_intercept(slope)

        return LinearEquation(slope=slope, y_intercept=y_intercept)

    def __compute_slope(self) -> SlopeInterface:
        """
            Compute the slope of a linear equation from a HammingWeightExtrema instance

            Returns
            -------
            SlopeInterface
                the slope of the linear equation
        """
        # With:
        # t_1 being the average measurements for the highest hamming weight
        t_1 = self._hamming_extrema.max_avg
        # t_0 being the average measurements for the lowest hamming weight
        t_0 = self._hamming_extrema.min_avg
        # HW(k_1) being the hamming weight maxima
        hw_k1 = self._hamming_extrema.maxima
        # HW(k_0) being the hamming weight minima
        hw_k0 = self._hamming_extrema.minima

        # Compute slope = (t_1 - t_0)/(HW(k_1) - HW(k_0)) with
        slope = (t_1 - t_0) / (hw_k1 - hw_k0)

        return Slope(slope)

    def __compute_y_intercept(self,
                              slope: SlopeInterface
                              ) -> YInterceptInterface:
        """
            Compute the y-intercept of a linear equation from a HammingWeightExtrema instance

            Parameters
            ----------
            slope: SlopeInterface
                the slope of the linear equation

            Returns
            -------
            YInterceptInterface
                the y-intercept of the linear equation
        """

        # With:
        # t_0 being the average measurements for the lowest hamming weight
        t_0 = self._hamming_extrema.min_avg
        # S being the slope of the linear equation
        s = slope.value
        # HW(k_0) being the hamming weight minima
        hw_k0 = self._hamming_extrema.minima

        # Compute y-intercept = t_0 - S.HW(k_0)
        y_intercept = t_0 - (s * hw_k0)

        return YIntercept(y_intercept)
