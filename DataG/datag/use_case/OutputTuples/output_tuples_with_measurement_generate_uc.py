"""
    Module defining the uses cases related to the generation of output tuples
     and the generation of the measurement in one class
"""
import numpy as np

from typing import List

from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from datag.domain.Measurements.measurements import Measurements
from datag.domain.OutputTuplesGenerationParams.output_tuples_generation_params import OutputTuplesGenerationParams

from datag.interface.domain.LinearEquation.linear_equation_interface import LinearEquationInterface
from datag.interface.domain.Noise.noise_interface import NoiseInterface

from datag.interface.domain.Random.random_interface import RandomInterface


class OutputTuplesMeasurementGenerateUC(UseCaseInterface):
    """
        OutputTuplesGenerateUC class defines the execute method that generate the output tuples
        needed to create the Measurements
    """

    def __init__(self,
                 output_tuples_generation_params: OutputTuplesGenerationParams,
                 rand_function: RandomInterface,
                 linear_equation: LinearEquationInterface,
                 noise: NoiseInterface
                 ):
        """ Constructor for the OutputTuplesGenerateUC class """
        self._output_tuples_generation_params = output_tuples_generation_params
        self._random_function = rand_function
        self._linear_equation = linear_equation
        self._noise = noise

    def execute(self):
        """
            The execute() method generates the output tuples needed to create the Measurements
            and returns a Measurements container holding the values
        """
        # Extract number of occurrences for each variable tuple
        # so that we know how many tuples should be generated
        nb_tuples_to_generate = cartesian_product_occurrences(
            self._output_tuples_generation_params.tainted_vars_occurrences)
        nb_tuples_to_generate *= cartesian_product_occurrences(
            self._output_tuples_generation_params.untainted_vars_occurrences)

        # Merge the list of variables occurrences
        merged_list = self._output_tuples_generation_params.tainted_vars_occurrences[:]
        merged_list.extend(self._output_tuples_generation_params.untainted_vars_occurrences)

        # Create Measurements
        measurements = Measurements(output_tuples_generation_params=self._output_tuples_generation_params,
                                    vars_occurrences=merged_list,
                                    nb_tuples=nb_tuples_to_generate,
                                    dataset=self._output_tuples_generation_params.side_channel_class)

        # How many variables (tainted + untainted) do we have ?
        nb_vars = len(self._output_tuples_generation_params.tainted_vars_occurrences) \
                  + len(self._output_tuples_generation_params.untainted_vars_occurrences)

        # Generated tuples container
        tuples = []
        # for the cartesian product of each number of occurrences
        for _ in range(nb_tuples_to_generate):
            # current tuple is of type tuple
            current_tuple = ()
            # for each variable (tainted and untainted) generate a value.
            for curr_var in range(nb_vars):
                # Generate the value for the current variable
                rand_value = self._random_function.rand()
                # Concatenate to the tuple
                current_tuple += (rand_value,)
                # The first generated value is the secret value.
                # In that case we compute the related measurement:
                if curr_var == 0:
                    measurement = self.__compute_measurement(rand_value)
            current_tuple += (measurement,)
            # Adding the new tuple to tuples
            tuples.append(current_tuple)

        measurements.data = np.array(tuples)
        # Returns:
        # the Measurements instance containing the tuples and their measurements value
        # in addition to the needed metadata
        return measurements

    def __compute_measurement(self,
                              rand_value: float):

        return self._linear_equation.slope.value \
               * rand_value \
               + self._linear_equation.y_intercept.value \
               + self._noise.noise()


def cartesian_product_occurrences(vars_occurrences: List[int]):
    """
        cartesian_product_occurrences method computes the cartesian product of the number of occurrences
        in the list vars_occurrences

        Attribute
        ----------
        vars_occurrences: List[int]
            List of the occurrences related to a set of variables

        Return the total number of OutputTuples that will be generated
    """
    total = 1
    for i in range(0, len(vars_occurrences)):
        total *= int(vars_occurrences[i])

    return total
