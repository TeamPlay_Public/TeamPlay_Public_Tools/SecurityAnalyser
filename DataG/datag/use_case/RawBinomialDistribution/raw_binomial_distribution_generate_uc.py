""" Module defining the uses cases related to the generation of the binomial distribution """
from pathlib import Path

from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict
from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples

from sqcommon.interface.adapter.Storage.distribution_store_repository_interface import \
    DistributionStoreRepositoryInterface
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from datag.domain.RawBinomialDistribution.raw_binomial_distribution import RawBinomialDistribution


class CSVRawBinomialDistributionGetUC(UseCaseInterface):
    """
        CSVRawBinomialDistributionGetUC class defines the execute method that generate a raw binomial distribution,
        ie a binomial distribution without shuffling, etc
        unlike SQCommon/sqcommon/domain/BinomialDistribution/binomial_distribution.py
    """

    def __init__(self,
                 output_storage_repo: DistributionStoreRepositoryInterface,
                 distribution_type: EnumDistributionTuples,
                 nb_elements: int,
                 output_path: Path
                 ):
        """ Constructor for the CSVRawBinomialDistributionGetUC class """
        self._repository = output_storage_repo
        self._distribution_type = distribution_type
        self._nb_elements = nb_elements
        self._output_path = output_path

    def execute(self):
        """
            The execute() method calls the adapter to store the distribution
        """
        # First we compute the binomial distribution with nb_elements elements.
        # For the raw binomial distribution, the only useful parameter is nb_tuples,
        # so we fill in the others with useless values.
        raw_binomial_distrib_class = RawBinomialDistribution(nb_tuples=self._nb_elements,
                                                             distribution_type=self._distribution_type)

        raw_binomial_distrib_as_dict = DistributionAsDict(data=raw_binomial_distrib_class.data)

        # We save it in a JSON file
        self._repository.store(raw_binomial_distrib_as_dict, self._output_path)
