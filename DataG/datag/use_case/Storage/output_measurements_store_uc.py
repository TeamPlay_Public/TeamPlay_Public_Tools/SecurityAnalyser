""" Module defining the uses cases related to the storage of the program output """

from sqcommon.interface.domain.Storage.storage_interface import StorageInterface
from sqcommon.interface.use_case.storage_use_case_interface import StorageUseCaseInterface

from datag.interface.adapter.Storage.measurements_store_repository_interface import MeasurementsStoreRepositoryInterface
from datag.interface.domain.Measurements.measurements_interface import MeasurementsInterface


class OutputMeasurementsStoreUC(StorageUseCaseInterface):
    """ OutputMeasurementsStoreUC class defines the execute method that store the generated Measurements """

    def __init__(self,
                 output_storage_repo: MeasurementsStoreRepositoryInterface,
                 data: MeasurementsInterface,
                 storage_data: StorageInterface
                 ):
        """ Constructor for the OutputMeasurementsStoreUC class """
        super().__init__(output_storage_repo=output_storage_repo,
                         data=data,
                         storage_data=storage_data)

    def execute(self):
        """
            The execute() method calls the adapter to store the Measurements
        """
        return self._repository.store(self._data, self._storage_data)
