-r prod.txt

pytest>=6.2.4
tox>=3.23.1
coverage>=5.5
pytest-cov>=2.12.1
