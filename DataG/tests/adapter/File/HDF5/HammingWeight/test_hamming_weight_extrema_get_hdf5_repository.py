"""
    Testing class related to the adapter HammingWeightExtremaHDF5Repository and containing testing methods
"""

from datag.adapter.Input.DataFrame.HammingWeightExtrema.hamming_weight_extrema_dataframe_get_repository import \
    HammingWeightExtremaDataframeGetRepository
from datag.domain.HammingWeight.hamming_weight_extrema import HammingWeightExtrema
from datag.interface.adapter.HammingWeight.hamming_weight_extrema_repository_interface import \
    HammingWeightExtremaRepositoryInterface


def test_hw_extrema_repo_model_init():
    """ Test the HDF5DataAsDataFrameGetRepository class constructor """

    hw_extrema_repo = HammingWeightExtremaDataframeGetRepository()

    return hw_extrema_repo


def test_hdf_is_instance_of_hdf5dataframerepo():
    """ Test whether HDF5DataAsDataFrameGetRepository is an implementation of DataRepositoryInterface """

    hdf = test_hw_extrema_repo_model_init()

    assert isinstance(hdf, HammingWeightExtremaDataframeGetRepository)
    assert isinstance(hdf, HammingWeightExtremaRepositoryInterface)


def test_returned_value_is_instance_of_hammingweightextrema(data_as_dataframe_extrema_2064_and_2124,
                                                            return_variables_list_a10_n20_secret5):
    """
        Test whether HDF5DataAsDataFrameGetRepository.find_extrema(DataFrame) returns a HammingWeightExtrema
    """

    returned_value = HammingWeightExtremaDataframeGetRepository.find_extrema(
        data=data_as_dataframe_extrema_2064_and_2124,
        nb_tainted_vars=0,
        variables_card=return_variables_list_a10_n20_secret5)

    assert isinstance(returned_value, HammingWeightExtrema)
