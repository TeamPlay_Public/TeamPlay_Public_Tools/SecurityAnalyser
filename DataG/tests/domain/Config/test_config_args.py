"""
    Testing class related to the domain ConfigArgs and containing testing methods
"""
from argparse import ArgumentParser

import pytest

from datag.domain.Config.config_args import ConfigArgs


@pytest.fixture
def conf_args_model_init():
    """ Generate an instance of ConfigArgs """
    conf_args = ConfigArgs()

    return conf_args


def test_conf_args_is_instance_of_configargs(conf_args_model_init):
    """ Test ConfigArgs constructor """

    conf_args = conf_args_model_init

    assert isinstance(conf_args, ConfigArgs)


def test_conf_args_parser_is_instance_argumentparser(conf_args_model_init):
    """ Test the parser returned by parser() is an instance of ArgumentParser"""

    conf_args = conf_args_model_init

    parser = conf_args.parser()

    assert isinstance(parser, ArgumentParser)
