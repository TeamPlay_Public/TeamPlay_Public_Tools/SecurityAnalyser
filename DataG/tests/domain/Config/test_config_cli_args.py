"""
    Testing class related to the domain ConfigCliArgs and containing testing methods
"""

import pytest

from datag.domain.Config.config_cli_args import ConfigCliArgs


@pytest.fixture
def conf_cli_args_model_init(conf_cli_args_model_init):
    """ Generate an instance of ConfigCliArgs """
    conf_cli_args = conf_cli_args_model_init

    return conf_cli_args


def test_conf_cli_args_is_instance_of_conf_cli_args(conf_cli_args_model_init_nb_tuples_equal_1000_bd):
    """ Test ConfigCliArgs constructor """

    conf_cli_args = conf_cli_args_model_init_nb_tuples_equal_1000_bd

    assert isinstance(conf_cli_args, ConfigCliArgs)


def test_conf_cli_args_defines_property_config_file():
    """
        Test that ConfigCliArgs has a property named config_file.
    """

    config_file = getattr(ConfigCliArgs, "config_file", None)

    assert isinstance(config_file, property)


def test_conf_cli_args_defines_property_input_f():
    """
        Test that ConfigCliArgs has a property named input_f.
    """

    input_f = getattr(ConfigCliArgs, "input_f", None)

    assert isinstance(input_f, property)


def test_conf_cli_args_defines_property_hdf5_output_dg():
    """
        Test that ConfigCliArgs has a property named hdf5_output_dg.
    """

    hdf5_output_dg = getattr(ConfigCliArgs, "hdf5_output_dg", None)

    assert isinstance(hdf5_output_dg, property)


def test_conf_cli_args_defines_property_is_data_generator():
    """
        Test that ConfigCliArgs has a property named is_data_generator.
    """

    is_data_generator = getattr(ConfigCliArgs, "is_data_generator", None)

    assert isinstance(is_data_generator, property)


def test_conf_cli_args_defines_property_is_binomial_distribution():
    """
        Test that ConfigCliArgs has a property named is_binomial_distribution.
    """

    is_binomial_distribution = getattr(ConfigCliArgs, "is_binomial_distribution", None)

    assert isinstance(is_binomial_distribution, property)


def test_conf_cli_args_defines_property_binomial_distribution_nb_tuples():
    """
        Test that ConfigCliArgs has a property named binomial_distribution_nb_tuples.
    """

    binomial_distribution_nb_tuples = getattr(ConfigCliArgs, "binomial_distribution_nb_tuples", None)

    assert isinstance(binomial_distribution_nb_tuples, property)


def test_conf_cli_args_defines_property_json_output_bd():
    """
        Test that ConfigCliArgs has a property named json_output_bd.
    """

    json_output_bd = getattr(ConfigCliArgs, "json_output_bd", None)

    assert isinstance(json_output_bd, property)
