"""
    Testing class related to the domain ConfigData and containing testing methods
"""

import pytest

from datag.domain.Config.config_data import ConfigData


@pytest.fixture
def conf_data_model_init(configdata_model_init_with_nb_tuples_equal_4_bd):
    """ Generate an instance of ConfigData """
    conf_data = configdata_model_init_with_nb_tuples_equal_4_bd

    return conf_data


def test_conf_data_is_instance_of_configdata(conf_data_model_init):
    """ Test ConfigData constructor """

    conf_data = conf_data_model_init

    assert isinstance(conf_data, ConfigData)


def test_configdata_defines_property_tainted_vars_names_2g():
    """
        Test that ConfigData has a property named tainted_vars_names_2g.
    """

    tainted_vars_names_2g = getattr(ConfigData, "tainted_vars_names_2g", None)

    assert isinstance(tainted_vars_names_2g, property)


def test_configdata_defines_property_tainted_vars_occurrences_2g():
    """
        Test that ConfigData has a property named tainted_vars_occurrences_2g.
    """

    tainted_vars_occurrences_2g = getattr(ConfigData, "tainted_vars_occurrences_2g", None)

    assert isinstance(tainted_vars_occurrences_2g, property)


def test_configdata_defines_property_untainted_vars_names_2g():
    """
        Test that ConfigData has a property named untainted_vars_names_2g.
    """

    untainted_vars_names_2g = getattr(ConfigData, "untainted_vars_names_2g", None)

    assert isinstance(untainted_vars_names_2g, property)


def test_configdata_defines_property_untainted_vars_occurrences_2g():
    """
        Test that ConfigData has a property named untainted_vars_occurrences_2g.
    """

    untainted_vars_occurrences_2g = getattr(ConfigData, "untainted_vars_occurrences_2g", None)

    assert isinstance(untainted_vars_occurrences_2g, property)


def test_configdata_defines_property_random_lower_bound():
    """
        Test that ConfigData has a property named random_lower_bound.
    """

    random_lower_bound = getattr(ConfigData, "random_lower_bound", None)

    assert isinstance(random_lower_bound, property)


def test_configdata_defines_property_random_upper_bound():
    """
        Test that ConfigData has a property named random_upper_bound.
    """

    random_upper_bound = getattr(ConfigData, "random_upper_bound", None)

    assert isinstance(random_upper_bound, property)


def test_configdata_defines_property_mean():
    """
        Test that ConfigData has a property named mean.
    """

    mean = getattr(ConfigData, "mean", None)

    assert isinstance(mean, property)


def test_configdata_defines_property_standard_deviation():
    """
        Test that ConfigData has a property named standard_deviation.
    """

    standard_deviation = getattr(ConfigData, "standard_deviation", None)

    assert isinstance(standard_deviation, property)
