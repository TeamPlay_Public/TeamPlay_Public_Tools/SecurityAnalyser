"""
    Testing class related to the domain HammingWeight and containing testing methods
"""
import pytest

from datag.domain.HammingWeight.hamming_weight import HammingWeight
from datag.interface.domain.HammingWeight.hamming_weight_interface import HammingWeightInterface


@pytest.fixture
def hamming_weight_model_init():
    """
        Initialize a HammingWeight instance
    """

    hw = HammingWeight(3)

    return hw


def test_hwe_is_instance_of_hammingweightextrema(hamming_weight_model_init):
    """
        Test whether HammingWeight is an implementation of HammingWeightInterface
    """

    hwe = hamming_weight_model_init

    assert isinstance(hwe, HammingWeight)
    assert isinstance(hwe, HammingWeightInterface)


def test_hamming_weight_0_equals_2(hamming_weight_model_init):
    """
        Test whether the object created by test_hamming_weight_extrema_model_init() returns a Hamming Weight equals to 2
    """
    hamming_weight = hamming_weight_model_init
    returned_value = hamming_weight.weight()

    assert returned_value == 2


def test_hamming_weight_1_equals_1():
    """
        Test whether HammingWeight(1) returns a Hamming Weight equals to 2
    """
    hamming_weight = HammingWeight(1)
    returned_value = hamming_weight.weight()

    assert returned_value == 1


def test_hamming_weight_7_equals_3():
    """
        Test whether HammingWeight(7) returns a Hamming Weight equals to 3
    """
    hamming_weight = HammingWeight(7)
    returned_value = hamming_weight.weight()

    assert returned_value == 3, "Should be 3"


def test_hamming_weight_255_equals_8():
    """
        Test whether HammingWeight(255) returns a Hamming Weight equals to 8
    """
    hamming_weight = HammingWeight(255)
    returned_value = hamming_weight.weight()

    assert returned_value == 8, "Should be 8"


def test_hamming_weight_699050_equals_10():
    """
        Test whether HammingWeight(699050) returns a Hamming Weight equals to 10
    """
    hamming_weight = HammingWeight(699050)
    returned_value = hamming_weight.weight()

    assert returned_value == 10, "Should be 10"


def test_hamming_weight_negative_integer_ok():
    """
        Test whether HammingWeight(x) with x < 0 works and returns a correct Hamming Weight
    """
    hamming_weight = HammingWeight(-345435)
    returned_value = hamming_weight.weight()

    assert returned_value == 10, "Should be 10"


def test_hamming_weight_with_float_fails():
    """
        Test that the method HammingWeight(x) fails with Type(x) = float
    """
    hamming_weight = HammingWeight(6541.35)
    with pytest.raises(Exception) as excinfo:
        hamming_weight.weight()
    assert str(excinfo.value) == "'float' object cannot be interpreted as an integer"


def test_hamming_weight_with_string_fails():
    """
        Test that the method HammingWeight(x) fails with Type(x) = string
    """
    hamming_weight = HammingWeight("010101010")
    with pytest.raises(Exception) as excinfo:
        hamming_weight.weight()
    assert str(excinfo.value) == "'str' object cannot be interpreted as an integer"
