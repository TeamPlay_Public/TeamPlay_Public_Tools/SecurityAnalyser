"""
    Testing class related to the domain HammingWeightExtrema and containing testing methods
"""
import pytest

from datag.domain.HammingWeight.hamming_weight_extrema import HammingWeightExtrema
from datag.domain.Tuple.input_tuples_list import InputTuplesList
from datag.interface.domain.HammingWeight.hamming_weight_extrema_interface import HammingWeightExtremaInterface


@pytest.fixture
def hamming_weight_extrema_model_init():
    """ Initialize a HammingWeightExtrema instance """

    hwe = HammingWeightExtrema(0, 0, InputTuplesList([[]]), InputTuplesList([[]]), 0.0, 0.0)

    return hwe


def test_hwe_is_instance_of_hammingweightextrema(hamming_weight_extrema_model_init):
    """ Test whether hwe is an implementation of HammingWeightExtremaInterface """

    hwe = hamming_weight_extrema_model_init

    assert isinstance(hwe, HammingWeightExtrema)
    assert isinstance(hwe, HammingWeightExtremaInterface)


def test_hwe_has_proper_attributes(hamming_weight_extrema_model_init):
    """ Test whether hwe contains the correct attributes """

    hwe = hamming_weight_extrema_model_init

    assert isinstance(hwe.minima, int)
    assert isinstance(hwe.maxima, int)
    assert isinstance(hwe.minima_tuples, InputTuplesList)
    assert isinstance(hwe.maxima_tuples, InputTuplesList)
    assert isinstance(hwe.min_avg, float)
    assert isinstance(hwe.max_avg, float)


def test_hamming_weight_minima_equals_0(hamming_weight_extrema_model_init):
    """ Test that the minima has the expected value """

    hwe = hamming_weight_extrema_model_init
    minima = hwe.minima

    assert minima == 0


def test_hamming_weight_maxima_equals_0(hamming_weight_extrema_model_init):
    """ Test that the maxima has the expected value """
    hwe = hamming_weight_extrema_model_init
    maxima = hwe.maxima

    assert maxima == 0
