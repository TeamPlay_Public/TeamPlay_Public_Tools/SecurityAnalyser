"""
    Testing class related to the domain LinearEquation and containing testing methods
"""
import pytest

from datag.domain.LinearEquation.linear_equation import LinearEquation
from datag.domain.LinearEquation.slope import Slope
from datag.domain.LinearEquation.y_intercept import YIntercept
from datag.interface.domain.LinearEquation.linear_equation_interface import LinearEquationInterface


@pytest.fixture
def linear_equation_model_init():
    """
        Instantiate a LinearEquation object
    """

    le = LinearEquation(Slope(3), YIntercept(4))

    return le


def test_le_is_instance_of_linearequation(linear_equation_model_init):
    """
        Test whether LinearEquation is an implementation of LinearEquationInterface
    """

    le = linear_equation_model_init

    assert isinstance(le, LinearEquation)
    assert isinstance(le, LinearEquationInterface)


def test_linear_equation_slope_equals_3(linear_equation_model_init):
    """
        Test whether slope from the instance provided by linear_equation_model_init equals 3
    """
    linear_equation = linear_equation_model_init
    returned_value = linear_equation.slope.value

    assert returned_value == 3


def test_linear_equation_y_intercept_equals_4(linear_equation_model_init):
    """
        Test whether y-intercept from the instance provided by linear_equation_model_init equals 4
    """
    linear_equation = linear_equation_model_init
    returned_value = linear_equation.y_intercept.value

    assert returned_value == 4
