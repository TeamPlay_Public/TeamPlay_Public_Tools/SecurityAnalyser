"""
    Testing class related to the domain Slope and containing testing methods
"""
import pytest

from datag.domain.LinearEquation.slope import Slope
from datag.interface.domain.LinearEquation.slope_interface import SlopeInterface


@pytest.fixture
def slope_model_init():
    """
        Instantiate a Slope object
    """

    s = Slope(3.14)

    return s


def test_s_is_instance_of_slope(slope_model_init):
    """
        Test whether Slope is an implementation of SlopeInterface
    """

    s = slope_model_init

    assert isinstance(s, Slope)
    assert isinstance(s, SlopeInterface)


def test_slope_equals_3(slope_model_init):
    """
        Test whether slope from the instance provided by slope_model_init equals 3.14
    """
    slope = slope_model_init
    returned_value = slope.value

    assert returned_value == 3.14


def test_returned_value_is_float(slope_model_init):
    """
        Test whether the returned value is a float
    """
    slope = slope_model_init
    returned_value = slope.value

    assert type(returned_value) is float
