"""
    Testing class related to the domain YIntercept and containing testing methods
"""
import pytest

from datag.domain.LinearEquation.y_intercept import YIntercept
from datag.interface.domain.LinearEquation.y_intercept_interface import YInterceptInterface


@pytest.fixture
def y_intercept_model_init():
    """
        Instantiate a YIntercept object
    """

    y = YIntercept(3.14)

    return y


def test_y_is_instance_of_y_intercept(y_intercept_model_init):
    """
        Test whether YIntercept is an implementation of YInterceptInterface
    """

    y = y_intercept_model_init

    assert isinstance(y, YIntercept)
    assert isinstance(y, YInterceptInterface)


def test_y_intercept_equals_3(y_intercept_model_init):
    """
        Test whether y_intercept from the instance provided by y_intercept_model_init equals 3.14
    """
    y_intercept = y_intercept_model_init
    returned_value = y_intercept.value

    assert returned_value == 3.14


def test_returned_value_is_float(y_intercept_model_init):
    """
        Test whether the returned value is a float
    """
    y_intercept = y_intercept_model_init
    returned_value = y_intercept.value

    assert type(returned_value) is float
