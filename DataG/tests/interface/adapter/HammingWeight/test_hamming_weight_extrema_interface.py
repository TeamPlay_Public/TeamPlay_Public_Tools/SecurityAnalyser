"""
    Testing class related to the Interface HammingWeightExtremaRepositoryInterface and containing testing methods
"""

# from datag.interface.adapter.HammingWeight.hamming_weight_extrema_interface import \
#     HammingWeightExtremaRepositoryInterface
#
#
# def test_hw_extrema_interface_model_init():
#     """ Test the HammingWeightExtremaRepositoryInterface class constructor """
#
#     hw_extrema_interface = HammingWeightExtremaRepositoryInterface()
#
#     return hw_extrema_interface
#
#
# def test_hw_extrema_interface_has_find_extrema_method():
#     """ Test whether HammingWeightExtremaRepositoryInterface has the find_extrema() method  """
#
#     hw_extrema_interface = test_hw_extrema_interface_model_init()
#
#     assert hasattr(hw_extrema_interface, "find_extrema")
