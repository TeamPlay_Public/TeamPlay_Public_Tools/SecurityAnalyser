"""
    Testing class related to the domain ConfigurationGetUC and containing testing methods
"""

import pytest

from datag.domain.Config.config_data import ConfigData
from datag.use_case.Config.config_get_uc import ConfigurationGetUC


@pytest.fixture
def conf_get_uc_model_init(return_conf_file,
                           return_hdf5_input_file_dg,
                           return_hdf5_output_file_for_dg,
                           return_is_data_generator_false,
                           return_is_binomial_distribution_true,
                           return_nb_tuples_1000,
                           return_json_output_file_for_bd_secret_public
                           ):
    """ Generate an instance of ConfigurationGetUC """
    conf_get_uc = ConfigurationGetUC(config_file=return_conf_file,
                                     hdf5_input_file_dg=return_hdf5_input_file_dg,
                                     hdf5_output_file_dg=return_hdf5_output_file_for_dg,
                                     is_data_generator=return_is_data_generator_false,
                                     is_binomial_distribution=return_is_binomial_distribution_true,
                                     binomial_distribution_nb_tuples=return_nb_tuples_1000,
                                     json_output_file_bd=return_json_output_file_for_bd_secret_public
                                     )

    return conf_get_uc


def test_conf_cli_args_is_instance_of_conf_cli_args(conf_get_uc_model_init):
    """ Test ConfigurationGetUC constructor """

    conf_get_uc = conf_get_uc_model_init

    assert isinstance(conf_get_uc, ConfigurationGetUC)


def test_conf_cli_args_return_configdata(conf_get_uc_model_init):
    """ Test ConfigurationGetUC execute method returned type """

    conf_get_uc = conf_get_uc_model_init

    configdata = conf_get_uc.execute()

    assert isinstance(configdata, ConfigData)


def test_conf_cli_args_return_configdata_with_data_from_conf_file(conf_get_uc_model_init,
                                                                  configdata_model_init_with_nb_tuples_equal_4_bd):
    """ Test ConfigurationGetUC execute method returned type has proper values """

    conf_get_uc = conf_get_uc_model_init

    configdata = conf_get_uc.execute()

    expected = configdata_model_init_with_nb_tuples_equal_4_bd

    assert configdata.tainted_vars_names_2g == expected.tainted_vars_names_2g
    assert configdata.tainted_vars_occurrences_2g == expected.tainted_vars_occurrences_2g
    assert configdata.untainted_vars_names_2g == expected.untainted_vars_names_2g
    assert configdata.untainted_vars_occurrences_2g == expected.untainted_vars_occurrences_2g
    assert configdata.random_lower_bound == expected.random_lower_bound
    assert configdata.random_upper_bound == expected.random_upper_bound
    assert configdata.mean == expected.mean
    assert configdata.standard_deviation == expected.standard_deviation
