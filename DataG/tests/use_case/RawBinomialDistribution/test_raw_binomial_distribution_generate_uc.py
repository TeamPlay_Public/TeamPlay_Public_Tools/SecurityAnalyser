"""
    Testing class related to the domain CSVRawBinomialDistributionGetUC and containing testing methods
"""

from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from datag.use_case.RawBinomialDistribution.raw_binomial_distribution_generate_uc \
    import CSVRawBinomialDistributionGetUC


def test_json_raw_binomial_distribution_uc_is_instance_of_interface(csv_raw_binomial_distrib_uc_init):
    """ Test CSVRawBinomialDistributionGetUC constructor """

    csv_raw_binomial_distribution_noise_uc = csv_raw_binomial_distrib_uc_init

    assert isinstance(csv_raw_binomial_distribution_noise_uc, CSVRawBinomialDistributionGetUC)
    assert isinstance(csv_raw_binomial_distribution_noise_uc, UseCaseInterface)


def test_json_raw_binomial_distribution_uc_defines_method_execute():
    """
        Test that the CSVRawBinomialDistributionGetUC has a method named execute.
    """

    execute = getattr(CSVRawBinomialDistributionGetUC, "execute", None)

    assert callable(execute)


def test_json_raw_distribution_get_uc_test_returned_value(csv_raw_binomial_distrib_uc_init):
    """ Test the CSVRawBinomialDistributionGetUC execute method return value """

    csv_raw_binomial_distribution_noise_uc = csv_raw_binomial_distrib_uc_init

    # Instantiating the UseCase with repo as a mock
    result = csv_raw_binomial_distribution_noise_uc.execute()

    # Fake Assert: The idea is to test the execute method with a debugger using this test case
    assert result is None
