"""
    File gathering pytest fixtures for testing scenario
"""
import os
import sys
from pathlib import Path

import pytest

from pandas import DataFrame

from sqcommon.adapter.File.JSON.Distribution.json_distribution_store_repository import JSONDistributionStoreRepository
from sqcommon.domain.BinomialDistribution.binomial_distribution import BinomialDistribution
from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.domain.Output.JSONFile.json_file_storage import JSONFileStorage
from sqcommon.domain.Shuffle.numpy_random_shuffle import NumpyRandomShuffle
from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.domain.Variable.variable import Variable
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.enums.enum_hdf5_attributes import HDF5Attributes
from sqcommon.enums.enum_side_channel_class import SideChannelClasses

from sqcommon.domain.Distribution.distribution_container import DistributionContainer
from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict
from sqcommon.domain.Distribution.distribution_for_tuples import Distribution
from sqcommon.domain.Distribution.distribution_tuples import DistributionTuples

from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples

sys.path.append('path')


# ---------------------------------------------------------------------------------
# Fixtures for files
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_absolute_dir_name():
    """ Return the directory name of pathname __file__ """
    return os.path.dirname(__file__)


@pytest.fixture
def return_input_file_secret5a10n20_time_worst(return_absolute_dir_name):
    """ Return the input file path with cards secret=5, a=10, n=20 and sidechannel is (TIME_WORST, time_worst)"""
    dirname = return_absolute_dir_name

    input_test_data = "tests/test_data/test_file_secret5a10n20_time_worst.h5"

    return Path(dirname, input_test_data)


@pytest.fixture
def return_input_file_secret5a10n20_time_worst_for_repository_test(return_absolute_dir_name):
    """ Return the input file path with cards secret=5, a=10, n=20 and sidechannel is (TIME_WORST, time_worst)"""
    dirname = return_absolute_dir_name

    input_test_data = "tests/test_data/test_file_secret5a10n20_time_worst.h5"

    return Path(dirname, input_test_data)


@pytest.fixture
def full_distribution_file_init(return_absolute_dir_name):
    """ Fixture method to get the test_distribution_full.json file """
    dirname = return_absolute_dir_name

    full_dist_file = 'tests/test_data/input/json/distribution/test_full_distribution.json'

    return Path(dirname, full_dist_file)


@pytest.fixture
def partial_distribution_file_init(return_absolute_dir_name):
    """ Fixture method to get the test_distribution_with_some_missing_dist.json file """
    dirname = return_absolute_dir_name

    partial_dist_file = 'tests/test_data/input/json/distribution/test_distribution_with_some_missing_dist.json'

    return Path(dirname, partial_dist_file)


@pytest.fixture
def joint_distribution_file_init(return_absolute_dir_name):
    """
        Fixture method to get the 20210708_joint_dist_secret_public_12_values.json file
        which contains the joint distribution secret_public with 12 values
        corresponding to a SE file with :
            - One secret var with 3 occurrences
            - Two public vars with 2 occurrences each
    """
    dirname = return_absolute_dir_name

    partial_dist_file = 'tests/test_data/input/json/distribution/joint/20210708_joint_dist_secret_public_12_values.json'

    return Path(dirname, partial_dist_file)


@pytest.fixture
def json_generated_binomial_distribution_file_init(return_absolute_dir_name):
    """
        Fixture method returning the json file that will contain the generated binomial distribution.
    """
    dirname = return_absolute_dir_name

    partial_dist_file = 'tests/test_data/output/json/distribution/generated_binomial_distribution.json'

    return Path(dirname, partial_dist_file)


# ---------------------------------------------------------------------------------
# Fixtures for Object instantiation
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_sidechannel_with_value_time_worst():
    """ Return a SideChannelClass with value=time_worst """

    return SideChannelClass(SideChannelClasses('time_worst').name, 'time_worst')


@pytest.fixture
def return_variable_with_value_a_10():
    """ Return a Variable with cardinality=10 and value=a """

    return Variable(value="a",
                    cardinality=10)


@pytest.fixture
def return_variable_with_value_n_20():
    """ Return a Variable with cardinality=20 and value=n """

    return Variable(value="n",
                    cardinality=20)


@pytest.fixture
def return_variable_with_value_secret_5():
    """ Return a Variable with cardinality=5 and value=secret """

    return Variable(value="secret",
                    cardinality=5)


@pytest.fixture
def return_variable_with_value_a_1():
    """ Return a Variable with cardinality=1 and value=a """

    return Variable(value="a",
                    cardinality=1)


@pytest.fixture
def return_variable_with_value_a_2():
    """ Return a Variable with cardinality=2 and value=a """

    return Variable(value="a",
                    cardinality=2)


@pytest.fixture
def return_variable_with_value_a_3():
    """ Return a Variable with cardinality=3 and value=a """

    return Variable(value="a",
                    cardinality=3)


@pytest.fixture
def return_variable_with_value_n_1():
    """ Return a Variable with cardinality=1 and value=n """

    return Variable(value="n",
                    cardinality=1)


@pytest.fixture
def return_variable_with_value_n_2():
    """ Return a Variable with cardinality=2 and value=n """

    return Variable(value="n",
                    cardinality=2)


@pytest.fixture
def return_variable_with_value_n_3():
    """ Return a Variable with cardinality=3 and value=n """

    return Variable(value="n",
                    cardinality=3)


@pytest.fixture
def return_variable_with_value_secret_3():
    """ Return a Variable with cardinality=3 and value=secret """

    return Variable(value="secret",
                    cardinality=3)


@pytest.fixture
def return_variables_list_a10_n20_secret5(return_variable_with_value_a_10,
                                          return_variable_with_value_n_20,
                                          return_variable_with_value_secret_5):
    """
        Return a VariableList with three variables:
        a=10
        n=20,
        secret=5
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_a_10)
    vars_list.lst.append(return_variable_with_value_n_20)
    vars_list.lst.append(return_variable_with_value_secret_5)

    return vars_list


@pytest.fixture
def return_variables_list_a1_n1_secret3(return_variable_with_value_a_1,
                                        return_variable_with_value_n_1,
                                        return_variable_with_value_secret_3):
    """
        Return a VariableList with three variables:
            a=3
            n=3,
            secret=3
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_a_1)
    vars_list.lst.append(return_variable_with_value_n_1)
    vars_list.lst.append(return_variable_with_value_secret_3)

    return vars_list


@pytest.fixture
def return_variables_list_a2_n2_secret3(return_variable_with_value_a_2,
                                        return_variable_with_value_n_2,
                                        return_variable_with_value_secret_3):
    """
        Return a VariableList with three variables:
            a=2
            n=2,
            secret=3
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_a_2)
    vars_list.lst.append(return_variable_with_value_n_2)
    vars_list.lst.append(return_variable_with_value_secret_3)

    return vars_list


@pytest.fixture
def return_variables_list_a3_n3_secret3(return_variable_with_value_a_3,
                                        return_variable_with_value_n_3,
                                        return_variable_with_value_secret_3):
    """
        Return a VariableList with three variables:
            a=3
            n=3,
            secret=3
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_a_3)
    vars_list.lst.append(return_variable_with_value_n_3)
    vars_list.lst.append(return_variable_with_value_secret_3)

    return vars_list


@pytest.fixture
def return_hdf5attributes_with_value_nb_tainted():
    """ Return a HDF5Attributes equal to NB_TAINTED """

    return HDF5Attributes.NB_TAINTED


@pytest.fixture
def return_hdf5attributes_with_value_nb_untainted():
    """ Return a HDF5Attributes equal to NB_UNTAINTED """

    return HDF5Attributes.NB_UNTAINTED


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution Objects - DistributionAsDict
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_distributionasdict_secret(return_distribution_as_dict_secret_indiscernability):
    """
        Return a DistributionAsDict with one entry: secret and its related values
    """

    distributionasdict = DistributionAsDict(return_distribution_as_dict_secret_indiscernability)

    return distributionasdict


@pytest.fixture
def return_distributionasdict_secret_v2(return_distribution_as_dict_secret_indiscernability_v2):
    """
        Return a DistributionAsDict with one entry: secret and its related values
    """

    distributionasdict = DistributionAsDict(return_distribution_as_dict_secret_indiscernability_v2)

    return distributionasdict


@pytest.fixture
def return_distributionasdict_secret_bl(return_distribution_as_dict_secret_indiscernability_bl):
    """
        Return a DistributionAsDict with one entry: secret and its related values
    """

    distributionasdict = DistributionAsDict(return_distribution_as_dict_secret_indiscernability_bl)

    return distributionasdict


@pytest.fixture
def return_distributionasdict_with_erroneous_secret_public_dist(
        return_erroneous_distribution_as_dict_secret_public_indiscernability_bl):
    """
        Return a DistributionAsDict containing an erroneous distribution under entry: secret_public.
        The sum of its elements is greater than 1 which should lead to the raise of the Exception ValueError
    """

    distributionasdict = DistributionAsDict(return_erroneous_distribution_as_dict_secret_public_indiscernability_bl)

    return distributionasdict


# ---------------------------------------------------------------------------------
# Fixtures for the BinomialDistribution Objects - BinomialDistribution
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_nb_secret_tuples_5():
    """
        Return a number of secret tuples equal to 5.
        Used by test_variables_helper.py
    """
    return 5


@pytest.fixture
def return_nb_public_tuples_200():
    """
        Return a number of public tuples equal to 10*20=200.
        Used by test_variables_helper.py
    """
    return 200


@pytest.fixture
def return_nb_tuples_1000():
    """
        Return a number of tuples equal to 1000.
        Used by :
            @pytest.fixture def binomial_distribution_model_init_nb_tuples_equals_1000
            test_variables_helper.py
     """
    return 1000


@pytest.fixture
def return_nb_tuples_4():
    """ Return a number of tuples equal to 4 """
    return 4


@pytest.fixture
def return_nb_tuples_200():
    """ Return a number of tuples equal to 200 """
    return 200


@pytest.fixture
def return_nb_tuples_0():
    """ Return a number of tuples equal to 0 """
    return 0


@pytest.fixture
def return_nb_tuples_1():
    """ Return a number of tuples equal to 1 """
    return 1


@pytest.fixture
def return_distribution_type_secret_public():
    """ Return a EnumDistributionTuples SECRET_PUBLIC"""
    return EnumDistributionTuples.SECRET_PUBLIC


@pytest.fixture
def return_binomial_distribution_for_nb_tuples_equal_4():
    """ Return the expected binomial distribution for a number of tuples equal to 4 """
    return [1 / 8, 3 / 8, 3 / 8, 1 / 8]


@pytest.fixture
def return_test_list_for_average_distribution_method_1():
    """ Return a list of 21 elements between 0 and 20"""
    return list(range(0, 21))


@pytest.fixture
def return_test_list_for_average_distribution_method_2():
    """ Return a list of 21 elements between 20 and 0"""
    return list(range(20, -1, -1))


@pytest.fixture
def return_test_list_of_length_40_for_average_distribution_method():
    """
        Return a list of 41 elements between 40 and 0.
        Used to check that average_distribution method raises a ListLengthException
        when the to lists given in parameter have different length
    """
    return list(range(40, -1, -1))


@pytest.fixture
def return_expected_result_list_for_average_distribution_method():
    """
        Return the expected result list for average_distribution method
        with parameters return_test_list_for_average_distribution_method_1
        and return_test_list_for_average_distribution_method_2
    """
    return [10] * 21


@pytest.fixture
def return_enumdistributiontuples_with_value_secret():
    """ Return a EnumDistributionTuples with value=secret """

    return EnumDistributionTuples.SECRET


@pytest.fixture
def return_enumdistributiontuples_with_value_public():
    """ Return a EnumDistributionTuples with value=public """

    return EnumDistributionTuples.PUBLIC


@pytest.fixture
def return_enumdistributiontuples_with_value_leakage():
    """ Return a EnumDistributionTuples with value=leakage """

    return EnumDistributionTuples.LEAKAGE


@pytest.fixture
def return_enumdistributiontuples_with_value_publicleakage():
    """ Return a EnumDistributionTuples with value=publicleakage """

    return EnumDistributionTuples.PUBLIC_LEAKAGE


@pytest.fixture
def return_enumdistributiontuples_with_value_secretpublic():
    """ Return a EnumDistributionTuples with value=secretpublic """

    return EnumDistributionTuples.SECRET_PUBLIC


@pytest.fixture
def return_numpy_random_shuffle():
    """ Return a NumpyRandomShuffle object """

    return NumpyRandomShuffle


# ---------------------------------------------------------------------------------
# Fixtures for simple attributes values
# ---------------------------------------------------------------------------------
@pytest.fixture
def returns_time_worst():
    """ Return the string time_worst """
    return "time_worst"


@pytest.fixture
def returned_value_value_is_time_worst():
    """ Return the side channel value returned by the SideChannelClassGetHDF5Repository find method """
    sdc_value = "time_worst"

    return sdc_value


@pytest.fixture
def returned_value_name_is_time_worst():
    """ Return the side channel name returned by the SideChannelClassGetHDF5Repository find method """
    sdc_value = "TIME_WORST"

    return sdc_value


@pytest.fixture
def return_var_type_tainted():
    """ Return the string nbTainted """
    return "nbTainted"


@pytest.fixture
def return_var_type_untainted():
    """ Return the string nbUntainted """
    return "nbUntainted"


@pytest.fixture
def return_data_col_names_as_list():
    """ Return the data columns names as a list """
    return ['secret', 'a', 'n', 'time_worst']


@pytest.fixture
def return_secret_vars_cards_equals_5_as_list():
    """ Return a secret variables cardinality equals to [5] as a list """
    return [5]


@pytest.fixture
def return_public_vars_cards_equals_10_20_as_list():
    """ Return a public variables cardinality equals to [10,20] as a list """
    return [10, 20]


@pytest.fixture
def return_nb_rows_equals_1000():
    """ Return a number of rows equal to 1000 """
    return 1000


@pytest.fixture
def return_nb_public_tuples_equals_200():
    """ Return a number of public tuples equal to 200 """
    return 200


@pytest.fixture
def return_nb_secret_tuples_equals_5():
    """ Return a number of secret tuples equal to 5 """
    return 5


@pytest.fixture
def return_nb_secret_equals_1():
    """ Return a number of secret variables equals to 1 """
    return 1


@pytest.fixture
def return_nb_public_equals_1():
    """ Return a number of public variables equals to 1 """
    return 1


@pytest.fixture
def return_nb_public_equals_2():
    """ Return a number of public variables equals to 2 """
    return 2


@pytest.fixture
def return_starting_index_equals_0():
    """ Return a starting index equals to 0 """
    return 0


@pytest.fixture
def return_starting_index_equals_1():
    """ Return a starting index equals to 1 """
    return 1


@pytest.fixture
def return_exponent_equals_2():
    """ Return an exponent equals to 2 """
    return 2


@pytest.fixture
def return_is_active_probe_true():
    """ Return a boolean stating that the probe is active """
    return True


@pytest.fixture
def return_is_active_probe_false():
    """ Return a boolean stating that the probe is not active """
    return False


@pytest.fixture
def return_nb_tuples__equals_9():
    """ Return a number of tuples equals to 9 """
    return 9


@pytest.fixture
def return_nb_untainted_tuples_equals_3():
    """ Return a number of untainted tuples equals to 3 """
    return 3


@pytest.fixture
def return_list_of_float_to_shuffle():
    """ Return a list of int to shuffle """
    return [1.1, 2.2, 3.3, 4.4, 5.5]


# ---------------------------------------------------------------------------------
# Fixtures for the Random
# ---------------------------------------------------------------------------------
@pytest.fixture
def random_shuffle_model_init(return_list_of_float_to_shuffle):
    """ Generate an instance of NumpyRandomShuffle """
    random_shuffle_instance = NumpyRandomShuffle(list_to_shuffle=return_list_of_float_to_shuffle)

    return random_shuffle_instance


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
# Fixtures for the Distribution - all_the_tuples
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_all_the_tuples_for_a():
    """ Return all_the_tuples for variable a following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [4, 4, 4,
            4, 4, 4,
            6, 6, 6,
            6, 6, 6]


@pytest.fixture
def return_all_the_tuples_for_n():
    """ Return all_the_tuples for variable n following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [33, 33, 33,
            33, 33, 33,
            35, 35, 35,
            35, 35, 35]


@pytest.fixture
def return_all_the_tuples_for_public():
    """ Return all_the_tuples for public variables following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [[4, 33], [4, 33], [4, 33],
            [4, 35], [4, 35], [4, 35],
            [6, 33], [6, 33], [6, 33],
            [6, 35], [6, 35], [6, 35]]


@pytest.fixture
def return_all_the_tuples_for_secrets():
    """ Return all_the_tuples as int for variable secret """
    return [1, 2, 3,
            1, 2, 3,
            1, 2, 3,
            1, 2, 3]


@pytest.fixture
def return_all_the_tuples_for_secret_as_bits():
    """ Return all_the_tuples for variable secret following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [0b01, 0b10, 0b11,
            0b01, 0b10, 0b11,
            0b01, 0b10, 0b11,
            0b01, 0b10, 0b11]


@pytest.fixture
def return_all_the_tuples_for_observations_a():
    """
        Return all_the_tuples for observations a
    """
    return [[6], [9], [14], [12], [1], [8], [2]]


@pytest.fixture
def return_all_the_tuples_for_indiscernability():
    """ Return all_the_tuples for leakage variables following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [[10], [2], [9], [11], [8], [3], [11], [10], [15], [4], [14], [16]]


@pytest.fixture
def return_all_the_tuples_for_public_leakage():
    """ Return all_the_tuples for public_leakage variables following example made by Yoann Marquer
        and available in slides on the INRIA gitlab repository:
        Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [[4, 33, 10], [4, 33, 8], [4, 33, 15],
            [4, 35, 2], [4, 35, 3], [4, 35, 4],
            [6, 33, 9], [6, 33, 11], [6, 33, 14],
            [6, 35, 11], [6, 35, 10], [6, 35, 16]]


@pytest.fixture
def return_all_the_tuples_for_secret_public():
    """ Return all_the_tuples as int for secret_public variables"""
    return [[1, 4, 33], [2, 4, 33], [3, 4, 33],
            [1, 6, 33], [2, 6, 33], [3, 6, 33],
            [1, 6, 35], [2, 6, 35], [3, 6, 35]]


@pytest.fixture
def return_all_the_tuples_for_secret_public_as_bits():
    """ Return all_the_tuples for secret_public variables following example made by Yoann Marquer
        and available in slides on the INRIA gitlab repository:
        Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [[0b01, 4, 33], [0b10, 4, 33], [0b11, 4, 33],
            [0b01, 6, 33], [0b10, 6, 33], [0b11, 6, 33],
            [0b01, 6, 35], [0b10, 6, 35], [0b11, 6, 35]]


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution - sorted_tuples_as_dict
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_sorted_tuples_as_dict_for_leakage_indiscernability():
    """ Return a dictionary with tuples (without duplicates) as key and number of occurrences as value
        for the variable leakage following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return {"[10]": 2, "[2]": 1, "[9]": 1, "[11]": 2, "[8]": 1,
            "[3]": 1, "[15]": 1, "[4]": 1, "[14]": 1, "[16]": 1
            }


@pytest.fixture
def return_related_secret_tuples_as_int_indiscernability():
    """ Return a list of secret values related to the leakage ones
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return ['[1]', '[1]', '[1]', '[1]', '[10]', '[10]', '[10]', '[10]', '[11]', '[11]', '[11]', '[11]']


@pytest.fixture
def return_tuples_as_list__no_duplicates_indiscernability():
    """ Return a list of leakage values unsorted without duplicates
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [10, 2, 9, 11, 8, 3, 15, 4, 14, 16]


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution - distribution
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_distribution_for_a():
    """ Return distribution for a following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return []


@pytest.fixture
def return_distribution_for_n():
    """ Return distribution for n following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return []


@pytest.fixture
def return_distribution_for_public():
    """ Return distribution for public following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return []


@pytest.fixture
def return_distribution_for_secret_for_obs_a():
    """ Return distribution for secret following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [[0.14], [0.35], [0.19],
            [0.06], [0.11], [0.01],
            [0.14]]


@pytest.fixture
def return_distribution_for_secret_for_indiscernability():
    """ Return distribution for secret following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [[0.1], [0.15], [0.1],
            [0.01], [0.01], [0.01],
            [0.14], [0.04], [0.2],
            [0.05], [0.09], [0.1]]


@pytest.fixture
def return_distribution_for_leakage():
    """ Return distribution for leakage following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return []


@pytest.fixture
def return_distribution_for_public_leakage():
    """ Return distribution for public_leakage following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return []


@pytest.fixture
def return_distribution_for_secret_public():
    """ Return distribution for secret_public following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return []


@pytest.fixture
def return_distribution_as_dict_secret_indiscernability():
    """ Return distribution as a dict for testing the JsonToDistributionContainerHelper methods"""
    dist_dict = {"secret":
                     [0.1, 0.15, 0.1,
                      0, 0, 0,
                      0.15, 0.05, 0.20,
                      0.05, 0.10, 0.1]}

    return dist_dict


@pytest.fixture
def return_distribution_as_dict_secret_indiscernability_v2():
    """ Return distribution as a dict for testing the JsonToDistributionContainerHelper methods"""
    dist_dict = {
        "secret_public":
            [0.05, 0.15, 0.05,
             0.10, 0.05, 0.10,
             0.15, 0.05, 0.10,
             0.05, 0.10, 0.05]
    }

    return dist_dict


@pytest.fixture
def return_distribution_as_dict_secret_indiscernability_bl():
    """ Return distribution as a dict following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing."""
    dist_dict = {
        "secret_public":
            [0.1, 0.01, 0.14,
             0.05, 0.15, 0.01,
             0.04, 0.09, 0.10,
             0.01, 0.2, 0.1]
    }

    return dist_dict


@pytest.fixture
def return_erroneous_distribution_as_dict_secret_public_indiscernability_bl():
    """ Return an erroneous distribution as a dict where the sum of its element is greater then 1"""
    dist_dict = {
        "secret_public":
            [1, 0.01, 0.14,
             0.05, 0.15, 0.01,
             0.04, 0.09, 0.10,
             0.01, 0.2, 0.1]
    }

    return dist_dict


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram : Observation, Public and Secret
# ---------------------------------------------------------------------------------
@pytest.fixture
def dendrogram_observations_a():
    """
        Returns a list of observations (leakage)
    """
    return [6, 9, 14, 12, 1, 8, 2]


@pytest.fixture
def dendrogram_observations_from_indiscernability():
    """
        Returns a list of observations (leakage) related to the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [10, 2, 9, 11, 8, 3, 11, 10, 15, 4, 14, 16]


@pytest.fixture
def dendrogram_sorted_observations_a():
    """
        Returns a list of observations (leakage)
    """
    return [1, 2, 6, 8, 9, 12, 14]


@pytest.fixture
def dendrogram_sorted_observations_from_indiscernability():
    """
        Returns a list of observations (leakage) related to the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [2, 3, 4, 8, 9, 10, 10, 11, 11, 14, 15, 16]


@pytest.fixture
def dendrogram_related_public_from_indiscernability():
    """
        Returns a list of public tuples
    """
    return [[4, 33], [4, 35], [6, 33], [6, 35]]


@pytest.fixture
def dendrogram_final_secret_as_bits_from_indiscernability():
    """
        Returns a list of secret keys related to the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [0b01, 0b10, 0b11, 0b01, 0b10, 0b11, 0b01, 0b10, 0b11, 0b01, 0b10, 0b11]


@pytest.fixture
def dendrogram_related_secret_for_observations_a():
    """
        Returns the associated secret values for the observations a.
        Note: these values are randomly chosen.
    """
    return [1, 2, 3, 4, 5, 6, 7]


@pytest.fixture
def dendrogram_related_secret_from_indiscernability():
    """
        Returns the associated secret values from the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [1, 1, 1, 1, 10, 10, 10, 10, 11, 11, 11, 11]


@pytest.fixture
def dendrogram_related_secret_from_indiscernability_as_bits():
    """
        Returns the associated secret values from the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [0b01, 0b01, 0b01, 0b01, 0b10, 0b10, 0b10, 0b10, 0b11, 0b11, 0b11, 0b11]


@pytest.fixture
def dendrogram_reordered_related_secret_for_observations_a():
    """
        Returns the associated secret values for the observations a.
        Note: Based on the secret values from dendrogram_related_secret_for_observations_a
    """
    return [5, 7, 1, 6, 2, 4, 3]


@pytest.fixture
def dendrogram_reordered_related_secret_from_indiscernability():
    """
        Returns the associated secret values from the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [1, 10, 11, 10, 1, 1, 10, 1, 10, 11, 11, 11]


@pytest.fixture
def dendrogram_reordered_related_secret_from_indiscernability_as_bits():
    """
        Returns the associated secret values from the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [0b01, 0b10, 0b11, 0b10, 0b01, 0b01, 0b10, 0b01, 0b10, 0b11, 0b11, 0b11]


@pytest.fixture
def return_dict_tuples_occurrences():
    """
        Return a dictionary of tuples occurrences for testing DendrogramGatheredTuples
    """
    return {}


@pytest.fixture
def return_tuples_list():
    """
        Return a list of tuples for testing DendrogramGatheredTuples
    """
    return []


@pytest.fixture
def return_final_leakage_clustering_4_33_bl():
    """
        Return the final leakage clustering for the public tuple [4,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[8, 10], [15]]


@pytest.fixture
def return_final_secret_clustering_4_33_bl():
    """
        Return the final secret clustering for the public tuple [4,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[10, 1], [11]]


@pytest.fixture
def return_final_secret_public_distrib_4_33_bl():
    """
        Return the final secret public clustering for the public tuple [4,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[0.15, 0.1], [0.1]]


@pytest.fixture
def return_final_leakage_clustering_4_35_bl():
    """
        Return the final leakage clustering for the public tuple [4,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[2, 3, 4]]


@pytest.fixture
def return_final_secret_clustering_4_35_bl():
    """
        Return the final secret clustering for the public tuple [4,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[1, 10, 11]]


@pytest.fixture
def return_final_secret_public_distrib_4_35_bl():
    """
        Return the final secret public clustering for the public tuple [4,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[0.01, 0.01, 0.01]]


@pytest.fixture
def return_final_leakage_clustering_6_33_bl():
    """
        Return the final leakage clustering for the public tuple [6,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[9, 11], [14]]


@pytest.fixture
def return_final_secret_clustering_6_33_bl():
    """
        Return the final secret clustering for the public tuple [6,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[1, 10], [11]]


@pytest.fixture
def return_final_secret_public_distrib_6_33_bl():
    """
        Return the final secret public clustering for the public tuple [6,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[0.14, 0.04], [0.2]]


@pytest.fixture
def return_final_leakage_clustering_6_35_bl():
    """
        Return the final leakage clustering for the public tuple [6,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[10, 11], [16]]


@pytest.fixture
def return_final_secret_clustering_6_35_bl():
    """
        Return the final secret clustering for the public tuple [6,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[10, 1], [11]]


@pytest.fixture
def return_final_secret_public_distrib_6_35_bl():
    """
        Return the final secret public clustering for the public tuple [6,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[0.09, 0.05], [0.1]]


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related: Distance
# ---------------------------------------------------------------------------------
@pytest.fixture
def distances_initial_for_observations_a():
    """
        Returns what the distances should be, given the dendrogram_observations_a.
    """

    return [1, 4, 2, 1, 3, 2]


@pytest.fixture
def distances_initial_for_observations_indiscernability():
    """
        Returns what the distances should be, given the dendrogram_observations_from_indiscernability.
    """

    return [1, 1, 4, 1, 1, 0, 1, 0, 3, 1, 1]


@pytest.fixture
def distances_sorted_no_duplicates_for_observations_a():
    """
        Returns what the distances should be, given the dendrogram_observations_a.
        The list is sorted and pruned for duplicates
    """

    return [1, 2, 3, 4]


@pytest.fixture
def distances_sorted_no_duplicates_for_observations_indiscernability():
    """
        Returns what the distances should be, given the dendrogram_observations_from_indiscernability.
        The list is sorted and pruned for duplicates
    """

    return [0, 1, 3, 4]


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related: resolution and nb_clusters
# ---------------------------------------------------------------------------------
@pytest.fixture
def resolution_equals_3():
    """
        Returns a resolution parameter equal to 3
    """
    return 3


@pytest.fixture
def resolution_equals_5():
    """
        Returns a resolution parameter equal to 5
    """
    return 5


@pytest.fixture
def resolution_equals_999999():
    """
        Returns a resolution parameter equal to 999999
    """
    return 999999


@pytest.fixture
def nb_clusters_equals_1():
    """
        Returns a nb_clusters parameter equal to 1
    """
    return 1


@pytest.fixture
def nb_clusters_equals_2():
    """
        Returns a nb_clusters parameter equal to 2
    """
    return 2


@pytest.fixture
def nb_clusters_equals_4():
    """
        Returns a nb_clusters parameter equal to 4
    """
    return 4


# ---------------------------------------------------------------------------------
# Fixtures for the Dataframe and associated
# ---------------------------------------------------------------------------------
@pytest.fixture
def dataframe_distance_equal_40cheb_init():
    """ Fixture method to generate a Dataframe for which results to expect are :
        2104 - 2064 = 40 for Chebyshev distance
    """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "00000001111100101111010111001011",
                               "00000001111100101111010111001011",
                               "10100001000001111000101011000111",
                               "10100001000001111000101011000111",
                               "10100001000001111000101011000111",
                               "10111111111100101001011110110001",
                               "10111111111100101001011110110001",
                               "10111111111100101001011110110001"],
                    'a': [2, 4, 8,
                          2, 4, 8,
                          2, 4, 8],
                    'n': [10, 15, 35,
                          10, 15, 35,
                          10, 15, 35],
                    'time_worst': [2064, 2142, 2376,
                                   2104, 2142, 2376,
                                   2084, 2142, 2376
                                   ]
                    })

    return df


@pytest.fixture
def dataframe_distance_equal_0_init():
    """ Fixture method to generate a Dataframe for which distance equals to 0 """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "10100001000001111000101011000111",
                               "10111111111100101001011110110001"],
                    'a': [2,
                          2,
                          2],
                    'n': [10,
                          10,
                          10],
                    'time_worst': [2064,
                                   2064,
                                   2064
                                   ]
                    })

    return df


@pytest.fixture
def dataframe_distance_equal_0_with_two_occurrences_init():
    """ Fixture method to generate a Dataframe for which distance equals to 0 """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "10111111111100101001011110110001"],
                    'a': [2,
                          2],
                    'n': [10,
                          10],
                    'time_worst': [2064,
                                   2064
                                   ]
                    })

    return df


@pytest.fixture
def data_as_dataframe_distance_equal_0_init(dataframe_distance_equal_0_init):
    """
         Fixture method to generate a DataAsDataframe from a DataFrame which distance equals to 0

        :param dataframe_distance_equal_0_init:
            pytest.fixture returning a DataFrame
        :return:
            DataAsDataFrame: DataFrame nested in a DataAsDataFrame
    """
    data_as_df = DataAsDataFrame(dataframe_distance_equal_0_init)

    return data_as_df


@pytest.fixture
def data_as_dataframe_distance_equal_40_init(dataframe_distance_equal_40cheb_init):
    """
         Fixture method to generate a DataAsDataframe from a DataFrame which distance equals to 0

        :param dataframe_distance_equal_40cheb_init:
            pytest.fixture returning a DataFrame
        :return:
            DataAsDataFrame: DataFrame nested in a DataAsDataFrame
    """
    data_as_df = DataAsDataFrame(dataframe_distance_equal_40cheb_init)

    return data_as_df


@pytest.fixture
def dataframe_from_indiscernability():
    """
        Fixture method to generate a Dataframe following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """

    df = DataFrame({'secret': ["01", "10", "11",
                               "01", "10", "11",
                               "01", "10", "11",
                               "01", "10", "11"],
                    'a': [4, 4, 4,
                          4, 4, 4,
                          6, 6, 6,
                          6, 6, 6],
                    'n': [33, 33, 33,
                          33, 33, 33,
                          35, 35, 35,
                          35, 35, 35],
                    'time_worst': [10, 8, 15,
                                   2, 3, 4,
                                   9, 11, 14,
                                   11, 10, 16]
                    })

    return df


@pytest.fixture
def dataframe_from_indiscernability_bl():
    """
        Fixture method to generate a Dataframe following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods
    """

    df = DataFrame({'secret': ["01", "01", "01", "01",
                               "10", "10", "10", "10",
                               "11", "11", "11", "11"],
                    'a': [4, 4, 6, 6,
                          4, 4, 6, 6,
                          4, 4, 6, 6],
                    'n': [33, 35, 33, 35,
                          33, 35, 33, 35,
                          33, 35, 33, 35],
                    'time_worst': [10, 2, 9, 11,
                                   8, 3, 11, 10,
                                   15, 4, 14, 16]
                    })

    return df


@pytest.fixture
def data_as_dataframe_from_indiscernability(dataframe_from_indiscernability):
    """
         Fixture method to generate a DataAsDataframe from a DataFrame example
         made by Yoann Marquer and available in slides
         on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary

        :param dataframe_from_indiscernability:
            pytest.fixture returning a DataFrame
        :return:
            DataAsDataFrame: DataFrame nested in a DataAsDataFrame
    """
    data_as_df = DataAsDataFrame(dataframe_from_indiscernability)

    return data_as_df


@pytest.fixture
def data_as_dataframe_from_indiscernability_bl(dataframe_from_indiscernability_bl):
    """
         Fixture method to generate a DataAsDataframe following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods

        :param dataframe_from_indiscernability_bl:
            pytest.fixture returning a DataFrame
        :return:
            DataAsDataFrame: DataFrame nested in a DataAsDataFrame
    """
    data_as_df = DataAsDataFrame(dataframe_from_indiscernability_bl)

    return data_as_df


# ---------------------------------------------------------------------------------
# Fixtures for the Dataframe Series
# ---------------------------------------------------------------------------------
@pytest.fixture
def series_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted(
        dataframe_distance_equal_0_with_two_occurrences_init):
    """
        Fixture method to return series from a dataframe with distance = 0,
        with one tainted vars and 2 untainted vars
    """

    df = dataframe_distance_equal_0_with_two_occurrences_init

    return df.iloc[0:1, 1:], df.iloc[1:2, 1:]


@pytest.fixture
def sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted(
        series_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted):
    """
        Fixture method to return the side channel values from the series
    """
    serie1, serie2 = series_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted

    return serie1.loc[0][-1:].values[0], serie2.loc[1][-1:].values[0]


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution and related
# ---------------------------------------------------------------------------------
@pytest.fixture
def tuples_distribution_model_init_for_a(return_all_the_tuples_for_a,
                                         return_distribution_for_a,
                                         return_enumdistributiontuples_with_value_secret):
    """ Generate an instance of Distribution """
    return_distribution_for_a = Distribution(dist_tuples=return_all_the_tuples_for_a,
                                             distribution=return_distribution_for_a,
                                             variable_name=return_enumdistributiontuples_with_value_secret)

    return return_distribution_for_a


@pytest.fixture
def tuples_distribution_model_init_for_n(return_all_the_tuples_for_n,
                                         return_distribution_for_n,
                                         return_enumdistributiontuples_with_value_secret):
    """ Generate an instance of Distribution """
    return_distribution_for_n = Distribution(dist_tuples=return_all_the_tuples_for_n,
                                             distribution=return_distribution_for_n,
                                             variable_name=return_enumdistributiontuples_with_value_secret)

    return return_distribution_for_n


@pytest.fixture
def distribution_model_init_for_public(return_all_the_tuples_for_public,
                                       return_distribution_for_public,
                                       return_enumdistributiontuples_with_value_public):
    """ Generate an instance of Distribution """
    return_distribution_for_public = Distribution(dist_tuples=return_all_the_tuples_for_public,
                                                  distribution=return_distribution_for_public,
                                                  variable_name=return_enumdistributiontuples_with_value_public)

    return return_distribution_for_public


@pytest.fixture
def distribution_model_init_for_secret(return_all_the_tuples_for_secret_as_bits,
                                       return_distribution_for_secret_for_indiscernability,
                                       return_enumdistributiontuples_with_value_secret):
    """ Generate an instance of Distribution """
    return_distribution_for_secret = Distribution(dist_tuples=return_all_the_tuples_for_secret_as_bits,
                                                  distribution=return_distribution_for_secret_for_indiscernability,
                                                  variable_name=return_enumdistributiontuples_with_value_secret)

    return return_distribution_for_secret


@pytest.fixture
def distribution_model_init_for_leakage(return_all_the_tuples_for_indiscernability,
                                        return_distribution_for_leakage,
                                        return_enumdistributiontuples_with_value_leakage):
    """ Generate an instance of Distribution """
    return_distribution_for_leakage = Distribution(dist_tuples=return_all_the_tuples_for_indiscernability,
                                                   distribution=return_distribution_for_leakage,
                                                   variable_name=return_enumdistributiontuples_with_value_leakage)

    return return_distribution_for_leakage


@pytest.fixture
def distribution_model_init_for_public_leakage(return_all_the_tuples_for_public_leakage,
                                               return_distribution_for_public_leakage,
                                               return_enumdistributiontuples_with_value_publicleakage):
    """ Generate an instance of Distribution for variables public and leakage"""
    return_distribution_for_public_leakage = Distribution(
        dist_tuples=return_all_the_tuples_for_public_leakage,
        distribution=return_distribution_for_public_leakage,
        variable_name=return_enumdistributiontuples_with_value_publicleakage)

    return return_distribution_for_public_leakage


@pytest.fixture
def distribution_model_init_for_secret_public(return_all_the_tuples_for_secret_public_as_bits,
                                              return_distribution_for_secret_public,
                                              return_enumdistributiontuples_with_value_secretpublic):
    """ Generate an instance of Distribution for variables secret and public"""
    return_distribution_for_secret_public = Distribution(
        dist_tuples=return_all_the_tuples_for_secret_public_as_bits,
        distribution=return_distribution_for_secret_public,
        variable_name=return_enumdistributiontuples_with_value_secretpublic)

    return return_distribution_for_secret_public


@pytest.fixture
def distribution_container_model_init(distribution_model_init_for_secret,
                                      distribution_model_init_for_public,
                                      distribution_model_init_for_leakage,
                                      distribution_model_init_for_public_leakage,
                                      distribution_model_init_for_secret_public):
    """ Generate an instance of DistributionContainer """
    dist_container = DistributionContainer(secret=distribution_model_init_for_secret,
                                           public=distribution_model_init_for_public,
                                           leakage=distribution_model_init_for_leakage,
                                           public_leakage=distribution_model_init_for_public_leakage,
                                           secret_public=distribution_model_init_for_secret_public)

    return dist_container


@pytest.fixture
def distribution_tuples_model_init_for_leakage(return_all_the_tuples_for_indiscernability,
                                               return_sorted_tuples_as_dict_for_leakage_indiscernability,
                                               return_tuples_as_list__no_duplicates_indiscernability,
                                               return_related_secret_tuples_as_int_indiscernability):
    """ Generate an instance of DistributionTuples for variable leakage """
    returned_distribution_tuples_for_leakage = DistributionTuples(
        all_the_tuples=return_all_the_tuples_for_indiscernability,
        tuples_as_dict=return_sorted_tuples_as_dict_for_leakage_indiscernability,
        tuples_as_list_no_duplicate=return_tuples_as_list__no_duplicates_indiscernability,
        related_secret_tuples=return_related_secret_tuples_as_int_indiscernability
    )

    return returned_distribution_tuples_for_leakage


# ---------------------------------------------------------------------------------
# Fixtures for the BinomialDistribution Objects - BinomialDistribution
# ---------------------------------------------------------------------------------
@pytest.fixture
def binomial_distribution_model_init_nb_tuples_equals_1000(return_nb_tuples_1000,
                                                           return_distribution_type_secret_public):
    """ Generate an instance of BinomialDistribution for nb_tuples equal 1000 """
    binomial_distribution = BinomialDistribution(nb_tuples=return_nb_tuples_1000,
                                                 distribution_type=return_distribution_type_secret_public,
                                                 shuffle_function=return_numpy_random_shuffle)

    return binomial_distribution


@pytest.fixture
def binomial_distribution_model_init_nb_tuples_equals_4(return_nb_tuples_4,
                                                        return_distribution_type_secret_public,
                                                        return_numpy_random_shuffle):
    """ Generate an instance of BinomialDistribution for nb_tuples equal 4"""
    binomial_distribution = BinomialDistribution(nb_tuples=return_nb_tuples_4,
                                                 distribution_type=return_distribution_type_secret_public,
                                                 shuffle_function=return_numpy_random_shuffle)

    return binomial_distribution


@pytest.fixture
def binomial_distribution_model_init_nb_tuples_equals_200(return_nb_tuples_200,
                                                          return_distribution_type_secret_public,
                                                          return_numpy_random_shuffle):
    """ Generate an instance of BinomialDistribution for nb_tuples equal 200"""
    binomial_distribution = BinomialDistribution(nb_tuples=return_nb_tuples_200,
                                                 distribution_type=return_distribution_type_secret_public,
                                                 shuffle_function=return_numpy_random_shuffle)

    return binomial_distribution


@pytest.fixture
def binomial_distribution_model_init_nb_tuples_equals_0(return_nb_tuples_0,
                                                        return_distribution_type_secret_public,
                                                        return_numpy_random_shuffle):
    """ Generate an instance of BinomialDistribution  for nb_tuples equal 0"""
    binomial_distribution = BinomialDistribution(nb_tuples=return_nb_tuples_0,
                                                 distribution_type=return_distribution_type_secret_public,
                                                 shuffle_function=return_numpy_random_shuffle)

    return binomial_distribution


@pytest.fixture
def binomial_distribution_model_init_nb_tuples_equals_1(return_nb_tuples_1,
                                                        return_distribution_type_secret_public,
                                                        return_numpy_random_shuffle):
    """ Generate an instance of BinomialDistribution  for nb_tuples equal 1"""
    binomial_distribution = BinomialDistribution(nb_tuples=return_nb_tuples_1,
                                                 distribution_type=return_distribution_type_secret_public,
                                                 shuffle_function=return_numpy_random_shuffle)

    return binomial_distribution


# ---------------------------------------------------------------------------------
# Fixtures for the Storage and Repository Objects
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_json_file_storage(json_generated_binomial_distribution_file_init):
    """ Return a JSONFileStorage with value json_generated_binomial_distribution_file_init """
    json_file_storage = JSONFileStorage(name=json_generated_binomial_distribution_file_init)

    return json_file_storage


@pytest.fixture
def return_json_distribution_store_repository():
    """ Return a JSONDistributionStoreRepository class """
    return JSONDistributionStoreRepository
