"""
    Module defining the Repository used to extract a DataFrame out of a HDF5 file
"""
from pathlib import Path

from pandas import HDFStore
from pandas import DataFrame

import sqcommon.helper.hdf5file_helper as helper

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.interface.domain.Data.dataframe_interface import DataFrameInterface
from sqcommon.interface.adapter.Data.data_repository_interface import DataRepositoryInterface


class HDF5DataAsDataFrameGetRepository(DataRepositoryInterface):
    """
        HDF5DataAsDataFrameGetRepository class defines the Repository to get data as a DataFrame, from a HDF5 file
    """
    @staticmethod
    def find(input_path: Path) -> DataFrameInterface:
        """
            Create a DataFrame from a HDF5 file

            Parameters
            ----------
            input_path : Path
                the hdf5 file containing the data

            Returns
            -------
            data_df: DataAsDataFrame
                DataFrame containing the data
        """

        # Get the keys from the file
        keys = helper.HDF5FileHelper.get_hdf5_keys_as_list(input_path)

        # Get a storage for a Pandas object
        try:
            with HDFStore(str(input_path), 'r') as store:

                # Generate a DataFrame for the current key
                df = DataFrame(store[keys[0]])
        except FileNotFoundError as e:
            print(e.errno)

        # Encapsulate the DataFrame into an application object
        data_df = DataAsDataFrame(df)

        # Return the DataAsDataFrame object
        return data_df

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
