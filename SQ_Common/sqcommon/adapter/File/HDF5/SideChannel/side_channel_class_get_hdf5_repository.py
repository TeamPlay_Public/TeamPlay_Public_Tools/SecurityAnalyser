"""
    Module defining the Repository used to extract the Side Channel Class from a HDF5 file
"""
from pathlib import Path

import sqcommon.helper.hdf5file_helper as helper

from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.interface.domain.SideChannel.side_channel_class_interface import SideChannelClassInterface

from sqcommon.interface.adapter.SideChannel.side_channel_class_repository_interface import \
    SideChannelClassRepositoryInterface
from sqcommon.enums.enum_side_channel_class import SideChannelClasses


class SideChannelClassGetHDF5Repository(SideChannelClassRepositoryInterface):
    """
        SideChannelClassGetHDF5Repository class defines the Repository to get the Side Channel Class from a HDF5 file
    """
    @staticmethod
    def find(input_path: Path) -> SideChannelClassInterface:
        """
            Find the side channel class associated with the HDF5 file

            Parameters
            ----------
            input_path : Path
                the HDF5 file containing the data

            Returns
            -------
            SideChannelClass
                SideChannelClass containing the side channel class associated with the HDF5 file
        """
        keys = helper.HDF5FileHelper.get_hdf5_keys_as_list(input_path)

        return SideChannelClass(SideChannelClasses(keys[0]).name, keys[0])

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
