"""
    Module defining the Repository used to return the cardinality of the variables
"""
from pathlib import Path

import sqcommon.helper.hdf5file_helper as helper

from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.interface.adapter.Variable.vars_cardinality_repository_interface import \
    VariablesCardinalityRepositoryInterface


class VariablesCardinalityHDF5Repository(VariablesCardinalityRepositoryInterface):
    """
        VariablesCardinalityRepository class returns the cardinality of the variables

    """

    @staticmethod
    def find_all(input_file: Path,
                 dataset: str,
                 hdf5_attributes: VariablesList) -> VariablesList:
        """
            Gather the cardinality of each variable in var_names

            Parameters
            ----------
            input_file : Path
                The HDF5 file
            hdf5_attributes: VariablesList
                the attributes to search
            dataset: str
                dataset to inspect

            Returns
            -------
            VariablesList
                the list of Variables with their cardinality
         """
        for hdf5_attribute in hdf5_attributes.lst:
            hdf5_attribute.cardinality = helper.HDF5FileHelper.count_variables(hdf5_file=input_file,
                                                                               dataset=dataset,
                                                                               hdf5_attribute=str(hdf5_attribute.value))
        return hdf5_attributes

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
