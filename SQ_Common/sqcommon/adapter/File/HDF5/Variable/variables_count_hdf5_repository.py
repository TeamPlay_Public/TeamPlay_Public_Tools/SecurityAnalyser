"""
    Module defining the Repository used to count the number of Variables from a HDF5 file
"""
from pathlib import Path

import sqcommon.helper.hdf5file_helper as helper

from sqcommon.interface.adapter.Variable.vars_count_repository_interface \
    import VariablesCountRepositoryInterface


class VariablesCountHDF5Repository(VariablesCountRepositoryInterface):
    """
        VariablesCountHDF5Repository class defines the Repository to count the number of variables of type var_type
        from a HDF5 file

    """
    @staticmethod
    def count(input_path: Path,
              dataset: str,
              hdf5_attribute: str) -> int:
        """
            Count the number of variables of type var_type (tainted or untainted, recorded as a hdf5 attribute)
            existing in the dataset side_channel of the HDF5 file input_path

            Parameters
            ----------
            input_path : Path
                the HDF5 file containing the data
            dataset: str
                the dataset to search in the HDF5 file.
            hdf5_attribute: str
                the attribute to search in the HDF5 file.

            Returns
            -------
            int
                the number of variables related to the hdf5 attribute retrieved in the dataset dataset of the HDF5 file
         """
        return helper.HDF5FileHelper.count_variables(hdf5_file=input_path,
                                                     dataset=dataset,
                                                     hdf5_attribute=hdf5_attribute)

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
