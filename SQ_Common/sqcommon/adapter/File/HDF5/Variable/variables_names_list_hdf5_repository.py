"""
    Module defining the Repository used to list the Variables names from a HDF5 file
"""
from pathlib import Path

import sqcommon.helper.hdf5file_helper as helper

from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.interface.adapter.Variable.vars_name_list_repository_interface import \
    VariablesNamesListRepositoryInterface


class VariablesNamesHDF5Repository(VariablesNamesListRepositoryInterface):
    """
        VariablesNamesHDF5Repository class defines the Repository to list the variables of type var_type
        from a HDF5 file

    """
    @staticmethod
    def find_all(input_path: Path) -> VariablesList:
        """
            List the variables found in the HDF5 file

            Parameters
            ----------
            input_path : Path
                the HDF5 file containing the data

            Returns
            -------
            VariablesList
                the list of variables found in the HDF5 file
         """
        return helper.HDF5FileHelper.get_variable_names_as_list(hdf5_file=input_path)

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
