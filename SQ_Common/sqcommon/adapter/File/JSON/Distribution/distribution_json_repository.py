"""
    Module defining the DistributionFileRepository
"""
import errno
import os
from pathlib import Path

import json

from sqcommon.adapter.distribution_file_repository import DistributionFileRepository
from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict
from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class DistributionKeyException(Error):
    """
        Exception raised in case the distribution does not contain the correct key secret_public.

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message


class DistributionJSONFileRepository(DistributionFileRepository):
    """
        DistributionFileRepository class defines the Repository to read user data from a JSON file
        and returns it as a Dict
    """

    @staticmethod
    def find_by_path(input_path: Path) -> DistributionAsDict:
        """
            Extract distribution data from a user-provided JSON file
            and return it as a DistributionAsDict

            Parameters
            ----------
            input_path : Path
                the JSON file containing the distribution data.
                It must contain a secret_public entry

            Returns
            -------
            distribution_data: DistributionAsDict
                A container holding the distribution data
        """

        try:
            with open(input_path) as f:
                distribution_dict = json.load(f)
        except FileNotFoundError:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), f)

        if EnumDistributionTuples.SECRET_PUBLIC.value not in distribution_dict:
            raise DistributionKeyException("The key secret_public does not appear in the distribution",
                                           "Provide a distribution file with key secret_public")
        return DistributionAsDict(data=distribution_dict)

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
