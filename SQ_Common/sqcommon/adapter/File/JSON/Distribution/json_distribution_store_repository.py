"""
    Module defining the Repository used to store the generated distribution
    using the binomial distribution-based algorithm into a JSON file
"""
import json
from pathlib import Path

from sqcommon.domain.Output.JSONFile.json_file_storage import JSONFileStorage

from sqcommon.interface.adapter.Storage.distribution_store_repository_interface \
    import DistributionStoreRepositoryInterface
from sqcommon.interface.domain.BinomialDistribution.distribution_as_dict_interface import DictInterface


class JSONDistributionStoreRepository(DistributionStoreRepositoryInterface):
    """
        JSONDistributionStoreRepository class defines the Repository
        to store the generated distribution into a JSON file
    """

    @staticmethod
    def store(distribution_as_dict: DictInterface,
              storage: JSONFileStorage):
        """
            Store the generated distribution into a JSON file

            Parameters
            ----------
            distribution_as_dict: DictInterface
                the generated distribution to store
            storage: JSONFileStorage
                the data needed to store the output of the program, here the path to the JSON file
         """
        json_path = Path(storage.name)
        # will create file, if it exists will do nothing
        json_path.touch()

        # Generates the JSON file from the given distribution dictionary
        try:
            with open(json_path, "w") as jsonfile:
                json.dump(distribution_as_dict.data, jsonfile)
        except FileNotFoundError as e:
            print(e.errno)
