"""
    Module defining the DistributionFileRepository
"""

import abc
from pathlib import Path

from sqcommon.domain.Distribution.distribution_container import DistributionContainer
from sqcommon.interface.adapter.Distribution.distribution_repository_interface import \
    DistributionRepositoryInterface


class DistributionFileRepository(DistributionRepositoryInterface):
    """ DistributionFileRepository class defines the generic Repository
        to read distribution data from a file
     """

    @staticmethod
    @abc.abstractmethod
    def find_by_path(input_path: Path) -> DistributionContainer:
        """
            find_by_path(...) reads the data from a file
            and returns a DistributionContainer holding the read distribution data
        """

        pass

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
