"""
    Module defining the Binomial Distribution domain that computes the binomial_distribution algorithm
"""
from typing import List

from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples
from sqcommon.exception.list_length_exception import ListLengthException
from sqcommon.exception.nb_tuples_lower_or_equal_to_one_exception import NbTuplesLowerOrEqualToOneException
from sqcommon.interface.domain.Shuffle.shuffle_interface import ShuffleInterface


class BinomialDistribution(object):
    """
        BinomialDistribution class holds the computed and shuffled binomial distribution

        Attribute
        ----------
        nb_tuples: int
            the number of elements the generated distribution will be made of.
            This number matches the number of tuples in the source file,
            ie the Security Exchange file with the public, secret and leakage tuples
        distribution_type: EnumDistributionTuples
            The type of distribution to generate
    """

    def __init__(
        self,
        nb_tuples: int,
        distribution_type: EnumDistributionTuples,
        shuffle_function: ShuffleInterface
    ):
        """ Constructor for the BinomialDistribution class """
        self._nb_tuples = nb_tuples
        self._distribution_type = distribution_type
        self._shuffle_function = shuffle_function
        self._original_binomial_list = []
        self._shuffled_binomial_list = []
        self._final_binomial_distribution_list = self.compute_shuffled_binomial_distribution()
        self._data = self.convert_to_dict()


    def compute_shuffled_binomial_distribution(self):
        """
            Generates the binomial distribution and averages/shuffles it.

        :return:
            A shuffled, averaged binomial distribution list
        """
        # Compute the binomial distribution with self._nb_tuples values
        binomial_distribution_list = self.compute_binomial_distribution()

        # Shuffle binomial_distribution_list and get in return both lists
        self._original_binomial_list, self._shuffled_binomial_list = self._shuffle_function(
            list_to_shuffle=binomial_distribution_list).shuffle()

        return self.average_distribution(self._original_binomial_list, self._shuffled_binomial_list)

    def compute_binomial_distribution(self):
        """
            Given the nb_tuples, the compute() method will generate a distribution with nb_tuples elements,
            following the binomial distribution algorithm

            :return
                A list of int representing the binomial distribution with nb_tuples elements
        """

        initial_list = [1]

        if self._nb_tuples <= 1:
            raise NbTuplesLowerOrEqualToOneException(
                expression="The parameter nb_tuples for class BinomialDistribution "
                           "must be higher than 1",
                message='"The given nb_tuples parameter is equal to %d", '
                        'self._nb_tuples')
        for _ in range(1, self._nb_tuples):
            # Append a zero to the end of list
            list1 = initial_list + [0]

            # Append a zero to the beginning of list
            list2 = [0] + initial_list
            # Add the two lists elements
            initial_list = self._add_list(list1, list2)

        # Turns the list into a list of probability
        # by dividing each element of initial_list by 2^nb_tuples
        return list(map(lambda x: x / (2 ** (self._nb_tuples - 1)), initial_list))

    def average_distribution(self,
                             binomial_list: List[float],
                             shuffled_binomial_list: List[float]):
        """
        Compute the average for each element in the two lists
        :param binomial_list: List containing the binomial distribution
        :param shuffled_binomial_list: List containing the shuffled binomial distribution

        :return:
            A list of same size containing the average between each element of the two lists
        """
        if len(binomial_list) != len(shuffled_binomial_list):
            raise ListLengthException(
                expression="The two lists given in parameter of the function average_distribution must have the "
                           "same length",
                message='"list1 length= %d , list2 length= %d", len(list1), len(list2)')

        averaged_dist_list = [self.average(x, y) for x, y in zip(binomial_list, shuffled_binomial_list)]

        if len(binomial_list) != len(averaged_dist_list):
            raise ListLengthException(
                expression="The returned list by method average_distribution must have the "
                           "same length as the lists given in parameter",
                message='"binomial_list length= %d , averaged_list= %d", len(list1), len(list2)')

        return averaged_dist_list

    @staticmethod
    def average(value1: float,
                value2: float):
        """
        Compute the average between two floats
        :param value1:
        :param value2:

        :return:
            The average between value1 and value2
        """
        return (value1 + value2) / 2

    def convert_to_dict(self):
        """
            Embed the distribution into a Dictionary with key equal to distribution_type.value
        """
        # Turn the list of distribution into a Dict with key equal to distribution_type
        return {self._distribution_type.value: self._final_binomial_distribution_list}

    @staticmethod
    def _add_list(list1: List[int],
                  list2: List[int]):
        """
        Element_wise addition of two lists

            :param list1: A list of int
            :param list2: A list of int

            :return:
                A list where elements from list1 and list2 are added up.
        """
        if len(list1) != len(list2):
            raise ListLengthException(
                expression="The two lists given in parameter of the function add_list must have the "
                           "same length",
                message='"list1 length= %d , list2 length= %d", len(list1), len(list2)')

        return [a + b for a, b in zip(list1, list2)]

    @property
    def nb_tuples(self) -> List[float]:
        """ Getter for the nb_tuples attribute """
        return self._nb_tuples

    @nb_tuples.setter
    def nb_tuples(self, nb_tuples):
        """ Setter for the nb_tuples """
        self._nb_tuples = nb_tuples

    @property
    def original_binomial_list(self) -> List[float]:
        """ Getter for the original_binomial_list attribute """
        return self._original_binomial_list

    @original_binomial_list.setter
    def original_binomial_list(self, original_binomial_list):
        """ Setter for the original_binomial_list """
        self._original_binomial_list = original_binomial_list

    @property
    def shuffled_binomial_list(self) -> List[float]:
        """ Getter for the shuffled_binomial_list attribute """
        return self._shuffled_binomial_list

    @shuffled_binomial_list.setter
    def shuffled_binomial_list(self, shuffled_binomial_list):
        """ Setter for the shuffled_binomial_list """
        self._shuffled_binomial_list = shuffled_binomial_list

    @property
    def final_binomial_distribution_list(self) -> List[float]:
        """ Getter for the final_binomial_distribution_list attribute """
        return self._final_binomial_distribution_list

    @final_binomial_distribution_list.setter
    def final_binomial_distribution_list(self, final_binomial_distribution_list):
        """ Setter for the final_binomial_distribution_list """
        self._final_binomial_distribution_list = final_binomial_distribution_list

    @property
    def data(self):
        """ Getter for the data attribute """
        return self._data

    @data.setter
    def data(self, data):
        """ Setter for the data attribute """
        self._data = data
