""" DataAsDataFrame module specifies an implementation of DataInterface
    in which data is stored in a DataFrame
"""
from pandas import DataFrame

from sqcommon.interface.domain.Data.dataframe_interface import DataFrameInterface


class DataAsDataFrame(DataFrameInterface):
    """ DataInterface class specifies encapsulates a Panda Dataframe"""

    def __init__(
            self,
            data: DataFrame
    ):
        """ Constructor for the DataInterface class """
        self._data = data

    @property
    def data(self) -> DataFrame:
        """ Getter for the data attribute """
        return self._data

    @data.setter
    def data(self, data):
        """ Setter for the data attribute """
        self._data = data

    def __str__(self):
        return self.__class__.__name__
