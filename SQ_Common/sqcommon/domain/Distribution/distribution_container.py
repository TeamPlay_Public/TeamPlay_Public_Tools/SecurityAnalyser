""" DistributionContainer module specifies the DistributionContainer class """
from typing import List

from sqcommon.domain.Distribution.distribution_for_tuples import Distribution
from sqcommon.domain.Distribution.distribution_tuples import DistributionTuples
from sqcommon.interface.domain.Distribution.distribution_container_interface \
    import DistributionContainerInterface


class DistributionContainer(DistributionContainerInterface):
    """
        DistributionContainer class specifies Distribution attributes and methods.
        It is a container holding the values of the different Distribution
        needed to compute the Mutual Information-based security metric, namely indiscernibility level
     """

    def __init__(
            self,
            secret: Distribution,
            public: Distribution,
            leakage: Distribution,
            public_leakage: Distribution,
            secret_public: Distribution
    ):
        """ Constructor for the Distribution class """
        self._secret = secret
        self._public = public
        self._leakage = leakage
        self._public_leakage = public_leakage
        self._secret_public = secret_public

    # Constructor attributes
    @property
    def secret(self) -> Distribution:
        """ Getter for the secret attribute """
        return self._secret

    @secret.setter
    def secret(self, secret):
        """ Setter for the secret attribute """
        self._secret = secret

    @property
    def public(self) -> Distribution:
        """ Getter for the public attribute """
        return self._public

    @public.setter
    def public(self, public):
        """ Setter for the public attribute """
        self._public = public

    @property
    def leakage(self) -> Distribution:
        """ Getter for the leakage attribute """
        return self._leakage

    @leakage.setter
    def leakage(self, leakage):
        """ Setter for the leakage attribute """
        self._leakage = leakage

    @property
    def public_leakage(self) -> Distribution:
        """ Getter for the public_leakage attribute """
        return self._public_leakage

    @public_leakage.setter
    def public_leakage(self, public_leakage):
        """ Setter for the public_leakage attribute """
        self._public_leakage = public_leakage

    @property
    def secret_public(self) -> Distribution:
        """ Getter for the secret_public attribute """
        return self._secret_public

    @secret_public.setter
    def secret_public(self, secret_public):
        """ Setter for the secret_public attribute """
        self._secret_public = secret_public

    # tuples attributes
    @property
    def secret_tuples(self) -> DistributionTuples:
        """ Getter for the gathered_tuples of the secret attribute """
        return self._secret.dist_tuples

    @property
    def public_tuples(self) -> DistributionTuples:
        """ Getter for the gathered_tuples of the public attribute """
        return self._public.dist_tuples

    @property
    def leakage_tuples(self) -> DistributionTuples:
        """ Getter for the gathered_tuples of the leakage attribute """
        return self._leakage.dist_tuples

    @property
    def public_leakage_tuples(self) -> DistributionTuples:
        """ Getter for the gathered_tuples of the public_leakage attribute """
        return self._public_leakage.dist_tuples

    @property
    def secret_public_tuples(self) -> DistributionTuples:
        """ Getter for the gathered_tuples of the secret_public attribute """
        return self._secret_public.dist_tuples

    # distribution attributes
    @property
    def secret_distribution(self) -> List[float]:
        """ Getter for the distribution of the secret attribute """
        return self._secret.distribution

    @property
    def public_distribution(self) -> List[float]:
        """ Getter for the distribution of the public attribute """
        return self._public.distribution

    @property
    def leakage_distribution(self) -> List[float]:
        """ Getter for the distribution of the leakage attribute """
        return self._leakage.distribution

    @property
    def public_leakage_distribution(self) -> List[float]:
        """ Getter for the distribution of the public_leakage attribute """
        return self._public_leakage.distribution

    @property
    def secret_public_distribution(self) -> List[float]:
        """ Getter for the distribution of the secret_public attribute """
        return self._secret_public.distribution
