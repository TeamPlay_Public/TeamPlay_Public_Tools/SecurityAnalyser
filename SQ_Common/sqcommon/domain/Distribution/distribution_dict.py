""" Distribution module specifies the DistributionAsDict class """
from typing import Dict

from math import isclose

from sqcommon.interface.domain.Distribution.distribution_dict_interface import DistributionDictInterface


class DistributionAsDict(DistributionDictInterface):
    """
        DistributionAsDict class contains a dictionary storing the distribution extracted from the JSON file
     """

    def __init__(
        self,
        data: Dict
    ):
        """ Constructor for the DistributionAsDict class """
        self._data = data
        if data:
            self._check_distribution_integrity()

    @property
    def data(self) -> Dict:
        """ Getter for the data attribute """
        return self._data

    def _check_distribution_integrity(self):
        """
            Check that the sum of the given probability is equal to 1
        """
        for distribution in list(self._data.values()):
            if not isclose(sum(distribution), 1, abs_tol=10 ** -5):
                raise ValueError("The sum of given probabilities should be equal to 1")
