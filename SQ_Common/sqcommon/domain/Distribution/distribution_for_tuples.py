""" Distribution module specifies the Distribution class """
from typing import List

from sqcommon.domain.Distribution.distribution_tuples import DistributionTuples
from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples


class Distribution:
    """
        Distribution class specifies Distribution attributes and methods.
        It contains :
            the name of the variable: EnumDistributionTuples
            a list to hold the different tuples of type DistributionTuples: DistributionTuples
            a list of their distribution: List[float]
     """

    def __init__(
            self,
            variable_name: EnumDistributionTuples,
            dist_tuples: DistributionTuples,
            distribution: List[float]
    ):
        """ Constructor for the Distribution class """
        self._variable_name = variable_name
        self._dist_tuples = dist_tuples
        self._distribution = distribution

    @property
    def variable_name(self) -> EnumDistributionTuples:
        """ Getter for the variable_name attribute """
        return self._variable_name

    @property
    def dist_tuples(self) -> DistributionTuples:
        """ Getter for the dist_tuples attribute """
        return self._dist_tuples

    @property
    def distribution(self) -> List[float]:
        """ Getter for the distribution attribute """
        return self._distribution
