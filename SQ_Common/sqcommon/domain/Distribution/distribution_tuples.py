""" DistributionTuples module specifies the DistributionTuples class """
from typing import List, Dict


class DistributionTuples:
    """
        DistributionTuples class specifies DistributionTuples attributes and methods.

        all_the_tuples: List[float]
            list of all the tuples of a certain type among EnumDistributionTuples

        tuples_as_dict: Dict[str, int]
            Dictionary of the tuples (without duplicates) and their relative number of occurrences

        tuples_as_list_no_duplicate: List
            List of the keys of the dictionary tuples_as_dict, so without duplicates

        related_secret_tuples: List[float]
            List of the secret variables related to the leakage.
            Warning: used only for Leakage type of tuples, empty otherwise.

     """

    def __init__(
            self,
            all_the_tuples: List[float],
            tuples_as_dict: Dict[str, int],
            tuples_as_list_no_duplicate: List,
            related_secret_tuples: List[float],
    ):
        """ Constructor for the Distribution class """
        self._all_the_tuples = all_the_tuples
        self._tuples_as_dict = tuples_as_dict
        self._tuples_as_list_no_duplicate = tuples_as_list_no_duplicate
        self._related_secret_tuples = related_secret_tuples

    @property
    def all_the_tuples(self) -> List[float]:
        """ Getter for the all_the_tuples attribute """
        return self._all_the_tuples

    @all_the_tuples.setter
    def all_the_tuples(self,
                       all_the_tuples: List[float]):
        """ Getter for the all_the_tuples attribute """
        self._all_the_tuples = all_the_tuples

    @property
    def tuples_as_dict(self) -> Dict[str, int]:
        """ Getter for the tuples_as_dict attribute """
        return self._tuples_as_dict

    @tuples_as_dict.setter
    def tuples_as_dict(self,
                       tuples_as_dict: Dict[str, int]):
        """ Getter for the tuples_as_dict attribute """
        self._tuples_as_dict = tuples_as_dict

    @property
    def tuples_as_list_no_duplicate(self) -> List:
        """ Getter for the tuples_as_list_no_duplicate attribute """
        return self._tuples_as_list_no_duplicate

    @tuples_as_list_no_duplicate.setter
    def tuples_as_list_no_duplicate(self,
                                    tuples_as_list_no_duplicate: List):
        """ Getter for the tuples_as_list_no_duplicate attribute """
        self._tuples_as_list_no_duplicate = tuples_as_list_no_duplicate

    @property
    def related_secret_tuples(self) -> List[float]:
        """ Getter for the related_secret_tuples attribute """
        return self._related_secret_tuples

    @related_secret_tuples.setter
    def related_secret_tuples(self,
                              related_secret_tuples: List[float]):
        """ Getter for the related_secret_tuples attribute """
        self._related_secret_tuples = related_secret_tuples
