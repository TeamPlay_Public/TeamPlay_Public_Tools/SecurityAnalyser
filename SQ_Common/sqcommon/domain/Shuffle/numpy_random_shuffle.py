"""
    Noise module specifies an implementation of the RandomInterface
    for the built-in random function random.sample(..) https://docs.python.org/3/library/random.html
    used to return a new shuffled list of the given parameter
"""
import copy
from typing import List

import numpy.random as nr

from sqcommon.interface.domain.Shuffle.shuffle_interface import ShuffleInterface


class NumpyRandomShuffle(ShuffleInterface):
    """
        RandomShuffle class uses the function random.shuffle()
        to shuffle the order of a list given in parameter

        Attribute
        ----------
        list_to_shuffle: List
            The list we want to randomly shuffle its elements
    """

    def __init__(
        self,
        list_to_shuffle: List[float]
    ):
        """ Constructor for the RandomShuffle class """
        self._list_to_shuffle = list_to_shuffle

    def shuffle(self) -> [List[float], List[float]]:
        """
            Implements shuffle() from the interface with the selected function, numpy.random.shuffle(...)
            https://numpy.org/doc/stable/reference/random/generated/numpy.random.shuffle.html

            Returns a list containing :
            - The original list
            - The shuffled list
        """
        original_list = copy.deepcopy(self._list_to_shuffle)

        nr.shuffle(self._list_to_shuffle)

        return [original_list, self._list_to_shuffle]

    @property
    def list_to_shuffle(self) -> List[float]:
        """ Getter for the list_to_shuffle attribute """
        return self._list_to_shuffle

    @list_to_shuffle.setter
    def list_to_shuffle(self, list_to_shuffle):
        """ Setter for the list_to_shuffle attribute """
        self._list_to_shuffle = list_to_shuffle
