""" SideChannelClass module specifies the Side Channel Class class """

from sqcommon.interface.domain.SideChannel.side_channel_class_interface import SideChannelClassInterface


class SideChannelClass(SideChannelClassInterface):
    """ SideChannelClass class specifies Side Channel Class attributes and methods """

    def __init__(
            self,
            name: str,
            value: str
    ):
        """ Constructor for the SideChannelClass class """
        self._name = name
        self._value = value

    @property
    def name(self):
        """ Getter for the name attribute """
        return self._name

    @name.setter
    def name(self, name):
        """ Setter for the name attribute """
        self._name = name

    @property
    def value(self):
        """ Getter for the value attribute """
        return self._value

    @value.setter
    def value(self, value):
        """ Setter for the value attribute """
        self._value = value
