""" TuplesInformation module specifies the TuplesInformation class """
from sqcommon.interface.domain.Tuples.tuples_info_interface import TuplesInformationInterface


class TuplesInformation(TuplesInformationInterface):
    """
        TuplesInformation class specifies TuplesInformation attributes and methods.
        This class is a container gathering information about the tuples from the Security Exchange file
    """

    def __init__(
        self,
        nb_secret_tuples: int,
        nb_public_tuples: int,
        total_nb_tuples: int
    ):
        """ Constructor for the Variable class """
        self._nb_secret_tuples = nb_secret_tuples
        self._nb_public_tuples = nb_public_tuples
        self._total_nb_tuples = total_nb_tuples

    @property
    def nb_secret_tuples(self) -> int:
        """ Getter for the nb_secret_tuples attribute """
        return self._nb_secret_tuples

    @nb_secret_tuples.setter
    def nb_secret_tuples(self, nb_secret_tuples):
        """ Setter for the nb_secret_tuples attribute """
        self._nb_secret_tuples = nb_secret_tuples

    @property
    def nb_public_tuples(self) -> int:
        """ Getter for the nb_public_tuples attribute """
        return self._nb_public_tuples

    @nb_public_tuples.setter
    def nb_public_tuples(self, nb_public_tuples):
        """ Setter for the nb_public_tuples attribute """
        self._nb_public_tuples = nb_public_tuples

    @property
    def total_nb_tuples(self) -> int:
        """ Getter for the total_nb_tuples attribute """
        return self._total_nb_tuples

    @total_nb_tuples.setter
    def total_nb_tuples(self, total_nb_tuples):
        """ Setter for the total_nb_tuples attribute """
        self._total_nb_tuples = total_nb_tuples
