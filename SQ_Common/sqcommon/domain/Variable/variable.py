""" Variable module specifies the Variable class """
from sqcommon.interface.domain.Variable.variable_interface import VariableInterface


class Variable(VariableInterface):
    """ Variable class specifies Variable attributes and methods """

    def __init__(
            self,
            value: str,
            cardinality: int
    ):
        """ Constructor for the Variable class """
        self._value = value
        self._cardinality = cardinality

    @property
    def cardinality(self):
        """ Getter for the cardinality attribute """
        return self._cardinality

    @cardinality.setter
    def cardinality(self, cardinality):
        """ Setter for the cardinality attribute """
        self._cardinality = cardinality

    @property
    def value(self):
        """ Getter for the value attribute """
        return self._value

    @value.setter
    def value(self, value):
        """ Setter for the value attribute """
        self._value = value
