""" VariablesList module specifies a list of VariableInterface """

from sqcommon.interface.domain.Variable.variables_list_interface import VariablesListInterface


class VariablesList(VariablesListInterface):
    """ VariablesList class specifies attributes and methods for a list of VariableInterface """

    def __init__(self):
        """
            Constructor for the VariablesList class
        """
        self._lst = list()

    @property
    def lst(self):
        """ Getter for the lst attribute """
        return self._lst

    @lst.setter
    def lst(self, lst):
        """ Setter for the lst attribute """
        self._lst = lst

    def nb_vars(self):
        """ Returns the length of the lst attribute, ie the number of variables """
        return len(self._lst)
