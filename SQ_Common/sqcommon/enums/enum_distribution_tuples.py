""" EnumDistributionTuples class enumerates the different tuples involved in the Indiscernability Metric  """
from enum import Enum


class EnumDistributionTuples(Enum):
    """
        EnumDistributionTuples class defines the tuples involved in the Indiscernability Metric .
        For each tuple a name and a value is provided.
    """
    SECRET = 'secret'
    PUBLIC = 'public'
    LEAKAGE = 'leakage'
    PUBLIC_LEAKAGE = 'public_leakage'
    SECRET_PUBLIC = 'secret_public'
