""" HDF5Attributes class enumerates the different attributes used in a HDF5 structure """
from enum import Enum


class HDF5Attributes(Enum):
    """ HDF5Attributes class defines the attributes used in a HDF5 structure """
    NB_TAINTED = 'nbTainted'
    NB_UNTAINTED = 'nbUntainted'
    VARIABLE_A = "a"
    VARIABLE_N = "n"
    VARIABLE_SECRET = "secret"
