""" SideChannelClasses class enumerates the different classes of side channel attack """
from enum import Enum


class SideChannelClasses(Enum):
    """
        SideChannelClasses class defines the classes of side channel attack.
        For each class a name and a value is provided.
        For each class are found the different type of measurements (worst or average)
    """
    TIME = 'time'
    TIME_WORST = 'time_worst'
    TIME_AVG = 'time_avg'
    TIME_RATIO = 'time_ratio'
    ENERGY = 'energy'
    ENERGY_WORST = 'energy_worst'
    ENERGY_AVG = 'energy_avg'
    ENERGY_RATIO = 'energy_ratio'
    POWER = 'power'
    POWER_WORST = 'power_worst'
    POWER_AVG = 'power_avg'
    POWER_RATIO = 'power_ratio'
