""" VariableTypes class enumerates the different types of variables """
from enum import Enum


class VariableTypes(Enum):
    """ VariableTypes class defines the types of variables"""
    TAINTED = 'tainted'
    UNTAINTED = 'untainted'
