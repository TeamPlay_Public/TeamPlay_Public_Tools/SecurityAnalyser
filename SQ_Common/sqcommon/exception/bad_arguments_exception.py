"""
    Erroneous arguments exception
"""
from sqcommon.exception.error import Error


class BadArgumentException(Error):
    """Exception raised in case of erroneous arguments in the configuration file.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message
