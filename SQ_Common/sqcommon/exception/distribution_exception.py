"""
    File-related Exceptions
"""
from sqcommon.exception.error import Error


class DistributionDataException(Error):
    """
        Exception raised for missing distribution data.

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message
