"""
    File-related Exceptions
"""
from sqcommon.exception.error import Error


class NoDistributionFileException(Error):
    """
        Exception raised in case no distribution file was provided.

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message
