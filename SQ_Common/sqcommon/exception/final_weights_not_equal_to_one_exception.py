"""
    Erroneous arguments exception
"""
from sqcommon.exception.error import Error


class FinalWeightsNotEqualToOneException(Error):
    """Exception raised when the sum of the final weights computed in metric_acsd is not equal to 1.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message
