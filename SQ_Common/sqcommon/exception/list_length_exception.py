"""
 ListLengthException
"""


class ListLengthException(Exception):
    """Exception raised when the two lists do not have the same length.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, expression, message):
        self._expression = expression
        self._message = message
