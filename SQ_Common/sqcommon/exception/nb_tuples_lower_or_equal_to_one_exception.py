"""
    File-related Exceptions
"""


class NbTuplesLowerOrEqualToOneException(Exception):
    """Exception raised when the given parameter nb_tuples is lower than one.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, expression, message):
        self._expression = expression
        self._message = message
