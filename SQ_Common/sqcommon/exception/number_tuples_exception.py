"""
    File-related Exceptions
"""
from sqcommon.exception.error import Error


class NumberTuplesException(Error):
    """
        Exception raised for forbidden number of tuples.

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message
