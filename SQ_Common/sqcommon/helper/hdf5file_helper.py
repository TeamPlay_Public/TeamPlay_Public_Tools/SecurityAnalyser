""" HDF5FileHelper module defines the tools to deal with HDF5 files """
from pathlib import Path
from typing import List

from h5py import File
from pandas import HDFStore

from sqcommon.domain.Variable.variable import Variable
from sqcommon.domain.Variable.variables_list import VariablesList


class HDF5FileHelper(object):
    """ HDF5FileHelper class defines the tools to deal with HDF5 files """

    @staticmethod
    def get_hdf5_keys_as_list(hdf5_file: Path) -> List[str]:
        """
        Finds keys contained in the HDF5 file

        Parameters
        ----------
        hdf5_file : Path
            The HDF5 file

        Returns
        -------
        List[str]
            The list of keys contained in the HDF5 file
        """
        try:
            with File(hdf5_file, 'r') as f:
                return list(f.keys())
        except FileNotFoundError as e:
            print(e.errno)

    @staticmethod
    def count_variables(hdf5_file: Path,
                        dataset: str,
                        hdf5_attribute: str) -> int:
        """
        From the HDF5 file, counts the number of variables
        of type var_type (tainted or untainted, recorded as a hdf5 attribute) in the dataset dataset

        Parameters
        ----------
        hdf5_file : Path
            The HDF5 file
        hdf5_attribute: str
            the attribute to search
        dataset: str
            dataset to inspect

        Returns
        -------
        int64
            Number of variables of type var_type
        """
        try:
            with File(hdf5_file, 'r') as f:
                return int(f.get(dataset).attrs[hdf5_attribute])
        except FileNotFoundError as e:
            print(e.errno)

    @staticmethod
    def get_variable_names_as_list(hdf5_file: Path) -> VariablesList:
        """
        From the HDF5 file, get the column names, aka the variables names

        Parameters
        ----------
        hdf5_file : Path
            The HDF5 file

        Returns
        -------
        VariablesList
            List of variables of type var_type
        """
        vars_list = VariablesList()

        #  Get the table keys, aka the side-channel value
        keys = HDF5FileHelper.get_hdf5_keys_as_list(hdf5_file)

        # Get the column names
        try:
            with HDFStore(str(hdf5_file), 'r') as store:
                varnames = store.select(keys[0], stop=1).columns
        except FileNotFoundError as e:
            print(e.errno)

        for name in varnames:
            # Removing the dataset from the variables list
            if name != str(keys[0]):
                vr = Variable(value=name, cardinality=0)
                vars_list.lst.append(vr)

        return vars_list
