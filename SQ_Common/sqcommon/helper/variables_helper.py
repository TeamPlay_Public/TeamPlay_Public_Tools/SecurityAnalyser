""" VariableHelper module defines the tools to deal with variable management """
from typing import List

from sqcommon.domain.Tuples.tuples_info import TuplesInformation
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.exception.number_tuples_exception import NumberTuplesException


class VariableHelper(object):
    """ VariableHelper class defines tools to deal with variable management """

    @staticmethod
    def list_nb_occurrences_per_var_type(data_col_names_as_list: List[str],
                                         nb_secret_vars: int,
                                         variables_cardinalities: VariablesList) -> [List[int], List[int]]:
        """
            Extract the number of occurrences for the secret and public variables
            in two different lists

         Parameters
         ----------
            data_col_names_as_list : List[str]
                The list of columns names
            nb_secret_vars: int
                Number of secret variables
            variables_cardinalities: VariablesList
                The cardinality for each Variable.

        Returns
        -------
        [List[int], List[int]]
            Two lists containing the number of occurrences for the secret and the public variables.
        """

        # Get secret and public variables cardinalities
        secret_vars_nb_occurrences_list = []
        public_vars_nb_occurrences_list = []
        for index1, data_var_name in enumerate(data_col_names_as_list):
            for _, var in enumerate(variables_cardinalities.lst):
                # if match get the cardinality
                # and add it to the list of cardinality
                if data_var_name == var.value:
                    # We have two lists, one for secret variables, the other for public
                    # The first variable(s) is/are secret.
                    if index1 < nb_secret_vars:
                        secret_vars_nb_occurrences_list.append(var.cardinality)
                    else:
                        # Then comes the untainted variables
                        public_vars_nb_occurrences_list.append(var.cardinality)

        return [secret_vars_nb_occurrences_list, public_vars_nb_occurrences_list]

    @staticmethod
    def total_nb_of_occurrences(secret_vars_nb_occurrences: List[int],
                                public_vars_nb_occurrences: List[int]) -> int:
        """
            Computes the total number of occurrences of tuples in the input

         Parameters
         ----------
         secret_vars_nb_occurrences : List[int]
            List containing the number of occurrences for each secret variable
         public_vars_nb_occurrences : List[int]
            List containing the number of occurrences for each public variable

        Returns
        -------
        total_nb_occurrences: int
            The number of tuples in the input
        """
        total_nb_occurrences = 1
        for element in secret_vars_nb_occurrences:
            total_nb_occurrences *= element
        for element in public_vars_nb_occurrences:
            total_nb_occurrences *= element

        return total_nb_occurrences

    @staticmethod
    def nb_of_occurrences_for_var_of_given_type(vars_nb_occurrences: List[int]) -> int:
        """
            Computes the number of (public xor secret) tuples in the input for the given parameter.
            The method multiplies the number of occurrences of each variable in vars_nb_occurrences
            and return its result

         Parameters
         ----------
         vars_nb_occurrences : List[int]
            List containing the number of occurrences for given (secret xor public) variables

        Returns
        -------
        nb_tuple: int
            The number of (public xor secret) tuples in the input
        """

        # If the list is empty we raise an exception
        if not vars_nb_occurrences:
            raise NumberTuplesException("List of tuple occurrences is empty",
                                        "The list of variable occurrences should contain at least one integer")

        nb_tuple = 1

        for element in vars_nb_occurrences:
            nb_tuple *= element

        return nb_tuple

    @staticmethod
    def get_tuples_information(data_col_names_as_list: List[str],
                               nb_secret_vars: int,
                               variables_cardinalities: VariablesList) -> TuplesInformation:
        """
            Return information about the tuples in the Security Exchange file:
                - Number of occurrences for each secret variable
                - Number of occurrences for each public variable

         Parameters
         ----------
            data_col_names_as_list : List[str]
                The list of columns names
            nb_secret_vars: int
                Number of secret variables
            variables_cardinalities: VariablesList
                The cardinality for each Variable.

        Returns
        -------
        tuples information: TuplesInformation
            A container holding the following information:
                - The number of occurrences of public tuples in the Security Exchange file
                - The number of occurrences of secret tuples in the Security Exchange file
                - The total number of occurrences of tuples in the Security Exchange file
        """
        secret_vars_nb_occurrences, public_vars_nb_occurrences = VariableHelper.list_nb_occurrences_per_var_type(
            data_col_names_as_list=data_col_names_as_list,
            nb_secret_vars=nb_secret_vars,
            variables_cardinalities=variables_cardinalities)

        total_nb_of_occurrences = VariableHelper.total_nb_of_occurrences(
            secret_vars_nb_occurrences=secret_vars_nb_occurrences,
            public_vars_nb_occurrences=public_vars_nb_occurrences)

        nb_of_occurrences_for_secret_var = VariableHelper.nb_of_occurrences_for_var_of_given_type(
            vars_nb_occurrences=secret_vars_nb_occurrences)

        nb_of_occurrences_for_public_var = VariableHelper.nb_of_occurrences_for_var_of_given_type(
            vars_nb_occurrences=public_vars_nb_occurrences)

        return TuplesInformation(nb_secret_tuples=nb_of_occurrences_for_secret_var,
                                 nb_public_tuples=nb_of_occurrences_for_public_var,
                                 total_nb_tuples=total_nb_of_occurrences)
