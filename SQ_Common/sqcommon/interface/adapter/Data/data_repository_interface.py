""" Module defining the interface for DataRepository.
    Interface to be implemented for a class to behave as a DataRepository """
import abc
from pathlib import Path

from sqcommon.interface.domain.Data.dataframe_interface import DataFrameInterface


class DataRepositoryInterface(metaclass=abc.ABCMeta):
    """ DataRepositoryInterface interface defines the behavior of a DataRepository """
    @staticmethod
    @abc.abstractmethod
    def find(input_path: Path) -> DataFrameInterface:
        """ The find(input_path: Path) is the method to implement to have a DataRepository behavior """
        pass
