""" Module defining the interface for DistributionRepository.
    Interface to be implemented for a class to behave as a DistributionRepository
"""
import abc
from pathlib import Path

from sqcommon.domain.Distribution.distribution_container import DistributionContainer


class DistributionRepositoryInterface(metaclass=abc.ABCMeta):
    """ DistributionRepositoryInterface interface defines the behavior of a DistributionRepository """

    @staticmethod
    @abc.abstractmethod
    def find_by_path(input_path: Path) -> DistributionContainer:
        """
            find_by_path(...) retrieves the distribution input using its path
        """
        pass
