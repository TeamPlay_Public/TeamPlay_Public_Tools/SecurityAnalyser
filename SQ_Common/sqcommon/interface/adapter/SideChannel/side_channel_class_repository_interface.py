"""
    Module defining the Interface Repository for the class used to extract the Side Channel Class from a HDF5 file
"""
import abc
from pathlib import Path

from sqcommon.interface.domain.SideChannel.side_channel_class_interface import SideChannelClassInterface


class SideChannelClassRepositoryInterface(metaclass=abc.ABCMeta):
    """
        SideChannelClassRepositoryInterface class defines the Repository Interface
        to implement to behave as a SideChannelClass Repository
    """
    @staticmethod
    @abc.abstractmethod
    def find(input_path: Path) -> SideChannelClassInterface:
        """
        Method to implement depending on the chosen repository to find the side channel class
        from the input_path

        :param input_path: the path to the input where to search for the side channel class
        :return:
            A SideChannelClass instance
        """
        pass
