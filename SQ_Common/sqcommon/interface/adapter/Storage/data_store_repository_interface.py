"""
    Module defining the interface for DataStoreRepository.
    Interface to be implemented for a class to behave as an DataStoreRepository
"""
import abc

from sqcommon.interface.domain.Data.data_interface import DataInterface

from sqcommon.interface.domain.Storage.storage_interface import StorageInterface


class DataStoreRepositoryInterface(metaclass=abc.ABCMeta):
    """
        DataStoreRepositoryInterface interface defines the behavior of a DataStoreRepository
    """

    @staticmethod
    @abc.abstractmethod
    def store(data: DataInterface,
              storage: StorageInterface
              ):
        """
            store(...) is the method to implement to have a DataStoreRepository behavior
        """
        pass
