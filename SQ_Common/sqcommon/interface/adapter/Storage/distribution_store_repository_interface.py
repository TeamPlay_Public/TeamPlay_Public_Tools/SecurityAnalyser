"""
    Module defining the interface for DistributionStoreRepository.
    Interface to be implemented for a class to behave as an DistributionStoreRepository
"""
import abc

from sqcommon.interface.adapter.Storage.data_store_repository_interface import \
    DataStoreRepositoryInterface
from sqcommon.interface.domain.BinomialDistribution.distribution_as_dict_interface import DictInterface
from sqcommon.interface.domain.Storage.storage_interface import StorageInterface


class DistributionStoreRepositoryInterface(DataStoreRepositoryInterface):
    """
        DistributionStoreRepositoryInterface interface defines the behavior of a DistributionStoreRepository
    """

    @staticmethod
    @abc.abstractmethod
    def store(data: DictInterface,
              storage: StorageInterface
              ):
        """
            store(...) is the method to implement to have a DistributionStoreRepository behavior
        """
        pass
