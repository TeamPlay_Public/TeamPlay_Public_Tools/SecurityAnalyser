"""
    VariablesCardinalityRepositoryInterface module defines the interface
    to be implemented to act as a VariablesCardinalityRepository
"""
import abc
from pathlib import Path

from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.interface.domain.Variable.variables_list_interface import VariablesListInterface


class VariablesCardinalityRepositoryInterface(metaclass=abc.ABCMeta):
    """ VariablesCardinalityRepositoryInterface class defines a method
        find_all(input_file: str, dataset: str, hdf5_attributes: VariablesList) -> VariablesListInterface
        to be implemented to act as a VariablesNamesListRepository
    """

    @staticmethod
    @abc.abstractmethod
    def find_all(input_file: Path, dataset: str, hdf5_attributes: VariablesList) -> VariablesListInterface:
        """
        Method to implement to behave as a VariablesCardinalityRepository.
        find_all retrieves the cardinalities of the different variables

        :param input_file: the input
        :param dataset: the name of the dataset
        :param hdf5_attributes: the list of variables VariablesList
        :return:
            the list of variables VariablesList with their respective cardinality
        """
        pass
