"""
    VariablesCountRepositoryInterface module defines the interface
    to be implemented to act as a VariablesCountRepository
"""
import abc
from pathlib import Path


class VariablesCountRepositoryInterface(metaclass=abc.ABCMeta):
    """ VariablesCountRepositoryInterface class defines a method
        def count(input_path: str, dataset: str, hdf5_attribute: str) -> int
        to be implemented to act as a VariablesCountRepository
    """

    @staticmethod
    @abc.abstractmethod
    def count(input_path: Path, dataset: str, hdf5_attribute: str) -> int:
        """
        Count the number of variables

        :param input_path: the input
        :param dataset: the dataset name
        :param hdf5_attribute: the variables name

        :return:
            how many different variables there are
        """
        pass
