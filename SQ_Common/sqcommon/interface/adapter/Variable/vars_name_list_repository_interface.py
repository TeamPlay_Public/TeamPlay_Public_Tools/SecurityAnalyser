"""
    VariablesNamesListRepositoryInterface module defines the interface
    to be implemented to act as a VariablesNamesListRepository
"""
import abc
from pathlib import Path

from sqcommon.interface.domain.Variable.variables_list_interface import VariablesListInterface


class VariablesNamesListRepositoryInterface(metaclass=abc.ABCMeta):
    """ VariablesNamesListRepositoryInterface class defines a method
        find_all(input_path: Path) -> VariablesListInterface
        to be implemented to act as a VariablesNamesListRepository
    """

    @staticmethod
    @abc.abstractmethod
    def find_all(input_path: Path) -> VariablesListInterface:
        """
        List the names of the variables

        :param input_path: the input

        :return:
            The names of the variables
        """
        pass
