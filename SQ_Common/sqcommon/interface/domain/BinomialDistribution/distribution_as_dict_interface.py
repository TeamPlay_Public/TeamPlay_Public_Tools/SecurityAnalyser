"""
    Interface to be implemented by class having data of type Dict
"""
import abc
from typing import Dict

from sqcommon.interface.domain.Data.data_interface import DataInterface


class DictInterface(DataInterface):
    """
        Interface to be implemented by class having data of type Dict
   """

    @property
    @abc.abstractmethod
    def data(self) -> Dict:
        """ Getter for the data attribute """
        pass
