"""
    Interface to be implemented by class storing the Data from the Input
"""
import abc


class DataInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class storing the Data from the Input
   """

    @property
    @abc.abstractmethod
    def data(self):
        """ Getter for the data attribute """
        pass
