"""
    Interface to be implemented by class storing the Data as a DataFrame from the Input
"""
import abc
from pandas import DataFrame

from sqcommon.interface.domain.Data.data_interface import DataInterface


class DataFrameInterface(DataInterface):
    """
        Interface to be implemented by class storing the Data as a DataFrame from the Input
   """

    @property
    @abc.abstractmethod
    def data(self) -> DataFrame:
        """ Getter for the data attribute """
        pass
