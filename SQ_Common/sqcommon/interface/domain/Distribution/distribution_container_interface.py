""" Interface to be implemented by class to behave as DistributionContainer  """
import abc


class DistributionContainerInterface:
    """
        Interface to be implemented by class to behave as DistributionContainer
        It is a container holding the values of the different distributions
        needed to compute the Mutual Information-based security metric, aka indiscernibility level

        Attribute
        ----------
        secret: Distribution
            distribution for the tainted variables tuples.

        public: Distribution
            distribution for the untainted variables tuples.

        leakage: Distribution
            distribution for the leakage tuples.

        public_leakage: Distribution
            Joint distribution for leakage and untainted variables

        secret_public: Distribution
            Joint distribution for tainted and untainted variables

    """

    @property
    @abc.abstractmethod
    def secret(self):
        """ Getter for the secret attribute """
        pass

    @property
    @abc.abstractmethod
    def public(self):
        """ Getter for the public attribute """
        pass

    @property
    @abc.abstractmethod
    def leakage(self):
        """ Getter for the leakage attribute """
        pass

    @property
    @abc.abstractmethod
    def public_leakage(self):
        """ Getter for the public_leakage attribute """
        pass

    @property
    @abc.abstractmethod
    def secret_public(self):
        """ Getter for the secret_public attribute """
        pass
