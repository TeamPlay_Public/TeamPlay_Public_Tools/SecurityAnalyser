"""
    Interface to be implemented by class to behave as Shuffle.
    Define the method shuffle(self) to shuffle a collection
"""
import abc


class ShuffleInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as Shuffle.
        Define the method shuffle(self) to shuffle a collection
   """

    @property
    @abc.abstractmethod
    def shuffle(self):
        """
            Method to implement with the desired shuffle generator
        """
        pass
