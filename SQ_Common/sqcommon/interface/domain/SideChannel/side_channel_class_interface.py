"""
    Interface to be implemented by class to behave as SideChannelClass
"""
import abc


class SideChannelClassInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as SideChannelClass
   """
    @property
    @abc.abstractmethod
    def name(self):
        """ Getter for the name attribute """
        pass

    @property
    @abc.abstractmethod
    def value(self):
        """ Getter for the value attribute """
        pass
