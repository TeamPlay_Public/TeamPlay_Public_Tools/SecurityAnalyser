"""
    Interface to be implemented by class to behave as Storage
"""
import abc


class StorageInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as Storage
   """

    @property
    @abc.abstractmethod
    def name(self) -> str:
        """
            Method to implement to return the name of the storage. Only method to implement to behave as a storage
        """
        pass
