"""
    Interface to be implemented by class to behave as TuplesInformation.
"""
import abc


class TuplesInformationInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as TuplesInformation.
    """

    @property
    @abc.abstractmethod
    def nb_secret_tuples(self) -> int:
        """
            Method to implement to return the nb_secret_tuples of the Security Exchange file.
        """
        pass

    @property
    @abc.abstractmethod
    def nb_public_tuples(self) -> int:
        """
            Method to implement to return the nb_public_tuples of the Security Exchange file.
        """
        pass

    @property
    @abc.abstractmethod
    def total_nb_tuples(self) -> int:
        """
            Method to implement to return the total_nb_tuples of the Security Exchange file.
        """
        pass
