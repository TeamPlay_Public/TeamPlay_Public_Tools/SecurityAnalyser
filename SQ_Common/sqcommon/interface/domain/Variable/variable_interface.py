"""
    Module containing the interface to be implemented by class to behave as Variable
"""
import abc


class VariableInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as Variable
   """

    @property
    @abc.abstractmethod
    def value(self) -> str:
        """ Getter for the value attribute """
        pass

    @property
    @abc.abstractmethod
    def cardinality(self) -> bytes:
        """ Getter for the cardinality attribute """
        pass
