"""
    Module containing the interface to be implemented by class to behave as VariablesList
"""
import abc


class VariablesListInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as VariablesList
   """

    @property
    @abc.abstractmethod
    def lst(self):
        """ Getter for the lst attribute """
        pass

    @abc.abstractmethod
    def nb_vars(self):
        """ Getter for the nb_vars attribute """
        pass
