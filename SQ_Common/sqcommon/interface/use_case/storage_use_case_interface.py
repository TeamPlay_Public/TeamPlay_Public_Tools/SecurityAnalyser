"""
    StorageUseCaseInterface module defines the interface to implement to be a StorageUseCase
"""
import abc

from sqcommon.interface.domain.Data.data_interface import DataInterface
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from sqcommon.interface.adapter.Storage.data_store_repository_interface import DataStoreRepositoryInterface
from sqcommon.interface.domain.Storage.storage_interface import StorageInterface


class StorageUseCaseInterface(UseCaseInterface):
    """
        StorageUseCaseInterface module defines the interface to implement to be a StorageUseCase
    """

    def __init__(self,
                 output_storage_repo: DataStoreRepositoryInterface,
                 data: DataInterface,
                 storage_data: StorageInterface
                 ):
        """ Constructor for the StorageUseCaseInterface class """
        self._repository = output_storage_repo
        self._data = data
        self._storage_data = storage_data

    @abc.abstractmethod
    def execute(self):
        """
            StorageUseCaseInterface class defines a method execute(self) to be implemented to act as a StorageUseCase
        """
        pass
