"""
    UseCaseInterface module defines the interface to implement to be a UseCase
"""
import abc


class UseCaseInterface(metaclass=abc.ABCMeta):

    """ UseCaseInterface class defines a method execute(self) to be implemented to act as a UseCase """
    @abc.abstractmethod
    def execute(self):
        """
        Method to implement to behave as a UseCase.
        Roll the scenario of the use case and eventually calls the chosen implementation method
        :return:
        The result the use case was made for
        """
        pass
