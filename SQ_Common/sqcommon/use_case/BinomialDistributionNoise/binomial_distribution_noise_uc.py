""" Module defining the uses cases related to the Binomial Distribution UC """
from pathlib import Path

from sqcommon.interface.domain.Shuffle.shuffle_interface import ShuffleInterface
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface
from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples

from sqcommon.domain.BinomialDistribution.binomial_distribution import BinomialDistribution

from sqcommon.interface.adapter.Storage.data_store_repository_interface import DataStoreRepositoryInterface
from sqcommon.interface.domain.Storage.storage_interface import StorageInterface


class BinomialDistributionNoiseUC(UseCaseInterface):
    """
        BinomialDistributionUC class generates a Gaussian distribution with nb_tuples values
        The new distribution is recorded in a Storage.
    """

    def __init__(self,
                 json_output_file: Path,
                 nb_tuples: int,
                 distribution_type: EnumDistributionTuples,
                 shuffle_function: ShuffleInterface,
                 storage_data: StorageInterface,
                 storage_repo: DataStoreRepositoryInterface
                 ):
        """ Constructor for the BinomialDistributionUC class """
        self._json_output_file = json_output_file
        self._nb_tuples = nb_tuples
        self._distribution_type = distribution_type
        self._shuffle_function = shuffle_function
        self._storage_data = storage_data
        self._storage_repo = storage_repo

    def execute(self):
        """
            The execute() method generates a Gaussian distribution with nb_tuples values
        """

        # Run the algorithm with the number of tuples as entry parameter
        bin_dist = BinomialDistribution(nb_tuples=self._nb_tuples,
                                        distribution_type=self._distribution_type,
                                        shuffle_function=self._shuffle_function)

        # Save the dict using the given repository
        self._storage_repo.store(bin_dist, self._storage_data(name=self._json_output_file))
