""" Module defining the uses cases related to Data as a Dataframe """
from pathlib import Path

from sqcommon.interface.adapter.Data.data_repository_interface import DataRepositoryInterface
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface


class HDF5DataAsDataFrameGetUC(UseCaseInterface):
    """ HDF5DataAsDataFrameGetUC class retrieves the data as a Panda DataFrame """

    def __init__(self,
                 data_repo: DataRepositoryInterface,
                 input_path: Path):
        """ Constructor for the DataAsDataFrameGetUC class """
        self._repository = data_repo
        self._input_path = input_path

    def execute(self):
        """ The execute() method calls the adapter to get the data as a Panda DataFrame """
        return self._repository.find(self._input_path)

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
