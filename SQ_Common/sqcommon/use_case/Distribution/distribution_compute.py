""" Module defining the use cases related to the Distribution Computation """
from typing import List

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.helper.variables_helper import VariableHelper
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from sqcommon.domain.Distribution.distribution_container import DistributionContainer
from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict
from sqcommon.domain.Distribution.distribution_for_tuples import Distribution
from sqcommon.domain.Distribution.distribution_tuples import DistributionTuples
from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples


def compute_distribution(nb_tuples, occurrences_dict):
    """
        Compute the distribution given the data in the Dict and the number of tuples
    """
    # We divide each element by the total number of tuples
    occurrences_dict = {k: v / nb_tuples for k, v in occurrences_dict.items()}

    return list(occurrences_dict.values())


class DistributionComputeUseCase(UseCaseInterface):
    """ DistributionComputeUseCase class defines the Probability Distribution computation methods """

    def __init__(self,
                 data: DataAsDataFrame,
                 distribution_as_dict: DistributionAsDict,
                 nb_tainted_vars: int,
                 nb_untainted_vars: int,
                 variables_card: VariablesList):
        """ Constructor for the DistributionComputeUseCase class """
        self._data = data
        self._distribution_as_dict = distribution_as_dict
        self._nb_tainted_vars = nb_tainted_vars
        self._nb_untainted_vars = nb_untainted_vars
        self._variables_card = variables_card

    def execute(self) -> DistributionContainer:
        """
            Method calling inner methods to provide the distributions
            for tainted_variables, untainted_variables, leakage variable
        """
        # Compute the total number of tuples and the number of untainted tuples
        total_nb_tuples = self.extract_tuples_info()

        # Get the indices of the columns needed in the Pandas Series
        # secret
        secret_indices = [i for i in range(0, self._nb_tainted_vars)]
        # untainted
        public_indices = [i for i in range(self._nb_tainted_vars, self._nb_untainted_vars + 1)]
        # leakage
        leakage_indices = [i for i in range(self._nb_tainted_vars + self._nb_untainted_vars,
                                            self._nb_tainted_vars + self._nb_untainted_vars + 1)]
        # [public, leakage]
        public_leakage_indices = [i for i in range(self._nb_tainted_vars,
                                                   self._nb_tainted_vars + self._nb_untainted_vars + 1)]
        # [secret, public]
        secret_public_indices = [i for i in range(0, self._nb_tainted_vars + self._nb_untainted_vars)]

        return DistributionContainer(
            secret=self.compute_distribution_for_indices(indices=secret_indices,
                                                         secret_indices=secret_indices,
                                                         nb_tuples=total_nb_tuples,
                                                         element=EnumDistributionTuples.SECRET
                                                         ),
            public=self.compute_distribution_for_indices(indices=public_indices,
                                                         secret_indices=secret_indices,
                                                         nb_tuples=total_nb_tuples,
                                                         element=EnumDistributionTuples.PUBLIC
                                                         ),
            leakage=self.compute_distribution_for_indices(indices=leakage_indices,
                                                          secret_indices=secret_indices,
                                                          nb_tuples=total_nb_tuples,
                                                          element=EnumDistributionTuples.LEAKAGE
                                                          ),
            public_leakage=self.compute_distribution_for_indices(indices=public_leakage_indices,
                                                                 secret_indices=secret_indices,
                                                                 nb_tuples=total_nb_tuples,
                                                                 element=EnumDistributionTuples.PUBLIC_LEAKAGE
                                                                 ),
            secret_public=self.compute_distribution_for_indices(indices=secret_public_indices,
                                                                secret_indices=secret_indices,
                                                                nb_tuples=total_nb_tuples,
                                                                element=EnumDistributionTuples.SECRET_PUBLIC)
        )

    def compute_distribution_for_indices(self,
                                         indices: List[int],
                                         secret_indices: List[int],
                                         nb_tuples: int,
                                         element: EnumDistributionTuples
                                         ):
        """
            The compute_distribution() method computes the distribution for the given indices in the Pandas dataframe.

            Parameters:
                indices: List[int]
                    The indices where to search for the needed tuples

                secret_indices: List[int]
                    The indices for the secret variables,
                    needed to get the associated secret variables for the leakage variables

                nb_tuples: int
                    The total number of tuples in the Pandas Dataframe

                element: EnumDistributionTuples
                    The concerned variable
        """

        # Initialize the dictionary that will contain the distribution
        distribution_tuples = self.gather_tuples(indices, secret_indices, nb_tuples, element)

        # Depending on whether we have a provided distribution (using the distribution_as_dict attribute) or not,
        # either we compute it or simply copy it from distribution_as_dict
        dict_dist = self._distribution_as_dict.data

        if element.value not in dict_dist:
            distribution = compute_distribution(nb_tuples, distribution_tuples.tuples_as_dict)
        else:
            distribution = dict_dist[element.value]

        return Distribution(variable_name=element,
                            dist_tuples=distribution_tuples,
                            distribution=distribution)

    def gather_tuples(self,
                      indices: List[int],
                      secret_indices: List[int],
                      nb_tuples: int,
                      element: EnumDistributionTuples) -> DistributionTuples:
        """
            Using the indices, the method scrolls in the Pandas Dataframe and extract the needed tuples

            Parameters:
                indices: List[int]
                    The indices where to search for the needed tuples

                secret_indices: List[int]
                    The indices for the secret variables,
                    needed to get the associated secret variables for the leakage variables

                nb_tuples: int
                    The total number of tuples in the Pandas Dataframe

                element: EnumDistributionTuples
                    The concerned variable
        """
        # Initialize the dictionary that will contain the distribution
        tuples_dict = {}
        # Initialize the list that will contain all the tuples for element
        all_the_tuples = []
        # Initialize the list that will contain all the related tuples for the leakage element
        related_secret_tuples = []
        # Initialize the list that will contain all the tuples for element, but without duplicates
        tuples_as_list_no_duplicate = []

        # Looping over the number of tuples to get the ones at the given indices
        for index1 in range(0, nb_tuples, 1):

            # Extract the current_tuple from the Dataframe
            current_tuple = self._data.data.iloc[[index1], indices].values[0]
            # Make it a list
            current_tuple_as_list = current_tuple.tolist()
            # and save it into all_the_tuples
            all_the_tuples.append(current_tuple_as_list)
            # current_tuple_for_dict will be used as a key in the dictionary, so we make it a string
            current_tuple_as_str = str(current_tuple_as_list)

            # When we deal with the leakage variables, we want to get their associated secret variable
            if element == EnumDistributionTuples.LEAKAGE:
                # From a ndarray we get [0] in order to extract the value, that we convert to a float
                related_secret_tuples.append(float((self._data.data.iloc[[index1], secret_indices].values[0])[0]))
                # related_secret_tuples.append((self._data.data.iloc[[index1], secret_indices].values[0])[0].tolist())

            # We record the current_tuple
            # If the current_tuple exists in the dict, increment its number of occurrences
            # else, add the new key with value = 1
            if current_tuple_as_str in tuples_dict:
                tuples_dict[current_tuple_as_str] += 1
            else:
                tuples_dict[current_tuple_as_str] = 1

        # We convert the list of keys of the distribution_tuples.sorted_tuples into a list of int
        # for the leakages
        if element == EnumDistributionTuples.LEAKAGE:
            for key in list(tuples_dict.keys()):
                tuples_as_list_no_duplicate.append(int(key[1:-1]))

        return DistributionTuples(all_the_tuples=all_the_tuples,
                                  tuples_as_dict=tuples_dict,
                                  tuples_as_list_no_duplicate=tuples_as_list_no_duplicate,
                                  related_secret_tuples=related_secret_tuples)

    def extract_tuples_info(self) -> int:
        """
            Method extracting information from the DataFrame
        """
        tainted_vars_cards, untainted_vars_cards = VariableHelper.list_nb_occurrences_per_var_type(
            self._data.data.columns.tolist(),
            self._nb_tainted_vars,
            self._variables_card)
        nb_rows = VariableHelper.total_nb_of_occurrences(tainted_vars_cards, untainted_vars_cards)

        return nb_rows

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
