""" Module defining the use cases related to getting data for the Distribution Computation """
from pathlib import Path

from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from sqcommon.interface.adapter.Distribution.distribution_repository_interface import \
    DistributionRepositoryInterface


class DistributionJSONGetUseCase(UseCaseInterface):
    """ DistributionGetUseCase class defines the methods to get data for the DistributionComputation """

    def __init__(self,
                 distribution_repository: DistributionRepositoryInterface,
                 dist_file: Path
                 ):
        """ Constructor for the DistributionGetUseCase class """
        self._repository = distribution_repository
        self._dist_file = dist_file

    def execute(self):
        """ The execute(..) method calls the adapter to get the JSON distribution file """
        return self._repository.find_by_path(input_path=self._dist_file)
