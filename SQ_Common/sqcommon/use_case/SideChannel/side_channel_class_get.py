""" Module defining the uses cases related to the Side Channel Class """
from pathlib import Path

from sqcommon.interface.adapter.SideChannel.side_channel_class_repository_interface import \
    SideChannelClassRepositoryInterface
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface


class SideChannelClassGetUC(UseCaseInterface):
    """ SideChannelClassGetUC class retrieves the side channel class """

    def __init__(self,
                 side_channel_class_repo: SideChannelClassRepositoryInterface,
                 input_path: Path):
        """ Constructor for the SideChannelClassGetUC class """
        self._repository = side_channel_class_repo
        self._input_path = input_path

    def execute(self):
        """ The execute() method calls the adapter to get the Side Channel Class """
        return self._repository.find(self._input_path)

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
