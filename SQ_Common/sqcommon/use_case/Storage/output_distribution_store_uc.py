""" Module defining the uses cases related to the storage of the program output """
from sqcommon.interface.adapter.Storage.distribution_store_repository_interface \
    import DistributionStoreRepositoryInterface
from sqcommon.interface.domain.BinomialDistribution.distribution_as_dict_interface import DictInterface

from sqcommon.interface.domain.Storage.storage_interface import StorageInterface
from sqcommon.interface.use_case.storage_use_case_interface import StorageUseCaseInterface


class OutputDistributionStoreUC(StorageUseCaseInterface):
    """ OutputDistributionStoreUC class defines the execute method that store the generated Distribution """

    def __init__(self,
                 output_storage_repo: DistributionStoreRepositoryInterface,
                 data: DictInterface,
                 storage_data: StorageInterface
                 ):
        """ Constructor for the OutputDistributionStoreUC class """
        super().__init__(output_storage_repo=output_storage_repo,
                         data=data,
                         storage_data=storage_data)

    def execute(self):
        """
            The execute() method calls the adapter to store the Distribution
        """
        return self._repository.store(data=self._data, storage=self._storage_data)
