""" Module defining the uses cases related to the Variables """
from pathlib import Path

from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.interface.adapter.Variable.vars_cardinality_repository_interface import \
    VariablesCardinalityRepositoryInterface
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface


class VariablesCardHDF5UC(UseCaseInterface):
    """ VariablesCardHDF5UC class retrieves the cardinality of each variable from the HDF5 file """

    def __init__(self,
                 variables_card_repo: VariablesCardinalityRepositoryInterface,
                 input_path: Path,
                 side_channel_class: SideChannelClass,
                 vars_list: VariablesList):
        """ Constructor for the VariablesHDF5CountUC class """
        self._repository = variables_card_repo
        self._input_path = input_path
        # From a SideChannelClass we get its value, which is the dataset value to search
        self._dataset = side_channel_class.value
        self._vars_list = vars_list

    def execute(self):
        """
            The execute() method calls the adapter to get for each variables in vars_list its cardinality
        """
        return self._repository.find_all(input_file=self._input_path,
                                         dataset=self._dataset,
                                         hdf5_attributes=self._vars_list)

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
