""" Module defining the uses cases related to the Variables """
from pathlib import Path

from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.enums.enum_var_types import VariableTypes
from sqcommon.interface.adapter.Variable.vars_count_repository_interface \
    import VariablesCountRepositoryInterface
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface


class VariablesHDF5CountUC(UseCaseInterface):
    """ VariablesHDF5CountUC class counts the number of variables of a type in the HDF5 file """

    def __init__(self,
                 variables_count_repo: VariablesCountRepositoryInterface,
                 input_path: Path,
                 side_channel_class: SideChannelClass,
                 hdf5_attribute: VariableTypes):
        """ Constructor for the VariablesHDF5CountUC class """
        self._repository = variables_count_repo
        self._input_path = input_path
        # From a SideChannelClass we get its value, which is the dataset value to search
        self._dataset = side_channel_class.value
        # We transfer only the value to the Helper
        self._hdf5_attribute = hdf5_attribute.value

    def execute(self):
        """
            The execute() method calls the adapter to count the number of variables of type var_type.
            The structure of a HDF5 file requires a dataset and an attribute to be searched
        """
        return self._repository.count(input_path=self._input_path,
                                      dataset=self._dataset,
                                      hdf5_attribute=self._hdf5_attribute)

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
