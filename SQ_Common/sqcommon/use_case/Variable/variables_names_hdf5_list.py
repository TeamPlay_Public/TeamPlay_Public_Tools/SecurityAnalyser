""" Module defining the uses cases related to the Variables Names using a HDF5 file """
from pathlib import Path

from sqcommon.interface.adapter.Variable.vars_name_list_repository_interface import \
    VariablesNamesListRepositoryInterface
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface


class VariablesNamesHDF5UC(UseCaseInterface):
    """ VariablesNamesHDF5UC class retrieves the variables names list """

    def __init__(self,
                 variables_names_repo: VariablesNamesListRepositoryInterface,
                 input_path: Path):
        """ Constructor for the UntaintedListUseCase class """
        self._repository = variables_names_repo
        self._input_path = input_path

    def execute(self):
        """ The execute() method calls the adapter to get the list of variables names """
        return self._repository.find_all(input_path=self._input_path)

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
