"""
    Testing class related to the adapter HDF5DataAsDataFrameGetRepository and containing testing methods
"""

import pytest
from pandas import DataFrame

from sqcommon.adapter.File.HDF5.Data.hdf5_data_as_dataframe_repository import HDF5DataAsDataFrameGetRepository

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame

from sqcommon.interface.adapter.Data.data_repository_interface import DataRepositoryInterface


@pytest.fixture
def hdf5_dataframe_repo_model_init():
    """ Instantiate the HDF5DataAsDataFrameGetRepository """

    hdf5_dataframe_repo = HDF5DataAsDataFrameGetRepository()

    return hdf5_dataframe_repo


def test_hdf_is_instance_of_hdf5dataframerepo(hdf5_dataframe_repo_model_init):
    """ Test whether HDF5DataAsDataFrameGetRepository is an implementation of DataRepositoryInterface """

    hdf = hdf5_dataframe_repo_model_init

    assert isinstance(hdf, HDF5DataAsDataFrameGetRepository)
    assert isinstance(hdf, DataRepositoryInterface)


def test_returned_value_is_instance_of_dataasdataframe(return_input_file_secret5a10n20_time_worst_for_repository_test):
    """ Test whether HDF5DataAsDataFrameGetRepository.find(file) returns a DataAsDataFrame """
    returned_value = HDF5DataAsDataFrameGetRepository.find(
                return_input_file_secret5a10n20_time_worst_for_repository_test)

    assert isinstance(returned_value, DataAsDataFrame)
    assert isinstance(returned_value._data, DataFrame)
