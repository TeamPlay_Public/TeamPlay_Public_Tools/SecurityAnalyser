"""
    Testing class related to the adapter SideChannelClassGetHDF5Repository and containing testing methods
"""
import os

import pytest

from sqcommon.adapter.File.HDF5.SideChannel.side_channel_class_get_hdf5_repository import \
    SideChannelClassGetHDF5Repository
from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass

from sqcommon.interface.adapter.SideChannel.side_channel_class_repository_interface import \
    SideChannelClassRepositoryInterface


@pytest.fixture
def sidechannel_repo_model_init():
    """ Instantiate the SideChannelClassGetHDF5Repository """

    side_channel_repo = SideChannelClassGetHDF5Repository()

    return side_channel_repo


def test_hdf_is_instance_of_sidechannelrepo(sidechannel_repo_model_init):
    """ Test whether SideChannelClassGetHDF5Repository is an implementation of SideChannelClassRepositoryInterface """

    sdc = sidechannel_repo_model_init

    assert isinstance(sdc, SideChannelClassGetHDF5Repository)
    assert isinstance(sdc, SideChannelClassRepositoryInterface)


def test_returned_value_is_instance_of_sidechannelclass(return_input_file_secret5a10n20_time_worst_for_repository_test):
    """ Test whether SideChannelClassGetHDF5Repository.find(file) returns a SideChannelClass """

    returned_value = SideChannelClassGetHDF5Repository.find(
        return_input_file_secret5a10n20_time_worst_for_repository_test)

    assert isinstance(returned_value, SideChannelClass)


def test_returned_value_dot_value_is_ok(returned_value_value_is_time_worst,
                                        return_input_file_secret5a10n20_time_worst_for_repository_test):
    """ Test whether SideChannelClassGetHDF5Repository.find(file) returned value is ok """

    returned_value = SideChannelClassGetHDF5Repository.find(
        return_input_file_secret5a10n20_time_worst_for_repository_test)

    assert returned_value.value == returned_value_value_is_time_worst


def test_returned_value_dot_name_is_ok(return_input_file_secret5a10n20_time_worst_for_repository_test,
                                       returned_value_name_is_time_worst):
    """ Test whether SideChannelClassGetHDF5Repository.find(file) returned name is ok """

    returned_value = SideChannelClassGetHDF5Repository.find(
        return_input_file_secret5a10n20_time_worst_for_repository_test)

    assert returned_value.name == returned_value_name_is_time_worst
