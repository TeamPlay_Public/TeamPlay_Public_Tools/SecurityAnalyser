"""
    Testing class related to the adapter VariablesCardinalityHDF5Repository and containing testing methods
"""

import pytest

from sqcommon.adapter.File.HDF5.Variable.variables_cardinality_repository import VariablesCardinalityHDF5Repository
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.interface.adapter.Variable.vars_cardinality_repository_interface import \
    VariablesCardinalityRepositoryInterface


@pytest.fixture
def vars_card_repo_model_init():
    """ Instantiate the VariablesCardinalityHDF5Repository """

    vars_card_repo = VariablesCardinalityHDF5Repository()

    return vars_card_repo


def test_is_instance_of_variablecardinalityrepo(vars_card_repo_model_init):
    """
        Test whether VariablesCardinalityHDF5Repository is an implementation of VariablesCardinalityRepositoryInterface
    """

    vcr = vars_card_repo_model_init

    assert isinstance(vcr, VariablesCardinalityHDF5Repository)
    assert isinstance(vcr, VariablesCardinalityRepositoryInterface)


def test_returned_value_is_instance_of_variableslist(return_input_file_secret5a10n20_time_worst_for_repository_test,
                                                     returns_time_worst,
                                                     return_variables_list_a10_n20_secret5):
    """ Test whether VariablesCardinalityHDF5Repository.find_all(...) returns a VariablesList """
    returned_value = VariablesCardinalityHDF5Repository.find_all(
        input_file=return_input_file_secret5a10n20_time_worst_for_repository_test,
        hdf5_attributes=return_variables_list_a10_n20_secret5,
        dataset=returns_time_worst)

    assert isinstance(returned_value, VariablesList)


def test_returned_value_has_vars_with_values(return_input_file_secret5a10n20_time_worst_for_repository_test,
                                             returns_time_worst,
                                             return_variables_list_a10_n20_secret5):
    """
    Test whether VariablesCardinalityHDF5Repository.find_all(...) returned value has variables with values a, n, secret
    """

    returned_value = VariablesCardinalityHDF5Repository.find_all(
        input_file=return_input_file_secret5a10n20_time_worst_for_repository_test,
        hdf5_attributes=return_variables_list_a10_n20_secret5,
        dataset=returns_time_worst)

    # Gathering values from VariablesList in a simple list
    values = set(var.value for var in returned_value.lst)

    # then check that we have the expected values
    assert "a" in values
    assert "secret" in values
    assert "n" in values


def test_returned_value_has_vars_with_cardinalities(return_input_file_secret5a10n20_time_worst_for_repository_test,
                                                    returns_time_worst,
                                                    return_variables_list_a10_n20_secret5):
    """
        Test whether VariablesCardinalityHDF5Repository.find_all(...) returned value has variables
        with cardinality a=2, n=2, secret=2
    """

    returned_value = VariablesCardinalityHDF5Repository.find_all(
        input_file=return_input_file_secret5a10n20_time_worst_for_repository_test,
        hdf5_attributes=return_variables_list_a10_n20_secret5,
        dataset=returns_time_worst)

    for var in returned_value.lst:
        if var.value == "a":
            assert int(var.cardinality) == 10
        elif var.value == "n":
            assert int(var.cardinality) == 20
        elif var.value == "secret":
            assert int(var.cardinality) == 5
