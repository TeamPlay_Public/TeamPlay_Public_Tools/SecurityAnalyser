"""
    Testing class related to the adapter VariablesCountHDF5Repository and containing testing methods
"""

import pytest

from sqcommon.adapter.File.HDF5.Variable.variables_count_hdf5_repository import VariablesCountHDF5Repository

from sqcommon.interface.adapter.Variable.vars_count_repository_interface import VariablesCountRepositoryInterface


@pytest.fixture
def vars_count_repo_model_init():
    """ Instantiate the VariablesCountHDF5Repository """

    vars_count_repo = VariablesCountHDF5Repository()

    return vars_count_repo


def test_is_instance_of_variablecountrepo(vars_count_repo_model_init):
    """
        Test whether VariablesCountHDF5Repository is an implementation of VariablesCountRepositoryInterface
    """

    vcr = vars_count_repo_model_init

    assert isinstance(vcr, VariablesCountHDF5Repository)
    assert isinstance(vcr, VariablesCountRepositoryInterface)


def test_returned_value_is_instance_of_int(return_input_file_secret5a10n20_time_worst_for_repository_test,
                                           returns_time_worst,
                                           return_var_type_tainted):
    """ Test whether VariablesCountHDF5Repository.count(...) returns an int """
    returned_value = VariablesCountHDF5Repository.count(
        input_path=return_input_file_secret5a10n20_time_worst_for_repository_test,
        hdf5_attribute=return_var_type_tainted,
        dataset=returns_time_worst)

    assert isinstance(returned_value, int)


def test_returned_value_is_1_for_var_type_nbtainted(return_input_file_secret5a10n20_time_worst_for_repository_test,
                                                    returns_time_worst,
                                                    return_var_type_tainted):
    """
        Test whether VariablesCountHDF5Repository.count(...) returned value is equal to 1 for variable type nbTainted
    """

    returned_value = VariablesCountHDF5Repository.count(
        input_path=return_input_file_secret5a10n20_time_worst_for_repository_test,
        hdf5_attribute=return_var_type_tainted,
        dataset=returns_time_worst)

    assert returned_value == 1


def test_returned_value_is_1_for_var_type_nbuntainted(return_input_file_secret5a10n20_time_worst_for_repository_test,
                                                      returns_time_worst,
                                                      return_var_type_untainted):
    """
        Test whether VariablesCountHDF5Repository.count(...) returned value is equal to 2 for variable type nbUntainted
    """

    returned_value = VariablesCountHDF5Repository.count(
        input_path=return_input_file_secret5a10n20_time_worst_for_repository_test,
        hdf5_attribute=return_var_type_untainted,
        dataset=returns_time_worst)

    assert returned_value == 2
