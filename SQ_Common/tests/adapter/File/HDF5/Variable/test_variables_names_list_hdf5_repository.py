"""
    Testing class related to the adapter VariablesNamesHDF5Repository and containing testing methods
"""

import pytest

from sqcommon.adapter.File.HDF5.Variable.variables_names_list_hdf5_repository import VariablesNamesHDF5Repository
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.interface.adapter.Variable.vars_name_list_repository_interface import \
    VariablesNamesListRepositoryInterface


@pytest.fixture
def vars_names_repo_model_init():
    """ Instantiate the VariablesNamesHDF5Repository """

    vars_names_repo = VariablesNamesHDF5Repository()

    return vars_names_repo


def test_is_instance_of_variablecountrepo(vars_names_repo_model_init):
    """
        Test whether VariablesNamesHDF5Repository is an implementation of VariablesNamesListRepositoryInterface
    """

    vcr = vars_names_repo_model_init

    assert isinstance(vcr, VariablesNamesHDF5Repository)
    assert isinstance(vcr, VariablesNamesListRepositoryInterface)


def test_returned_value_is_instance_of_variableslist(return_input_file_secret5a10n20_time_worst_for_repository_test):
    """ Test whether VariablesNamesHDF5Repository.find_all(...) returns VariablesList """
    returned_value = VariablesNamesHDF5Repository.find_all(
        input_path=return_input_file_secret5a10n20_time_worst_for_repository_test)

    assert isinstance(returned_value, VariablesList)


def test_returned_value_has_vars_with_values(return_input_file_secret5a10n20_time_worst_for_repository_test):
    """
    Test whether VariablesNamesHDF5Repository.find_all(...) returned value has variables with values a, n, secret
    """

    returned_value = VariablesNamesHDF5Repository.find_all(
        input_path=return_input_file_secret5a10n20_time_worst_for_repository_test)

    # Gathering values from VariablesList in a simple list
    values = set(var.value for var in returned_value.lst)

    # then check that we have the expected values
    assert "a" in values
    assert "secret" in values
    assert "n" in values


def test_returned_value_has_vars_with_cardinalities(return_input_file_secret5a10n20_time_worst_for_repository_test):
    """
        Test whether VariablesNamesHDF5Repository.find_all(...) returned value has variables
        with cardinality a=2, n=2, secret=2
    """

    returned_value = VariablesNamesHDF5Repository.find_all(
        input_path=return_input_file_secret5a10n20_time_worst_for_repository_test)

    for var in returned_value.lst:
        if var.value == "a":
            assert int(var.cardinality) == 0
        elif var.value == "n":
            assert int(var.cardinality) == 0
        elif var.value == "secret":
            assert int(var.cardinality) == 0
