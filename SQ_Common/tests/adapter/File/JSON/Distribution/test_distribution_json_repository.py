"""
    Test case for the DistributionJSONFileRepository
"""


from sqcommon.adapter.File.JSON.Distribution.distribution_json_repository import \
    DistributionJSONFileRepository
from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict


def test_distribution_json_repository_defines_method_find_by_path():
    """ Test that DistributionJSONFileRepository has method find_by_path(...) """

    find_by_path = getattr(DistributionJSONFileRepository, "find_by_path", None)

    assert callable(find_by_path)


def test_distribution_json_repository_find_method_returns_dict(joint_distribution_file_init):
    """
        Test that DistributionJSONFileRepository.find(...) returns a DistributionAsDict
    """

    returned_value = DistributionJSONFileRepository.find_by_path(joint_distribution_file_init)

    assert isinstance(returned_value, DistributionAsDict)
