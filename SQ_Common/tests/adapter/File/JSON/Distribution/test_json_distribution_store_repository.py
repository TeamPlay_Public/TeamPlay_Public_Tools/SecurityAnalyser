"""
    Test case for the DistributionJSONFileRepository
"""
from sqcommon.adapter.File.JSON.Distribution.json_distribution_store_repository import JSONDistributionStoreRepository


def test_json_distribution_store_repository_defines_method_store():
    """ Test that JSONDistributionStoreRepository has method store(...) """

    store = getattr(JSONDistributionStoreRepository, "store", None)

    assert callable(store)
