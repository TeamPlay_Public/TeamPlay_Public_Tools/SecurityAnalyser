"""
    Testing class related to the usecase DistributionFileRepository and containing testing methods
"""
from sqcommon.adapter.distribution_file_repository import DistributionFileRepository


def test_interface_defines_method_find_by_path():
    """
        Test that the DistributionFileRepository has a method named find_by_path.
    """

    find_by_path = getattr(DistributionFileRepository, "find_by_path", None)

    assert callable(find_by_path)
