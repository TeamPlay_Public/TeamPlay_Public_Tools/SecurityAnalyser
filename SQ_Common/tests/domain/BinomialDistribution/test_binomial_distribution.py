"""
    Testing class related to the domain BinomialDistribution and containing testing methods
"""
from typing import List, Dict

import pytest

from sqcommon.domain.BinomialDistribution.binomial_distribution import BinomialDistribution
from sqcommon.exception.list_length_exception import ListLengthException
from sqcommon.exception.nb_tuples_lower_or_equal_to_one_exception import NbTuplesLowerOrEqualToOneException


def test_binomial_distribution_is_instance_of_interface(binomial_distribution_model_init_nb_tuples_equals_4):
    """ Test whether BinomialDistribution's constructor """

    binomial_distribution = binomial_distribution_model_init_nb_tuples_equals_4

    assert isinstance(binomial_distribution, BinomialDistribution)


def test_binomial_distribution_defines_property_binomial_distribution():
    """
        Test that BinomialDistribution has a property named data.
    """

    data = getattr(BinomialDistribution, "data", None)

    assert isinstance(data, property)


def test_binomial_distribution_returns_a_list(binomial_distribution_model_init_nb_tuples_equals_4):
    """
        Test that BinomialDistribution.compute_binomial_distribution returns a list
    """

    binomial_distribution = binomial_distribution_model_init_nb_tuples_equals_4

    assert isinstance(binomial_distribution.final_binomial_distribution_list, List)


def test_binomial_distribution_data_returns_a_dict(binomial_distribution_model_init_nb_tuples_equals_4):
    """
        Test that BinomialDistribution.data returns a Dict
    """

    binomial_distribution = binomial_distribution_model_init_nb_tuples_equals_4

    assert isinstance(binomial_distribution.data, Dict)


def test_binomial_distribution_returns_an_original_list_with_proper_values(
        binomial_distribution_model_init_nb_tuples_equals_4,
        return_binomial_distribution_for_nb_tuples_equal_4):
    """
        Test that BinomialDistribution.compute_binomial_distribution returns a list
        with values [1 / 8, 3 / 8, 3 / 8, 1 / 8]
    """

    binomial_distribution = binomial_distribution_model_init_nb_tuples_equals_4

    assert binomial_distribution.original_binomial_list == return_binomial_distribution_for_nb_tuples_equal_4


def test_binomial_distribution_returns_a_shuffled_list_different_from_original_list(
        binomial_distribution_model_init_nb_tuples_equals_200):
    """
        Test that BinomialDistribution.compute_binomial_distribution returns two lists
        with elements in different orders
    """

    binomial_distribution = binomial_distribution_model_init_nb_tuples_equals_200

    assert len(binomial_distribution.original_binomial_list) == len(binomial_distribution.shuffled_binomial_list)
    assert binomial_distribution.original_binomial_list != binomial_distribution.shuffled_binomial_list


def test_binomial_distribution_returns_a_shuffled_list_and_an_original_list_which_have_same_elements(
        binomial_distribution_model_init_nb_tuples_equals_200):
    """
        Test that BinomialDistribution.compute_binomial_distribution returns two lists
        containing the same elements in different orders
    """

    binomial_distribution = binomial_distribution_model_init_nb_tuples_equals_200

    assert len(binomial_distribution.original_binomial_list) == len(binomial_distribution.shuffled_binomial_list)
    assert sorted(binomial_distribution.original_binomial_list) == sorted(binomial_distribution.shuffled_binomial_list)


def test_binomial_distribution_convert_to_dict_method_returned_value(
        binomial_distribution_model_init_nb_tuples_equals_4):
    """
        Test that BinomialDistribution.convert_to_dict returns a Dict
    """

    binomial_distribution = binomial_distribution_model_init_nb_tuples_equals_4

    assert isinstance(binomial_distribution.convert_to_dict(), Dict)


def test_binomial_distribution_average_method(
        binomial_distribution_model_init_nb_tuples_equals_4):
    """
        Test that BinomialDistribution.average returns the expected values
    """

    binomial_distribution = binomial_distribution_model_init_nb_tuples_equals_4

    assert pytest.approx(binomial_distribution.average(2, 2), 0.0000001) == 2
    assert pytest.approx(binomial_distribution.average(2, 4), 0.0000001) == 3
    assert pytest.approx(binomial_distribution.average(2.2, 4.4), 0.0000001) == 3.3


def test_binomial_distribution_average_distribution_methode(
        binomial_distribution_model_init_nb_tuples_equals_4,
        return_test_list_for_average_distribution_method_1,
        return_test_list_for_average_distribution_method_2,
        return_expected_result_list_for_average_distribution_method):
    """
        Test that BinomialDistribution.average_distribution returns the expected values,
        ie a list where each element is equal to the average of the related elements of
        the two lists given in parameter.
         Ex: average_distribution([1, 2, 3], [0, 1, 2]) = [1/2, 3/2, 5/2]
    """

    binomial_distribution = binomial_distribution_model_init_nb_tuples_equals_4

    result_list = binomial_distribution.average_distribution(return_test_list_for_average_distribution_method_1,
                                                             return_test_list_for_average_distribution_method_2)

    assert len(result_list) == len(return_expected_result_list_for_average_distribution_method)
    assert all([a == b for a, b in zip(result_list, return_expected_result_list_for_average_distribution_method)])


def test_binomial_distribution_raises_nbtupleslowerorequaltooneexception_nb_tuples_equal_0(
        binomial_distribution_model_init_nb_tuples_equals_4):
    """
        Test that BinomialDistribution.compute_binomial_distribution raises NbTuplesLowerOrEqualToOneException
        if nb_tuples argument is <= 1
    """
    binomial_distribution = binomial_distribution_model_init_nb_tuples_equals_4

    binomial_distribution.nb_tuples = 0
    with pytest.raises(NbTuplesLowerOrEqualToOneException):
        binomial_distribution.compute_binomial_distribution()


def test_binomial_distribution_raises_nbtupleslowerorequaltooneexception_nb_tuples_equal_1(
        binomial_distribution_model_init_nb_tuples_equals_4):
    """
        Test that BinomialDistribution.compute_binomial_distribution raises NbTuplesLowerOrEqualToOneException
        if nb_tuples argument is <= 1
    """
    binomial_distribution = binomial_distribution_model_init_nb_tuples_equals_4

    binomial_distribution.nb_tuples = 1
    with pytest.raises(NbTuplesLowerOrEqualToOneException):
        binomial_distribution.compute_binomial_distribution()


def test_binomial_distribution_average_method_raises_listlengthexception_for_lists_of_different_length(
        binomial_distribution_model_init_nb_tuples_equals_4,
        return_test_list_for_average_distribution_method_1,
        return_test_list_of_length_40_for_average_distribution_method):
    """
        Test that BinomialDistribution.compute_binomial_distribution raises ListLengthException
        if the two lists given in parameter have different length
    """
    binomial_distribution = binomial_distribution_model_init_nb_tuples_equals_4

    with pytest.raises(ListLengthException):
        binomial_distribution.average_distribution(
            return_test_list_for_average_distribution_method_1,
            return_test_list_of_length_40_for_average_distribution_method)
