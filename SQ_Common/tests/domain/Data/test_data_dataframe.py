"""
    Testing class related to the domain DataAsDataFrame and containing testing methods
"""
import pytest
from pandas import DataFrame

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.interface.domain.Data.dataframe_interface import DataFrameInterface


@pytest.fixture
def dataasdataframe_domain_model_init(pandas_dataframe_model_init):
    """ Generate a DataAsDataFrame """

    dataframe = DataAsDataFrame(pandas_dataframe_model_init)

    return dataframe


@pytest.fixture
def pandas_dataframe_model_init():
    """ Initialize a DataFrame """
    col_names = ['A', 'B', 'C']
    my_df = DataFrame(columns=col_names)

    return my_df


def test_dadf_is_instance_of_dataasdataframe(dataasdataframe_domain_model_init):
    """ Test whether DataAsDataFrame is an implementation of DataInterface """

    dadf = dataasdataframe_domain_model_init

    assert isinstance(dadf, DataAsDataFrame)
    assert isinstance(dadf, DataFrameInterface)


def test_dadf_contains_an_instance_of_dataframe(dataasdataframe_domain_model_init):
    """ Test whether DataAsDataFrame contains a Pandas DataFrame """

    dadf = dataasdataframe_domain_model_init

    assert isinstance(dadf.data, DataFrame)
