"""
    Testing class related to the domain DistributionContainer and containing testing methods
"""
from sqcommon.domain.Distribution.distribution_container import DistributionContainer
from sqcommon.interface.domain.Distribution.distribution_container_interface import \
    DistributionContainerInterface


def test_distribution_container_is_instance_of_interface(distribution_container_model_init):
    """ Test whether DistributionContainer is an implementation of DistributionContainerInterface """

    dist_container = distribution_container_model_init

    assert isinstance(dist_container, DistributionContainer)
    assert isinstance(dist_container, DistributionContainerInterface)


# ----------------------------------------------------------------------------------
# tuples-related properties
# ----------------------------------------------------------------------------------

def test_distribution_container_defines_property_secret_tuples():
    """
        Test that DistributionContainer has a property named secret_tuples.
    """

    secret_tuples = getattr(DistributionContainer, "secret_tuples", None)

    assert isinstance(secret_tuples, property)


def test_distribution_container_defines_property_public_tuples():
    """
        Test that DistributionContainer has a property named public_tuples.
    """

    public_tuples = getattr(DistributionContainer, "public_tuples", None)

    assert isinstance(public_tuples, property)


def test_distribution_container_defines_property_leakage_tuples():
    """
        Test that DistributionContainer has a property named leakage_tuples.
    """

    leakage_tuples = getattr(DistributionContainer, "leakage_tuples", None)

    assert isinstance(leakage_tuples, property)


def test_distribution_container_defines_property_public_leakage_tuples():
    """
        Test that DistributionContainer has a property named public_leakage_tuples.
    """

    public_leakage_tuples = getattr(DistributionContainer, "public_leakage_tuples", None)

    assert isinstance(public_leakage_tuples, property)


def test_distribution_container_defines_property_secret_public_tuples():
    """
        Test that DistributionContainer has a property named secret_public_tuples.
    """

    secret_public_tuples = getattr(DistributionContainer, "secret_public_tuples", None)

    assert isinstance(secret_public_tuples, property)


# ----------------------------------------------------------------------------------
# distribution-related properties
# ----------------------------------------------------------------------------------

def test_distribution_container_defines_property_secret_distribution():
    """
        Test that DistributionContainer has a property named secret_distribution.
    """

    secret_distribution = getattr(DistributionContainer, "secret_distribution", None)

    assert isinstance(secret_distribution, property)


def test_distribution_container_defines_property_public_distribution():
    """
        Test that DistributionContainer has a property named public_distribution.
    """

    public_distribution = getattr(DistributionContainer, "public_distribution", None)

    assert isinstance(public_distribution, property)


def test_distribution_container_defines_property_leakage_distribution():
    """
        Test that DistributionContainer has a property named leakage_distribution.
    """

    leakage_distribution = getattr(DistributionContainer, "leakage_distribution", None)

    assert isinstance(leakage_distribution, property)


def test_distribution_container_defines_property_public_leakage_distribution():
    """
        Test that DistributionContainer has a property named public_leakage_distribution.
    """

    public_leakage_distribution = getattr(DistributionContainer, "public_leakage_distribution", None)

    assert isinstance(public_leakage_distribution, property)


def test_distribution_container_defines_property_secret_public_distribution():
    """
        Test that DistributionContainer has a property named secret_public_distribution.
    """

    secret_public_distribution = getattr(DistributionContainer, "secret_public_distribution", None)

    assert isinstance(secret_public_distribution, property)
