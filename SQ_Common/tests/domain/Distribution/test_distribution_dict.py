"""
    Testing class related to the domain DistributionAsDict and containing testing methods
"""
import pytest

from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict


def test_distributionasdict_is_instance_of_distributionasdict(return_distributionasdict_secret):
    """ Test whether DistributionAsDict constructor returns a correctly setup DistributionAsDict object """

    distributionasdict = return_distributionasdict_secret

    assert isinstance(distributionasdict, DistributionAsDict)


def test_distributionasdict_defines_property_data():
    """
        Test that DistributionAsDict has a property named data.
    """

    data = getattr(DistributionAsDict, "data", None)

    assert isinstance(data, property)


def test_distributionasdict_constructor_raises_value_error_exception(
        return_erroneous_distribution_as_dict_secret_public_indiscernability_bl):
    """ Test that DistributionAsDict constructor raises an Exception ValueError
        due to the fact that
    """

    with pytest.raises(Exception):
        DistributionAsDict(return_erroneous_distribution_as_dict_secret_public_indiscernability_bl)
