"""
    Testing class related to the domain Distribution and containing testing methods
"""
from sqcommon.domain.Distribution.distribution_for_tuples import Distribution


def test_distribution_is_instance_of_distribution(tuples_distribution_model_init_for_a):
    """ Test whether Distribution constructor returns a correctly setup Distribution object """

    distribution = tuples_distribution_model_init_for_a

    assert isinstance(distribution, Distribution)


def test_distribution_defines_property_dist_tuples():
    """
        Test that Distribution has a property named dist_tuples.
    """

    dist_tuples = getattr(Distribution, "dist_tuples", None)

    assert isinstance(dist_tuples, property)


def test_distribution_defines_property_distribution():
    """
        Test that Distribution has a property named distribution.
    """

    distribution = getattr(Distribution, "distribution", None)

    assert isinstance(distribution, property)
