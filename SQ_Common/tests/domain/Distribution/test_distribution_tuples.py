"""
    Testing class related to the domain DistributionTuples and containing testing methods
"""
from sqcommon.domain.Distribution.distribution_tuples import DistributionTuples


def test_distributionasdict_is_instance_of_distributiontuples(distribution_tuples_model_init_for_leakage):
    """ Test whether DistributionTuples constructor returns a correctly setup DistributionTuples object """

    distribution_tuples = distribution_tuples_model_init_for_leakage

    assert isinstance(distribution_tuples, DistributionTuples)


def test_distribution_defines_property_all_the_tuples():
    """
        Test that DistributionTuples has a property named all_the_tuples.
    """

    all_the_tuples = getattr(DistributionTuples, "all_the_tuples", None)

    assert isinstance(all_the_tuples, property)


def test_distribution_defines_property_sorted_tuples_as_dict():
    """
        Test that DistributionTuples has a property named sorted_tuples_as_dict.
    """

    tuples_as_dict = getattr(DistributionTuples, "tuples_as_dict", None)

    assert isinstance(tuples_as_dict, property)


def test_distribution_defines_property_sorted_tuples_as_list():
    """
        Test that DistributionTuples has a property named tuples_as_list_no_duplicate.
    """

    tuples_as_list_no_duplicate = getattr(DistributionTuples, "tuples_as_list_no_duplicate", None)

    assert isinstance(tuples_as_list_no_duplicate, property)


def test_distribution_defines_property_related_secret_tuples():
    """
        Test that DistributionTuples has a property named related_secret_tuples.
    """

    related_secret_tuples = getattr(DistributionTuples, "related_secret_tuples", None)

    assert isinstance(related_secret_tuples, property)
