"""
    Testing class related to the domain RandomShuffle and containing testing methods
"""
import copy

from sqcommon.domain.Shuffle.numpy_random_shuffle import NumpyRandomShuffle


def test_random_shuffle_is_instance_of_random_shuffle(random_shuffle_model_init):
    """ Test whether NumpyRandomShuffle constructor returns a correctly setup RandomShuffle object """

    random_shuffle = random_shuffle_model_init

    assert isinstance(random_shuffle, NumpyRandomShuffle)


def test_random_shuffle_defines_property_list_to_shuffle():
    """
        Test that NumpyRandomShuffle has a property named list_to_shuffle.
    """

    list_to_shuffle = getattr(NumpyRandomShuffle, "list_to_shuffle", None)

    assert isinstance(list_to_shuffle, property)


def test_original_list_not_equal_to_final_list(random_shuffle_model_init):
    """
        Test that NumpyRandomShuffle do shuffle the list.
    """

    random_shuffle = random_shuffle_model_init

    original_list = copy.deepcopy(random_shuffle.list_to_shuffle)

    final_list = random_shuffle.shuffle()

    assert original_list != final_list
