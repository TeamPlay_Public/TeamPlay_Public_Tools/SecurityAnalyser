"""
    Testing class related to the domain SideChannelClass and containing testing methods
"""
import pytest

from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.interface.domain.SideChannel.side_channel_class_interface import SideChannelClassInterface


@pytest.fixture
def sdc_model_init():
    """ Generate an instance of SideChannelClass """
    sdc = SideChannelClass(name="TIME_WORST",
                           value="time_worst")

    return sdc


def test_sdc_is_instance_of_sidechannel(sdc_model_init):
    """ Test whether SideChannelClass is an implementation of SideChannelClassInterface """

    sdc = sdc_model_init

    assert isinstance(sdc, SideChannelClass)
    assert isinstance(sdc, SideChannelClassInterface)


def test_sdc_value_equals_time_worst(sdc_model_init):
    """ Test that sdc contains the right value attribute """

    sdc = sdc_model_init

    assert sdc.value == 'time_worst'


def test_sdc_name_equals_time_worst(sdc_model_init):
    """ Test that sdc contains the right name attribute """

    sdc = sdc_model_init

    assert sdc.name == 'TIME_WORST'
