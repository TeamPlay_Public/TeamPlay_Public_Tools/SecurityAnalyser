"""
    Testing class related to the domain TuplesInformation and containing testing methods
"""
import pytest

from sqcommon.domain.Tuples.tuples_info import TuplesInformation
from sqcommon.interface.domain.Tuples.tuples_info_interface import TuplesInformationInterface


@pytest.fixture
def tuples_info_model_init():
    """ Generate an instance of tuples_info_model_init """
    tuples_info = TuplesInformation(nb_secret_tuples=1,
                                    nb_public_tuples=2,
                                    total_nb_tuples=12)

    return tuples_info


def test_tuples_info_is_instance_of_tuples_info(tuples_info_model_init):
    """ Test TuplesInformation's constructor' """

    tuples_info = tuples_info_model_init

    assert isinstance(tuples_info, TuplesInformation)
    assert isinstance(tuples_info, TuplesInformationInterface)


def test_tuples_info_nb_tainted_tuples_equals_1(tuples_info_model_init):
    """ Test that tuples_info contains the right nb_tainted_tuples attribute """

    tuples_info = tuples_info_model_init

    assert tuples_info.nb_secret_tuples == 1
    assert isinstance(tuples_info.nb_secret_tuples, int)


def test_tuples_info_nb_untainted_tuples_equals_2(tuples_info_model_init):
    """ Test that tuples_info contains the right nb_untainted_tuples attribute """

    tuples_info = tuples_info_model_init

    assert tuples_info.nb_public_tuples == 2
    assert isinstance(tuples_info.nb_public_tuples, int)


def test_tuples_info_nb_tuples_equals_12(tuples_info_model_init):
    """ Test that tuples_info contains the right nb_tuples attribute """

    tuples_info = tuples_info_model_init

    assert tuples_info.total_nb_tuples == 12
    assert isinstance(tuples_info.total_nb_tuples, int)
