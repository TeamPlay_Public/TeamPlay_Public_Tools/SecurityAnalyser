"""
    Testing class related to the enum EnumDistributionTuples and containing testing methods
"""
from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples


def test_enum_distribution_tuples_model_init():
    """ Test the EnumDistributionTuples class constructor """

    return EnumDistributionTuples.SECRET


def test_edt_is_instance_of_enum_distribution_tuples():
    """ Test whether EnumDistributionTuples is an implementation of EnumDistributionTuples """

    edt = test_enum_distribution_tuples_model_init()

    assert isinstance(edt, EnumDistributionTuples)


def test_enum_edt_value_equal_time_worst():
    """ Test EnumDistributionTuples enums for value """

    edt_enum = test_enum_distribution_tuples_model_init()

    assert edt_enum.value == "secret"


def test_enum_edt_name_equal_time_worst():
    """ Test EnumDistributionTuples for name """

    edt_enum = test_enum_distribution_tuples_model_init()

    assert edt_enum.name == "SECRET"
