"""
    Testing class related to the domain HDF5Attributes and containing testing methods
"""
from sqcommon.enums.enum_hdf5_attributes import HDF5Attributes


def test_name_in_enum():
    """ Test that HDF5Attributes Enum contains the right names """

    assert 'NB_TAINTED' in HDF5Attributes._member_names_
    assert 'NB_UNTAINTED' in HDF5Attributes._member_names_
    assert 'VARIABLE_A' in HDF5Attributes._member_names_
    assert 'VARIABLE_N' in HDF5Attributes._member_names_
    assert 'VARIABLE_SECRET' in HDF5Attributes._member_names_


def test_value_in_enum():
    """ Test that HDF5Attributes Enum contains the right values """

    values = set(item.value for item in HDF5Attributes)

    assert "nbTainted" in values
    assert "nbUntainted" in values
    assert "a" in values
    assert "n" in values
    assert "secret" in values
