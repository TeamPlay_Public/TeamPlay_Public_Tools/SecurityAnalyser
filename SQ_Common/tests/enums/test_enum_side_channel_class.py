"""
    Testing class related to the domain SideChannelClasses and containing testing methods
"""
from sqcommon.enums.enum_side_channel_class import SideChannelClasses


def test_name_in_enum():
    """ Test that SideChannelClasses Enum contains the right names """

    assert 'TIME' in SideChannelClasses._member_names_
    assert 'TIME_WORST' in SideChannelClasses._member_names_
    assert 'TIME_AVG' in SideChannelClasses._member_names_
    assert 'ENERGY' in SideChannelClasses._member_names_
    assert 'ENERGY_WORST' in SideChannelClasses._member_names_
    assert 'ENERGY_AVG' in SideChannelClasses._member_names_
    assert 'POWER' in SideChannelClasses._member_names_
    assert 'POWER_WORST' in SideChannelClasses._member_names_
    assert 'POWER_AVG' in SideChannelClasses._member_names_


def test_value_in_enum():
    """ Test that SideChannelClasses Enum contains the right values """

    values = set(item.value for item in SideChannelClasses)

    assert 'time' in values
    assert 'time_worst' in values
    assert 'time_avg' in values
    assert 'energy' in values
    assert 'energy_worst' in values
    assert 'energy_avg' in values
    assert 'power' in values
    assert 'power_worst' in values
    assert 'power_avg' in values
