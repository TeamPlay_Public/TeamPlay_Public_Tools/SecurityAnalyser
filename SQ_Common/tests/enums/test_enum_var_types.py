"""
    Testing class related to the domain VariableTypes and containing testing methods
"""
from sqcommon.enums.enum_var_types import VariableTypes


def test_name_in_enum():
    """ Test that VariableTypes Enum contains the right names """

    assert 'TAINTED' in VariableTypes._member_names_
    assert 'UNTAINTED' in VariableTypes._member_names_


def test_value_in_enum():
    """ Test that VariableTypes Enum contains the right values """

    values = set(item.value for item in VariableTypes)

    assert "tainted" in values
    assert "untainted" in values
