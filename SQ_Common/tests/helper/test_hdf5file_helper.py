"""
    Testing class related to the domain HDF5FileHelper and containing testing methods
"""
import os
from typing import List

from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.helper.hdf5file_helper import HDF5FileHelper


def test_method_get_hdf5_keys_as_list(return_input_file_secret5a10n20_time_worst,
                                      returns_time_worst):
    """ Test the get_hdf5_keys_as_list(hdf5_file)-> List static method """
    cwd = os.getcwd()
    returned_value = HDF5FileHelper.get_hdf5_keys_as_list(hdf5_file=return_input_file_secret5a10n20_time_worst)

    assert isinstance(returned_value, List)
    assert isinstance(returned_value[0], str)
    assert returned_value[0] == returns_time_worst
    assert len(returned_value) == 1


def test_method_count_variables(return_input_file_secret5a10n20_time_worst,
                                returns_time_worst,
                                return_var_type_tainted,
                                return_var_type_untainted):
    """ Test count_variables(hdf5_file: str,dataset: str,hdf5_attribute: str) -> int static method """

    returned_value = HDF5FileHelper.count_variables(hdf5_file=return_input_file_secret5a10n20_time_worst,
                                                    dataset=returns_time_worst,
                                                    hdf5_attribute=return_var_type_tainted)

    assert isinstance(returned_value, int)
    assert returned_value == 1

    returned_value = HDF5FileHelper.count_variables(hdf5_file=return_input_file_secret5a10n20_time_worst,
                                                    dataset=returns_time_worst,
                                                    hdf5_attribute=return_var_type_untainted)

    assert isinstance(returned_value, int)
    assert returned_value == 2


def test_method_get_variable_names_as_list(return_input_file_secret5a10n20_time_worst):
    """ Test get_variable_names_as_list(hdf5_file: str) -> VariablesList static method """

    returned_value = HDF5FileHelper.get_variable_names_as_list(hdf5_file=return_input_file_secret5a10n20_time_worst)

    # Gathering values from VariablesList in a simple list
    values = set(var.value for var in returned_value.lst)

    # then check that we have the expected values
    assert "a" in values
    assert "secret" in values
    assert "n" in values

    assert isinstance(returned_value, VariablesList)
    assert len(returned_value.lst) == 3
