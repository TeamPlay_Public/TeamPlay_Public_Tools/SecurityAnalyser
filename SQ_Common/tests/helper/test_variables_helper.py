"""
    Testing class related to the domain VariableHelper and containing testing methods
"""
from typing import List

from sqcommon.domain.Tuples.tuples_info import TuplesInformation
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.helper.variables_helper import VariableHelper


def test_method_list_card_per_var_type(return_data_col_names_as_list,
                                       return_nb_secret_equals_1,
                                       return_variable_with_value_a_10,
                                       return_variable_with_value_n_20,
                                       return_variable_with_value_secret_5,
                                       return_secret_vars_cards_equals_5_as_list,
                                       return_public_vars_cards_equals_10_20_as_list
                                       ):
    """ Test static method list_card_per_var_type """
    var_cards = VariablesList()
    var_cards.lst.append(return_variable_with_value_a_10)
    var_cards.lst.append(return_variable_with_value_n_20)
    var_cards.lst.append(return_variable_with_value_secret_5)

    secret, public = VariableHelper.list_nb_occurrences_per_var_type(
        data_col_names_as_list=return_data_col_names_as_list,
        nb_secret_vars=return_nb_secret_equals_1,
        variables_cardinalities=var_cards)

    assert isinstance(secret, List)
    assert isinstance(public, List)
    assert secret == return_secret_vars_cards_equals_5_as_list
    assert public == return_public_vars_cards_equals_10_20_as_list


def test_method_total_nb_of_occurrences(return_secret_vars_cards_equals_5_as_list,
                                        return_public_vars_cards_equals_10_20_as_list,
                                        return_nb_rows_equals_1000):
    """ Test static method total_nb_of_occurrences """

    returned_value = VariableHelper.total_nb_of_occurrences(
        secret_vars_nb_occurrences=return_secret_vars_cards_equals_5_as_list,
        public_vars_nb_occurrences=return_public_vars_cards_equals_10_20_as_list)

    assert isinstance(returned_value, int)
    assert returned_value == return_nb_rows_equals_1000


def test_method_nb_of_occurrences_for_var_of_given_type_with_public(return_public_vars_cards_equals_10_20_as_list,
                                                                    return_nb_public_tuples_equals_200):
    """ Test static method nb_of_occurrences_for_var_of_given_type for public """

    returned_value = VariableHelper.nb_of_occurrences_for_var_of_given_type(
        vars_nb_occurrences=return_public_vars_cards_equals_10_20_as_list)

    assert isinstance(returned_value, int)
    assert returned_value == return_nb_public_tuples_equals_200


def test_method_nb_of_occurrences_for_var_of_given_type_with_secret(return_secret_vars_cards_equals_5_as_list,
                                                                    return_nb_secret_tuples_equals_5):
    """ Test static method nb_of_occurrences_for_var_of_given_type for secret """

    returned_value = \
        VariableHelper.nb_of_occurrences_for_var_of_given_type(
            vars_nb_occurrences=return_secret_vars_cards_equals_5_as_list)

    assert isinstance(returned_value, int)
    assert returned_value == return_nb_secret_tuples_equals_5


def test_method_get_tuples_information(return_data_col_names_as_list,
                                       return_nb_secret_equals_1,
                                       return_variable_with_value_a_10,
                                       return_variable_with_value_n_20,
                                       return_variable_with_value_secret_5,
                                       return_nb_secret_tuples_5,
                                       return_nb_public_tuples_200,
                                       return_nb_tuples_1000
                                       ):
    """ Test static method get_tuples_information """
    var_cards = VariablesList()
    var_cards.lst.append(return_variable_with_value_a_10)
    var_cards.lst.append(return_variable_with_value_n_20)
    var_cards.lst.append(return_variable_with_value_secret_5)

    tuple_information = VariableHelper.get_tuples_information(
        data_col_names_as_list=return_data_col_names_as_list,
        nb_secret_vars=return_nb_secret_equals_1,
        variables_cardinalities=var_cards)

    assert isinstance(tuple_information, TuplesInformation)

    assert tuple_information.total_nb_tuples == return_nb_tuples_1000
    assert tuple_information.nb_secret_tuples == return_nb_secret_tuples_5
    assert tuple_information.nb_public_tuples == return_nb_public_tuples_200
