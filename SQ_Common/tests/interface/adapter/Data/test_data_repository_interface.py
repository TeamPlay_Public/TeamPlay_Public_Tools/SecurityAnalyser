"""
    Test case for the DataRepositoryInterface interface
"""
from sqcommon.interface.adapter.Data.data_repository_interface import DataRepositoryInterface


def test_interface_defines_method_find():
    """ Test that the interface has method find(...) """

    find = getattr(DataRepositoryInterface, "find", None)

    assert callable(find)
