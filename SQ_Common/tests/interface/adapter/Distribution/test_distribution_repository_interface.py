"""
    Test case for the DistributionRepositoryInterface interface
"""
from sqcommon.interface.adapter.Distribution.distribution_repository_interface import \
    DistributionRepositoryInterface


def test_interface_defines_method_find_by_path():
    """ Test that the interface has method find_by_path(...) """

    find_by_path = getattr(DistributionRepositoryInterface, "find_by_path", None)

    assert callable(find_by_path)
