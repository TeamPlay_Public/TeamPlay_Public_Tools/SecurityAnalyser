"""
    Test case for the SideChannelClassRepositoryInterface interface
"""

from sqcommon.interface.adapter.SideChannel.side_channel_class_repository_interface import \
    SideChannelClassRepositoryInterface


def test_interface_defines_method_find():
    """ Test that the interface has method find(...) """

    find = getattr(SideChannelClassRepositoryInterface, "find", None)

    assert callable(find)
