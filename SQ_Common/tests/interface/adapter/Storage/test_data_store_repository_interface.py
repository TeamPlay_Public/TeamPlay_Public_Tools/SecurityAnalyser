"""
    Test case for the DataStoreRepositoryInterface interface
"""
from sqcommon.interface.adapter.Storage.data_store_repository_interface import DataStoreRepositoryInterface


def test_interface_defines_method_store():
    """ Test that the interface has method store(...) """

    store = getattr(DataStoreRepositoryInterface, "store", None)

    assert callable(store)
