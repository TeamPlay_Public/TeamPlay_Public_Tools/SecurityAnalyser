"""
    Test case for the DistributionStoreRepositoryInterface interface
"""
from sqcommon.interface.adapter.Storage.distribution_store_repository_interface import \
    DistributionStoreRepositoryInterface


def test_interface_defines_method_store():
    """ Test that the interface has method store(...) """

    store = getattr(DistributionStoreRepositoryInterface, "store", None)

    assert callable(store)
