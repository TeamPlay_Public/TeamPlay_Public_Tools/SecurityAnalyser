"""
    Test case for the VariablesCardinalityRepositoryInterface interface
"""
from sqcommon.interface.adapter.Variable.vars_cardinality_repository_interface import \
    VariablesCardinalityRepositoryInterface


def test_interface_defines_method_find_all():
    """ Test that the interface has method find_all(...) """

    find_all = getattr(VariablesCardinalityRepositoryInterface, "find_all", None)

    assert callable(find_all)
