"""
    Test case for the VariablesCountRepositoryInterface interface
"""

from sqcommon.interface.adapter.Variable.vars_count_repository_interface import VariablesCountRepositoryInterface


def test_interface_defines_method_count():
    """ Test that the interface has method count(...) """

    count = getattr(VariablesCountRepositoryInterface, "count", None)

    assert callable(count)
