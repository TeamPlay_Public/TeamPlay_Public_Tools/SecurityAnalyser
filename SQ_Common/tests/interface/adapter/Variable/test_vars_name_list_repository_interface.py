"""
    Test case for the VariablesNamesListRepositoryInterface interface
"""

from sqcommon.interface.adapter.Variable.vars_name_list_repository_interface import \
    VariablesNamesListRepositoryInterface


def test_interface_defines_method_find_all():
    """ Test that the interface has method find_all(...) """

    find_all = getattr(VariablesNamesListRepositoryInterface, "find_all", None)

    assert callable(find_all)
