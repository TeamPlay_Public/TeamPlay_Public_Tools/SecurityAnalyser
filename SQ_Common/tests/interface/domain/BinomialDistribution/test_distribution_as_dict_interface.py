"""
    Test case for the DictInterface interface
"""
from sqcommon.interface.domain.BinomialDistribution.distribution_as_dict_interface import DictInterface


def test_interface_defines_method_data():
    """ Test that the interface has property data(...) """

    data = getattr(DictInterface, "data", None)

    assert isinstance(data, property)
