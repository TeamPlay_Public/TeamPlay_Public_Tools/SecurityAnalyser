"""
    Test case for the DataInterface interface
"""
from sqcommon.interface.domain.Data.data_interface import DataInterface


def test_interface_defines_method_data():
    """ Test that the interface has property data(...) """

    data = getattr(DataInterface, "data", None)

    assert isinstance(data, property)
