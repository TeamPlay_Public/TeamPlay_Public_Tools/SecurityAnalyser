"""
    Test case for the DataFrameInterface interface
"""
from sqcommon.interface.domain.Data.dataframe_interface import DataFrameInterface


def test_interface_defines_method_data():
    """
        Test that the interface has property data.
    """

    data = getattr(DataFrameInterface, "data", None)

    assert isinstance(data, property)
