"""
    Test case for the DistributionContainerInterface interface
"""
from sqcommon.interface.domain.Distribution.distribution_container_interface import \
    DistributionContainerInterface


def test_interface_defines_property_secret():
    """
        Test that the interface has a property named secret.
    """

    secret = getattr(DistributionContainerInterface, "secret", None)

    assert isinstance(secret, property)


def test_interface_defines_property_public():
    """
        Test that the interface has a property named public.
    """

    public = getattr(DistributionContainerInterface, "public", None)

    assert isinstance(public, property)


def test_interface_defines_property_leakage():
    """
        Test that the interface has a property named leakage.
    """

    leakage = getattr(DistributionContainerInterface, "leakage", None)

    assert isinstance(leakage, property)


def test_interface_defines_property_public_leakage():
    """
        Test that the interface has a property named public_leakage.
    """

    public_leakage = getattr(DistributionContainerInterface, "public_leakage", None)

    assert isinstance(public_leakage, property)


def test_interface_defines_property_secret_public():
    """
        Test that the interface has a property named secret_public.
    """

    secret_public = getattr(DistributionContainerInterface, "secret_public", None)

    assert isinstance(secret_public, property)
