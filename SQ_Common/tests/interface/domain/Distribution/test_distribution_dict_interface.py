"""
    Test case for the DistributionDictInterface interface
"""
from sqcommon.interface.domain.Distribution.distribution_dict_interface import DistributionDictInterface


def test_interface_defines_property_secret():
    """
        Test that the interface has a property named data.
    """

    data = getattr(DistributionDictInterface, "data", None)

    assert isinstance(data, property)
