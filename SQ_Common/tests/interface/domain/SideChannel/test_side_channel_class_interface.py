"""
    Test case for the SideChannelClassInterface interface
"""
from sqcommon.interface.domain.SideChannel.side_channel_class_interface import SideChannelClassInterface


def test_interface_defines_method_name():
    """ Test that the interface has property name(...) """

    name = getattr(SideChannelClassInterface, "name", None)

    assert isinstance(name, property)


def test_interface_defines_method_value():
    """ Test that the interface has property value(...) """

    value = getattr(SideChannelClassInterface, "value", None)

    assert isinstance(value, property)
