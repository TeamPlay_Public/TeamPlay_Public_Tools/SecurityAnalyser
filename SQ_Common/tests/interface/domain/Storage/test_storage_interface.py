"""
    Test case for the StorageInterface interface
"""
from sqcommon.interface.domain.Storage.storage_interface import StorageInterface


def test_interface_defines_method_name():
    """ Test that the interface has property name(...) """

    name = getattr(StorageInterface, "name", None)

    assert isinstance(name, property)
