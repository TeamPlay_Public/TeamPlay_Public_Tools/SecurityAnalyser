"""
    Test case for the TuplesInformationInterface interface
"""
from sqcommon.interface.domain.Tuples.tuples_info_interface import TuplesInformationInterface


def test_interface_defines_method_nb_secret_tuples():
    """ Test that the interface has property nb_secret_tuples(...) """

    nb_secret_tuples = getattr(TuplesInformationInterface, "nb_secret_tuples", None)

    assert isinstance(nb_secret_tuples, property)


def test_interface_defines_method_nb_public_tuples():
    """ Test that the interface has property nb_public_tuples(...) """

    nb_public_tuples = getattr(TuplesInformationInterface, "nb_public_tuples", None)

    assert isinstance(nb_public_tuples, property)


def test_interface_defines_method_total_nb_tuples():
    """ Test that the interface has property total_nb_tuples(...) """

    total_nb_tuples = getattr(TuplesInformationInterface, "total_nb_tuples", None)

    assert isinstance(total_nb_tuples, property)
