"""
    Test case for the VariableInterface interface
"""
from sqcommon.interface.domain.Variable.variable_interface import VariableInterface


def test_interface_defines_method_value():
    """ Test that the interface has property value(...) """

    value = getattr(VariableInterface, "value", None)

    assert isinstance(value, property)


def test_interface_defines_method_cardinality():
    """ Test that the interface has property cardinality(...) """

    cardinality = getattr(VariableInterface, "cardinality", None)

    assert isinstance(cardinality, property)
