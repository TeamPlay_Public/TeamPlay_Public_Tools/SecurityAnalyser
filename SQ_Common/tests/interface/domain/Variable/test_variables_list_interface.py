"""
    Test case for the VariablesListInterface interface
"""
from sqcommon.interface.domain.Variable.variables_list_interface import VariablesListInterface


def test_interface_defines_method_lst():
    """ Test that the interface has property lst(...) """

    lst = getattr(VariablesListInterface, "lst", None)

    assert isinstance(lst, property)


def test_interface_defines_method_nb_vars():
    """ Test that the interface has property nb_vars(...) """

    nb_vars = getattr(VariablesListInterface, "nb_vars", None)

    assert callable(nb_vars)
