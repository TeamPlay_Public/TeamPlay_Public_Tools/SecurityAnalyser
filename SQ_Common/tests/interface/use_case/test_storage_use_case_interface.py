"""
    Test case for the StorageUseCaseInterface interface
"""
from sqcommon.interface.use_case.storage_use_case_interface import StorageUseCaseInterface


def test_interface_defines_method_execute():
    """ Test that the interface has property execute(...) """

    execute = getattr(StorageUseCaseInterface, "execute", None)

    assert callable(execute)
