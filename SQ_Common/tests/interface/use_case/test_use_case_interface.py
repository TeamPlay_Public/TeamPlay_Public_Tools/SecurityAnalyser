"""
    Test case for the UseCaseInterface interface
"""
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface


def test_interface_defines_method_execute():
    """ Test that the interface has property execute(...) """

    execute = getattr(UseCaseInterface, "execute", None)

    assert callable(execute)
