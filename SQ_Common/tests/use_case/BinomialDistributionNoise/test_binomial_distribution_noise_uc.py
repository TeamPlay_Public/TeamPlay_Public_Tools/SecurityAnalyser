"""
    Testing class related to the usecase BinomialDistributionNoiseUC and containing testing methods
"""

import pytest
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from sqcommon.use_case.BinomialDistributionNoise.binomial_distribution_noise_uc import BinomialDistributionNoiseUC


@pytest.fixture
def binomial_distribution_uc_model_init(json_generated_binomial_distribution_file_init,
                                        return_nb_tuples_4,
                                        return_distribution_type_secret_public,
                                        random_shuffle_model_init,
                                        return_json_file_storage,
                                        return_json_distribution_store_repository
                                        ):
    """ Generate an instance of DistributionComputeUseCase """
    binomial_distribution_noise_uc = BinomialDistributionNoiseUC(
        json_output_file=json_generated_binomial_distribution_file_init,
        nb_tuples=return_nb_tuples_4,
        distribution_type=return_distribution_type_secret_public,
        shuffle_function=random_shuffle_model_init,
        storage_data=return_json_file_storage,
        storage_repo=return_json_distribution_store_repository)

    return binomial_distribution_noise_uc


def test_binomial_distribution_uc_is_instance_of_interface(binomial_distribution_uc_model_init):
    """ Test BinomialDistributionNoiseUC constructor """

    binomial_distribution_noise_uc = binomial_distribution_uc_model_init

    assert isinstance(binomial_distribution_noise_uc, BinomialDistributionNoiseUC)
    assert isinstance(binomial_distribution_noise_uc, UseCaseInterface)


def test_binomial_distribution_uc_defines_method_execute():
    """
        Test that the BinomialDistributionNoiseUC has a method named execute.
    """

    execute = getattr(BinomialDistributionNoiseUC, "execute", None)

    assert callable(execute)
