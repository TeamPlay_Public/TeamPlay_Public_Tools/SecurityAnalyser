"""
    Test case for the HDF5DataAsDataFrameGetUC class
"""
from unittest.mock import Mock

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.use_case.Data.hdf5_data_as_dataframe_get import HDF5DataAsDataFrameGetUC


def test_uc_hdf5_datadataframe_get(data_as_dataframe_distance_equal_0_init,
                                   return_input_file_secret5a10n20_time_worst):
    """ Test the HDF5DataAsDataFrameGetUC execute method return value """
    # Creating the Mock
    repo = Mock()

    # Instantiating the UseCase with repo as a mock
    uc = HDF5DataAsDataFrameGetUC(data_repo=repo,
                                  input_path=return_input_file_secret5a10n20_time_worst)

    # Setting the return value when calling the method find on the mock
    repo.find.return_value = data_as_dataframe_distance_equal_0_init

    # Executing the execute method of the UseCase, which calls the method find() from the mock
    result = uc.execute()
    # Assert find was called with the proper argument
    repo.find.assert_called_with(return_input_file_secret5a10n20_time_worst)

    # check we have the correct return value
    assert result == data_as_dataframe_distance_equal_0_init

    # Check the return value is of correct type
    assert isinstance(result, DataAsDataFrame)
