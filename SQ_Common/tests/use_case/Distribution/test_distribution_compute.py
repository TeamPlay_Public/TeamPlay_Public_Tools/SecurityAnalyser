"""
    Testing class related to the usecase DistributionComputeUseCase and containing testing methods
"""

import pytest
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from sqcommon.domain.Distribution.distribution_container import DistributionContainer
from sqcommon.use_case.Distribution.distribution_compute import DistributionComputeUseCase


@pytest.fixture
def distribution_compute_uc_model_init(data_as_dataframe_from_indiscernability_bl,
                                       return_distributionasdict_secret,
                                       return_nb_secret_equals_1,
                                       return_nb_public_equals_2,
                                       return_variables_list_a2_n2_secret3):
    """ Generate an instance of DistributionComputeUseCase """
    distribution_uc = DistributionComputeUseCase(
        data=data_as_dataframe_from_indiscernability_bl,
        distribution_as_dict=return_distributionasdict_secret,
        nb_tainted_vars=return_nb_secret_equals_1,
        nb_untainted_vars=return_nb_public_equals_2,
        variables_card=return_variables_list_a2_n2_secret3)

    return distribution_uc


def test_distribution_compute_uc_is_instance_of_interface(
        distribution_compute_uc_model_init):
    """ Test whether DistributionComputeUseCase is an implementation of UseCaseInterface """

    distribution_uc = distribution_compute_uc_model_init

    assert isinstance(distribution_uc, DistributionComputeUseCase)
    assert isinstance(distribution_uc, UseCaseInterface)


def test_distribution_compute_uc_defines_method_execute():
    """
        Test that the DistributionComputeUseCase has a method named execute.
    """

    execute = getattr(DistributionComputeUseCase, "execute", None)

    assert callable(execute)


def test_distribution_compute_uc_method_execute_returns_distribution_container(distribution_compute_uc_model_init):
    """
        Test that the DistributionComputeUseCase execute method returns a DistributionContainer.
    """
    distribution_uc = distribution_compute_uc_model_init

    returned = distribution_uc.execute()

    assert isinstance(returned, DistributionContainer)


def test_distribution_compute_uc_defines_method_compute_distribution_for_indices():
    """
        Test that the DistributionComputeUseCase has a method named compute_distribution_for_indices.
    """

    compute_distribution_for_indices = getattr(DistributionComputeUseCase, "compute_distribution_for_indices", None)

    assert callable(compute_distribution_for_indices)


def test_distribution_compute_uc_defines_method_gather_tuples():
    """
        Test that the DistributionComputeUseCase has a method named gather_tuples.
    """

    gather_tuples = getattr(DistributionComputeUseCase, "gather_tuples", None)

    assert callable(gather_tuples)


def test_distribution_compute_uc_defines_method_extract_tuples_info():
    """
        Test that the DistributionComputeUseCase has a method named extract_tuples_info.
    """

    extract_tuples_info = getattr(DistributionComputeUseCase, "extract_tuples_info", None)

    assert callable(extract_tuples_info)
