"""
    Testing class related to the usecase DistributionGetUseCase and containing testing methods
"""
from unittest.mock import Mock

from sqcommon.domain.Distribution.distribution_container import DistributionContainer
from sqcommon.use_case.Distribution.distribution_json_get import DistributionJSONGetUseCase


def distribution_get_uc_test_returned_value(distribution_container_model_init,
                                            full_distribution_file_init):
    """ Test the DistributionComputeUseCase execute method return value """
    # Creating the Mock
    repo = Mock()

    # Instantiating the UseCase with repo as a mock
    uc = DistributionJSONGetUseCase(distribution_repository=repo,
                                    dist_file=full_distribution_file_init)

    # Setting the return value when calling the method find on the mock
    repo.find_by_path.return_value = distribution_container_model_init

    # Executing the execute method of the UseCase, which calls the method find_by_path() from the mock
    result = uc.execute()

    # Assert find was called with the proper argument
    repo.find.assert_called_with(full_distribution_file_init)

    # check we have the correct return value
    assert result == distribution_container_model_init

    # Check the return value is of correct type
    assert isinstance(result, DistributionContainer)


def test_distribution_get_uc_defines_method_execute():
    """
        Test that the DistributionComputeUseCase has a method named execute.
    """

    execute = getattr(DistributionJSONGetUseCase, "execute", None)

    assert callable(execute)
