"""
    Test case for the SideChannelClassGetUC class
"""
from unittest.mock import Mock

from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.use_case.SideChannel.side_channel_class_get import SideChannelClassGetUC


def test_uc_sidechannel_get(return_sidechannel_with_value_time_worst,
                            return_input_file_secret5a10n20_time_worst):
    """ Test the SideChannelClassGetUC execute method return value """
    # Creating the Mock
    repo = Mock()

    # Instantiating the UseCase with repo as a mock
    uc = SideChannelClassGetUC(side_channel_class_repo=repo,
                               input_path=return_input_file_secret5a10n20_time_worst)

    # Setting the return value when calling the method find on the mock
    repo.find.return_value = return_sidechannel_with_value_time_worst

    # Executing the execute method of the UseCase, which calls the method find() from the mock
    result = uc.execute()
    # Assert find was called with the proper argument
    repo.find.assert_called_with(return_input_file_secret5a10n20_time_worst)

    # check we have the correct return value
    assert result == return_sidechannel_with_value_time_worst

    # Check the return value is of correct type
    assert isinstance(result, SideChannelClass)
