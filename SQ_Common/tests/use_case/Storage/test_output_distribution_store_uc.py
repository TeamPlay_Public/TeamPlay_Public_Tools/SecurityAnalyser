"""
    Test case for the OutputDistributionStoreUC class
"""
from unittest.mock import Mock

from sqcommon.use_case.Storage.output_distribution_store_uc import OutputDistributionStoreUC


def test_output_distribution_store_uc(return_sidechannel_with_value_time_worst,
                                      return_input_file_secret5a10n20_time_worst):
    """ Test the OutputDistributionStoreUC execute method return value """
    # Creating the Mock
    repo = Mock()

    # Instantiating the UseCase with repo as a mock
    uc = OutputDistributionStoreUC(output_storage_repo=repo,
                                   data=return_input_file_secret5a10n20_time_worst,
                                   storage_data="")

    # Executing the execute method of the UseCase, which calls the method find() from the mock
    result = uc.execute()

    # Assert find was called with the proper argument
    repo.store.assert_called_with(data=return_input_file_secret5a10n20_time_worst,
                                  storage="")
