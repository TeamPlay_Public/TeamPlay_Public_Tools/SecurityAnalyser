"""
    Test case for the VariablesCardHDF5UC class
"""
from unittest.mock import Mock

from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.use_case.Variable.variables_cardinality_hdf5 import VariablesCardHDF5UC


def test_uc_vars_cardinality_get(returns_time_worst,
                                 return_sidechannel_with_value_time_worst,
                                 return_input_file_secret5a10n20_time_worst,
                                 return_variables_list_a10_n20_secret5):
    """ Test the SideChannelClassGetUC execute method return value """
    # Creating the Mock
    repo = Mock()

    # Instantiating the UseCase with repo as a mock
    uc = VariablesCardHDF5UC(variables_card_repo=repo,
                             input_path=return_input_file_secret5a10n20_time_worst,
                             side_channel_class=return_sidechannel_with_value_time_worst,
                             vars_list=return_variables_list_a10_n20_secret5)

    # Setting the return value when calling the method find on the mock
    repo.find_all.return_value = return_variables_list_a10_n20_secret5

    # Executing the execute method of the UseCase, which calls the method find() from the mock
    result = uc.execute()

    # Assert find was called with the proper argument
    repo.find_all.assert_called_with(input_file=return_input_file_secret5a10n20_time_worst,
                                     dataset=returns_time_worst,
                                     hdf5_attributes=return_variables_list_a10_n20_secret5)

    # check we have the correct return value
    assert result == return_variables_list_a10_n20_secret5

    # Check the return value is of correct type
    assert isinstance(result, VariablesList)
