"""
    Test case for the VariablesHDF5CountUC class
"""
from unittest.mock import Mock

from sqcommon.use_case.Variable.variables_hdf5_count import VariablesHDF5CountUC


def test_uc_vars_count_get_nbtainted(returns_time_worst,
                                     return_sidechannel_with_value_time_worst,
                                     return_input_file_secret5a10n20_time_worst,
                                     return_hdf5attributes_with_value_nb_tainted,
                                     return_var_type_tainted,
                                     return_nb_secret_equals_1):
    """ Test the VariablesHDF5CountUC execute method return value for attribute nbTainted"""
    # Creating the Mock
    repo = Mock()

    # Instantiating the UseCase with repo as a mock
    uc = VariablesHDF5CountUC(variables_count_repo=repo,
                              input_path=return_input_file_secret5a10n20_time_worst,
                              side_channel_class=return_sidechannel_with_value_time_worst,
                              hdf5_attribute=return_hdf5attributes_with_value_nb_tainted)

    # Setting the return value when calling the method find on the mock
    repo.count.return_value = return_nb_secret_equals_1

    # Executing the execute method of the UseCase, which calls the method find() from the mock
    result = uc.execute()

    # Assert find was called with the proper argument
    repo.count.assert_called_with(input_path=return_input_file_secret5a10n20_time_worst,
                                  dataset=returns_time_worst,
                                  hdf5_attribute=return_var_type_tainted)

    # check we have the correct return value
    assert result == return_nb_secret_equals_1

    # Check the return value is of correct type
    assert isinstance(result, int)


def test_uc_vars_count_get_nbuntainted(returns_time_worst,
                                       return_sidechannel_with_value_time_worst,
                                       return_input_file_secret5a10n20_time_worst,
                                       return_hdf5attributes_with_value_nb_untainted,
                                       return_var_type_untainted,
                                       return_nb_public_equals_2):
    """ Test the VariablesHDF5CountUC execute method return value for attribute nbUntainted"""
    # Creating the Mock
    repo = Mock()

    # Instantiating the UseCase with repo as a mock
    uc = VariablesHDF5CountUC(variables_count_repo=repo,
                              input_path=return_input_file_secret5a10n20_time_worst,
                              side_channel_class=return_sidechannel_with_value_time_worst,
                              hdf5_attribute=return_hdf5attributes_with_value_nb_untainted)

    # Setting the return value when calling the method find on the mock
    repo.count.return_value = return_nb_public_equals_2

    # Executing the execute method of the UseCase, which calls the method find() from the mock
    result = uc.execute()

    # Assert find was called with the proper argument
    repo.count.assert_called_with(input_path=return_input_file_secret5a10n20_time_worst,
                                  dataset=returns_time_worst,
                                  hdf5_attribute=return_var_type_untainted)
    # check we have the correct return value
    assert result == return_nb_public_equals_2

    # Check the return value is of correct type
    assert isinstance(result, int)
