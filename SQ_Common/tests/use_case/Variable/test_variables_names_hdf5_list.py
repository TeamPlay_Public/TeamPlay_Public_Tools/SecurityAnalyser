"""
    Test case for the VariablesHDF5CountUC class
"""
from unittest.mock import Mock

from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.use_case.Variable.variables_names_hdf5_list import VariablesNamesHDF5UC


def test_uc_vars_names_hdf5(return_variables_list_a10_n20_secret5,
                            return_input_file_secret5a10n20_time_worst, ):
    """ Test the VariablesNamesHDF5UC execute method return value """
    # Creating the Mock
    repo = Mock()

    # Instantiating the UseCase with repo as a mock
    uc = VariablesNamesHDF5UC(variables_names_repo=repo,
                              input_path=return_input_file_secret5a10n20_time_worst)

    # Setting the return value when calling the method find on the mock
    repo.find_all.return_value = return_variables_list_a10_n20_secret5

    # Executing the execute method of the UseCase, which calls the method find() from the mock
    result = uc.execute()

    # Assert find was called with the proper argument
    repo.find_all.assert_called_with(input_path=return_input_file_secret5a10n20_time_worst)

    # check we have the correct return value
    assert result == return_variables_list_a10_n20_secret5

    # Check the return value is of correct type
    assert isinstance(result, VariablesList)
