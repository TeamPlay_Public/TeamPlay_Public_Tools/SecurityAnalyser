# SecurityQuantifier  



Team play module analysing data collected by the Data Collector and providing in return a security level indicator  \



## Features  


##Installation  

**There are two ways to install SQ, either on your machine or via a Docker container.**

### 1- Installation of the project from scratch:  \

#### Run the following:  \

sudo python3 -m pip install --upgrade pip setuptools wheel \ 

sudo dnf install python3-virtualenv (for Fedora, adapt to your OS!)  \


#### Create your working directory. This will be the folder where you will copy the project and run all the other following commands  \

#### Copy the project in this directory : kind of like: cp SecurityQuantifier workingdirectory  \

#### Go into your working directory and run:  \

virtualenv venv \ 

cd venv  \

source bin/activate  \


#### Still in your working directory run:  \

cd SecurityQuantifier  \

sudo python3 setup.py sdist bdist_wheel  \


#### Install the package locally:  \

sudo python3 -m pip install dist/SecurityQuantifier-1.0.0.tar.gz  \


#### Now install the needed python modules  \

sudo python3 -m pip install wheel  \

sudo python3 -m pip install pandas  \

sudo python3 -m pip install h5py  \

sudo python3 -m pip install tables  \



#### Now you can run (finally......) :\  

python3 main.py \ 


### 2- Using Anaconda:
How to install SecurityQuantifier on a remote server:\

#### 1- From the machine hosting the SQ code source:\
Copy all the files to the server using scp:\
scp -r config <login>@<server-url>:/es_home/<login>/SecurityAnalyser/SecurityQuantifier/ \
scp -r data/card/v1_vulnerable/WCET.h5  <login>@<server-url>:/es_home/<login>/SecurityAnalyser/SecurityQuantifier/data/wcet/  \
scp -r securityquantifier  <login>@<server-url>:/es_home/<login>/SecurityAnalyser/SecurityQuantifier/  \
scp -r tests  <login>@<server-url>:/es_home/<login>/SecurityAnalyser/SecurityQuantifier/  \

#### 2- Login to the remote server:  \
ssh <login>@<server-url>  \

#### 3- On the remote server load anaconda and use it to create the virtual env and install in it the libs SQ requires:  \
module load anaconda  \
conda create --name secquantvenv  \
conda install -n secquantvenv pandas numpy h5py tables  \

#### 4- Install sqcommon on the remore machine  \

#### 5- Activate the virtual env  \
source activate secquantvenv  \

#### 6- Run SQ from folder <install_dir>/securityquantifier/:  \
python3 main.py -i "../config/securityquantifier.ini"  \

### Using a Docker container \ 
A Docker file exists in the folder $Project_location/dist

#### Build the image with:
docker build --build-arg SSH_KEY="$(cat ~/.ssh/id_rsa)" --rm --tag securityquantifier dist/ 
where ~/.ssh/id_rsa is the path and the name of your ssh private key

Worked ? Great !
Time to run the Security Quantifier:
cd to SecurityAnalyser/SecurityQuantifier

#### Run:

With :
(NB: NB: -v $PWD/data/xp:/tmp means mount the volume from host $PWD/data/xp into the container at /tmp. The rest of the command refers to the container file hierarchy.)

docker run -ti -v $PWD/data/xp:/tmp --rm securityquantifier python3 ../main.py [+arguments]


##Configuration of the SQ:

The configuration of the SQ can be made using two modes, either with a configuration file or with command-line arguments, or a combinaison of both.

If no arguments are given, the SQ will use a default configuration file location.
Otherwise a configuration file may be provided as an argument from the command line, using either -c or --Conf

Every other arguments may be provided, either in the configuration file or via the command line. If both are provided, it is the command line argument that has the priority.

The SQ requires a HDF5 file as input, using either -i or --Input


It is possible to provide a distribution for the different tuples. This is done providing a file path, using either -d or --Dist.

By default the distance-based metric to be computed will be  extracted from the input file. 
It can be of two types, either Worst-case or average-case, for three kinds of side-channel attacks; time, power or energy.

Another Metric can be computed, the Indiscernability Informatiofollowing CLI argument -mi or --MI

This mode requires two additional arguments, used by the Dendrogram algorithm and provided in the configuration file or as a CLI argument:
A number of clusters (Optional, in that case a default value will be computed by the SQ):
-nc=x or --NbClust=x
A resolution criteria:
-r=x, or --Res=x

Finally for debugging purposes, for distance-based metrics, it might be interesting to record all the computed distances. To do so, a boolean should be activated
-p or --Probe
Together with an output type:
-t="file"|"db" or --OType="file"|"db"

Arguments that are boolean such as -p or --Probe and -gd or --GenerateTheDistribution do not appear in the configuration file. If provided as an argument of the command-line then their value is True, False otherwise.



## HDF5 file structure


The HDF5 file used as an input for the Security Quantifier (SQ) contains the measurements (also known as Leakage) made by different tools and collected by the Data Collector (DC), either Time, Energy or Power-related, and the values of the variables extracted from the analysed program that gave these measurements.
This data is structured in tuples composed of one or several secret variable(s), public variable(s) and a leakage value and stored in a HDF5 Table structure.

In addition, metadata has been added by the DC so that the SQ can proceed with the metric computations:
- nbTainted: Expresses how many tainted (secret) variables there are in the HDF5 file
- nbUntainted: Expresses how many untainted (public) variables there are in the HDF5 file
There appears also one (meta)variable per secret and public variables with how many different values they have.

For Power side channel-related hdf5 file, measurements are made for different timestamps.
These timestamps need to appear in the file, together with their related measurement.
Timestamp is considered a public variable (so it will appear in the HDF5 metadata in the count of public (untainted) variable and will have their own value stating how many different values it has).

An example of such a HDF5 file for power measurements is given thereafter.
k	x	t	power_avg
0	0	0	7
0	0	25	8
0	0	50	10
0	0	75	13
0	0	100	15
0	1	0	8
0	1	25	9
0	1	50	11
0	1	75	13
0	1	100	16

In the given example we have one secret (tainted) variable named k, a public (untainted) variable x. For this couple (k, x) five measurements were made at different timestamps, between 0 and 100 milliseconds. In the HDF5 file this data is added as a new column name t (for timestamp) which have five different values 0, 25, 50, 75, 100, so for each tuple (x, k, t) we have our leakage (measurement) value.
For this file the metadata is as follow:
nbTainted=1 (k)
nbUntainted=2 (x and t)
k=1
x=2
t=5


