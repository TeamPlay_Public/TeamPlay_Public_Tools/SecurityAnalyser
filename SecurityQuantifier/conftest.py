"""
    File gathering pytest fixtures for testing scenario
"""
import configparser
import os
import sys
from pathlib import Path

import pytest
from pandas import DataFrame
from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.domain.Distribution.distribution_container import DistributionContainer
from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict
from sqcommon.domain.Distribution.distribution_for_tuples import Distribution
from sqcommon.domain.Distribution.distribution_tuples import DistributionTuples
from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.domain.Tuples.tuples_info import TuplesInformation
from sqcommon.domain.Variable.variable import Variable
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples
from sqcommon.enums.enum_side_channel_class import SideChannelClasses
from sqcommon.enums.enum_var_types import VariableTypes

from securityquantifier.adapter.File.CSV.Probe.csv_file_probe_repository import CSVFileProbeRepository
from securityquantifier.domain.Data.probe_data import ProbeData
from securityquantifier.domain.Dendrogram.dendrogram import Dendrogram
from securityquantifier.domain.Dendrogram.dendrogram_gathered_tuples import DendrogramGatheredTuples
from securityquantifier.domain.Dendrogram.dendrogram_result import DendrogramResult
from securityquantifier.domain.Distance.distance_chebyshev import DistanceChebyshev
from securityquantifier.domain.Distance.distance_euclidian import DistanceEuclidian
from securityquantifier.domain.Metric.metric_aced import MetricACED
from securityquantifier.domain.Metric.metric_acpd import MetricACPD
from securityquantifier.domain.Metric.metric_actd import MetricACTD
from securityquantifier.domain.Metric.metric_wced import MetricWCED
from securityquantifier.domain.Metric.metric_wcpd import MetricWCPD
from securityquantifier.domain.Metric.metric_wctd import MetricWCTD
from securityquantifier.domain.Probe.probe_container import ProbeContainer
from securityquantifier.domain.Storage.TextFile.text_file_storage import TextFileStorage
from securityquantifier.enums.enum_marginal_probability import EnumJointMarginalProbability
from securityquantifier.enums.enum_termination_criteria import EnumTerminationCriteria
from securityquantifier.use_case.Metric.IndiscernibilityLevel.indiscernibility_level_energy_compute import \
    IndiscernabilityLevelEnergyComputeUseCase
from securityquantifier.use_case.Metric.IndiscernibilityLevel.indiscernibility_level_power_compute import \
    IndiscernabilityLevelPowerComputeUseCase
from securityquantifier.use_case.Metric.IndiscernibilityLevel.indiscernibility_level_time_compute import \
    IndiscernabilityLevelTimeComputeUseCase
from securityquantifier.use_case.Tuples.tuples_information_compute_uc import TuplesInformationHDF5ComputeUseCase

sys.path.append('path')


# ---------------------------------------------------------------------------------
# Fixtures for files
# ---------------------------------------------------------------------------------

@pytest.fixture
def return_absolute_dir_name():
    """ Return the directory name of pathname __file__ """
    return os.path.dirname(__file__)


@pytest.fixture
def config_file_epsilon_init(return_absolute_dir_name):
    """ Fixture method to get the epsilon value config file """
    dirname = return_absolute_dir_name

    conf_file = 'tests/test_config/securityquantifier.ini'
    conf_path = Path(dirname, conf_file)

    # Configuration file call
    config = configparser.ConfigParser()
    config.read(conf_path)

    return float(config['test']['epsilon'])


@pytest.fixture
def return_input_file_indiscernability_csv_file_for_repository_test(return_absolute_dir_name):
    """ Return the indiscernability csv file path """
    dirname = return_absolute_dir_name

    input_test_data = "tests/test_data/input/csv/indiscernability/data_from_indiscernability.csv"

    return Path(dirname, input_test_data)


@pytest.fixture
def return_input_file_indiscernability_csv_time_worst_file_for_repository_test(return_absolute_dir_name):
    """ Return the indiscernability csv file path """
    dirname = return_absolute_dir_name

    input_test_data = "tests/test_data/input/csv/indiscernability/data_from_indiscernability_time_worst.csv"

    return Path(dirname, input_test_data)


@pytest.fixture
def return_input_file_indiscernability_csv_file_for_file_repository_test(return_absolute_dir_name):
    """ Return the indiscernability csv file path """
    dirname = return_absolute_dir_name

    input_test_data = "tests/test_data/input/csv/indiscernability/data_from_indiscernability.csv"

    return Path(dirname, input_test_data)


@pytest.fixture
def return_output_file_csv_probing(return_absolute_dir_name):
    """ Return the csv output file which contains the probing data """
    dirname = return_absolute_dir_name

    output_file_csv_probing = "tests/test_data/output/csv/csv_file_probe_data.csv"

    return Path(dirname, output_file_csv_probing)


@pytest.fixture
def full_distribution_file_init(return_absolute_dir_name):
    """ Fixture method to get the test_distribution_full.json file """
    dirname = return_absolute_dir_name

    full_dist_file = 'tests/test_data/input/json/distribution/test_distribution_full.json'

    return Path(dirname, full_dist_file)


@pytest.fixture
def secret_public_distribution_file_12_tuples_init(return_absolute_dir_name):
    """ Fixture method to get the test_distribution_full.json file """
    dirname = return_absolute_dir_name

    full_dist_file = 'tests/test_data/input/json/distribution/secret_public_distribution_12_tuples.json'

    return Path(dirname, full_dist_file)


@pytest.fixture
def partial_distribution_file_init(return_absolute_dir_name):
    """ Fixture method to get the test_distribution_with_some_missing_dist.json file """
    dirname = return_absolute_dir_name

    partial_dist_file = 'tests/test_data/input/json/distribution/test_distribution_with_some_missing_dist.json'

    return Path(dirname, partial_dist_file)


@pytest.fixture
def return_secret_distribution_from_indiscernability_data_file(return_absolute_dir_name):
    """ Fixture method to get the test_distribution_with_some_missing_dist.json file """
    dirname = return_absolute_dir_name

    partial_dist_file = \
        'tests/test_data/input/json/distribution/test_20210508_SEF_distribution_from_indiscernability_data.json'

    return Path(dirname, partial_dist_file)


# ---------------------------------------------------------------------------------
# Fixtures for Object instantiation
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
# Fixtures for Object instantiation - Probe
# ---------------------------------------------------------------------------------

@pytest.fixture
def return_probe_repository_is_csv_file_probe_repository():
    """
        Returns a CSVFileProbeRepository type.
    """

    return CSVFileProbeRepository


@pytest.fixture
def return_storage_is_text_file_storage(return_output_file_csv_probing):
    """
        Returns a CSVFileProbeRepository type.
    """

    return TextFileStorage(name=Path(return_output_file_csv_probing))


@pytest.fixture
def return_probe_container_with_probe_not_active(return_is_active_probe_false):
    """
        Returns a ProbeContainer instance where the probe is not active.
        In that case probe_repository and storage are not used
    """

    return ProbeContainer(active_probe=return_is_active_probe_false,
                          probe="",
                          storage="")


@pytest.fixture
def return_probe_container_with_probe_active(return_is_active_probe_true,
                                             return_storage_is_text_file_storage,
                                             return_probe_repository_is_csv_file_probe_repository
                                             ):
    """
        Returns a ProbeContainer instance where the probe is active.
    """

    return ProbeContainer(active_probe=return_is_active_probe_true,
                          probe=return_probe_repository_is_csv_file_probe_repository,
                          storage=return_storage_is_text_file_storage)


@pytest.fixture
def return_probedata_100_50_first_distance():
    """ Return a ProbeData object for a first distance, meaning that current_distance = total_distance"""

    return ProbeData(value1="100",
                     value2="50",
                     current_distance="50",
                     total_distance="50")


# ---------------------------------------------------------------------------------
# Fixtures for Object instantiation - DistanceChebyshev
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_distance_chebyshev_type():
    """ Return a DistanceChebyshev type object """

    return DistanceChebyshev


# ---------------------------------------------------------------------------------
# Fixtures for Object instantiation - DistanceEuclidian
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_distance_euclidian_type():
    """ Return a DistanceEuclidian type object """

    return DistanceEuclidian


# ---------------------------------------------------------------------------------
# Fixtures for Object instantiation - SideChannelClass
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_sidechannel_with_value_time_worst():
    """ Return a SideChannelClass with value=time_worst """

    return SideChannelClass(SideChannelClasses('time_worst').name, 'time_worst')


# ---------------------------------------------------------------------------------
# Fixtures for Object instantiation - Variable
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_variable_with_value_a_1():
    """ Return a Variable with cardinality=1 and value=a """

    return Variable(value="a",
                    cardinality=1)


@pytest.fixture
def return_variable_with_value_a_2():
    """ Return a Variable with cardinality=2 and value=a """

    return Variable(value="a",
                    cardinality=2)


@pytest.fixture
def return_variable_with_value_a_3():
    """ Return a Variable with cardinality=3 and value=a """

    return Variable(value="a",
                    cardinality=3)


@pytest.fixture
def return_variable_with_value_k_3_for_d4_dot_5():
    """
        Return a Variable with cardinality=3 and value=k.
        Related to example from deliverable D4.5
    """

    return Variable(value="k",
                    cardinality=3)


@pytest.fixture
def return_variable_with_value_k_10_for_d4_dot_4():
    """
        Return a Variable with cardinality=10 and value=k.
        Related to example from deliverable D4.4
    """

    return Variable(value="k",
                    cardinality=10)


@pytest.fixture
def return_variable_with_value_n_1():
    """ Return a Variable with cardinality=1 and value=n """

    return Variable(value="n",
                    cardinality=1)


@pytest.fixture
def return_variable_with_value_n_2():
    """ Return a Variable with cardinality=2 and value=n """

    return Variable(value="n",
                    cardinality=2)


@pytest.fixture
def return_variable_with_value_n_3():
    """ Return a Variable with cardinality=3 and value=n """

    return Variable(value="n",
                    cardinality=3)


@pytest.fixture
def return_variable_with_value_t_5_for_d4_dot_5():
    """
        Return a Variable with cardinality=5 and value=t.
        Related to example from deliverable D4.5
    """

    return Variable(value="t",
                    cardinality=5)


@pytest.fixture
def return_variable_with_value_x_2_for_d4_dot_5():
    """
        Return a Variable with cardinality=2 and value=x
        Related to example from deliverable D4.5
    """

    return Variable(value="x",
                    cardinality=2)


@pytest.fixture
def return_variable_with_value_x_3_for_d4_dot_4():
    """
        Return a Variable with cardinality=3 and value=x
        Related to example from deliverable D4.4
    """

    return Variable(value="x",
                    cardinality=3)


@pytest.fixture
def return_variable_with_value_secret_3():
    """ Return a Variable with cardinality=3 and value=secret """

    return Variable(value="secret",
                    cardinality=3)


# ---------------
# ------------------------------------------------------------------
# Fixtures for Object instantiation - VariablesList
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_variables_list_untainted_a2_n2(return_variable_with_value_a_2,
                                          return_variable_with_value_n_2
                                          ):
    """
        Return a VariableList with two variables:
            a=2
            n=2,
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_a_2)
    vars_list.lst.append(return_variable_with_value_n_2)

    return vars_list


@pytest.fixture
def return_variables_list_tainted_secret3(return_variable_with_value_secret_3):
    """
        Return a VariableList with one variable:
            secret=3
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_secret_3)

    return vars_list


@pytest.fixture
def return_variables_list_a1_n1_secret3(return_variable_with_value_a_1,
                                        return_variable_with_value_n_1,
                                        return_variable_with_value_secret_3):
    """
        Return a VariableList with three variables:
            a=1
            n=1,
            secret=3
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_a_1)
    vars_list.lst.append(return_variable_with_value_n_1)
    vars_list.lst.append(return_variable_with_value_secret_3)

    return vars_list


@pytest.fixture
def return_variables_list_a2_n2_secret3(return_variable_with_value_a_2,
                                        return_variable_with_value_n_2,
                                        return_variable_with_value_secret_3):
    """
        Return a VariableList with three variables:
            a=2
            n=2,
            secret=3
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_secret_3)
    vars_list.lst.append(return_variable_with_value_a_2)
    vars_list.lst.append(return_variable_with_value_n_2)

    return vars_list


@pytest.fixture
def return_variables_list_a2_n2(return_variable_with_value_a_2,
                                return_variable_with_value_n_2):
    """
        Return a VariableList with two variables:
            a=2
            n=2
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_a_2)
    vars_list.lst.append(return_variable_with_value_n_2)

    return vars_list


@pytest.fixture
def return_variables_list_secret3(return_variable_with_value_secret_3):
    """
        Return a VariableList with one variable secret:
            secret=3
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_secret_3)

    return vars_list


@pytest.fixture
def return_variables_list_x3_k10_for_d4_dot_4(return_variable_with_value_a_3,
                                              return_variable_with_value_n_3,
                                              return_variable_with_value_secret_3):
    """
        Return a VariableList with three variables:
            a=3
            n=3,
            secret=3
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_a_3)
    vars_list.lst.append(return_variable_with_value_n_3)
    vars_list.lst.append(return_variable_with_value_secret_3)

    return vars_list


@pytest.fixture
def return_variables_list_k10_x3_for_d4_dot_4(return_variable_with_value_k_10_for_d4_dot_4,
                                              return_variable_with_value_x_3_for_d4_dot_4):
    """
        Return a VariableList with two variables:
            k=10
            x=3
        Related to example from deliverable D4.4
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_k_10_for_d4_dot_4)
    vars_list.lst.append(return_variable_with_value_x_3_for_d4_dot_4)

    return vars_list


@pytest.fixture
def return_variables_list_k3_x2_t5_for_d4_dot_5(return_variable_with_value_k_3_for_d4_dot_5,
                                                return_variable_with_value_x_2_for_d4_dot_5,
                                                return_variable_with_value_t_5_for_d4_dot_5):
    """
        Return a VariableList with two variables:
            k=3
            x=2
            t=5
        Related to example from deliverable D4.5
    """

    vars_list = VariablesList()
    vars_list.lst.append(return_variable_with_value_k_3_for_d4_dot_5)
    vars_list.lst.append(return_variable_with_value_x_2_for_d4_dot_5)
    vars_list.lst.append(return_variable_with_value_t_5_for_d4_dot_5)

    return vars_list


# ---------------------------------------------------------------------------------
# Fixtures for Object instantiation - DistributionAsDict
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_distributionasdict_secret(return_distribution_as_dict_secret_indiscernability):
    """
        Return a DistributionAsDict with one entry: secret and its related values
    """

    distributionasdict = DistributionAsDict(data=return_distribution_as_dict_secret_indiscernability)

    return distributionasdict


@pytest.fixture
def return_distributionasdict_secret_for_average(return_distribution_as_dict_secret_indiscernability_for_average):
    """
        Return a DistributionAsDict with one entry: secret and its related values, for testing average-related methods
    """

    distributionasdict = DistributionAsDict(data=return_distribution_as_dict_secret_indiscernability_for_average)

    return distributionasdict


@pytest.fixture
def return_distributionasdict_secret_empty_for_average(return_distribution_as_dict_secret_empty_for_average):
    """
        Return a DistributionAsDict where data is empty
    """

    distributionasdict = DistributionAsDict(data=return_distribution_as_dict_secret_empty_for_average)

    return distributionasdict


@pytest.fixture
def return_distributionasdict_secret_v2(return_distribution_as_dict_secret_indiscernability_v2):
    """
        Return a DistributionAsDict with one entry: secret and its related values
    """

    distributionasdict = DistributionAsDict(data=return_distribution_as_dict_secret_indiscernability_v2)

    return distributionasdict


@pytest.fixture
def return_distributionasdict_secret_bl(return_distribution_as_dict_secret_indiscernability_bl):
    """
        Return a DistributionAsDict with one entry: secret and its related values
    """

    distributionasdict = DistributionAsDict(data=return_distribution_as_dict_secret_indiscernability_bl)

    return distributionasdict


@pytest.fixture
def return_distributionasdict_secret_public_12_tuples(return_distribution_dict_secret_public_12_tuples):
    """
        Return a DistributionAsDict with one entry: secret_public and its related values
    """

    distributionasdict = DistributionAsDict(data=return_distribution_dict_secret_public_12_tuples)

    return distributionasdict


@pytest.fixture
def return_uniform_distributionasdict_secret_public_for_d4_dot4(
        return_uniform_distribution_dict_secret_public_for_d4_dot_4):
    """
        Return a DistributionAsDict with one entry: secret_public and its related values,
        ie a uniform distribution for the 30 values from the D4.4
    """

    distributionasdict = DistributionAsDict(data=return_uniform_distribution_dict_secret_public_for_d4_dot_4)

    return distributionasdict


@pytest.fixture
def return_uniform_distributionasdict_secret_public_for_d4_dot5(
        return_uniform_distribution_dict_secret_public_for_d4_dot_5):
    """
        Return a DistributionAsDict with one entry: secret_public and its related values,
        ie a uniform distribution for the 30 values from the D4.5
    """

    distributionasdict = DistributionAsDict(data=return_uniform_distribution_dict_secret_public_for_d4_dot_5)

    return distributionasdict


@pytest.fixture
def return_secret_public_joint_distributionasdict_for_d4_dot5(
        return_secret_public_joint_distribution_dict_for_d4_dot_5):
    """
        Return a DistributionAsDict with one entry: secret_public joint distribution
        for the D4.5 deliverable example
    """

    distributionasdict = DistributionAsDict(data=return_secret_public_joint_distribution_dict_for_d4_dot_5)

    return distributionasdict


@pytest.fixture
def return_secret_and_public_marginal_distributionasdict_for_d4_dot5(
        return_secret_and_public_marginal_distributions_dict_for_d4_dot_5):
    """
        Return a DistributionAsDict with two entries: secret and public marginal distributions
        for the D4.5 deliverable example
    """

    distributionasdict = DistributionAsDict(data=return_secret_and_public_marginal_distributions_dict_for_d4_dot_5)

    return distributionasdict


# ---------------------------------------------------------------------------------
# Fixtures for Object instantiation - VariableTypes
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_variabletypes_with_value_tainted():
    """ Return a VariableTypes with value=TAINTED """

    return VariableTypes.TAINTED


@pytest.fixture
def return_variabletypes_with_value_untainted():
    """ Return a VariableTypes with value=UNTAINTED """

    return VariableTypes.UNTAINTED


# ---------------------------------------------------------------------------------
# Fixtures for Object instantiation - EnumDistributionTuples
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_enumdistributiontuples_with_value_secret():
    """ Return a EnumDistributionTuples with value=secret """

    return EnumDistributionTuples.SECRET


@pytest.fixture
def return_enumdistributiontuples_value_for_secret():
    """ Return the value of EnumDistributionTuples.SECRET """

    return EnumDistributionTuples.SECRET.value


@pytest.fixture
def return_enumdistributiontuples_with_value_public():
    """ Return a EnumDistributionTuples with value=public """

    return EnumDistributionTuples.PUBLIC


@pytest.fixture
def return_enumdistributiontuples_value_for_public():
    """ Return the value of EnumDistributionTuples.PUBLIC """

    return EnumDistributionTuples.PUBLIC.value


@pytest.fixture
def return_enumdistributiontuples_with_value_leakage():
    """ Return a EnumDistributionTuples with value=leakage """

    return EnumDistributionTuples.LEAKAGE


@pytest.fixture
def return_enumdistributiontuples_with_value_publicleakage():
    """ Return a EnumDistributionTuples with value=publicleakage """

    return EnumDistributionTuples.PUBLIC_LEAKAGE


@pytest.fixture
def return_enumdistributiontuples_with_value_secretpublic():
    """ Return a EnumDistributionTuples with value=secretpublic """

    return EnumDistributionTuples.SECRET_PUBLIC


@pytest.fixture
def return_enumdistributiontuples_value_for_secretpublic():
    """ Return the value of EnumDistributionTuples.SECRET_PUBLIC """

    return EnumDistributionTuples.SECRET_PUBLIC.value


# ---------------------------------------------------------------------------------
# Fixtures for Object instantiation - TextFileStorage
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_textfilestorage_csv(return_absolute_dir_name):
    """ Return a TextFileStorage object for file in tests/test_data/output/csv/csv_file_probe_data.csv  """
    dirname = return_absolute_dir_name

    probe_csv_file = 'tests/test_data/output/csv/csv_file_probe_data.csv'

    return TextFileStorage(name=Path(dirname, probe_csv_file))


# ---------------------------------------------------------------------------------
# Fixtures for simple attributes values
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_first_tuple_for_dataframe_distance_equal_60cheb_20_eucl_init():
    """ Return the first tuple contained in the dataframe dataframe_distance_equal_60cheb_20_eucl_init """
    return ["00000001111100101111010111001011", 2, 10]


# ---------------------------------------------------------------------------------
# Fixtures for simple attributes values - SideChannel
# ---------------------------------------------------------------------------------
@pytest.fixture
def returned_value_value_is_time_worst():
    """ Return the side channel value returned by the SideChannelClassGetHDF5Repository find method """
    sdc_value = "time_worst"

    return sdc_value


@pytest.fixture
def returned_value_name_is_time_worst():
    """ Return the side channel name returned by the SideChannelClassGetHDF5Repository find method """
    sdc_value = "TIME_WORST"

    return sdc_value


# ---------------------------------------------------------------------------------
# Fixtures for simple attributes values - Tuples
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_nb_tuples_equals_2():
    """ Return a number of tuples equals to 2 """
    return 2


@pytest.fixture
def return_nb_tuples_equals_3():
    """ Return a number of tuples equals to 3 """
    return 3


@pytest.fixture
def return_nb_tuples_equals_4():
    """ Return a number of tuples equals to 4 """
    return 4


@pytest.fixture
def return_nb_tuples_equals_9():
    """ Return a number of tuples equals to 9 """
    return 9


@pytest.fixture
def return_nb_tuples_equals_10():
    """ Return a number of tuples equals to 10 """
    return 10


@pytest.fixture
def return_nb_tuples_equals_12():
    """ Return a number of tuples equals to 12 """
    return 12


@pytest.fixture
def return_nb_tuples_equals_30():
    """ Return a number of tuples equals to 30 """
    return 30


# ---------------------------------------------------------------------------------
# Fixtures for simple attributes values - Tainted and Untainted
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_nb_tainted_equals_1():
    """ Return a number of tainted variables equals to 1 """
    return 1


@pytest.fixture
def return_nb_untainted_equals_1():
    """ Return a number of untainted variables equals to 1 """
    return 1


@pytest.fixture
def return_nb_untainted_equals_2():
    """ Return a number of untainted variables equals to 2 """
    return 2


@pytest.fixture
def return_nb_untainted_tuples_equals_3():
    """ Return a number of untainted tuples equals to 3 """
    return 3


# ---------------------------------------------------------------------------------
# Fixtures for simple attributes values - Distance and Metric
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_homogeneity_property_equals_100():
    """ Return an homogeneity property equals to 100 """
    return 100


@pytest.fixture
def return_computed_euclidian_distance_equals_0():
    """ Return a computed euclidian distance equals to 0 """
    return 0


@pytest.fixture
def return_computed_chebyshev_distance_equals_zero():
    """ Return a computed chebyshev distance equals to zero """
    return 0


@pytest.fixture
def return_computed_wctd_metric_equals_zero():
    """ Return a computed wctd metric equals to zero """
    return 0


@pytest.fixture
def return_computed_wctd_metric_equals_sixty():
    """ Return a computed wctd metric equals to sixty """
    return 60


@pytest.fixture
def return_computed_wced_metric_equals_zero():
    """ Return a computed wced metric equals to zero """
    return 0


@pytest.fixture
def return_computed_wced_metric_equals_sixty():
    """ Return a computed wced metric equals to sixty """
    return 60


@pytest.fixture
def return_computed_wcpd_metric_equals_zero():
    """ Return a computed wcpd metric equals to zero """
    return 0


@pytest.fixture
def return_computed_wcpd_metric_equals_sixty():
    """ Return a computed wcpd metric equals to sixty """
    return 60


@pytest.fixture
def return_computed_actd_metric_equals_zero():
    """ Return a computed actd metric equals to zero """
    return 0


@pytest.fixture
def return_computed_actd_metric_equals_6_point_4967():
    """ Return a computed actd metric equals to 6.4967348"""
    return 6.4967348


@pytest.fixture
def return_computed_actd_metric_equals_twenty():
    """ Return a computed actd metric equals to twenty """
    return 20


@pytest.fixture
def return_computed_actd_metric_equals_forty_two():
    """ Return a computed actd metric equals to forty two """
    return 42


@pytest.fixture
def return_computed_aced_metric_equals_zero():
    """ Return a computed aced metric equals to zero """
    return 0


@pytest.fixture
def return_computed_aced_metric_equals_6_point_4967():
    """ Return a computed aced metric equals to 6.4967348"""
    return 6.4967348


@pytest.fixture
def return_computed_aced_metric_equals_twenty():
    """ Return a computed aced metric equals to twenty """
    return 20


@pytest.fixture
def return_computed_aced_metric_equals_forty_two():
    """ Return a computed aced metric equals to forty two """
    return 42


@pytest.fixture
def return_computed_acpd_metric_equals_zero():
    """ Return a computed acpd metric equals to zero """
    return 0


@pytest.fixture
def return_computed_acpd_metric_equals_6_point_4967():
    """ Return a computed acpd metric equals to 6.4967348"""
    return 6.4967348


@pytest.fixture
def return_computed_acpd_metric_equals_twenty():
    """ Return a computed acpd metric equals to twenty """
    return 20


@pytest.fixture
def return_computed_acpd_metric_equals_forty_two():
    """ Return a computed acpd metric equals to forty two """
    return 42


# ---------------------------------------------------------------------------------
# Fixtures for simple attributes values - Cardinality
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_cardinality_equals_0():
    """ Return a cardinality equal to 0 """
    return 0


@pytest.fixture
def return_cardinalities_equal_to_3_2_2():
    """ Return a list of cardinalities equal to [3, 2, 2] """
    return [3, 2, 2]


@pytest.fixture
def return_cardinalities_equal_to_10_3_for_d4_dot_4():
    """
        Return a list of cardinalities equal to [10, 3]
        Related to example from deliverable D4.4
    """
    return [10, 3]


@pytest.fixture
def return_cardinalities_equal_to_3_2_5_for_d4_dot_5():
    """
        Return a list of cardinalities equal to [3, 2, 5]
        Related to example from deliverable D4.5
    """
    return [3, 2, 5]


@pytest.fixture
def return_value_equals_secret():
    """ Return a value equal to secret """
    return "secret"


@pytest.fixture
def return_starting_index_equals_0():
    """ Return a starting index equals to 0 """
    return 0


@pytest.fixture
def return_starting_index_equals_1():
    """ Return a starting index equals to 1 """
    return 1


@pytest.fixture
def return_exponent_equals_2():
    """ Return an exponent equals to 2 """
    return 2


@pytest.fixture
def return_is_active_probe_true():
    """ Return a boolean stating that the probe is active """
    return True


@pytest.fixture
def return_is_active_probe_false():
    """ Return a boolean stating that the probe is not active """
    return False


# ---------------------------------------------------------------------------------
# Fixtures for the ACSD: Metric computed values
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_acsd_metric_value_equal_0():
    """
        Return the acsd metric computed value equal to 0.
        This value is returned for case:
        metric_model_init_with_acsd_metric_uniform_dist_equal_0
    """
    return 0


@pytest.fixture
def return_acsd_metric_value_equal_20():
    """
        Return the acsd metric computed value equal to 20.
        This value is returned for case:
        metric_model_init_with_acsd_metric_uniform_dist_equal_20
    """
    return 20


@pytest.fixture
def return_acsd_metric_value_equal_42():
    """
        Return the acsd metric computed value equal to 42.
        This value is returned for case:
        metric_model_init_with_acsd_metric_uniform_dist_equal_42
    """
    return 42


@pytest.fixture
def return_acsd_metric_value_equal_1_point_5():
    """
        Return the acsd metric computed value equal to 1.5
        This value is returned for case:
        metric_model_init_with_acsd_metric_uniform_dist_equal_1_point_5
    """
    return 1.5


@pytest.fixture
def return_acsd_metric_value_equal_4_point_159896():
    """
        Return the acsd metric computed value equal to 4.159896844176533.
        This value is returned for case:
        metric_model_init_with_acsd_metric_non_uniform_dist_12_tuples_equal_2_point_079948
    """
    return 4.159896844176533


@pytest.fixture
def return_acsd_metric_value_equal_81_point_8936_for_d4_dot_4():
    """
        Return the acsd metric computed value equal to 81.89361725514381
        This value is returned for case:
        metric_model_init_with_acsd_metric_uniform_dist_d4_dot_4_equal_81_point_8936
        Related to example from deliverable D4.4
    """
    return 81.89361725514381


@pytest.fixture
def return_acsd_metric_value_equal_3_point_819_for_d4_dot_5():
    """
        Return the acsd metric computed value equal to 3.81945942278026
        This value is returned for case:
        metric_model_init_with_acsd_metric_uniform_dist_d4_dot_5_equal_3_point_819
        Related to example from deliverable D4.5
    """
    return 3.81945942278026


# ---------------------------------------------------------------------------------
# Fixtures for the WCSD: Metric computed values
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_acsd_metric_value_equal_276_for_d4_dot_4():
    """
        Return the wcsd metric computed value equal to 276
        This value is returned for case:
        metric_model_init_with_wcsd_metric_uniform_dist_d4_dot_4_equal_276
        Related to example from deliverable D4.4
    """
    return 276


@pytest.fixture
def return_acsd_metric_value_equal_10_for_d4_dot_5():
    """
        Return the wcsd metric computed value equal to 10
        This value is returned for case:
        metric_model_init_with_wcsd_metric_uniform_dist_d4_dot_5_equal_10
        Related to example from deliverable D4.5
    """
    return 10


# ---------------------------------------------------------------------------------
# Fixtures for simple attributes values - Entropy
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_fair_distribution():
    """
        Return a fair distribution
    """
    return [1 / 2, 1 / 2]


@pytest.fixture
def return_entropy_fair_coin():
    """
        Return the entropy for a fair distribution
    """
    return 1.0


@pytest.fixture
def return_biased_distribution():
    """
        Return a biased distribution
    """
    return [9 / 10, 1 / 10]


@pytest.fixture
def return_entropy_biased_coin():
    """
        Return the entropy for a biased distribution
    """
    return 0.46899559358928117


# ---------------------------------------------------------------------------------
# Fixtures for the Database
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_db_name():
    """ Return the name of the database """
    return "db_sq_probe"


@pytest.fixture
def return_db_user():
    """ Return the user for the database """
    return "tp_sq"


@pytest.fixture
def return_db_user_pwd():
    """ Return the password for the user of the database """
    return "password"


@pytest.fixture
def return_db_host():
    """ Return the host of the database """
    return "pg-container"


@pytest.fixture
def return_db_port():
    """ Return the port of the database """
    return "5432"


@pytest.fixture
def return_db_info_as_dict(return_db_name,
                           return_db_user,
                           return_db_user_pwd,
                           return_db_host,
                           return_db_port):
    """ Return the database info as a Dict """
    d = {'db_name': return_db_name, 'db_user': return_db_user, 'db_user_passwd': return_db_user_pwd,
         'db_host': return_db_host, 'db_port': return_db_port}

    return d


# ---------------------------------------------------------------------------------
# Fixtures for the ProbeData
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_probedata_value_10():
    """ Return a value equal to 10 for a ProbeData """
    return 10


@pytest.fixture
def return_probedata_value_20():
    """ Return a value equal to 20 for a ProbeData """
    return 20


@pytest.fixture
def return_probedata_current_distance_10():
    """ Return a current distance equal to 10 for a ProbeData """
    return 20


@pytest.fixture
def return_probedata_total_distance_50():
    """ Return a total distance equal to 50 for a ProbeData """
    return 50


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
# Fixtures for the Distribution - all_the_tuples - Discernability slides
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_all_the_tuples_for_a():
    """ Return all_the_tuples for variable a following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [4, 4, 4,
            4, 4, 4,
            6, 6, 6,
            6, 6, 6]


@pytest.fixture
def return_all_the_tuples_for_n():
    """ Return all_the_tuples for variable n following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [33, 33, 33,
            33, 33, 33,
            35, 35, 35,
            35, 35, 35]


@pytest.fixture
def return_all_the_tuples_for_public():
    """ Return all_the_tuples for public variables following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [[4, 33], [4, 33], [4, 33],
            [4, 35], [4, 35], [4, 35],
            [6, 33], [6, 33], [6, 33],
            [6, 35], [6, 35], [6, 35]]


@pytest.fixture
def return_all_the_tuples_for_secrets():
    """ Return all_the_tuples as int for variable secret """
    return [1, 2, 3,
            1, 2, 3,
            1, 2, 3,
            1, 2, 3]


@pytest.fixture
def return_all_the_tuples_for_secret_as_bits():
    """ Return all_the_tuples for variable secret following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [0b01, 0b10, 0b11,
            0b01, 0b10, 0b11,
            0b01, 0b10, 0b11,
            0b01, 0b10, 0b11]


@pytest.fixture
def return_all_the_tuples_for_observations_a():
    """
        Return all_the_tuples for observations a
    """
    return [[6], [9], [14], [12], [1], [8], [2]]


@pytest.fixture
def return_all_the_tuples_for_indiscernability():
    """ Return all_the_tuples for leakage variables following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [[10], [2], [9], [11], [8], [3], [11], [10], [15], [4], [14], [16]]


@pytest.fixture
def return_all_the_tuples_for_public_leakage():
    """ Return all_the_tuples for public_leakage variables following example made by Yoann Marquer
        and available in slides on the INRIA gitlab repository:
        Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [[4, 33, 10], [4, 33, 8], [4, 33, 15],
            [4, 35, 2], [4, 35, 3], [4, 35, 4],
            [6, 33, 9], [6, 33, 11], [6, 33, 14],
            [6, 35, 11], [6, 35, 10], [6, 35, 16]]


@pytest.fixture
def return_all_the_tuples_for_secret_public():
    """ Return all_the_tuples as int for secret_public variables"""
    return [[1, 4, 33], [2, 4, 33], [3, 4, 33],
            [1, 6, 33], [2, 6, 33], [3, 6, 33],
            [1, 6, 35], [2, 6, 35], [3, 6, 35]]


@pytest.fixture
def return_all_the_tuples_for_secret_public_as_bits():
    """ Return all_the_tuples for secret_public variables following example made by Yoann Marquer
        and available in slides on the INRIA gitlab repository:
        Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [[0b01, 4, 33], [0b10, 4, 33], [0b11, 4, 33],
            [0b01, 6, 33], [0b10, 6, 33], [0b11, 6, 33],
            [0b01, 6, 35], [0b10, 6, 35], [0b11, 6, 35]]


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution - all_the_tuples - Deliverable D4.4
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_all_the_tuples_for_k_for_d4_dot_4():
    """
        Return all_the_tuples for variable k following example from deliverable D4.4
    """
    return ["00000001111100101111010111001011",
            "00000001111100101111010111001011",
            "00000001111100101111010111001011",
            "10100001000001111000101011000111",
            "10100001000001111000101011000111",
            "10100001000001111000101011000111",
            "10111111111100101001011110110001",
            "10111111111100101001011110110001",
            "10111111111100101001011110110001",
            "01100011111000011010010000001110",
            "01100011111000011010010000001110",
            "01100011111000011010010000001110",
            "11010010110100000101010101110100",
            "11010010110100000101010101110100",
            "11010010110100000101010101110100",
            "11110000101111111111001010010010",
            "11110000101111111111001010010010",
            "11110000101111111111001010010010",
            "10111011000011111110010010000001",
            "10111011000011111110010010000001",
            "10111011000011111110010010000001",
            "11110001100101000100001100000111",
            "11110001100101000100001100000111",
            "11110001100101000100001100000111",
            "01110101010001100000010100100111",
            "01110101010001100000010100100111",
            "01110101010001100000010100100111",
            "10010110101001001000100010111100",
            "10010110101001001000100010111100",
            "10010110101001001000100010111100"]


@pytest.fixture
def return_all_the_tuples_for_x_for_d4_dot_4():
    """
        Return all_the_tuples for variable x following example from deliverable D4.4
    """
    return [2.10, 4.15, 8.35]


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution - all_the_tuples - Deliverable D4.5
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_all_the_tuples_for_k_for_d4_dot_5():
    """
        Return all_the_tuples for variable K following example from deliverable D4.5
    """
    return [0, 1, 10]


@pytest.fixture
def return_all_the_tuples_for_x_for_d4_dot_5():
    """
        Return all_the_tuples for variable x following example from deliverable D4.5
    """
    return [0, 1]


@pytest.fixture
def return_all_the_tuples_for_t_for_d4_dot_5():
    """
        Return all_the_tuples for variable t following example from deliverable D4.5
    """
    return [0, 25, 50, 75, 100]


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution - sorted_tuples_as_dict
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_sorted_tuples_as_dict_for_leakage_indiscernability():
    """ Return a dictionary with tuples (without duplicates) as key and number of occurrences as value
        for the variable leakage following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return {"[10]": 2, "[2]": 1, "[9]": 1, "[11]": 2, "[8]": 1,
            "[3]": 1, "[15]": 1, "[4]": 1, "[14]": 1, "[16]": 1
            }


@pytest.fixture
def return_related_secret_tuples_as_int_indiscernability():
    """ Return a list of secret values related to the leakage ones
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return ['[1]', '[1]', '[1]', '[1]', '[10]', '[10]', '[10]', '[10]', '[11]', '[11]', '[11]', '[11]']


@pytest.fixture
def return_tuples_as_list__no_duplicates_indiscernability():
    """ Return a list of leakage values unsorted without duplicates
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [10, 2, 9, 11, 8, 3, 15, 4, 14, 16]


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution - distributions
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_distribution_for_a():
    """ Return distribution for a following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return []


@pytest.fixture
def return_distribution_for_n():
    """ Return distribution for n following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return []


@pytest.fixture
def return_distribution_for_public():
    """ Return distribution for public following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return []


@pytest.fixture
def return_distribution_for_secret_for_obs_a():
    """ Return distribution for secret following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [[0.14], [0.35], [0.19],
            [0.06], [0.11], [0.01],
            [0.14]]


@pytest.fixture
def return_distribution_for_secret_for_indiscernability():
    """ Return distribution for secret following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return [[0.1], [0.15], [0.1],
            [0.01], [0.01], [0.01],
            [0.14], [0.04], [0.2],
            [0.05], [0.09], [0.1]]


@pytest.fixture
def return_distribution_for_leakage():
    """ Return distribution for leakage following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return []


@pytest.fixture
def return_distribution_for_public_leakage():
    """ Return distribution for public_leakage following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return []


@pytest.fixture
def return_distribution_for_secret_public():
    """ Return distribution for secret_public following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary"""
    return []


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution - distributions in a dictionary
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_distribution_as_dict_secret_indiscernability():
    """ Return distribution as a dict for testing the JsonToDistributionContainerHelper methods"""
    dist_dict = {
        "secret":
            [0.1, 0.15, 0.1,
             0, 0, 0,
             0.15, 0.05, 0.20,
             0.05, 0.10, 0.1]
    }

    return dist_dict


@pytest.fixture
def return_distribution_as_dict_secret_indiscernability_for_average():
    """ Return distribution as a dict for testing the Average Metric related methods"""
    dist_dict = {
        "secret":
            [0.7, 0.25, 0.05]
    }

    return dist_dict


@pytest.fixture
def return_distribution_as_dict_secret_empty_for_average():
    """ Return an empty distribution as a dict for testing the Average Metric related methods"""
    dist_dict = {}

    return dist_dict


@pytest.fixture
def return_distribution_as_dict_secret_indiscernability_v2():
    """ Return distribution as a dict for testing the JsonToDistributionContainerHelper methods"""
    dist_dict = {
        "secret_public":
            [0.05, 0.15, 0.05,
             0.10, 0.05, 0.10,
             0.15, 0.05, 0.10,
             0.05, 0.10, 0.05]
    }

    return dist_dict


@pytest.fixture
def return_distribution_as_dict_secret_indiscernability_bl():
    """ Return distribution as a dict following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing."""
    dist_dict = {
        "secret_public":
            [0.1, 0.01, 0.14,
             0.05, 0.15, 0.01,
             0.04, 0.09, 0.10,
             0.01, 0.2, 0.1]
    }

    return dist_dict


@pytest.fixture
def return_distribution_dict_secret_public_12_tuples():
    """ Return distribution as a dict with an entry secret_public and 12 values"""
    dist_dict = {
        "secret_public":
            [0.08, 0.05, 0.03,
             0.12, 0.02, 0.04,
             0.06, 0.1, 0.2,
             0.15, 0.06, 0.09]
    }

    return dist_dict


@pytest.fixture
def return_uniform_distribution_dict_secret_public_for_d4_dot_4():
    """
        Return distribution as a dict with an entry secret_public and 30 values.
        Matches a uniform distribution for the data from D4.4
    """
    dist_dict = {
        'secret_public': [0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333]
    }

    return dist_dict


@pytest.fixture
def return_uniform_distribution_dict_secret_public_for_d4_dot_5():
    """
        Return distribution as a dict with an entry secret_public and 30 values.
        Matches a uniform distribution for the data from D4.5
    """
    dist_dict = {
        'secret_public': [0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333, 0.03333333333333333, 0.03333333333333333,
                          0.03333333333333333, 0.03333333333333333]
    }

    return dist_dict


@pytest.fixture
def return_secret_public_joint_distribution_dict_for_d4_dot_5():
    """
        Return the joint distribution as a dict with an entry secret_public
        for D4.5.
        Matches the case when the given distribution is made of two marginal distributions:
                'secret': [0.2, 0.3, 0.5],
                'public': [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]
    """
    dist_dict = {
        'secret_public': [0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02,
                          0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03,
                          0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05]
    }

    return dist_dict


@pytest.fixture
def return_secret_and_public_marginal_distributions_dict_for_d4_dot_5():
    """
        Return distribution as a dict with an entry secret and an entry public
        for D4.5
    """
    dist_dict = {
        'secret': [0.2, 0.3, 0.5],
        'public': [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]
    }

    return dist_dict


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution - UniformJointDistributionComputeUseCase
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_uniform_joint_proba_secret_for_12_tuples():
    """
        Fixture method returning the values of a uniform joint distribution for the secret and public variables
        for the 12 tuples example
     """
    return [0.083333333, 0.083333333, 0.083333333, 0.083333333, 0.083333333, 0.083333333,
            0.083333333, 0.083333333, 0.083333333, 0.083333333, 0.083333333, 0.083333333]


@pytest.fixture
def return_uniform_joint_proba_public_for_12_tuples():
    """
        Fixture method returning the values of the uniform joint distribution for the public tuples
        for the 12 tuples example
     """
    return [0.25, 0.25, 0.25, 0.25]


@pytest.fixture
def return_uniform_joint_proba_public_and_secret_as_dict_for_12_tuples(return_enumdistributiontuples_value_for_secret,
                                                                       return_uniform_joint_proba_secret_for_12_tuples,
                                                                       return_enumdistributiontuples_value_for_public,
                                                                       return_uniform_joint_proba_public_for_12_tuples):
    """
        Fixture method returning the values of the uniform joint distribution for the public tuples and for the secret
         tuples for the 12 tuples example, as a dict.
        Used for test_metric_acsd.py
     """
    return {return_enumdistributiontuples_value_for_secret: return_uniform_joint_proba_secret_for_12_tuples,
            return_enumdistributiontuples_value_for_public: return_uniform_joint_proba_public_for_12_tuples}


@pytest.fixture
def return_distributionasdict_uniform_joint_proba_secret_and_public_12_tuples(
        return_uniform_joint_proba_public_and_secret_as_dict_for_12_tuples):
    """
        Fixture method returning a DistributionAsDict
        containing the uniform joint distributions for secret and public tuples.
        Used for test_metric_acsd.py
     """

    return DistributionAsDict(data=return_uniform_joint_proba_public_and_secret_as_dict_for_12_tuples)


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution - MarginalDistributionJSONGetUseCase
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_marginal_proba_dict_key_proba_secret():
    """
        Fixture method returning the key 'P(SECRET)'
        for the MarginalDistributionJSONGetUseCase.execute() returned value
     """

    return EnumJointMarginalProbability.MARGINAL_PROBA_SECRET.value


@pytest.fixture
def return_marginal_proba_dict_key_proba_public():
    """
        Fixture method returning the key 'P(PUBLIC)'
        for the MarginalDistributionJSONGetUseCase.execute() returned value
     """

    return EnumJointMarginalProbability.MARGINAL_PROBA_PUBLIC.value


@pytest.fixture
def return_marginal_proba_secret_for_data_as_dataframe_distance_equal_0_init():
    """
        Fixture method returning the values of the marginal distribution for the secret tuples
        for data_as_dataframe_distance_equal_0_init
     """
    return [1 / 3, 1 / 3, 1 / 3]


@pytest.fixture
def return_marginal_proba_public_for_data_as_dataframe_distance_equal_0_init():
    """
        Fixture method returning the values of the marginal distribution for the public tuples
        for data_as_dataframe_distance_equal_0_init
     """
    return [1]


@pytest.fixture
def return_empty_marginal_proba_secret():
    """
        Fixture method returning empty values for the marginal distribution of the secret variable
     """
    return []


@pytest.fixture
def return_empty_marginal_proba_public():
    """
        Fixture method returning empty values for the marginal distribution of the public variable
     """
    return []


@pytest.fixture
def return_marginal_proba_secret_for_12_tuples():
    """
        Fixture method returning the values of the marginal distribution for the secret tuples
        for the 12 tuples example
     """
    return [0.28, 0.22, 0.5]


@pytest.fixture
def return_marginal_proba_public_for_12_tuples():
    """
        Fixture method returning the values of the marginal distribution for the public tuples
        for the 12 tuples example
     """
    return [0.3, 0.24, 0.15, 0.31]


@pytest.fixture
def return_marginal_proba_public_and_secret_as_dict_for_data_as_dataframe_distance_equal_0_init(
        return_enumdistributiontuples_value_for_secret,
        return_marginal_proba_secret_for_data_as_dataframe_distance_equal_0_init,
        return_enumdistributiontuples_value_for_public,
        return_marginal_proba_public_for_data_as_dataframe_distance_equal_0_init):
    """
        Fixture method returning the values of the marginal distribution for the public tuples and for the secret tuples
        for data_as_dataframe_distance_equal_0_init, as a dict.
        Used for test_metric_acsd.py
     """
    return {
        return_enumdistributiontuples_value_for_secret:
            return_marginal_proba_secret_for_data_as_dataframe_distance_equal_0_init,
        return_enumdistributiontuples_value_for_public:
            return_marginal_proba_public_for_data_as_dataframe_distance_equal_0_init}


@pytest.fixture
def return_erroneous_empty_marginal_proba_public_and_secret_as_dict(return_enumdistributiontuples_value_for_secret,
                                                                    return_empty_marginal_proba_secret,
                                                                    return_enumdistributiontuples_value_for_public,
                                                                    return_empty_marginal_proba_public):
    """
        Fixture method returning empty values for the marginal distribution of the public variable
         and the secret variable, as a dict.
        Used for test_metric_acsd.py
     """
    return {return_enumdistributiontuples_value_for_secret: return_empty_marginal_proba_secret,
            return_enumdistributiontuples_value_for_public: return_empty_marginal_proba_public}


@pytest.fixture
def return_marginal_proba_public_and_secret_as_dict_for_12_tuples(return_enumdistributiontuples_value_for_secret,
                                                                  return_marginal_proba_secret_for_12_tuples,
                                                                  return_enumdistributiontuples_value_for_public,
                                                                  return_marginal_proba_public_for_12_tuples):
    """
        Fixture method returning the values of the marginal distribution for the public tuples and for the secret tuples
        for the 12 tuples example, as a dict.
        Used for test_metric_acsd.py
     """
    return {return_enumdistributiontuples_value_for_secret: return_marginal_proba_secret_for_12_tuples,
            return_enumdistributiontuples_value_for_public: return_marginal_proba_public_for_12_tuples}


@pytest.fixture
def return_distributionasdict_marginal_proba_secret_and_public_data_as_dataframe_distance_equal_0_init(
        return_marginal_proba_public_and_secret_as_dict_for_data_as_dataframe_distance_equal_0_init):
    """
        Fixture method returning a DistributionAsDict
        containing the marginal distributions for secret and public tuples for data_as_dataframe_distance_equal_0_init.
        Used for test_metric_acsd.py
     """

    return DistributionAsDict(
        data=return_marginal_proba_public_and_secret_as_dict_for_data_as_dataframe_distance_equal_0_init)


@pytest.fixture
def return_erroneous_distributionasdict_marginal_proba_with_empty_secret_and_public_dist(
        return_erroneous_empty_marginal_proba_public_and_secret_as_dict):
    """
        Fixture method returning a DistributionAsDict
        containing empty marginal distributions for the secret and the public variables.
        Used for test_metric_acsd.py
     """

    return DistributionAsDict(data=return_erroneous_empty_marginal_proba_public_and_secret_as_dict)


@pytest.fixture
def return_distributionasdict_marginal_proba_secret_and_public_12_tuples(
        return_marginal_proba_public_and_secret_as_dict_for_12_tuples):
    """
        Fixture method returning a DistributionAsDict
        containing the marginal distributions for secret and public tuples for the 12 tuples example.
        Used for test_metric_acsd.py
     """

    return DistributionAsDict(data=return_marginal_proba_public_and_secret_as_dict_for_12_tuples)


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram : Observation, Public and Secret
# ---------------------------------------------------------------------------------
@pytest.fixture
def dendrogram_observations_a():
    """
        Returns a list of observations (leakage)
    """
    return [6, 9, 14, 12, 1, 8, 2]


@pytest.fixture
def dendrogram_observations_from_indiscernability():
    """
        Returns a list of observations (leakage) related to the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [10, 2, 9, 11, 8, 3, 11, 10, 15, 4, 14, 16]


@pytest.fixture
def dendrogram_sorted_observations_a():
    """
        Returns a list of observations (leakage)
    """
    return [1, 2, 6, 8, 9, 12, 14]


@pytest.fixture
def dendrogram_sorted_observations_from_indiscernability():
    """
        Returns a list of observations (leakage) related to the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [2, 3, 4, 8, 9, 10, 10, 11, 11, 14, 15, 16]


@pytest.fixture
def dendrogram_related_public_from_indiscernability():
    """
        Returns a list of public tuples
    """
    return [[4, 33], [4, 35], [6, 33], [6, 35]]


@pytest.fixture
def dendrogram_related_public_from_indiscernability_4_33():
    """
        Returns the public tuple [4, 33] as a string
    """
    return '[4, 33]'


@pytest.fixture
def dendrogram_related_public_from_indiscernability_4_35():
    """
        Returns the public tuple [4, 35] as a string
    """
    return '[4, 35]'


@pytest.fixture
def dendrogram_related_public_from_indiscernability_6_33():
    """
        Returns the public tuple [6, 33] as a string
    """
    return '[6, 33]'


@pytest.fixture
def dendrogram_related_public_from_indiscernability_6_35():
    """
        Returns the public tuple [6, 35] as a string
    """
    return '[6, 35]'


@pytest.fixture
def dendrogram_final_secret_as_bits_from_indiscernability():
    """
        Returns a list of secret keys related to the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [0b01, 0b10, 0b11, 0b01, 0b10, 0b11, 0b01, 0b10, 0b11, 0b01, 0b10, 0b11]


@pytest.fixture
def dendrogram_related_secret_for_observations_a():
    """
        Returns the associated secret values for the observations a.
        Note: these values are randomly chosen.
    """
    return [1, 2, 3, 4, 5, 6, 7]


@pytest.fixture
def dendrogram_related_secret_from_indiscernability():
    """
        Returns the associated secret values from the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [1, 1, 1, 1, 10, 10, 10, 10, 11, 11, 11, 11]


@pytest.fixture
def dendrogram_related_secret_from_indiscernability_as_bits():
    """
        Returns the associated secret values from the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [0b01, 0b01, 0b01, 0b01, 0b10, 0b10, 0b10, 0b10, 0b11, 0b11, 0b11, 0b11]


@pytest.fixture
def dendrogram_reordered_related_secret_for_observations_a():
    """
        Returns the associated secret values for the observations a.
        Note: Based on the secret values from dendrogram_related_secret_for_observations_a
    """
    return [5, 7, 1, 6, 2, 4, 3]


@pytest.fixture
def dendrogram_reordered_related_secret_from_indiscernability():
    """
        Returns the associated secret values from the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [1, 10, 11, 10, 1, 1, 10, 1, 10, 11, 11, 11]


@pytest.fixture
def dendrogram_reordered_related_secret_from_indiscernability_as_bits():
    """
        Returns the associated secret values from the indiscernability example,
        following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """
    return [0b01, 0b10, 0b11, 0b10, 0b01, 0b01, 0b10, 0b01, 0b10, 0b11, 0b11, 0b11]


@pytest.fixture
def return_dict_tuples_occurrences():
    """
        Return a dictionary of tuples occurrences for testing DendrogramGatheredTuples
    """
    return {}


@pytest.fixture
def return_tuples_list():
    """
        Return a list of tuples for testing DendrogramGatheredTuples
    """
    return []


@pytest.fixture
def return_final_leakage_clustering_4_33_bl():
    """
        Return the final leakage clustering for the public tuple [4,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[8, 10], [15]]


@pytest.fixture
def return_final_secret_clustering_4_33_bl():
    """
        Return the final secret clustering for the public tuple [4,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[10, 1], [11]]


@pytest.fixture
def return_final_secret_public_distrib_4_33_bl():
    """
        Return the final secret public clustering for the public tuple [4,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[0.15, 0.1], [0.1]]


@pytest.fixture
def return_final_leakage_clustering_4_35_bl():
    """
        Return the final leakage clustering for the public tuple [4,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[2, 3, 4]]


@pytest.fixture
def return_final_secret_clustering_4_35_bl():
    """
        Return the final secret clustering for the public tuple [4,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[1, 10, 11]]


@pytest.fixture
def return_final_secret_public_distrib_4_35_bl():
    """
        Return the final secret public clustering for the public tuple [4,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[0.01, 0.01, 0.01]]


@pytest.fixture
def return_final_leakage_clustering_6_33_bl():
    """
        Return the final leakage clustering for the public tuple [6,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[9, 11], [14]]


@pytest.fixture
def return_final_secret_clustering_6_33_bl():
    """
        Return the final secret clustering for the public tuple [6,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[1, 10], [11]]


@pytest.fixture
def return_final_secret_public_distrib_6_33_bl():
    """
        Return the final secret public clustering for the public tuple [6,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[0.14, 0.04], [0.2]]


@pytest.fixture
def return_final_leakage_clustering_6_35_bl():
    """
        Return the final leakage clustering for the public tuple [6,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[10, 11], [16]]


@pytest.fixture
def return_final_secret_clustering_6_35_bl():
    """
        Return the final secret clustering for the public tuple [6,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[10, 1], [11]]


@pytest.fixture
def return_final_secret_public_distrib_6_35_bl():
    """
        Return the final secret public clustering for the public tuple [6,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.
    """
    return [[0.09, 0.05], [0.1]]


@pytest.fixture
def return_final_leakage_clustering_for_default_indiscernability_bl():
    """
        Return the final leakage clustering for DendrogramResultsElement of type default following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods
    """
    return [[1, 2, 6, 8, 9, 12, 14]]


@pytest.fixture
def return_final_leakage_clustering_for_resolution_indiscernability_bl():
    """
        Return the final leakage clustering for DendrogramResultsElement of type resolution following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods
    """
    return [[1, 2], [6, 8, 9], [12, 14]]


@pytest.fixture
def return_final_leakage_clustering_for_nb_clusters_indiscernability_bl():
    """
        Return the final leakage clustering for DendrogramResultsElement of type nb_clusters
        following example documented and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods
    """
    return [[1, 2], [6, 8, 9], [12, 14]]


# -----------------------------------------------------------------------------------
# Fixtures for the Dendrogram : Leakage, Public and Secret - For deliverable D4.4
# -----------------------------------------------------------------------------------
@pytest.fixture
def dendrogram_leakages_for_public_tuple_2_dot_1_in_d4_dot_4():
    """
        Returns the list of leakages for public tuple [2.1] in deliverable D4.4.
    """
    return [2064, 2064, 2044, 1976, 1996, 1976, 1860, 1976, 1996, 2064]


@pytest.fixture
def dendrogram_leakages_for_public_tuple_4_dot_15_in_d4_dot_4():
    """
        Returns the list of leakages for public tuple [4.15] in deliverable D4.4.
    """
    return [2142, 2142, 2116, 2042, 2068, 2042, 1920, 2042, 2068, 2142]


@pytest.fixture
def dendrogram_leakages_for_public_tuple_8_dot_35_in_d4_dot_4():
    """
        Returns the list of leakages for public tuple [8.35] in deliverable D4.4.
    """
    return [2376, 2376, 2332, 2240, 2284, 2240, 2100, 2240, 2284, 2376]


@pytest.fixture
def dendrogram_public_tuples_in_d4_dot_4():
    """
        Returns the list of public tuples in deliverable D4.4.
    """
    return [2.1, 4.15, 8.35]


@pytest.fixture
def dendrogram_public_tuple_2_dot_1_in_d4_dot_4():
    """
        Returns the public tuple [2.1] in deliverable D4.4.
    """
    return [2.1]


@pytest.fixture
def dendrogram_public_tuple_4_dot_15_in_d4_dot_4():
    """
        Returns the public tuple [4.15] in deliverable D4.4.
    """
    return [4.15]


@pytest.fixture
def dendrogram_public_tuple_8_dot_35_in_d4_dot_4():
    """
        Returns the public tuple [8.35] in deliverable D4.4.
    """
    return [8.35]


# -----------------------------------------------------------------------------------
# Fixtures for the Dendrogram : Observation, Public and Secret - For deliverable D4.5
# -----------------------------------------------------------------------------------
@pytest.fixture
def dendrogram_leakages_for_public_tuple_0_0_in_d4_dot_5():
    """
        Returns the list of leakages for public tuple '[0,0]' in deliverable D4.4.
    """
    return [7, 11, 13]


@pytest.fixture
def dendrogram_leakages_for_public_tuple_0_25_in_d4_dot_5():
    """
        Returns the list of leakages for public tuple '[0,25]' in deliverable D4.4.
    """
    return [8, 10, 12]


@pytest.fixture
def dendrogram_leakages_for_public_tuple_0_50_in_d4_dot_5():
    """
        Returns the list of leakages for public tuple '[0,50]' in deliverable D4.4.
    """
    return [10, 9, 9]


@pytest.fixture
def dendrogram_leakages_for_public_tuple_0_75_in_d4_dot_5():
    """
        Returns the list of leakages for public tuple '[0,75]' in deliverable D4.4.
    """
    return [13, 10, 7]


@pytest.fixture
def dendrogram_leakages_for_public_tuple_0_100_in_d4_dot_5():
    """
        Returns the list of leakages for public tuple '[0,100]' in deliverable D4.4.
    """
    return [15, 9, 5]


@pytest.fixture
def dendrogram_leakages_for_public_tuple_1_0_in_d4_dot_5():
    """
        Returns the list of leakages for public tuple '[1,0]' in deliverable D4.4.
    """
    return [8, 10, 13]


@pytest.fixture
def dendrogram_leakages_for_public_tuple_1_25_in_d4_dot_5():
    """
        Returns the list of leakages for public tuple '[1,25]' in deliverable D4.4.
    """
    return [9, 11, 11]


@pytest.fixture
def dendrogram_leakages_for_public_tuple_1_50_in_d4_dot_5():
    """
        Returns the list of leakages for public tuple '[1,50]' in deliverable D4.4.
    """
    return [11, 10, 11]


@pytest.fixture
def dendrogram_leakages_for_public_tuple_1_75_in_d4_dot_5():
    """
        Returns the list of leakages for public tuple '[1,75]' in deliverable D4.4.
    """
    return [13, 9, 8]


@pytest.fixture
def dendrogram_leakages_for_public_tuple_1_100_in_d4_dot_5():
    """
        Returns the list of leakages for public tuple '[1,100]' in deliverable D4.4.
    """
    return [16, 11, 6]


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related: Distance - Indiscernability
# ---------------------------------------------------------------------------------
@pytest.fixture
def distances_initial_for_observations_a():
    """
        Returns what the distances should be, given the dendrogram_observations_a.
    """

    return [1, 4, 2, 1, 3, 2]


@pytest.fixture
def distances_initial_for_observations_indiscernability():
    """
        Returns what the distances should be, given the dendrogram_observations_from_indiscernability.
    """

    return [1, 1, 4, 1, 1, 0, 1, 0, 3, 1, 1]


@pytest.fixture
def distances_sorted_no_duplicates_for_observations_a():
    """
        Returns what the distances should be, given the dendrogram_observations_a.
        The list is sorted and pruned for duplicates
    """

    return [1, 2, 3, 4]


@pytest.fixture
def distances_sorted_no_duplicates_for_observations_indiscernability():
    """
        Returns what the distances should be, given the dendrogram_observations_from_indiscernability.
        The list is sorted and pruned for duplicates
    """

    return [0, 1, 3, 4]


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related: Distance - D4.4
# ---------------------------------------------------------------------------------
@pytest.fixture
def distances_initial_for_leakages_of_public_tuple_2_dot_1_from_d4_dot_4():
    """
        Returns what the distances between the leakages associated to public tuple 2.1 from D4.4 should be.
        Leakages are : [2064, 2064, 2044, 1976, 1996, 1976, 1860, 1976, 1996, 2064]
    """

    return [0, 20, 68, 20, 20, 116, 116, 20, 68]


@pytest.fixture
def distances_sorted_no_duplicates_for_leakages_of_public_tuple_2_dot_1_from_d4_dot_4():
    """
        Returns what the distances between the leakages associated to public tuple 2.1 from D4.4 should be
        once the list is sorted and pruned for duplicates.
    """

    return [0, 20, 68, 116]


@pytest.fixture
def distances_initial_for_leakages_of_public_tuple_4_dot_15_from_d4_dot_4():
    """
        Returns what the distances between the leakages associated to public tuple 4.15 from D4.4 should be.
        Leakages are : [2142, 2142, 2116, 2042, 2068, 2042, 1920, 2042, 2068, 2142]
    """

    return [0, 26, 74, 26, 26, 122, 122, 26, 74]


@pytest.fixture
def distances_sorted_no_duplicates_for_leakages_of_public_tuple_4_dot_15_from_d4_dot_4():
    """
        Returns what the distances between the leakages associated to public tuple 4.15 from D4.4 should be
        once the list is sorted and pruned for duplicates.
    """

    return [0, 26, 74, 122]


@pytest.fixture
def distances_initial_for_leakages_of_public_tuple_8_dot_35_from_d4_dot_4():
    """
        Returns what the distances between the leakages associated to public tuple 8.35 from D4.4 should be.
        Leakages are : [2376, 2376, 2332, 2240, 2284, 2240, 2100, 2240, 2284, 2376]
    """

    return [0, 42, 92, 44, 44, 140, 140, 44, 92]


@pytest.fixture
def distances_sorted_no_duplicates_for_leakages_of_public_tuple_8_dot_35_from_d4_dot_4():
    """
        Returns what the distances between the leakages associated to public tuple 8.35 from D4.4 should be
        once the list is sorted and pruned for duplicates.
    """

    return [0, 42, 44, 92, 140]


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related: Distance - D4.5
# ---------------------------------------------------------------------------------
@pytest.fixture
def distances_initial_for_leakages_of_public_tuple_0_0_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['0,0'] from D4.5 should be.
        Leakages are : [7, 11, 13]
    """

    return [4, 2]


@pytest.fixture
def distances_sorted_no_duplicates_for_leakages_of_public_tuple_0_0_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['0,0'] from D4.5 should be
        once the list is sorted and pruned for duplicates.
    """

    return [2, 4]


@pytest.fixture
def distances_initial_for_leakages_of_public_tuple_0_25_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['0,25'] from D4.5 should be.
        Leakages are : [8, 10, 12]
    """

    return [2, 2]


@pytest.fixture
def distances_sorted_no_duplicates_for_leakages_of_public_tuple_0_25_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['0,25'] from D4.5 should be
        once the list is sorted and pruned for duplicates.
    """

    return [2]


@pytest.fixture
def distances_initial_for_leakages_of_public_tuple_0_50_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['0,50'] from D4.5 should be.
        Leakages are : [10, 9, 9]
    """

    return [1, 0]


@pytest.fixture
def distances_sorted_no_duplicates_for_leakages_of_public_tuple_0_50_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['0,50'] from D4.5 should be
        once the list is sorted and pruned for duplicates.
    """

    return [0, 1]


@pytest.fixture
def distances_initial_for_leakages_of_public_tuple_0_75_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['0,75'] from D4.5 should be.
        Leakages are : [13, 10, 7]
    """

    return [3, 3]


@pytest.fixture
def distances_sorted_no_duplicates_for_leakages_of_public_tuple_0_75_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['0,75'] from D4.5 should be
        once the list is sorted and pruned for duplicates.
    """

    return [3]


@pytest.fixture
def distances_initial_for_leakages_of_public_tuple_0_100_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['0,100'] from D4.5 should be.
        Leakages are : [15, 9, 5]
    """

    return [6, 4]


@pytest.fixture
def distances_sorted_no_duplicates_for_leakages_of_public_tuple_0_100_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['0,100'] from D4.5 should be
        once the list is sorted and pruned for duplicates.
    """

    return [4, 6]


@pytest.fixture
def distances_initial_for_leakages_of_public_tuple_1_0_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['1,0'] from D4.5 should be.
        Leakages are : [8, 10, 13]
    """

    return [2, 3]


@pytest.fixture
def distances_sorted_no_duplicates_for_leakages_of_public_tuple_1_0_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['1,0'] from D4.5 should be
        once the list is sorted and pruned for duplicates.
    """

    return [2, 3]


@pytest.fixture
def distances_initial_for_leakages_of_public_tuple_1_25_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['1,25'] from D4.5 should be.
        Leakages are : [9, 11, 11]
    """

    return [2, 0]


@pytest.fixture
def distances_sorted_no_duplicates_for_leakages_of_public_tuple_1_25_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['1,25'] from D4.5 should be
        once the list is sorted and pruned for duplicates.
    """

    return [0, 2]


@pytest.fixture
def distances_initial_for_leakages_of_public_tuple_1_50_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['1,50'] from D4.5 should be.
        Leakages are : [11, 10, 11]
    """

    return [1, 1]


@pytest.fixture
def distances_sorted_no_duplicates_for_leakages_of_public_tuple_1_50_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['1,50'] from D4.5 should be
        once the list is sorted and pruned for duplicates.
    """

    return [1]


@pytest.fixture
def distances_initial_for_leakages_of_public_tuple_1_75_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['1,75'] from D4.5 should be.
        Leakages are : [13, 9, 8]
    """

    return [4, 1]


@pytest.fixture
def distances_sorted_no_duplicates_for_leakages_of_public_tuple_1_75_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['1,75'] from D4.5 should be
        once the list is sorted and pruned for duplicates.
    """

    return [1, 4]


@pytest.fixture
def distances_initial_for_leakages_of_public_tuple_1_100_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['1,100'] from D4.5 should be.
        Leakages are : [16, 11, 6]
    """

    return [5, 5]


@pytest.fixture
def distances_sorted_no_duplicates_for_leakages_of_public_tuple_1_100_from_d4_dot_5():
    """
        Returns what the distances between the leakages associated to public tuple ['1,100'] from D4.5 should be
        once the list is sorted and pruned for duplicates.
    """

    return [5]


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related: resolution and nb_clusters
# ---------------------------------------------------------------------------------
@pytest.fixture
def resolution_equals_1():
    """
        Returns a resolution parameter equal to 1
    """
    return 1


@pytest.fixture
def resolution_equals_3():
    """
        Returns a resolution parameter equal to 3
    """
    return 3


@pytest.fixture
def resolution_equals_5():
    """
        Returns a resolution parameter equal to 5
    """
    return 5


@pytest.fixture
def resolution_equals_999999():
    """
        Returns a resolution parameter equal to 999999
    """
    return 999999


@pytest.fixture
def resolution_equals_none():
    """
        Returns a resolution parameter equal None
    """
    return None


@pytest.fixture
def nb_clusters_equals_1():
    """
        Returns a nb_clusters parameter equal to 1
    """
    return 1


@pytest.fixture
def nb_clusters_equals_2():
    """
        Returns a nb_clusters parameter equal to 2
    """
    return 2


@pytest.fixture
def nb_clusters_equals_4():
    """
        Returns a nb_clusters parameter equal to 4
    """
    return 4


@pytest.fixture
def nb_clusters_equals_6():
    """
        Returns a nb_clusters parameter equal to 6
    """
    return 6


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related: nb_clusters for D4.4
# ---------------------------------------------------------------------------------
@pytest.fixture
def nb_clusters_equals_6_for_d4_dot_4():
    """
        Returns a nb_clusters parameter equal to 6 which is the one that should be used
        to compute the IIR given the values in D4.4
    """
    return 6


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related: resolution and nb_clusters for D4.5
# ---------------------------------------------------------------------------------
@pytest.fixture
def nb_clusters_equals_2_for_d4_dot_5():
    """
        Returns a nb_clusters parameter equal to 2 which is the one that should be used
        to compute the IIR given the values in D4.5
    """
    return 2


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related: Termination Criteria
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_termination_criterium_default():
    """
        Returns the termination criterium DEFAULT
    """
    return EnumTerminationCriteria.DEFAULT


@pytest.fixture
def return_termination_criterium_default_value():
    """
        Returns the termination criterium DEFAULT value
    """
    return EnumTerminationCriteria.DEFAULT.value


@pytest.fixture
def return_termination_criterium_resolution():
    """
        Returns the termination criterium RESOLUTION
    """
    return EnumTerminationCriteria.RESOLUTION


@pytest.fixture
def return_termination_criterium_resolution_value():
    """
        Returns the termination criterium RESOLUTION value
    """
    return EnumTerminationCriteria.RESOLUTION.value


@pytest.fixture
def return_termination_criterium_nb_clusters():
    """
        Returns the termination criterium NB_CLUSTERS
    """
    return EnumTerminationCriteria.NB_CLUSTERS


@pytest.fixture
def return_termination_criterium_nb_clusters_value():
    """
        Returns the termination criterium NB_CLUSTERS value
    """
    return EnumTerminationCriteria.NB_CLUSTERS.value


# ---------------------------------------------------------------------------------
# Fixtures for the IndiscernabilityLevel and related: IIR, HK, H(K|L,X)
# ---------------------------------------------------------------------------------

@pytest.fixture
def return_iir_equals_059929():
    """ Return an IIR equal to 0.5992956902270417 """
    return 0.5992956902270417


@pytest.fixture
def return_iir_equals_09422():
    """ Return an IIR equal to 0.942291207136538 """
    return 0.942291207136538


@pytest.fixture
def return_iir_equals_05992():
    """ Return an IIR equal to 0.942291207136538 """
    return 0.5992956902270417


@pytest.fixture
def return_hklx__equals_024273():
    """ Return a value for H(K | L,X) equal to 0.24273764861366717 """
    return 0.24273764861366717


@pytest.fixture
def return_hklx__equals_00475():
    """ Return a value for H(K | L,X) equal to 0.04754887502163469 """
    return 0.04754887502163469


@pytest.fixture
def return_hklx__equals_01375():
    """ Return a value for H(K | L,X) equal to 0.1375568111715517 """
    return 0.1375568111715517


@pytest.fixture
def return_hklx__equals_013164():
    """ Return a value for H(K | L,X) equal to 0.13164003421388837 """
    return 0.13164003421388837


# ---------------------------------------------------------------------------------
# Fixtures for the IndiscernabilityLevel and related: IIR, HK, H(K|L,X) for D4.4
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_hk_equals_3321_for_d4_dot_4():
    """ Return a value for H(K) equal to 3.321928094887362 """
    return 3.321928094887362


@pytest.fixture
def return_hklx_equals_11509_for_d4_dot_4():
    """ Return a value for H(K | L,X) equal to ???? """
    return 1.1509775004326936


@pytest.fixture
def return_iir_equals_03464_for_d4_dot_4():
    """ Return an IIR equal to 0.3464787519645937 """
    return 0.3464787519645937


# ---------------------------------------------------------------------------------
# Fixtures for the IndiscernabilityLevel and related: IIR, HK, H(K|L,X) for D4.5
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_hk_equals_14854_for_d4_dot_5():
    """ Return a value for H(K) equal to 1.4854752972273346 """
    return 1.4854752972273346


@pytest.fixture
def return_hklx_equals_09363_for_d4_dot_5():
    """ Return a value for H(K | L,X) equal to 0.9363821598605842 """
    return 0.9363821598605842


@pytest.fixture
def return_iir_equals_06303_for_d4_dot_5():
    """ Return an IIR equal to 0.6303586209803406 """
    return 0.6303586209803406


# ---------------------------------------------------------------------------------
# Fixtures for the Dataframe and associated
# ---------------------------------------------------------------------------------
@pytest.fixture
def dataframe_distance_equal_60cheb_20_eucl_init():
    """ Fixture method to generate a Dataframe for which results to expect are :
        2124 - 2064 = 60 for Chebyshev distance
        20 for Euclidian distance
    """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "10100001000001111000101011000111",
                               "10111111111100101001011110110001"],
                    'a': [2,
                          2,
                          2],
                    'n': [10,
                          10,
                          10],
                    'time_worst': [2064,
                                   2124,
                                   2064
                                   ]
                    })

    return df


@pytest.fixture
def dataframe_distance_equal_0_init():
    """ Fixture method to generate a Dataframe for which distance equals to 0 """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "10100001000001111000101011000111",
                               "10111111111100101001011110110001"],
                    'a': [2,
                          2,
                          2],
                    'n': [10,
                          10,
                          10],
                    'time_worst': [2064,
                                   2064,
                                   2064
                                   ]
                    })

    return df


@pytest.fixture
def dataframe_distance_equal_0_with_two_occurrences_init():
    """ Fixture method to generate a Dataframe for which distance equals to 0 """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "10111111111100101001011110110001"],
                    'a': [2,
                          2],
                    'n': [10,
                          10],
                    'time_worst': [2064,
                                   2064
                                   ]
                    })

    return df


@pytest.fixture
def dataframe_distance_equal_0_with_two_occurrences_init():
    """ Fixture method to generate a Dataframe for which distance equals to 0 """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "10111111111100101001011110110001"],
                    'a': [2,
                          2],
                    'n': [10,
                          10],
                    'time_worst': [2064,
                                   2064
                                   ]
                    })

    return df


@pytest.fixture
def dataframe_from_indiscernability():
    """
        Fixture method to generate a Dataframe following example made by Yoann Marquer and available in slides
        on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary
    """

    df = DataFrame({'secret': ["01", "10", "11",
                               "01", "10", "11",
                               "01", "10", "11",
                               "01", "10", "11"],
                    'a': [4, 4, 4,
                          4, 4, 4,
                          6, 6, 6,
                          6, 6, 6],
                    'n': [33, 33, 33,
                          33, 33, 33,
                          35, 35, 35,
                          35, 35, 35],
                    'time_worst': [10, 8, 15,
                                   2, 3, 4,
                                   9, 11, 14,
                                   11, 10, 16]
                    })

    return df


@pytest.fixture
def dataframe_from_indiscernability_bl():
    """
        Fixture method to generate a Dataframe following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods
    """

    df = DataFrame({'secret': ["01", "01", "01", "01",
                               "10", "10", "10", "10",
                               "11", "11", "11", "11"],
                    'a': [4, 4, 6, 6,
                          4, 4, 6, 6,
                          4, 4, 6, 6],
                    'n': [33, 35, 33, 35,
                          33, 35, 33, 35,
                          33, 35, 33, 35],
                    'time_worst': [10, 2, 9, 11,
                                   8, 3, 11, 10,
                                   15, 4, 14, 16]
                    })

    return df


# ---------------------------------------------------------------------------------
# Fixtures for the Dataframe and associated for D4.4
# ---------------------------------------------------------------------------------
@pytest.fixture
def dataframe_from_d4_dot_4_page_73():
    """
        Fixture method to generate a Dataframe following example from deliverable D4.4 page 73:
        https://gitlab.inria.fr/TeamPlay/TeamPlay_Management/-/tree/master/Deliverables/D4.4
    """

    df = DataFrame({'k': ["00000001111100101111010111001011",
                          "00000001111100101111010111001011",
                          "00000001111100101111010111001011",
                          "10100001000001111000101011000111",
                          "10100001000001111000101011000111",
                          "10100001000001111000101011000111",
                          "10111111111100101001011110110001",
                          "10111111111100101001011110110001",
                          "10111111111100101001011110110001",
                          "01100011111000011010010000001110",
                          "01100011111000011010010000001110",
                          "01100011111000011010010000001110",
                          "11010010110100000101010101110100",
                          "11010010110100000101010101110100",
                          "11010010110100000101010101110100",
                          "11110000101111111111001010010010",
                          "11110000101111111111001010010010",
                          "11110000101111111111001010010010",
                          "10111011000011111110010010000001",
                          "10111011000011111110010010000001",
                          "10111011000011111110010010000001",
                          "11110001100101000100001100000111",
                          "11110001100101000100001100000111",
                          "11110001100101000100001100000111",
                          "01110101010001100000010100100111",
                          "01110101010001100000010100100111",
                          "01110101010001100000010100100111",
                          "10010110101001001000100010111100",
                          "10010110101001001000100010111100",
                          "10010110101001001000100010111100"],
                    'x': ["2.1", "4.15", "8.35",
                          "2.1", "4.15", "8.35",
                          "2.1", "4.15", "8.35",
                          "2.1", "4.15", "8.35",
                          "2.1", "4.15", "8.35",
                          "2.1", "4.15", "8.35",
                          "2.1", "4.15", "8.35",
                          "2.1", "4.15", "8.35",
                          "2.1", "4.15", "8.35",
                          "2.1", "4.15", "8.35"],
                    'power_avg': [2064, 2142, 2376,
                                  2064, 2142, 2376,
                                  2044, 2116, 2332,
                                  1976, 2042, 2240,
                                  1996, 2068, 2284,
                                  1976, 2042, 2240,
                                  1860, 1920, 2100,
                                  1976, 2042, 2240,
                                  1996, 2068, 2284,
                                  2064, 2142, 2376]
                    })

    return df


# ---------------------------------------------------------------------------------
# Fixtures for the Dataframe and associated for D4.5
# ---------------------------------------------------------------------------------
@pytest.fixture
def dataframe_from_d45_page_94():
    """
        Fixture method to generate a Dataframe following example from deliverable D4.5 page 94:
        https://gitlab.inria.fr/TeamPlay/TeamPlay_Management/-/tree/master/Deliverables/D4.5
    """

    df = DataFrame({'k': ["0", "0", "0", "0", "0", "0", "0", "0", "0", "0",
                          "1", "1", "1", "1", "1", "1", "1", "1", "1", "1",
                          "10", "10", "10", "10", "10", "10", "10", "10", "10", "10"],
                    'x': ["0", "0", "0", "0", "0",
                          "1", "1", "1", "1", "1",
                          "0", "0", "0", "0", "0",
                          "1", "1", "1", "1", "1",
                          "0", "0", "0", "0", "0",
                          "1", "1", "1", "1", "1"],
                    't': ["0", "25", "50", "75", "100",
                          "0", "25", "50", "75", "100",
                          "0", "25", "50", "75", "100",
                          "0", "25", "50", "75", "100",
                          "0", "25", "50", "75", "100",
                          "0", "25", "50", "75", "100"],
                    'time_worst': [7, 8, 10, 13, 15,
                                   8, 9, 11, 13, 16,
                                   11, 10, 9, 10, 9,
                                   10, 11, 10, 9, 11,
                                   13, 12, 9, 7, 5,
                                   13, 11, 11, 8, 6]
                    })

    return df


# ---------------------------------------------------------------------------------
# Fixtures for the DataAsDataframe and associated
# ---------------------------------------------------------------------------------
@pytest.fixture
def data_as_dataframe_distance_equal_0_init(dataframe_distance_equal_0_init):
    """
         Fixture method to generate a DataAsDataframe from a DataFrame which distance equals to 0

        :param dataframe_distance_equal_0_init:
            pytest.fixture returning a DataFrame
        :return:
            DataAsDataFrame: DataFrame nested in a DataAsDataFrame
    """
    data_as_df = DataAsDataFrame(dataframe_distance_equal_0_init)

    return data_as_df


@pytest.fixture
def data_as_dataframe_distance_equal_60cheb_20_eucl_init(dataframe_distance_equal_60cheb_20_eucl_init):
    """
         Fixture method to generate a DataAsDataframe from a DataFrame which distance equals to:
            60 for Chebyshev
            20 for Euclidian

        :param dataframe_distance_equal_60cheb_20_eucl_init:
            pytest.fixture returning a DataFrame
        :return:
            DataAsDataFrame: DataFrame nested in a DataAsDataFrame
    """
    data_as_df = DataAsDataFrame(dataframe_distance_equal_60cheb_20_eucl_init)

    return data_as_df


@pytest.fixture
def data_as_dataframe_from_indiscernability(dataframe_from_indiscernability):
    """
         Fixture method to generate a DataAsDataframe from a DataFrame example
         made by Yoann Marquer and available in slides
         on the INRIA gitlab repository: Git/TeamPlay/discernibility/Slides/2021-04-20_Metrics_Summary

        :param dataframe_from_indiscernability:
            pytest.fixture returning a DataFrame
        :return:
            DataAsDataFrame: DataFrame nested in a DataAsDataFrame
    """
    data_as_df = DataAsDataFrame(dataframe_from_indiscernability)

    return data_as_df


@pytest.fixture
def data_as_dataframe_from_indiscernability_bl(dataframe_from_indiscernability_bl):
    """
         Fixture method to generate a DataAsDataframe following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods

        :param dataframe_from_indiscernability_bl:
            pytest.fixture returning a DataFrame
        :return:
            DataAsDataFrame: DataFrame nested in a DataAsDataFrame
    """
    data_as_df = DataAsDataFrame(dataframe_from_indiscernability_bl)

    return data_as_df


# ---------------------------------------------------------------------------------
# Fixtures for the Dataframe and associated for D4.4
# ---------------------------------------------------------------------------------
@pytest.fixture
def data_as_dataframe_from_d4_dot_4_page_73(dataframe_from_d4_dot_4_page_73):
    """
         Fixture method to generate a Dataframe following example from deliverable D4.4 page 73:
        https://gitlab.inria.fr/TeamPlay/TeamPlay_Management/-/tree/master/Deliverables/D4.4

        :param dataframe_from_d4_dot_4_page_73:
            pytest.fixture returning a DataFrame
        :return:
            DataAsDataFrame: DataFrame nested in a DataAsDataFrame
    """
    data_as_df = DataAsDataFrame(dataframe_from_d4_dot_4_page_73)

    return data_as_df


# ---------------------------------------------------------------------------------
# Fixtures for the Dataframe and associated for D4.5
# ---------------------------------------------------------------------------------
@pytest.fixture
def data_as_dataframe_from_d4_dot_5_page_94(dataframe_from_d45_page_94):
    """
        Fixture method to generate a Dataframe following example from deliverable D4.5 page 94:
        https://gitlab.inria.fr/TeamPlay/TeamPlay_Management/-/tree/master/Deliverables/D4.5

        :param dataframe_from_d45_page_94:
            pytest.fixture returning a DataFrame
        :return:
            DataAsDataFrame: DataFrame nested in a DataAsDataFrame
    """
    data_as_df = DataAsDataFrame(dataframe_from_d45_page_94)

    return data_as_df


@pytest.fixture
def dataframe_distance_equal_0dot19_init():
    """ Fixture method to generate a Dataframe
        2104 - 2064 = 40 for euclidian distance
    """

    df = DataFrame({'secret': ["00000001111100101111010111001011", "00000001111100101111010111001011",
                               "00000001111100101111010111001011",
                               "10100001000001111000101011000111", "10100001000001111000101011000111",
                               "10100001000001111000101011000111",
                               "10111111111100101001011110110001", "10111111111100101001011110110001",
                               "10111111111100101001011110110001"],
                    'a': [2, 4, 8,
                          2, 4, 8,
                          2, 4, 8],
                    'n': [10, 15, 35,
                          10, 15, 35,
                          10, 15, 35],
                    'time': [2064, 2142, 2376,
                             2104, 2142, 2376,
                             2084, 2142, 2376
                             ]
                    })

    return df


@pytest.fixture
def dataframe_distance_equal_0_init():
    """ Fixture method to generate a Dataframe with 2 tuples only """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "10100001000001111000101011000111",
                               "10111111111100101001011110110001"],
                    'a': [2,
                          2,
                          2],
                    'n': [10,
                          10,
                          10],
                    'time': [2064,
                             2064,
                             2064
                             ]
                    })

    return df


@pytest.fixture
def dataframe_distance_homogeneity_ax_init():
    """ Fixture method to generate a Dataframe to validate the homogeneity property:
        homogeneity: d(a*x,0) = |a|*d(x,0)
        Here we deal with the d(a*x,0) distance with a = 100 and x = 1
     """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "10100001000001111000101011000111"],
                    'a': [2,
                          2],
                    'n': [10,
                          10],
                    'time': [100,
                             0
                             ]
                    })

    return df


@pytest.fixture
def dataframe_distance_homogeneity_x_init():
    """ Fixture method to generate a Dataframe to validate the homogeneity property:
        homogeneity: d(a*x,0) = |a|*d(x,0)
        Here we deal with the |a|*d(x,0) distance with a = 100 and x = 1
     """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "10100001000001111000101011000111"],
                    'a': [2,
                          2],
                    'n': [10,
                          10],
                    'time': [1,
                             0
                             ]
                    })

    return df


@pytest.fixture
def dataframe_distance_triangle_inequality_dxz_init():
    """ Fixture method to generate a Dataframe to validate the triangle inequality property:
        triangle inequality: d(x,z) <= d(x,y) + d(y,z)
     """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "10100001000001111000101011000111"],
                    'a': [2,
                          2],
                    'n': [10,
                          10],
                    'time': [1,
                             100
                             ]
                    })

    return df


@pytest.fixture
def dataframe_distance_triangle_inequality_dxy_init():
    """ Fixture method to generate a Dataframe to validate the triangle inequality property:
        triangle inequality: d(x,z) <= d(x,y) + d(y,z)
     """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "10100001000001111000101011000111"],
                    'a': [2,
                          2],
                    'n': [10,
                          10],
                    'time': [1,
                             50
                             ]
                    })

    return df


@pytest.fixture
def dataframe_distance_triangle_inequality_dyz_init():
    """
        Fixture method to generate a Dataframe to validate the triangle inequality property:
        triangle inequality: d(x,z) <= d(x,y) + d(y,z)
     """

    df = DataFrame({'secret': ["00000001111100101111010111001011",
                               "10100001000001111000101011000111"],
                    'a': [2,
                          2],
                    'n': [10,
                          10],
                    'time': [50,
                             100
                             ]
                    })

    return df


# ---------------------------------------------------------------------------------
# Fixtures for the Dataframe Series: DataAsDataframeAsDict associated for D4.4
# ---------------------------------------------------------------------------------

@pytest.fixture
def return_dataframe_from_d44_page_73_as_dict(return_se_file_as_dict_key_2_10,
                                              return_se_file_as_dict_value_for_2_10,
                                              return_se_file_as_dict_key_4_15,
                                              return_se_file_as_dict_value_for_4_15,
                                              return_se_file_as_dict_key_8_35,
                                              return_se_file_as_dict_value_for_8_35):
    """
        Fixture method returning a Dict using data from dataframe_from_d44_page_73
     """
    data_as_dataframe_as_dict = {
        return_se_file_as_dict_key_2_10: return_se_file_as_dict_value_for_2_10,
        return_se_file_as_dict_key_4_15: return_se_file_as_dict_value_for_4_15,
        return_se_file_as_dict_key_8_35: return_se_file_as_dict_value_for_8_35
    }

    return data_as_dataframe_as_dict


@pytest.fixture
def return_se_file_as_dict_key_2_10():
    """
        Fixture method returning the key '2,10' for dataframe_from_d44_page_73_as_dict
     """

    return '2,10'


@pytest.fixture
def return_se_file_as_dict_value_for_2_10():
    """
        Fixture method returning the value at key '2,10' in dataframe_from_d44_page_73_as_dict
     """
    return [
        ['00000001111100101111010111001011', '2064'],
        ['10100001000001111000101011000111', '2064'],
        ['10111111111100101001011110110001', '2044'],
        ['01100011111000011010010000001110', '1976'],
        ['11010010110100000101010101110100', '1996'],
        ['11110000101111111111001010010010', '1976'],
        ['10111011000011111110010010000001', '1860'],
        ['11110001100101000100001100000111', '1976'],
        ['01110101010001100000010100100111', '1996'],
        ['10010110101001001000100010111100', '2064']
    ]


@pytest.fixture
def return_se_file_as_dict_key_4_15():
    """
        Fixture method returning the key '4,15' for dataframe_from_d44_page_73_as_dict
     """

    return '4,15'


@pytest.fixture
def return_se_file_as_dict_value_for_4_15():
    """
        Fixture method returning the value at key '4,15' in dataframe_from_d44_page_73_as_dict
     """
    return [
        ['00000001111100101111010111001011', '2142'],
        ['10100001000001111000101011000111', '2142'],
        ['10111111111100101001011110110001', '2116'],
        ['01100011111000011010010000001110', '2042'],
        ['11010010110100000101010101110100', '2068'],
        ['11110000101111111111001010010010', '2042'],
        ['10111011000011111110010010000001', '1920'],
        ['11110001100101000100001100000111', '2042'],
        ['01110101010001100000010100100111', '2068'],
        ['10010110101001001000100010111100', '2142']
    ]


@pytest.fixture
def return_se_file_as_dict_key_8_35():
    """
        Fixture method returning the key '8,35' for dataframe_from_d44_page_73_as_dict
     """

    return '8,35'


@pytest.fixture
def return_se_file_as_dict_value_for_8_35():
    """
        Fixture method returning the value at key '8,35' in dataframe_from_d44_page_73_as_dict
     """
    return [
        ['00000001111100101111010111001011', '2376'],
        ['10100001000001111000101011000111', '2376'],
        ['10111111111100101001011110110001', '2332'],
        ['01100011111000011010010000001110', '2240'],
        ['11010010110100000101010101110100', '2284'],
        ['11110000101111111111001010010010', '2240'],
        ['10111011000011111110010010000001', '2100'],
        ['11110001100101000100001100000111', '2240'],
        ['01110101010001100000010100100111', '2284'],
        ['10010110101001001000100010111100', '2376']
    ]


# ---------------------------------------------------------------------------------
# Fixtures for the Dataframe Series: DataAsDataframeAsDict associated for D4.5
# ---------------------------------------------------------------------------------
@pytest.fixture
def return_dataframe_from_d45_page_94_as_dict(return_se_file_as_dict_key_0_0,
                                              return_se_file_as_dict_value_for_2_10,
                                              return_se_file_as_dict_key_4_15,
                                              return_se_file_as_dict_value_for_4_15,
                                              return_se_file_as_dict_key_8_35,
                                              return_se_file_as_dict_value_for_8_35):
    """
        Fixture method returning a Dict using data from dataframe_from_d45_page_94
     """
    data_as_dataframe_as_dict = {
        return_se_file_as_dict_key_0_0: return_se_file_as_dict_value_for_0_0,
        return_se_file_as_dict_key_0_25: return_se_file_as_dict_value_for_0_25,
        return_se_file_as_dict_key_0_5: return_se_file_as_dict_value_for_0_5,
        return_se_file_as_dict_key_0_75: return_se_file_as_dict_value_for_0_75,
        return_se_file_as_dict_key_0_1: return_se_file_as_dict_value_for_0_1,
        return_se_file_as_dict_key_1_0: return_se_file_as_dict_value_for_1_0,
        return_se_file_as_dict_key_1_25: return_se_file_as_dict_value_for_1_25,
        return_se_file_as_dict_key_1_5: return_se_file_as_dict_value_for_1_5,
        return_se_file_as_dict_key_1_75: return_se_file_as_dict_value_for_1_75,
        return_se_file_as_dict_key_1_1: return_se_file_as_dict_value_for_1_1
    }

    return data_as_dataframe_as_dict


@pytest.fixture
def return_se_file_as_dict_key_0_0():
    """
        Fixture method returning the key '0' for dataframe_from_d45_page_94
     """

    return '0'


@pytest.fixture
def return_se_file_as_dict_value_for_0_0():
    """
        Fixture method returning the value at key '0' in dataframe_from_d45_page_94
     """
    return [
        ['0', '7'],
        ['1', '11'],
        ['10', '13']
    ]


@pytest.fixture
def return_se_file_as_dict_key_0_25():
    """
        Fixture method returning the key '0,25' for dataframe_from_d45_page_94
     """

    return '0,25'


@pytest.fixture
def return_se_file_as_dict_value_for_0_25():
    """
        Fixture method returning the value at key '0,25' in dataframe_from_d45_page_94
     """
    return [
        ['0', '8'],
        ['1', '10'],
        ['10', '12']
    ]


@pytest.fixture
def return_se_file_as_dict_key_0_5():
    """
        Fixture method returning the key '0,5' for dataframe_from_d45_page_94
     """

    return '0,5'


@pytest.fixture
def return_se_file_as_dict_value_for_0_5():
    """
        Fixture method returning the value at key '0,5' in dataframe_from_d45_page_94
     """
    return [
        ['0', '10'],
        ['1', '9'],
        ['10', '9']
    ]


@pytest.fixture
def return_se_file_as_dict_key_0_75():
    """
        Fixture method returning the key '0,75' for dataframe_from_d45_page_94
     """

    return '0,75'


@pytest.fixture
def return_se_file_as_dict_value_for_0_75():
    """
        Fixture method returning the value at key '0,75' in dataframe_from_d45_page_94
     """
    return [
        ['0', '13'],
        ['1', '10'],
        ['10', '7']
    ]


@pytest.fixture
def return_se_file_as_dict_key_0_1():
    """
        Fixture method returning the key '0,1' for dataframe_from_d45_page_94
     """

    return '0,1'


@pytest.fixture
def return_se_file_as_dict_value_for_0_1():
    """
        Fixture method returning the value at key '0,1' in dataframe_from_d45_page_94
     """
    return [
        ['0', '15'],
        ['1', '9'],
        ['10', '5']
    ]


@pytest.fixture
def return_se_file_as_dict_key_1_0():
    """
        Fixture method returning the key '1,0' for dataframe_from_d45_page_94
     """

    return '1,0'


@pytest.fixture
def return_se_file_as_dict_value_for_1_0():
    """
        Fixture method returning the value at key '1,0' in dataframe_from_d45_page_94
     """
    return [
        ['0', '8'],
        ['1', '10'],
        ['10', '13']
    ]


@pytest.fixture
def return_se_file_as_dict_key_1_25():
    """
        Fixture method returning the key '1,25' for dataframe_from_d45_page_94
     """

    return '1,25'


@pytest.fixture
def return_se_file_as_dict_value_for_1_25():
    """
        Fixture method returning the value at key '1,25' in dataframe_from_d45_page_94
     """
    return [
        ['0', '9'],
        ['1', '11'],
        ['10', '11']
    ]


@pytest.fixture
def return_se_file_as_dict_key_1_5():
    """
        Fixture method returning the key '1,5' for dataframe_from_d45_page_94
     """

    return '1,5'


@pytest.fixture
def return_se_file_as_dict_value_for_1_5():
    """
        Fixture method returning the value at key '1,5' in dataframe_from_d45_page_94
     """
    return [
        ['0', '11'],
        ['1', '10'],
        ['10', '11']
    ]


@pytest.fixture
def return_se_file_as_dict_key_1_75():
    """
        Fixture method returning the key '1,75' for dataframe_from_d45_page_94
     """

    return '1,75'


@pytest.fixture
def return_se_file_as_dict_value_for_1_75():
    """
        Fixture method returning the value at key '1,75' in dataframe_from_d45_page_94
     """
    return [
        ['0', '13'],
        ['1', '9'],
        ['10', '8']
    ]


@pytest.fixture
def return_se_file_as_dict_key_1_1():
    """
        Fixture method returning the key '1,1' for dataframe_from_d45_page_94
     """

    return '1,1'


@pytest.fixture
def return_se_file_as_dict_value_for_1_1():
    """
        Fixture method returning the value at key '1,1' in dataframe_from_d45_page_94
     """
    return [
        ['0', '16'],
        ['1', '11'],
        ['10', '6']
    ]


# ---------------------------------------------------------------------------------
# Fixtures for the Dataframe Series: Series
# ---------------------------------------------------------------------------------
@pytest.fixture
def series_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted(
        dataframe_distance_equal_0_with_two_occurrences_init):
    """
        Fixture method to return series from a dataframe with distance = 0,
        with one tainted vars and 2 untainted vars
    """

    df = dataframe_distance_equal_0_with_two_occurrences_init

    return df.iloc[0:1, 1:], df.iloc[1:2, 1:]


@pytest.fixture
def serie_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows(
        dataframe_distance_equal_0_with_two_occurrences_init):
    """
        Fixture method to return series from a dataframe with distance = 0,
        with one tainted vars and 2 untainted vars
    """

    df = dataframe_distance_equal_0_with_two_occurrences_init

    return df.iloc[0:2, 1:]


@pytest.fixture
def sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted(
        series_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted):
    """
        Fixture method to return the side channel values from the series
    """
    serie1, serie2 = series_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted

    return serie1.loc[0][-1:].values[0], serie2.loc[1][-1:].values[0]


@pytest.fixture
def series_for_distance_property_identity_k1():
    """
        Fixture method to return a serie k1 to check the identity property for distance
    """
    return ["00000001111100101111010111001011"]


@pytest.fixture
def series_for_distance_property_identity_k2():
    """
        Fixture method to return a serie k2 to check the identity property for distance
    """
    return ["10100001000001111000101011000111"]


# ---------------------------------------------------------------------------------
# Fixtures for the Dataframe Series: Series as list
# ---------------------------------------------------------------------------------
@pytest.fixture
def series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages(
        dataframe_distance_equal_0_with_two_occurrences_init):
    """
        Fixture method to return leakages from the dataframe, as a list
    """

    df = dataframe_distance_equal_0_with_two_occurrences_init

    return df.iloc[0:1, -1].tolist(), df.iloc[1:2, -1].tolist()


@pytest.fixture
def serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages(
        dataframe_distance_equal_0_with_two_occurrences_init):
    """
        Fixture method to return series from a dataframe with distance = 0,
        with one tainted vars and 2 untainted vars
    """

    df = dataframe_distance_equal_0_with_two_occurrences_init

    return df.iloc[0:2, -1].tolist()


# ---------------------------------------------------------------------------------
# Fixtures for the Euclidian distance
# ---------------------------------------------------------------------------------

@pytest.fixture
def distance_euclidian_distance_between_k1_and_k2(
        series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages,
        return_marginal_proba_public_for_12_tuples,
        return_exponent_equals_2,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Compute distance between k1 and k2 """

    return DistanceEuclidian.compute_distance(
        leakages1=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[0],
        leakages2=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[1],
        public_marginal_dist=return_marginal_proba_public_for_12_tuples,
        exponent=return_exponent_equals_2,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)


@pytest.fixture
def distance_euclidian_distance_between_k2_and_k1(
        series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages,
        return_marginal_proba_public_for_12_tuples,
        return_exponent_equals_2,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Compute distance between k2 and k1 """

    return DistanceEuclidian.compute_distance(
        leakages1=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[1],
        leakages2=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[0],
        public_marginal_dist=return_marginal_proba_public_for_12_tuples,
        exponent=return_exponent_equals_2,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)


@pytest.fixture
def domain_distance_euclidian_equal_zero(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_marginal_proba_public_for_12_tuples,
        return_exponent_equals_2,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Fixture method to return a DistanceEuclidian object """
    distance = DistanceEuclidian.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        public_marginal_dist=return_marginal_proba_public_for_12_tuples,
        exponent=return_exponent_equals_2,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_euclidian_equal_to_0dot19(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_marginal_proba_public_for_12_tuples,
        return_exponent_equals_2,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Fixture method to return a DistanceEuclidian object for which euclidian distance will equal 0.19 """
    distance = DistanceEuclidian.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        public_marginal_dist=return_marginal_proba_public_for_12_tuples,
        exponent=return_exponent_equals_2,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_euclidian_check_identity_1(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_marginal_proba_public_for_12_tuples,
        return_exponent_equals_2,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Fixture method to return a DistanceEuclidian object where leakages1 = leakages2 in order to check identity """
    distance = DistanceEuclidian.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        public_marginal_dist=return_marginal_proba_public_for_12_tuples,
        exponent=return_exponent_equals_2,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_euclidian_check_identity_2(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_marginal_proba_public_for_12_tuples,
        return_exponent_equals_2,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Fixture method to return a DistanceEuclidian object where leakages1 = leakages2 in order to check identity """
    distance = DistanceEuclidian.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        public_marginal_dist=return_marginal_proba_public_for_12_tuples,
        exponent=return_exponent_equals_2,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_euclidian_check_symmetry_1(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_marginal_proba_public_for_12_tuples,
        return_exponent_equals_2,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceEuclidian object
        where leakages1 = serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages
        and leakages2 = serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages
         in order to check symmetry
    """
    distance = DistanceEuclidian.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        public_marginal_dist=return_marginal_proba_public_for_12_tuples,
        exponent=return_exponent_equals_2,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_euclidian_check_symmetry_2(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_marginal_proba_public_for_12_tuples,
        return_exponent_equals_2,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceEuclidian object
        where k1 = serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages
        and k2 = serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages
        in order to check symmetry
    """
    distance = DistanceEuclidian.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        public_marginal_dist=return_marginal_proba_public_for_12_tuples,
        exponent=return_exponent_equals_2,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_euclidian_check_homogeneity_ax(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_marginal_proba_public_for_12_tuples,
        return_exponent_equals_2,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceEuclidian object
        with dataframe = dataframe_distance_homogeneity_ax_init
        in order to check homogeneity
    """
    distance = DistanceEuclidian.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        public_marginal_dist=return_marginal_proba_public_for_12_tuples,
        exponent=return_exponent_equals_2,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_euclidian_check_homogeneity_x(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_marginal_proba_public_for_12_tuples,
        return_exponent_equals_2,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceEuclidian object
        with dataframe = dataframe_distance_homogeneity_ax_init
        in order to check homogeneity
    """
    distance = DistanceEuclidian.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        public_marginal_dist=return_marginal_proba_public_for_12_tuples,
        exponent=return_exponent_equals_2,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_euclidian_check_triangle_inequality_dxz(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_marginal_proba_public_for_12_tuples,
        return_exponent_equals_2,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceEuclidian object
        with dataframe = dataframe_distance_triangle_inequality_dxz_init
        in order to check triangle inequality
    """
    distance = DistanceEuclidian.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        public_marginal_dist=return_marginal_proba_public_for_12_tuples,
        exponent=return_exponent_equals_2,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_euclidian_check_triangle_inequality_dxy(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_marginal_proba_public_for_12_tuples,
        return_exponent_equals_2,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceEuclidian object
        with dataframe = dataframe_distance_triangle_inequality_dxy_init
        in order to check triangle inequality
    """
    distance = DistanceEuclidian.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        public_marginal_dist=return_marginal_proba_public_for_12_tuples,
        exponent=return_exponent_equals_2,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_euclidian_check_triangle_inequality_dyz(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_marginal_proba_public_for_12_tuples,
        return_exponent_equals_2,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceEuclidian object
        with dataframe = dataframe_distance_triangle_inequality_dyz_init
        in order to check triangle inequality
    """
    distance = DistanceEuclidian.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        public_marginal_dist=return_marginal_proba_public_for_12_tuples,
        exponent=return_exponent_equals_2,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


# ---------------------------------------------------------------------------------
# Fixtures for the Chebyshev distance
# ---------------------------------------------------------------------------------

@pytest.fixture
def distance_chebyshev_distance_between_k1_and_k2(
        series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Compute distance between k1 and k2 """

    return DistanceChebyshev.compute_distance(
        leakages1=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[0],
        leakages2=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[1],
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)


@pytest.fixture
def distance_chebyshev_distance_between_k2_and_k1(
        series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Compute distance between k2 and k1 """

    return DistanceChebyshev.compute_distance(
        leakages1=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[1],
        leakages2=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[0],
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)


@pytest.fixture
def distance_chebyshev_distance_between_k1_and_k1(
        series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Compute distance between k2 and k1 """

    return DistanceChebyshev.compute_distance(
        leakages1=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[0],
        leakages2=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[0],
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)


@pytest.fixture
def distance_chebyshev_distance_between_k2_and_k2(
        series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Compute distance between k2 and k1 """

    return DistanceChebyshev.compute_distance(
        leakages1=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[0],
        leakages2=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[1],
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)


@pytest.fixture
def distance_chebyshev_distance_between_k2_and_k1(
        series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Compute distance between k2 and k1 """

    return DistanceChebyshev.compute_distance(
        leakages1=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[0],
        leakages2=series_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_leakages[1],
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)


@pytest.fixture
def domain_distance_chebyshev_equal_zero(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Fixture method to return a DistanceChebyshev object """
    distance = DistanceChebyshev.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_chebyshev_check_identity_1(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Fixture method to return a DistanceChebyshev object where leakages1 = leakages2 in order to check identity """
    distance = DistanceChebyshev.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_chebyshev_check_identity_2(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """ Fixture method to return a DistanceChebyshev object where leakages1 = leakages2 in order to check identity """
    distance = DistanceChebyshev.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_chebyshev_check_symmetry_1(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceChebyshev object
        where k1 = serie_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows
        and k2 = serie_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows in order to check symmetry
    """
    distance = DistanceChebyshev.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_chebyshev_check_symmetry_2(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceChebyshev object
        where k1 = serie_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows
        and k2 = serie_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows in order to check symmetry
    """
    distance = DistanceChebyshev.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_chebyshev_check_homogeneity_ax(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceChebyshev object
        with dataframe = dataframe_distance_homogeneity_ax_init
        in order to check homogeneity
    """
    distance = DistanceChebyshev.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_chebyshev_check_homogeneity_x(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceChebyshev object
        with dataframe = dataframe_distance_homogeneity_ax_init
        in order to check homogeneity
    """
    distance = DistanceChebyshev.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_chebyshev_check_triangle_inequality_dxz(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceChebyshev object
        with dataframe = dataframe_distance_triangle_inequality_dxz_init
        in order to check triangle inequality
    """
    distance = DistanceChebyshev.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_chebyshev_check_triangle_inequality_dxy(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceChebyshev object
        with dataframe = dataframe_distance_triangle_inequality_dxy_init
        in order to check triangle inequality
    """
    distance = DistanceChebyshev.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


@pytest.fixture
def domain_distance_chebyshev_check_triangle_inequality_dyz(
        serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        return_is_active_probe_false,
        return_probe_container_with_probe_not_active):
    """
        Fixture method to return a DistanceChebyshev object
        with dataframe = dataframe_distance_triangle_inequality_dyz_init
        in order to check triangle inequality
    """
    distance = DistanceChebyshev.compute_distance(
        leakages1=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        leakages2=serie_to_list_from_dataframe_with_distance_0_and_1_tainted_and_2_untainted_two_rows_leakages,
        is_active_probe=return_is_active_probe_false,
        probe=return_probe_container_with_probe_not_active)

    return distance


# ---------------------------------------------------------------------------------
# Fixtures for the WCTD Metric
# ---------------------------------------------------------------------------------


@pytest.fixture
def metric_model_init_with_wctd_metric_equal_0(data_as_dataframe_distance_equal_0_init,
                                               return_distance_chebyshev_type,
                                               return_nb_tainted_equals_1,
                                               return_nb_untainted_equals_2,
                                               return_variables_list_a1_n1_secret3,
                                               return_probe_container_with_probe_not_active
                                               ):
    """  Generate an instance of MetricWCTD for which the computed metric is equal to 0 """

    metric = MetricWCTD(data=data_as_dataframe_distance_equal_0_init,
                        distance=return_distance_chebyshev_type,
                        nb_tainted_vars=return_nb_tainted_equals_1,
                        nb_untainted_vars=return_nb_untainted_equals_2,
                        variables_card=return_variables_list_a1_n1_secret3,
                        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_wctd_metric_equal_0_with_active_probe(data_as_dataframe_distance_equal_0_init,
                                                                 return_distance_chebyshev_type,
                                                                 return_nb_tainted_equals_1,
                                                                 return_nb_untainted_equals_2,
                                                                 return_variables_list_a1_n1_secret3,
                                                                 return_exponent_equals_2,
                                                                 return_probe_container_with_probe_active
                                                                 ):
    """  Generate an instance of MetricWCTD for which the computed metric is equal to 0 """

    metric = MetricWCTD(data=data_as_dataframe_distance_equal_0_init,
                        distance=return_distance_chebyshev_type,
                        nb_tainted_vars=return_nb_tainted_equals_1,
                        nb_untainted_vars=return_nb_untainted_equals_2,
                        variables_card=return_variables_list_a1_n1_secret3,
                        probe_container=return_probe_container_with_probe_active)

    return metric


@pytest.fixture
def metric_model_init_with_wctd_metric_equal_60(data_as_dataframe_distance_equal_60cheb_20_eucl_init,
                                                return_distance_chebyshev_type,
                                                return_nb_tainted_equals_1,
                                                return_nb_untainted_equals_2,
                                                return_variables_list_a1_n1_secret3,
                                                return_probe_container_with_probe_not_active
                                                ):
    """
      Generate an instance of MetricWCTD for which the computed metric is equal to 60
    """

    metric = MetricWCTD(data=data_as_dataframe_distance_equal_60cheb_20_eucl_init,
                        distance=return_distance_chebyshev_type,
                        nb_tainted_vars=return_nb_tainted_equals_1,
                        nb_untainted_vars=return_nb_untainted_equals_2,
                        variables_card=return_variables_list_a1_n1_secret3,
                        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_wctd_metric_equal_60_with_active_probe(
        data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        return_distance_chebyshev_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_active
):
    """
        Generate an instance of MetricWCTD for which the computed metric is equal to 60
    """

    metric = MetricWCTD(data=data_as_dataframe_distance_equal_60cheb_20_eucl_init,
                        distance=return_distance_chebyshev_type,
                        nb_tainted_vars=return_nb_tainted_equals_1,
                        nb_untainted_vars=return_nb_untainted_equals_2,
                        variables_card=return_variables_list_a1_n1_secret3,
                        probe_container=return_probe_container_with_probe_active)

    return metric


# ---------------------------------------------------------------------------------
# Fixtures for the ACTD Metric
# ---------------------------------------------------------------------------------

@pytest.fixture
def metric_model_init_with_actd_metric_uniform_dist_equal_0(
        data_as_dataframe_distance_equal_0_init,
        return_distributionasdict_marginal_proba_secret_and_public_data_as_dataframe_distance_equal_0_init,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """  Generate an instance of MetricACTD for which the computed metric is equal to 0 for uniform distribution """

    metric = MetricACTD(
        data=data_as_dataframe_distance_equal_0_init,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_data_as_dataframe_distance_equal_0_init,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_actd_metric_non_uniform_dist_equal_0(
        data_as_dataframe_distance_equal_0_init,
        return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """  Generate an instance of MetricACTD for which the computed metric is equal to 0 for non uniform distribution """

    metric = MetricACTD(
        data=data_as_dataframe_distance_equal_0_init,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_actd_metric_uniform_dist_equal_20(
        data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        return_distributionasdict_marginal_proba_secret_and_public_data_as_dataframe_distance_equal_0_init,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """
        Generate an instance of MetricACTD with a uniform distribution
        for which the computed metric is equal to 20
    """

    metric = MetricACTD(
        data=data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_data_as_dataframe_distance_equal_0_init,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_actd_metric_uniform_dist_equal_1_point_5(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_uniform_joint_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """
        Generate an instance of MetricACTD with a uniform distribution
        for which the computed metric is equal to 1.5
    """

    metric = MetricACTD(
        data=data_as_dataframe_from_indiscernability_bl,
        marginal_distributions_as_dict=return_distributionasdict_uniform_joint_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_actd_metric_non_uniform_dist_equal_6_point_4967(
        data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """  Generate an instance of MetricACTD with a non uniform distribution
        for which the computed metric is equal to 6.496734828430982
    """

    metric = MetricACTD(
        data=data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_actd_metric_non_uniform_dist_12_tuples_equal_2_point_079948(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a2_n2_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """
        Generate an instance of MetricACTD with a non uniform distribution
        for which the computed metric is equal 2.07994842
    """

    metric = MetricACTD(
        data=data_as_dataframe_from_indiscernability_bl,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a2_n2_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_actd_metric_non_uniform_erroneous_dist_12_tuples(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a2_n2_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """
        Generate an instance of MetricACTD with a non uniform erroneous distribution where the total sum of distribution
        is greater than 1
    """

    metric = MetricACTD(
        data=data_as_dataframe_from_indiscernability_bl,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a2_n2_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_actd_metric_erroneous_empty(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a2_n2_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """
        Generate an instance of MetricACTD for which the distribution_as_dict is empty.
        Should raise a DistributionDataException
    """

    metric = MetricACTD(
        data=data_as_dataframe_from_indiscernability_bl,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a2_n2_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


# ---------------------------------------------------------------------------------
# Fixtures for the WCED Metric
# ---------------------------------------------------------------------------------
@pytest.fixture
def metric_model_init_with_wced_metric_equal_0(data_as_dataframe_distance_equal_0_init,
                                               return_distance_chebyshev_type,
                                               return_nb_tainted_equals_1,
                                               return_nb_untainted_equals_2,
                                               return_variables_list_a1_n1_secret3,
                                               return_probe_container_with_probe_not_active
                                               ):
    """  Generate an instance of MetricWCED for which the computed metric is equal to 0 """

    metric = MetricWCED(data=data_as_dataframe_distance_equal_0_init,
                        distance=return_distance_chebyshev_type,
                        nb_tainted_vars=return_nb_tainted_equals_1,
                        nb_untainted_vars=return_nb_untainted_equals_2,
                        variables_card=return_variables_list_a1_n1_secret3,
                        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_wced_metric_equal_0_with_active_probe(data_as_dataframe_distance_equal_0_init,
                                                                 return_distance_chebyshev_type,
                                                                 return_nb_tainted_equals_1,
                                                                 return_nb_untainted_equals_2,
                                                                 return_variables_list_a1_n1_secret3,
                                                                 return_exponent_equals_2,
                                                                 return_probe_container_with_probe_active
                                                                 ):
    """  Generate an instance of MetricWCED for which the computed metric is equal to 0 """

    metric = MetricWCED(data=data_as_dataframe_distance_equal_0_init,
                        distance=return_distance_chebyshev_type,
                        nb_tainted_vars=return_nb_tainted_equals_1,
                        nb_untainted_vars=return_nb_untainted_equals_2,
                        variables_card=return_variables_list_a1_n1_secret3,
                        probe_container=return_probe_container_with_probe_active)

    return metric


@pytest.fixture
def metric_model_init_with_wced_metric_equal_60(data_as_dataframe_distance_equal_60cheb_20_eucl_init,
                                                return_distance_chebyshev_type,
                                                return_nb_tainted_equals_1,
                                                return_nb_untainted_equals_2,
                                                return_variables_list_a1_n1_secret3,
                                                return_probe_container_with_probe_not_active
                                                ):
    """
      Generate an instance of MetricWCED for which the computed metric is equal to :
            60 for Chebyshev
            20 for Euclidian
    """

    metric = MetricWCED(data=data_as_dataframe_distance_equal_60cheb_20_eucl_init,
                        distance=return_distance_chebyshev_type,
                        nb_tainted_vars=return_nb_tainted_equals_1,
                        nb_untainted_vars=return_nb_untainted_equals_2,
                        variables_card=return_variables_list_a1_n1_secret3,
                        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_wced_metric_equal_60_with_active_probe(
        data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        return_distance_chebyshev_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_probe_container_with_probe_active
):
    """
        Generate an instance of MetricWCED for which the computed metric is equal to 60
    """

    metric = MetricWCED(data=data_as_dataframe_distance_equal_60cheb_20_eucl_init,
                        distance=return_distance_chebyshev_type,
                        nb_tainted_vars=return_nb_tainted_equals_1,
                        nb_untainted_vars=return_nb_untainted_equals_2,
                        variables_card=return_variables_list_a1_n1_secret3,
                        probe_container=return_probe_container_with_probe_active)

    return metric


# ---------------------------------------------------------------------------------
# Fixtures for the ACED Metric
# ---------------------------------------------------------------------------------

@pytest.fixture
def metric_model_init_with_aced_metric_uniform_dist_equal_0(
        data_as_dataframe_distance_equal_0_init,
        return_distributionasdict_marginal_proba_secret_and_public_data_as_dataframe_distance_equal_0_init,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """  Generate an instance of MetricACED for which the computed metric is equal to 0 for uniform distribution """

    metric = MetricACED(
        data=data_as_dataframe_distance_equal_0_init,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_data_as_dataframe_distance_equal_0_init,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_aced_metric_non_uniform_dist_equal_0(
        data_as_dataframe_distance_equal_0_init,
        return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """  Generate an instance of MetricACED for which the computed metric is equal to 0 for non uniform distribution """

    metric = MetricACED(
        data=data_as_dataframe_distance_equal_0_init,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_aced_metric_uniform_dist_equal_20(
        data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        return_distributionasdict_marginal_proba_secret_and_public_data_as_dataframe_distance_equal_0_init,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """
        Generate an instance of MetricACED with a uniform distribution
        for which the computed metric is equal to 20
    """

    metric = MetricACED(
        data=data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_data_as_dataframe_distance_equal_0_init,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_aced_metric_uniform_dist_equal_1_point_5(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_uniform_joint_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """
        Generate an instance of MetricACED with a uniform distribution
        for which the computed metric is equal to 1.5
    """

    metric = MetricACED(
        data=data_as_dataframe_from_indiscernability_bl,
        marginal_distributions_as_dict=return_distributionasdict_uniform_joint_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_aced_metric_non_uniform_dist_equal_6_point_4967(
        data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """  Generate an instance of MetricACED with a non uniform distribution
        for which the computed metric is equal to 6.496734828430982
    """

    metric = MetricACED(
        data=data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_aced_metric_non_uniform_dist_12_tuples_equal_2_point_079948(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a2_n2_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """
        Generate an instance of MetricACED with a non uniform distribution
        for which the computed metric is equal 2.07994842
    """

    metric = MetricACED(
        data=data_as_dataframe_from_indiscernability_bl,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a2_n2_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_aced_metric_non_uniform_erroneous_dist_12_tuples(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a2_n2_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """
        Generate an instance of MetricACED with a non uniform erroneous distribution where the total sum of distribution
        is greater than 1
    """

    metric = MetricACED(
        data=data_as_dataframe_from_indiscernability_bl,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a2_n2_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


# ---------------------------------------------------------------------------------
# Fixtures for the WCPD Metric
# ---------------------------------------------------------------------------------
@pytest.fixture
def metric_model_init_with_wcpd_metric_equal_0(data_as_dataframe_distance_equal_0_init,
                                               return_distance_chebyshev_type,
                                               return_nb_tainted_equals_1,
                                               return_nb_untainted_equals_2,
                                               return_variables_list_a1_n1_secret3,
                                               return_exponent_equals_2,
                                               return_probe_container_with_probe_not_active
                                               ):
    """  Generate an instance of MetricWCPD for which the computed metric is equal to 0 """

    metric = MetricWCPD(data=data_as_dataframe_distance_equal_0_init,
                        distance=return_distance_chebyshev_type,
                        nb_tainted_vars=return_nb_tainted_equals_1,
                        nb_untainted_vars=return_nb_untainted_equals_2,
                        variables_card=return_variables_list_a1_n1_secret3,
                        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_wcpd_metric_equal_0_with_active_probe(data_as_dataframe_distance_equal_0_init,
                                                                 return_distance_chebyshev_type,
                                                                 return_nb_tainted_equals_1,
                                                                 return_nb_untainted_equals_2,
                                                                 return_variables_list_a1_n1_secret3,
                                                                 return_probe_container_with_probe_active
                                                                 ):
    """  Generate an instance of MetricWCPD for which the computed metric is equal to 0 """

    metric = MetricWCPD(data=data_as_dataframe_distance_equal_0_init,
                        distance=return_distance_chebyshev_type,
                        nb_tainted_vars=return_nb_tainted_equals_1,
                        nb_untainted_vars=return_nb_untainted_equals_2,
                        variables_card=return_variables_list_a1_n1_secret3,
                        probe_container=return_probe_container_with_probe_active)

    return metric


@pytest.fixture
def metric_model_init_with_wcpd_metric_equal_60(data_as_dataframe_distance_equal_60cheb_20_eucl_init,
                                                return_distance_chebyshev_type,
                                                return_nb_tainted_equals_1,
                                                return_nb_untainted_equals_2,
                                                return_variables_list_a1_n1_secret3,
                                                return_probe_container_with_probe_not_active
                                                ):
    """
      Generate an instance of MetricWCPD for which the computed metric is equal to 60
    """

    metric = MetricWCPD(data=data_as_dataframe_distance_equal_60cheb_20_eucl_init,
                        distance=return_distance_chebyshev_type,
                        nb_tainted_vars=return_nb_tainted_equals_1,
                        nb_untainted_vars=return_nb_untainted_equals_2,
                        variables_card=return_variables_list_a1_n1_secret3,
                        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_wcpd_metric_equal_60_with_active_probe(
        data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        return_distance_chebyshev_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_probe_container_with_probe_active
):
    """
        Generate an instance of MetricWCPD for which the computed metric is equal to 60
    """

    metric = MetricWCPD(data=data_as_dataframe_distance_equal_60cheb_20_eucl_init,
                        distance=return_distance_chebyshev_type,
                        nb_tainted_vars=return_nb_tainted_equals_1,
                        nb_untainted_vars=return_nb_untainted_equals_2,
                        variables_card=return_variables_list_a1_n1_secret3,
                        probe_container=return_probe_container_with_probe_active)

    return metric


# ---------------------------------------------------------------------------------
# Fixtures for the ACPD Metric
# ---------------------------------------------------------------------------------
@pytest.fixture
def metric_model_init_with_acpd_metric_uniform_dist_equal_0(
        data_as_dataframe_distance_equal_0_init,
        return_distributionasdict_marginal_proba_secret_and_public_data_as_dataframe_distance_equal_0_init,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """  Generate an instance of MetricACPD for which the computed metric is equal to 0 for uniform distribution """

    metric = MetricACPD(
        data=data_as_dataframe_distance_equal_0_init,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_data_as_dataframe_distance_equal_0_init,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_acpd_metric_non_uniform_dist_equal_0(
        data_as_dataframe_distance_equal_0_init,
        return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """  Generate an instance of MetricACPD for which the computed metric is equal to 0 for non uniform distribution """

    metric = MetricACPD(
        data=data_as_dataframe_distance_equal_0_init,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_acpd_metric_uniform_dist_equal_20(
        data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        return_distributionasdict_marginal_proba_secret_and_public_data_as_dataframe_distance_equal_0_init,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """
        Generate an instance of MetricACPD with a uniform distribution
        for which the computed metric is equal to 20
    """

    metric = MetricACPD(
        data=data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_data_as_dataframe_distance_equal_0_init,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_acpd_metric_uniform_dist_equal_1_point_5(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_uniform_joint_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """
        Generate an instance of MetricACPD with a uniform distribution
        for which the computed metric is equal to 1.5
    """

    metric = MetricACPD(
        data=data_as_dataframe_from_indiscernability_bl,
        marginal_distributions_as_dict=return_distributionasdict_uniform_joint_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_acpd_metric_non_uniform_dist_equal_6_point_4967(
        data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a1_n1_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """  Generate an instance of MetricACPD with a non uniform distribution
        for which the computed metric is equal to 6.496734828430982
    """

    metric = MetricACPD(
        data=data_as_dataframe_distance_equal_60cheb_20_eucl_init,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a1_n1_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_acpd_metric_non_uniform_dist_12_tuples_equal_2_point_079948(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a2_n2_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """
        Generate an instance of MetricACPD with a non uniform distribution
        for which the computed metric is equal 2.07994842
    """

    metric = MetricACPD(
        data=data_as_dataframe_from_indiscernability_bl,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a2_n2_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


@pytest.fixture
def metric_model_init_with_acpd_metric_non_uniform_erroneous_dist_12_tuples(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        return_distance_euclidian_type,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        return_variables_list_a2_n2_secret3,
        return_exponent_equals_2,
        return_probe_container_with_probe_not_active
):
    """
        Generate an instance of MetricACPD with a non uniform erroneous distribution where the total sum of distribution
        is greater than 1
    """

    metric = MetricACPD(
        data=data_as_dataframe_from_indiscernability_bl,
        marginal_distributions_as_dict=return_distributionasdict_marginal_proba_secret_and_public_12_tuples,
        distance=return_distance_euclidian_type,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        variables_card=return_variables_list_a2_n2_secret3,
        exponent=return_exponent_equals_2,
        probe_container=return_probe_container_with_probe_not_active)

    return metric


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related :Dendrogram
# ---------------------------------------------------------------------------------
def dendrogram_model_init(dendrogram_observations,
                          dendrogram_related_secret_distribution,
                          dendrogram_related_secret,
                          dendrogram_related_public,
                          resolution,
                          nb_clusters):
    """ Factorized instantiation of a Dendrogram """
    dendrogram = Dendrogram(leakage_tuples=dendrogram_observations,
                            secret_public_joint_dist_for_current_public_tuple=dendrogram_related_secret_distribution,
                            related_secret_tuples=dendrogram_related_secret,
                            related_public_tuple=dendrogram_related_public,
                            given_resolution=resolution,
                            given_nb_clusters=nb_clusters)

    return dendrogram


@pytest.fixture
def dendrogram_model_init_with_1_cluster_3_resolutions_with_obs_a(return_all_the_tuples_for_observations_a,
                                                                  dendrogram_related_secret_for_observations_a,
                                                                  return_distribution_for_secret_for_obs_a,
                                                                  dendrogram_related_public_from_indiscernability,
                                                                  resolution_equals_3,
                                                                  nb_clusters_equals_1):
    """
         Generate an instance of Dendrogram for:
             observations a
             nb_clusters = 1
             resolution = 3
     """
    dendrogram = dendrogram_model_init(return_all_the_tuples_for_observations_a,
                                       return_distribution_for_secret_for_obs_a,
                                       dendrogram_related_secret_for_observations_a,
                                       dendrogram_related_public_from_indiscernability,
                                       resolution_equals_3,
                                       nb_clusters_equals_1)

    return dendrogram


@pytest.fixture
def dendrogram_model_init_with_1_cluster_5_resolutions_with_obs_a(return_all_the_tuples_for_observations_a,
                                                                  dendrogram_related_secret_for_observations_a,
                                                                  return_distribution_for_secret_for_obs_a,
                                                                  dendrogram_related_public_from_indiscernability,
                                                                  resolution_equals_5,
                                                                  nb_clusters_equals_1):
    """
        Generate an instance of Dendrogram for:
            observations a
            nb_clusters = 1
            resolution = 5
    """
    dendrogram = dendrogram_model_init(return_all_the_tuples_for_observations_a,
                                       return_distribution_for_secret_for_obs_a,
                                       dendrogram_related_secret_for_observations_a,
                                       dendrogram_related_public_from_indiscernability,
                                       resolution_equals_5,
                                       nb_clusters_equals_1)

    return dendrogram


@pytest.fixture
def dendrogram_model_init_with_2_cluster_5_resolutions_observations_a(return_all_the_tuples_for_observations_a,
                                                                      return_distribution_for_secret_for_obs_a,
                                                                      dendrogram_related_secret_for_observations_a,
                                                                      dendrogram_related_public_from_indiscernability,
                                                                      resolution_equals_5,
                                                                      nb_clusters_equals_2):
    """
        Generate an instance of Dendrogram for:
            observations a
            nb_clusters = 2
            resolution = 5
    """
    dendrogram = dendrogram_model_init(return_all_the_tuples_for_observations_a,
                                       return_distribution_for_secret_for_obs_a,
                                       dendrogram_related_secret_for_observations_a,
                                       dendrogram_related_public_from_indiscernability,
                                       resolution_equals_5,
                                       nb_clusters_equals_2)

    return dendrogram


@pytest.fixture
def dendrogram_model_init_with_2_cluster_5_resolutions_indiscernability(
        return_all_the_tuples_for_indiscernability,
        return_distribution_for_secret_for_indiscernability,
        dendrogram_related_secret_from_indiscernability,
        dendrogram_related_public_from_indiscernability,
        resolution_equals_5,
        nb_clusters_equals_2):
    """
        Generate an instance of Dendrogram for:
            observations from indiscernability
            nb_clusters = 2
            resolution = 5
    """
    dendrogram = dendrogram_model_init(return_all_the_tuples_for_indiscernability,
                                       return_distribution_for_secret_for_indiscernability,
                                       dendrogram_related_secret_from_indiscernability,
                                       dendrogram_related_public_from_indiscernability,
                                       resolution_equals_5,
                                       nb_clusters_equals_2)

    return dendrogram


@pytest.fixture
def dendrogram_model_init_with_2_cluster_5_resolutions_indiscernability_with_secret_as_bits(
        return_all_the_tuples_for_indiscernability,
        dendrogram_related_secret_from_indiscernability,
        return_distribution_for_secret_for_indiscernability,
        dendrogram_related_public_from_indiscernability,
        resolution_equals_5,
        nb_clusters_equals_2):
    """
        Generate an instance of Dendrogram for:
            observations from indiscernability
            related_secret as bits
            nb_clusters = 2
            resolution = 5
    """
    dendrogram = dendrogram_model_init(return_all_the_tuples_for_indiscernability,
                                       return_distribution_for_secret_for_indiscernability,
                                       dendrogram_related_secret_from_indiscernability_as_bits,
                                       dendrogram_related_public_from_indiscernability,
                                       resolution_equals_5,
                                       nb_clusters_equals_2)

    return dendrogram


@pytest.fixture
def dendrogram_model_init_with_4_cluster_3_resolutions_observations_a(return_all_the_tuples_for_observations_a,
                                                                      dendrogram_related_secret_for_observations_a,
                                                                      return_distribution_for_secret_for_obs_a,
                                                                      dendrogram_related_public_from_indiscernability,
                                                                      resolution_equals_3,
                                                                      nb_clusters_equals_4):
    """
        Generate an instance of Dendrogram for:
            observations a
            nb_clusters = 4
            resolution = 3
    """
    dendrogram = dendrogram_model_init(return_all_the_tuples_for_observations_a,
                                       return_distribution_for_secret_for_obs_a,
                                       dendrogram_related_secret_for_observations_a,
                                       dendrogram_related_public_from_indiscernability,
                                       resolution_equals_3,
                                       nb_clusters_equals_4)

    return dendrogram


@pytest.fixture
def dendrogram_model_init_with_4_cluster_3_resolutions_indiscernability(
        return_all_the_tuples_for_indiscernability,
        dendrogram_related_secret_from_indiscernability,
        return_distribution_for_secret_for_indiscernability,
        dendrogram_related_public_from_indiscernability,
        resolution_equals_3,
        nb_clusters_equals_4):
    """
        Generate an instance of Dendrogram for:
            observations from indiscernability
            nb_clusters = 4
            resolution = 3
    """
    dendrogram = dendrogram_model_init(return_all_the_tuples_for_indiscernability,
                                       return_distribution_for_secret_for_indiscernability,
                                       dendrogram_related_secret_from_indiscernability,
                                       dendrogram_related_public_from_indiscernability,
                                       resolution_equals_3,
                                       nb_clusters_equals_4)

    return dendrogram


@pytest.fixture
def dendrogram_model_init_with_4_clusters_5_resolutions_with_obs_a(return_all_the_tuples_for_observations_a,
                                                                   dendrogram_related_secret_for_observations_a,
                                                                   return_distribution_for_secret_for_obs_a,
                                                                   dendrogram_related_public_from_indiscernability,
                                                                   resolution_equals_5,
                                                                   nb_clusters_equals_4):
    """
        Generate an instance of Dendrogram for:
            observations a
            nb_clusters = 4
            resolution = 5
    """
    dendrogram = dendrogram_model_init(return_all_the_tuples_for_observations_a,
                                       return_distribution_for_secret_for_obs_a,
                                       dendrogram_related_secret_for_observations_a,
                                       dendrogram_related_public_from_indiscernability,
                                       resolution_equals_5,
                                       nb_clusters_equals_4)

    return dendrogram


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related: DendrogramResult
# ---------------------------------------------------------------------------------
@pytest.fixture
def dendrogramresult_model_init_with_4_cluster_1_resolution_indiscernability(
        dendrogram_observations_from_indiscernability,
        dendrogram_related_public_from_indiscernability_4_33,
        dendrogram_final_secret_as_bits_from_indiscernability,
        return_distribution_for_secret_for_indiscernability,
        resolution_equals_1,
        nb_clusters_equals_4,
        return_termination_criterium_nb_clusters):
    """ Generate an instance of DendrogramResultsElement for the indiscernability data
      and nb_clusters=4, resolution=1"""
    dendrogram_result = DendrogramResult(
        related_public_tuple=dendrogram_related_public_from_indiscernability_4_33,
        final_leakage_clustering=dendrogram_observations_from_indiscernability,
        final_secret_clustering=dendrogram_final_secret_as_bits_from_indiscernability,
        final_public_marginal_dist_for_current_public_tuple=return_distribution_for_secret_for_indiscernability,
        final_resolution=resolution_equals_1,
        final_nb_clusters=nb_clusters_equals_4,
        satisfied_criterium=return_termination_criterium_nb_clusters)

    return dendrogram_result


@pytest.fixture
def dendrogramresult_model_init_indiscernability_bl_for_public_4_33(
        return_final_leakage_clustering_4_33_bl,
        dendrogram_related_public_from_indiscernability_4_33,
        return_final_secret_clustering_4_33_bl,
        return_final_secret_public_distrib_4_33_bl,
        resolution_equals_999999,
        nb_clusters_equals_2,
        return_termination_criterium_nb_clusters):
    """ Generate an instance of DendrogramResultsElement following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods
        and nb_clusters=2, resolution=999999
    """
    dendrogram_result = DendrogramResult(
        related_public_tuple=dendrogram_related_public_from_indiscernability_4_33,
        final_leakage_clustering=return_final_leakage_clustering_4_33_bl,
        final_secret_clustering=return_final_secret_clustering_4_33_bl,
        final_public_marginal_dist_for_current_public_tuple=return_final_secret_public_distrib_4_33_bl,
        final_resolution=resolution_equals_999999,
        final_nb_clusters=nb_clusters_equals_2,
        satisfied_criterium=return_termination_criterium_nb_clusters)

    return dendrogram_result


@pytest.fixture
def dendrogramresult_model_init_indiscernability_bl_for_public_4_35(
        return_final_leakage_clustering_4_35_bl,
        dendrogram_related_public_from_indiscernability_4_35,
        return_final_secret_clustering_4_35_bl,
        return_final_secret_public_distrib_4_35_bl,
        resolution_equals_999999,
        nb_clusters_equals_2,
        return_termination_criterium_nb_clusters):
    """ Generate an instance of DendrogramResultsElement following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods
        and nb_clusters=2, resolution=999999
    """
    dendrogram_result = DendrogramResult(
        related_public_tuple=dendrogram_related_public_from_indiscernability_4_35,
        final_leakage_clustering=return_final_leakage_clustering_4_35_bl,
        final_secret_clustering=return_final_secret_clustering_4_35_bl,
        final_public_marginal_dist_for_current_public_tuple=return_final_secret_public_distrib_4_35_bl,
        final_resolution=resolution_equals_999999,
        final_nb_clusters=nb_clusters_equals_2,
        satisfied_criterium=return_termination_criterium_nb_clusters)

    return dendrogram_result


@pytest.fixture
def dendrogramresult_model_init_indiscernability_bl_for_public_6_33(
        return_final_leakage_clustering_6_33_bl,
        dendrogram_related_public_from_indiscernability_6_33,
        return_final_secret_clustering_6_33_bl,
        return_final_secret_public_distrib_6_33_bl,
        resolution_equals_999999,
        nb_clusters_equals_2,
        return_termination_criterium_nb_clusters):
    """ Generate an instance of DendrogramResultsElement following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods
        and nb_clusters=2, resolution=999999
    """
    dendrogram_result = DendrogramResult(
        related_public_tuple=dendrogram_related_public_from_indiscernability_6_33,
        final_leakage_clustering=return_final_leakage_clustering_6_33_bl,
        final_secret_clustering=return_final_secret_clustering_6_33_bl,
        final_public_marginal_dist_for_current_public_tuple=return_final_secret_public_distrib_6_33_bl,
        final_resolution=resolution_equals_999999,
        final_nb_clusters=nb_clusters_equals_2,
        satisfied_criterium=return_termination_criterium_nb_clusters)

    return dendrogram_result


@pytest.fixture
def dendrogramresult_model_init_indiscernability_bl_for_public_6_35(
        return_final_leakage_clustering_6_35_bl,
        dendrogram_related_public_from_indiscernability_6_35,
        return_final_secret_clustering_6_35_bl,
        return_final_secret_public_distrib_6_35_bl,
        resolution_equals_999999,
        nb_clusters_equals_2,
        return_termination_criterium_nb_clusters):
    """ Generate an instance of DendrogramResultsElement following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods
        and nb_clusters=2, resolution=999999
    """
    dendrogram_result = DendrogramResult(
        related_public_tuple=dendrogram_related_public_from_indiscernability_6_35,
        final_leakage_clustering=return_final_leakage_clustering_6_35_bl,
        final_secret_clustering=return_final_secret_clustering_6_35_bl,
        final_public_marginal_dist_for_current_public_tuple=return_final_secret_public_distrib_6_35_bl,
        final_resolution=resolution_equals_999999,
        final_nb_clusters=nb_clusters_equals_2,
        satisfied_criterium=return_termination_criterium_nb_clusters)

    return dendrogram_result


# ---------------------------------------------------------------------------------
# Fixtures for the Dendrogram and related: DendrogramGatheredTuples
# ---------------------------------------------------------------------------------
@pytest.fixture
def dendrogramgatheredtuples_model_init(return_dict_tuples_occurrences,
                                        return_tuples_list):
    """ Generate an instance of DendrogramGatheredTuples """
    dendrogram_gathered_tuples = DendrogramGatheredTuples(dict_tuples_occurrences=return_dict_tuples_occurrences,
                                                          tuples_list=return_tuples_list)

    return dendrogram_gathered_tuples


# ---------------------------------------------------------------------------------
# Fixtures for the Distribution and related
# ---------------------------------------------------------------------------------
@pytest.fixture
def tuples_distribution_model_init_for_a(return_all_the_tuples_for_a,
                                         return_distribution_for_a,
                                         return_enumdistributiontuples_with_value_secret):
    """ Generate an instance of Distribution """
    return_distribution_for_a = Distribution(dist_tuples=return_all_the_tuples_for_a,
                                             distribution=return_distribution_for_a,
                                             variable_name=return_enumdistributiontuples_with_value_secret)

    return return_distribution_for_a


@pytest.fixture
def tuples_distribution_model_init_for_n(return_all_the_tuples_for_n,
                                         return_distribution_for_n,
                                         return_enumdistributiontuples_with_value_secret):
    """ Generate an instance of Distribution """
    return_distribution_for_n = Distribution(dist_tuples=return_all_the_tuples_for_n,
                                             distribution=return_distribution_for_n,
                                             variable_name=return_enumdistributiontuples_with_value_secret)

    return return_distribution_for_n


@pytest.fixture
def distribution_model_init_for_public(return_all_the_tuples_for_public,
                                       return_distribution_for_public,
                                       return_enumdistributiontuples_with_value_public):
    """ Generate an instance of Distribution """
    return_distribution_for_public = Distribution(dist_tuples=return_all_the_tuples_for_public,
                                                  distribution=return_distribution_for_public,
                                                  variable_name=return_enumdistributiontuples_with_value_public)

    return return_distribution_for_public


@pytest.fixture
def distribution_model_init_for_secret(return_all_the_tuples_for_secret_as_bits,
                                       return_distribution_for_secret_for_indiscernability,
                                       return_enumdistributiontuples_with_value_secret):
    """ Generate an instance of Distribution """
    return_distribution_for_secret = Distribution(dist_tuples=return_all_the_tuples_for_secret_as_bits,
                                                  distribution=return_distribution_for_secret_for_indiscernability,
                                                  variable_name=return_enumdistributiontuples_with_value_secret)

    return return_distribution_for_secret


@pytest.fixture
def distribution_model_init_for_leakage(return_all_the_tuples_for_indiscernability,
                                        return_distribution_for_leakage,
                                        return_enumdistributiontuples_with_value_leakage):
    """ Generate an instance of Distribution """
    return_distribution_for_leakage = Distribution(dist_tuples=return_all_the_tuples_for_indiscernability,
                                                   distribution=return_distribution_for_leakage,
                                                   variable_name=return_enumdistributiontuples_with_value_leakage)

    return return_distribution_for_leakage


@pytest.fixture
def distribution_model_init_for_public_leakage(return_all_the_tuples_for_public_leakage,
                                               return_distribution_for_public_leakage,
                                               return_enumdistributiontuples_with_value_publicleakage):
    """ Generate an instance of Distribution for variables public and leakage"""
    return_distribution_for_public_leakage = Distribution(
        dist_tuples=return_all_the_tuples_for_public_leakage,
        distribution=return_distribution_for_public_leakage,
        variable_name=return_enumdistributiontuples_with_value_publicleakage)

    return return_distribution_for_public_leakage


@pytest.fixture
def distribution_model_init_for_secret_public(return_all_the_tuples_for_secret_public_as_bits,
                                              return_distribution_for_secret_public,
                                              return_enumdistributiontuples_with_value_secretpublic):
    """ Generate an instance of Distribution for variables secret and public"""
    return_distribution_for_secret_public = Distribution(
        dist_tuples=return_all_the_tuples_for_secret_public_as_bits,
        distribution=return_distribution_for_secret_public,
        variable_name=return_enumdistributiontuples_with_value_secretpublic)

    return return_distribution_for_secret_public


@pytest.fixture
def distribution_container_model_init(distribution_model_init_for_secret,
                                      distribution_model_init_for_public,
                                      distribution_model_init_for_leakage,
                                      distribution_model_init_for_public_leakage,
                                      distribution_model_init_for_secret_public):
    """ Generate an instance of DistributionContainer """
    dist_container = DistributionContainer(secret=distribution_model_init_for_secret,
                                           public=distribution_model_init_for_public,
                                           leakage=distribution_model_init_for_leakage,
                                           public_leakage=distribution_model_init_for_public_leakage,
                                           secret_public=distribution_model_init_for_secret_public)

    return dist_container


@pytest.fixture
def distribution_tuples_model_init_for_leakage(return_all_the_tuples_for_indiscernability,
                                               return_sorted_tuples_as_dict_for_leakage_indiscernability,
                                               return_tuples_as_list__no_duplicates_indiscernability,
                                               return_related_secret_tuples_as_int_indiscernability):
    """ Generate an instance of DistributionTuples for variable leakage """
    returned_distribution_tuples_for_leakage = DistributionTuples(
        all_the_tuples=return_all_the_tuples_for_indiscernability,
        tuples_as_dict=return_sorted_tuples_as_dict_for_leakage_indiscernability,
        tuples_as_list_no_duplicate=return_tuples_as_list__no_duplicates_indiscernability,
        related_secret_tuples=return_related_secret_tuples_as_int_indiscernability
    )

    return returned_distribution_tuples_for_leakage


# ---------------------------------------------------------------------------------
# Fixtures for the IndiscernabilityLevel and related
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
# Fixtures for the IndiscernabilityLevel and related - Time
# ---------------------------------------------------------------------------------
@pytest.fixture
def indiscernabilityleveltimecompute_uc_model_init_nb_clusters_2_resolution_5_indiscernability_bl(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_secret_bl,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        tuples_information_model_init_for_12tuples,
        nb_clusters_equals_2,
        resolution_equals_5):
    """ Generate an instance of IndiscernabilityLevelComputeUseCase """
    indiscernability_level_uc = IndiscernabilityLevelTimeComputeUseCase(
        data=data_as_dataframe_from_indiscernability_bl,
        all_distributions_as_dict=return_distributionasdict_secret_bl,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        tuples_information=tuples_information_model_init_for_12tuples,
        nb_clusters=nb_clusters_equals_2,
        resolution=resolution_equals_5)

    return indiscernability_level_uc


@pytest.fixture
def indiscernabilityleveltimecompute_uc_model_init_nb_clusters_2_resolution_999999_indiscernability_bl(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_secret_bl,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        tuples_information_model_init_for_12tuples,
        nb_clusters_equals_2,
        resolution_equals_999999):
    """ Generate an instance of IndiscernabilityLevelComputeUseCase """
    indiscernability_level_uc = IndiscernabilityLevelTimeComputeUseCase(
        data=data_as_dataframe_from_indiscernability_bl,
        all_distributions_as_dict=return_distributionasdict_secret_bl,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        tuples_information=tuples_information_model_init_for_12tuples,
        nb_clusters=nb_clusters_equals_2,
        resolution=resolution_equals_999999)

    return indiscernability_level_uc


@pytest.fixture
def indiscernabilityleveltimecompute_uc_model_init_nb_clusters_6_no_resolution_for_d4_dot_4(
        data_as_dataframe_from_d4_dot_4_page_73,
        return_uniform_distributionasdict_secret_public_for_d4_dot4,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_1,
        tuples_information_model_init_for_d4_dot_4,
        nb_clusters_equals_6_for_d4_dot_4,
        resolution_equals_none):
    """ Generate an instance of IndiscernabilityLevelComputeUseCase """
    indiscernability_level_uc = IndiscernabilityLevelTimeComputeUseCase(
        data=data_as_dataframe_from_d4_dot_4_page_73,
        all_distributions_as_dict=return_uniform_distributionasdict_secret_public_for_d4_dot4,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_1,
        tuples_information=tuples_information_model_init_for_d4_dot_4,
        nb_clusters=nb_clusters_equals_6_for_d4_dot_4,
        resolution=resolution_equals_none)

    return indiscernability_level_uc


@pytest.fixture
def indiscernabilityleveltimecompute_uc_model_init_nb_clusters_2_no_resolution_for_d4_dot_5(
        data_as_dataframe_from_d4_dot_5_page_94,
        return_secret_public_joint_distributionasdict_for_d4_dot5,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        tuples_information_model_init_for_d4_dot_5,
        nb_clusters_equals_2_for_d4_dot_5,
        resolution_equals_none):
    """ Generate an instance of IndiscernabilityLevelComputeUseCase """
    indiscernability_level_uc = IndiscernabilityLevelTimeComputeUseCase(
        data=data_as_dataframe_from_d4_dot_5_page_94,
        all_distributions_as_dict=return_secret_public_joint_distributionasdict_for_d4_dot5,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        tuples_information=tuples_information_model_init_for_d4_dot_5,
        nb_clusters=nb_clusters_equals_2_for_d4_dot_5,
        resolution=resolution_equals_none)

    return indiscernability_level_uc


# ---------------------------------------------------------------------------------
# Fixtures for the IndiscernabilityLevel and related - Energy
# ---------------------------------------------------------------------------------
@pytest.fixture
def indiscernabilitylevelenergycompute_uc_model_init_nb_clusters_2_resolution_5_indiscernability_bl(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_secret_bl,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        tuples_information_model_init_for_12tuples,
        nb_clusters_equals_2,
        resolution_equals_5):
    """ Generate an instance of IndiscernabilityLevelEnergyComputeUseCase """
    indiscernability_level_uc = IndiscernabilityLevelEnergyComputeUseCase(
        data=data_as_dataframe_from_indiscernability_bl,
        all_distributions_as_dict=return_distributionasdict_secret_bl,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        tuples_information=tuples_information_model_init_for_12tuples,
        nb_clusters=nb_clusters_equals_2,
        resolution=resolution_equals_5)

    return indiscernability_level_uc


@pytest.fixture
def indiscernabilitylevelenergycompute_uc_model_init_nb_clusters_2_resolution_999999_indiscernability_bl(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_secret_bl,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        tuples_information_model_init_for_12tuples,
        nb_clusters_equals_2,
        resolution_equals_999999):
    """ Generate an instance of IndiscernabilityLevelEnergyComputeUseCase """
    indiscernability_level_uc = IndiscernabilityLevelEnergyComputeUseCase(
        data=data_as_dataframe_from_indiscernability_bl,
        all_distributions_as_dict=return_distributionasdict_secret_bl,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        tuples_information=tuples_information_model_init_for_12tuples,
        nb_clusters=nb_clusters_equals_2,
        resolution=resolution_equals_999999)

    return indiscernability_level_uc


# ---------------------------------------------------------------------------------
# Fixtures for the IndiscernabilityLevel and related - Power
# ---------------------------------------------------------------------------------
@pytest.fixture
def indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_5_indiscernability_bl(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_secret_bl,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        tuples_information_model_init_for_12tuples,
        nb_clusters_equals_2,
        resolution_equals_5):
    """ Generate an instance of IndiscernabilityLevelPowerComputeUseCase """
    indiscernability_level_uc = IndiscernabilityLevelPowerComputeUseCase(
        data=data_as_dataframe_from_indiscernability_bl,
        all_distributions_as_dict=return_distributionasdict_secret_bl,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        tuples_information=tuples_information_model_init_for_12tuples,
        nb_clusters=nb_clusters_equals_2,
        resolution=resolution_equals_5)

    return indiscernability_level_uc


@pytest.fixture
def indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_999999_indiscernability_bl(
        data_as_dataframe_from_indiscernability_bl,
        return_distributionasdict_secret_bl,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2,
        tuples_information_model_init_for_12tuples,
        nb_clusters_equals_2,
        resolution_equals_999999):
    """ Generate an instance of IndiscernabilityLevelPowerComputeUseCase """
    indiscernability_level_uc = IndiscernabilityLevelPowerComputeUseCase(
        data=data_as_dataframe_from_indiscernability_bl,
        all_distributions_as_dict=return_distributionasdict_secret_bl,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2,
        tuples_information=tuples_information_model_init_for_12tuples,
        nb_clusters=nb_clusters_equals_2,
        resolution=resolution_equals_999999)

    return indiscernability_level_uc


# ---------------------------------------------------------------------------------
# Fixtures for the TuplesInformation and related
# ---------------------------------------------------------------------------------
@pytest.fixture
def tuples_information_compute_uc_model_init(data_as_dataframe_from_indiscernability_bl,
                                             return_variables_list_a2_n2_secret3,
                                             return_nb_tainted_equals_1
                                             ):
    """ Generate an instance of TuplesInformationHDF5ComputeUseCase """
    tuples_information_uc = TuplesInformationHDF5ComputeUseCase(
        data=data_as_dataframe_from_indiscernability_bl,
        nb_secret_vars=return_nb_tainted_equals_1,
        vars_cardinalities=return_variables_list_a2_n2_secret3)

    return tuples_information_uc


@pytest.fixture
def tuples_information_model_init_for_12tuples(
        return_nb_tuples_equals_3,
        return_nb_tuples_equals_4,
        return_nb_tuples_equals_12
):
    """ Generate an instance of TuplesInformation """
    tuples_information = TuplesInformation(
        nb_secret_tuples=return_nb_tuples_equals_3,
        nb_public_tuples=return_nb_tuples_equals_4,
        total_nb_tuples=return_nb_tuples_equals_12)

    return tuples_information


@pytest.fixture
def tuples_information_model_init_for_d4_dot_4(
        return_nb_tuples_equals_10,
        return_nb_tuples_equals_3,
        return_nb_tuples_equals_30
):
    """ Generate an instance of TuplesInformation """
    tuples_information = TuplesInformation(
        nb_secret_tuples=return_nb_tuples_equals_10,
        nb_public_tuples=return_nb_tuples_equals_3,
        total_nb_tuples=return_nb_tuples_equals_30)

    return tuples_information


@pytest.fixture
def tuples_information_model_init_for_d4_dot_5(
        return_nb_tuples_equals_3,
        return_nb_tuples_equals_10,
        return_nb_tuples_equals_30
):
    """ Generate an instance of TuplesInformation """
    tuples_information = TuplesInformation(
        nb_secret_tuples=return_nb_tuples_equals_3,
        nb_public_tuples=return_nb_tuples_equals_10,
        total_nb_tuples=return_nb_tuples_equals_30)

    return tuples_information
