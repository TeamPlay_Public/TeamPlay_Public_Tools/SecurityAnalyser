"""   Main module """
import argparse
import math
import os
import sys
import time
from configparser import ConfigParser

from pathlib import Path
import errno

import logging

from sqcommon.adapter.File.JSON.Distribution.json_distribution_store_repository import JSONDistributionStoreRepository

from sqcommon.domain.Output.JSONFile.json_file_storage import JSONFileStorage
from sqcommon.domain.Shuffle.numpy_random_shuffle import NumpyRandomShuffle
from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples
from sqcommon.enums.enum_side_channel_class import SideChannelClasses
from sqcommon.exception.bad_arguments_exception import BadArgumentException
from sqcommon.use_case.BinomialDistributionNoise.binomial_distribution_noise_uc import BinomialDistributionNoiseUC


from sqcommon.adapter.File.HDF5.Data.hdf5_data_as_dataframe_repository import HDF5DataAsDataFrameGetRepository
from sqcommon.adapter.File.HDF5.SideChannel.side_channel_class_get_hdf5_repository \
    import SideChannelClassGetHDF5Repository
from sqcommon.adapter.File.HDF5.Variable.variables_cardinality_repository \
    import VariablesCardinalityHDF5Repository
from sqcommon.adapter.File.HDF5.Variable.variables_count_hdf5_repository import VariablesCountHDF5Repository
from sqcommon.adapter.File.HDF5.Variable.variables_names_list_hdf5_repository \
    import VariablesNamesHDF5Repository
from sqcommon.adapter.File.JSON.Distribution.distribution_json_repository \
    import DistributionJSONFileRepository

from sqcommon.enums.enum_hdf5_attributes import HDF5Attributes

from sqcommon.use_case.Data.hdf5_data_as_dataframe_get import HDF5DataAsDataFrameGetUC
from sqcommon.use_case.Distribution.distribution_json_get import DistributionJSONGetUseCase
from sqcommon.use_case.SideChannel.side_channel_class_get import SideChannelClassGetUC
from sqcommon.use_case.Variable.variables_hdf5_count import VariablesHDF5CountUC
from sqcommon.use_case.Variable.variables_names_hdf5_list import VariablesNamesHDF5UC
from sqcommon.use_case.Variable.variables_cardinality_hdf5 import VariablesCardHDF5UC

from securityquantifier.adapter.Database.Probe.probe_db_repository import ProbeDatabaseRepository
from securityquantifier.adapter.File.CSV.Probe.csv_file_probe_repository import CSVFileProbeRepository

from securityquantifier.domain.Distance.distance_chebyshev import DistanceChebyshev
from securityquantifier.domain.Distance.distance_euclidian import DistanceEuclidian
from securityquantifier.domain.Probe.probe_container import ProbeContainer

from securityquantifier.helper.dict_to_storageinfo_helper import DictToStorageInfoHelper
from securityquantifier.use_case.Distribution.all_distributions_get import AllDistributionsGetUseCase
from securityquantifier.use_case.Distribution.uniform_joint_distribution_compute import \
    UniformJointDistributionComputeUseCase
from securityquantifier.use_case.Metric.IndiscernibilityLevel.indiscernibility_level_energy_compute import \
    IndiscernabilityLevelEnergyComputeUseCase
from securityquantifier.use_case.Metric.IndiscernibilityLevel.indiscernibility_level_power_compute import \
    IndiscernabilityLevelPowerComputeUseCase

from securityquantifier.use_case.Metric.IndiscernibilityLevel.indiscernibility_level_time_compute \
    import IndiscernabilityLevelTimeComputeUseCase
from securityquantifier.use_case.Metric.metric_aced_compute import MetricACEDComputeUseCase
from securityquantifier.use_case.Metric.metric_acpd_compute import MetricACPDComputeUseCase
from securityquantifier.use_case.Metric.metric_actd_compute import MetricACTDComputeUseCase
from securityquantifier.use_case.Metric.metric_wced_compute import MetricWCEDComputeUseCase
from securityquantifier.use_case.Metric.metric_wcpd_compute import MetricWCPDComputeUseCase
from securityquantifier.use_case.Metric.metric_wctd_compute import MetricWCTDComputeUseCase
from securityquantifier.use_case.Tuples.tuples_information_compute_uc import TuplesInformationHDF5ComputeUseCase

path = os.path.join(os.path.dirname(__file__), os.pardir)
sys.path.append(path)

# Default configuration file.
#DEF_CONF_FILE = "../config/securityquantifier.ini"

# Storage Type
STORAGE_TYPE_FILE = "file"
STORAGE_TYPE_DB = "db"


def main():
    """   Main function """

    logging_file_path = "/var/log/TeamPlay/SQ/logging.log"

    log_path = Path(logging_file_path)
    log_path.parent.mkdir(parents=True, exist_ok=True)

    if not os.path.exists(logging_file_path):
        with open(logging_file_path, 'w+') as _:
            pass

    logging.basicConfig(filename=logging_file_path, level=logging.DEBUG, format='%(asctime)s %(message)s')

    # Getting arguments from command line
    msg = "Computing Security Level for the given input"

    # Initialize parser
    parser = argparse.ArgumentParser(description=msg)

    # Adding optional argument
    parser.add_argument("-c", "--Conf", help="Conf file location")

    parser.add_argument("-i", "--Input", help="Input file location")
    parser.add_argument("-o", "--Output", help="Output file location")

    parser.add_argument("-e", "--Exponent", help="Exponent value. Used to compute the Euclidian distance")

    parser.add_argument("-p", "--Probe", action='store_true', help="Activate debugging probe")
    parser.add_argument("-t", "--OType", help="Output type for the Probe")

    parser.add_argument("-ud", "--UniformDistribution", action='store_true', help="Use a uniform distribution")
    parser.add_argument("-bd", "--GenerateBinomialDistribution", action='store_true', help="Generates a binomial "
                                                                                           "distribution")
    parser.add_argument("-obd", "--BinomialDistributionFile", help="Binomial Distribution file location")

    parser.add_argument("-nc", "--NbClust", help="Final number of clusters for the Dendrogram Algorithm")
    parser.add_argument("-r", "--Resolution", help="Resolution for the Dendrogram Algorithm")

    parser.add_argument("-me", "--MetricToBeComputed", help="Allows to compute a specific metric instead of the one "
                                                            "provided in the Security Exchange file. For stand-alone "
                                                            "mode only")

    # Read arguments from command line
    args = parser.parse_args()

    # Configuration file call
    config = ConfigParser(allow_no_value=True)

    # # Extracting information from the configuration file if option -c or --Conf is provided
    # if args.Conf is None:
    #     # No configuration file provided, switching to default one
    #     conf_file = Path(DEF_CONF_FILE)
    # else:
    #     conf_file = Path(args.Conf)
    #     logging.info('Using configuration file %s', conf_file)
    #
    # if not os.path.isfile(conf_file):
    #     raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), conf_file)

    # Reading configuration file
    #config.read(conf_file)

    # Using input given as argument if option -i or --Input is provided
    if args.Input is None:
        # Argument not provided from command line, get it from the configuration file
        raise FileNotFoundError("No Input is provided", "Specify the path to the input using -i in the command line")
    else:
        # Argument was retrieved from the command line
        hdf5_f = Path(args.Input)
    logging.info('Getting input file %s', hdf5_f)

    if not os.path.isfile(hdf5_f):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), hdf5_f)

    # Getting Annotation value
    if args.MetricToBeComputed is None:
        # Argument not provided from command line, we proceed normally by extracting the annotation data
        # from the Security Exchange file
        annotation = ""
    else:
        # Argument was retrieved from the command line
        annotation = SideChannelClass(SideChannelClasses(args.MetricToBeComputed).name, args.MetricToBeComputed)
        logging.warning('Annotation argument is being used: %s. Please beware that this argument should be used only '
                        'in Stand-Alone mode, as it overrides the given side channel and mode from the input file ',
                        annotation)

    # Getting the distribution file
    # If boolean args.GenerateDistribution is not provided as cli arg
    if args.GenerateBinomialDistribution:
        # Generate the distribution
        is_generate_distribution = True
        logging.info('Distribution will be generated')
    else:
        is_generate_distribution = False
        logging.info('Distribution will NOT be generated')

    # If boolean args.UniformDistribution is provided as cli arg
    if args.UniformDistribution:
        # Use a uniform distribution
        is_uniform_distribution = True
        logging.info('A uniform distribution will be generated')
    else:
        is_uniform_distribution = False

    # Now we know where our distribution file comes from: generated or provided
    distribution_file = ""
    if not is_uniform_distribution:
        logging.info('Using non uniform distribution')
        if args.BinomialDistributionFile is None:
            # Argument not provided from command line
            raise FileNotFoundError("No distribution file is provided",
                                    "Specify the path to the distribution file using -obd in the command line")
        else:
            # Argument was retrieved from the command line
            distribution_file = Path(args.BinomialDistributionFile)
            logging.info('Retrieving the file where to store the Binomial distribution from the command line')
            # If the file does not exist, create it.
            if not os.path.exists(distribution_file):
                logging.info('Binomial Distribution file does not exist. Creating it.')
                with open(distribution_file, 'w+') as _:
                    pass
        if not os.path.isfile(distribution_file):
            raise FileNotFoundError(errno.ENOENT, "Path to the distribution file does not exist.", distribution_file)

    # Getting active_probe
    if args.Probe:
        # No probe
        active_probe = True
        logging.info('Probing is activated')
    else:
        active_probe = False
        logging.info('No probe activated')

    # Using output given as argument if option -o or --Output is provided
    # If Output is given as argument from the command line, then output type is file
    # else if no Output is given then we need to get the output type, either from the command line or conf file
    if active_probe:
        if args.Output is None:
            # If no Output arg is given, we get the type of storage, either from args or config file
            if args.OType:
                storage_type = args.OType
            else:
                raise BadArgumentException("No output type is given for the probe",
                                           "Provide an output type using -t in the command line")
            logging.info('Probe storage type: %s', storage_type)

            # Storage can be File or Database
            if storage_type == STORAGE_TYPE_FILE:
                storage_info = dict(config.items('output_file'))
                storage = DictToStorageInfoHelper.dict_to_filestorage(storage_info["file_name"])
                probe_repository = CSVFileProbeRepository
            elif storage_type == STORAGE_TYPE_DB:
                storage_info = dict(config.items('output_db'))
                storage = DictToStorageInfoHelper.dict_to_databasestorage(storage_info)
                probe_repository = ProbeDatabaseRepository
        else:
            storage_info = Path(args.Output)
            probe_repository = CSVFileProbeRepository
            storage = DictToStorageInfoHelper.dict_to_filestorage(storage_info)
    else:
        # probe_repository and storage are not used when probe is not active but it still needs to be initialized
        probe_repository = ""
        storage = ""

    # Defining the ProbeContainer to gather probe-related data
    probe_container = ProbeContainer(active_probe=active_probe,
                                     probe=probe_repository,
                                     storage=storage)

    # UC: Get the HDF5 file as a InputHDF5 object in a DataFrame
    logging.info('UC: Get the HDF5 file as a InputHDF5 object in a DataFrame')
    data_as_dataframe_uc = HDF5DataAsDataFrameGetUC(
        data_repo=HDF5DataAsDataFrameGetRepository,
        input_path=hdf5_f
    )
    data = data_as_dataframe_uc.execute()

    # UC: Get Side Channel Class from the HDF5 file
    logging.info('UC: Get Side Channel Class from the HDF5 file')
    scc_uc = SideChannelClassGetUC(
        side_channel_class_repo=SideChannelClassGetHDF5Repository,
        input_path=hdf5_f
    )
    scc = scc_uc.execute()

    # UC: Get number of tainted variables from the HDF5 file
    logging.info('UC: Get number of tainted variables from the HDF5 file')
    nb_tainted_vars_uc = VariablesHDF5CountUC(
        variables_count_repo=VariablesCountHDF5Repository,
        input_path=hdf5_f,
        side_channel_class=scc,
        hdf5_attribute=HDF5Attributes.NB_TAINTED
    )
    nb_tainted_vars = nb_tainted_vars_uc.execute()

    # UC: Get number of untainted variables from the HDF5 file
    logging.info('UC: Get number of untainted variables from the HDF5 file')
    nb_untainted_vars_uc = VariablesHDF5CountUC(
        variables_count_repo=VariablesCountHDF5Repository,
        input_path=hdf5_f,
        side_channel_class=scc,
        hdf5_attribute=HDF5Attributes.NB_UNTAINTED
    )
    nb_untainted_vars = nb_untainted_vars_uc.execute()

    # UC: Get the variables names from the HDF5 file
    logging.info('UC: Get the variables names from the HDF5 file')
    vars_names_uc = VariablesNamesHDF5UC(
        variables_names_repo=VariablesNamesHDF5Repository,
        input_path=hdf5_f
    )
    vars_names = vars_names_uc.execute()

    # UC: Get the cardinality of each variable
    logging.info('UC: Get the cardinality of each variable')
    vars_cardinality_uc = VariablesCardHDF5UC(
        variables_card_repo=VariablesCardinalityHDF5Repository,
        input_path=hdf5_f,
        side_channel_class=scc,
        vars_list=vars_names
    )
    vars_cardinalities = vars_cardinality_uc.execute()

    # UC: Get the information about the tuples in the Security Exchange file
    logging.info('UC: Get the information about the tuples in the Security Exchange file')
    tuples_information_uc = TuplesInformationHDF5ComputeUseCase(
        data=data,
        nb_secret_vars=nb_tainted_vars,
        vars_cardinalities=vars_cardinalities
    )
    tuples_information = tuples_information_uc.execute()

    # UC: Using the Dataframe, extract the values in a dict
    # dataframe_to_dict_uc = DataAsDataFrameToDictGetUC(
    #     data_as_dataframe=data,
    #     nb_tainted_vars=nb_tainted_vars,
    #     nb_untainted_vars=nb_untainted_vars,
    # )
    # dataframe_as_dict = dataframe_to_dict_uc.execute()

    # For experiments only: We take into account the annotation given as a parameter
    # in order to bypass the annotation from the Security Exchange file.
    # Thus we can compute different metrics using the same Security Exchange file.
    if annotation:
        scc = annotation

    # Getting exponent value
    if "avg" in scc.value:
        if args.Exponent is None:
            # Argument not provided from command line, get it from the configuration file
            raise BadArgumentException("No exponent were provided",
                                       "Provide an exponent value using -e in the command line")
        else:
            # Argument was retrieved from the command line
            exponent = int(args.Exponent)
        logging.info('Retrieving exponent value %s', exponent)

    # Dendrogram Algorithm:
    # Parameter resolution may be given through CLI args or configuration file
    if "ratio" in scc.value:
        logging.info('Gathering arguments for aspect of type %s', scc.value)
        # Optional argument.
        if args.Resolution is None:
            resolution = None
            logging.info('No Resolution provided')
        else:
            # Argument was retrieved from the command line
            resolution = int(args.Resolution)
            logging.info('Retrieving Resolution value : %s', resolution)

        if args.NbClust is None:
            # Argument not provided from command line.
            # For annotation aspect ratio, if the argument nb_clusters is not provided, we compute a default value
            nb_clusters = (math.floor(math.log(vars_cardinalities.lst[0].cardinality, 2))) + 1
            logging.info('No Number of clusters given, computing default value : %s', nb_clusters)
        else:
            # Argument was retrieved from the command line
            nb_clusters = int(args.NbClust)
            logging.info('Retrieving Number of clusters value : %s', nb_clusters)

    # UC: Get the JSON distribution file
    # This file is needed only for ratio and average aspects
    if "ratio" in scc.value or "avg" in scc.value:
        logging.info('Gathering distributions for aspect of type %s', scc.value)
        # Generate the distribution if required
        if is_generate_distribution:
            logging.info('Generating a joint Binomial distribution in file %s', distribution_file)
            # We call the UC to generate the distribution file which has the same number of values as the SE file
            ng_uc = BinomialDistributionNoiseUC(json_output_file=distribution_file,
                                                nb_tuples=len(data.data),
                                                distribution_type=EnumDistributionTuples.SECRET_PUBLIC,
                                                shuffle_function=NumpyRandomShuffle,
                                                storage_data=JSONFileStorage,
                                                storage_repo=JSONDistributionStoreRepository
                                                )
            ng_uc.execute()

        if not is_uniform_distribution:
            logging.info('UC: From the JSON Distribution file %s, extract the distribution as a Dictionary', distribution_file)
            # UC: From the JSON Distribution file, extract the distribution as a Dictionary
            dist_json_get_uc = DistributionJSONGetUseCase(
                distribution_repository=DistributionJSONFileRepository,
                dist_file=distribution_file
            )
            distributions_as_dict = dist_json_get_uc.execute()

        else:
            # We compute the uniform distribution with values equal to 1/total_nb_tuples
            # TODO: This is ok as long as we have a cartesian product total_tuples=public_tuples*secret_tuples.
            #   Adapt value of UC.total_nb_tuples when the new version
            #   that takes into account non-cartesian product files
            # UC: Generate a uniform joint distribution for secret and public variables
            logging.info('UC: Generate a uniform joint distribution for secret and public variables')
            uniform_joint_dist_compute_uc = UniformJointDistributionComputeUseCase(
                total_nb_tuples=tuples_information.total_nb_tuples
            )
            distributions_as_dict = uniform_joint_dist_compute_uc.execute()

        # UC: From the information on the Security Exchange file tuples and the distribution file data,
        # we compute the marginal distributions P(X) and P(K) or the joint distribution P(K,X),
        # depending on the content of the distribution file
        logging.info('UC: From the distribution file, computing the missing distributions')
        all_distributions_get_uc = AllDistributionsGetUseCase(
            dist_as_dict=distributions_as_dict,
            tuples_information=tuples_information
        )
        all_distributions_dict = all_distributions_get_uc.execute()

    if scc.value == "time_ratio":
        logging.info('UC: Compute the Indiscernability Level for Time')
        # UC: Compute the Indiscernability Level for Time
        indiscernability_level_uc = IndiscernabilityLevelTimeComputeUseCase(
            data=data,
            all_distributions_as_dict=all_distributions_dict,
            nb_tainted_vars=nb_tainted_vars,
            nb_untainted_vars=nb_untainted_vars,
            tuples_information=tuples_information,
            nb_clusters=nb_clusters,
            resolution=resolution
        )
        # Hack: We turn the dictionary content into a string for displaying purposes
        # metric_value = 'IIR:'.join([f'{key}:: {value}' for key, value in indiscernability_level_uc.execute().items()])
        metric_value = indiscernability_level_uc.execute()

    elif scc.value == "energy_ratio":
        logging.info('UC: Compute the Indiscernability Level for Energy')
        # UC: Compute the Indiscernability Level for Energy
        indiscernability_level_uc = IndiscernabilityLevelEnergyComputeUseCase(
            data=data,
            all_distributions_as_dict=all_distributions_dict,
            nb_tainted_vars=nb_tainted_vars,
            nb_untainted_vars=nb_untainted_vars,
            tuples_information=tuples_information,
            nb_clusters=nb_clusters,
            resolution=resolution
        )
        metric_value = indiscernability_level_uc.execute()

    elif scc.value == "power_ratio":
        logging.info('UC: Compute the Indiscernability Level for Power')
        # UC: Compute the Indiscernability Level for Power
        indiscernability_level_uc = IndiscernabilityLevelPowerComputeUseCase(
            data=data,
            all_distributions_as_dict=all_distributions_dict,
            nb_tainted_vars=nb_tainted_vars,
            nb_untainted_vars=nb_untainted_vars,
            tuples_information=tuples_information,
            nb_clusters=nb_clusters,
            resolution=resolution
        )
        metric_value = indiscernability_level_uc.execute()

    elif scc.value == "time_worst":
        logging.info('UC: Compute distance-based metric WCTD')
        # UC: Compute distance-based metric WCTD
        metric_wctd_uc = MetricWCTDComputeUseCase(
            distance_chebyshev=DistanceChebyshev,
            data=data,
            nb_tainted_vars=nb_tainted_vars,
            nb_untainted_vars=nb_untainted_vars,
            variables_cards=vars_cardinalities,
            probe_container=probe_container
        )
        metric_value = metric_wctd_uc.execute()

    elif scc.value == "energy_worst":
        logging.info('UC: Compute distance-based metric WCED')
        # UC: Compute distance-based metric WCED
        metric_wced_uc = MetricWCEDComputeUseCase(
            distance_chebyshev=DistanceChebyshev,
            data=data,
            nb_tainted_vars=nb_tainted_vars,
            nb_untainted_vars=nb_untainted_vars,
            variables_cards=vars_cardinalities,
            probe_container=probe_container
        )
        metric_value = metric_wced_uc.execute()

    elif scc.value == "power_worst":
        logging.info('UC: Compute distance-based metric WCPD')
        # UC: Compute distance-based metric WCPD
        # Compute metric with cardinality
        metric_wcpd_uc = MetricWCPDComputeUseCase(
            distance_chebyshev=DistanceChebyshev,
            data=data,
            nb_tainted_vars=nb_tainted_vars,
            nb_untainted_vars=nb_untainted_vars,
            variables_cards=vars_cardinalities,
            probe_container=probe_container
        )
        metric_value = metric_wcpd_uc.execute()

    elif scc.value == "time_avg":
        logging.info('UC: Compute distance-based metric ACTD')
        # UC: Compute distance-based metric ACTD
        metric_actd_uc = MetricACTDComputeUseCase(
            distance_euclidian=DistanceEuclidian,
            data=data,
            distribution_as_dict=all_distributions_dict,
            nb_tainted_vars=nb_tainted_vars,
            nb_untainted_vars=nb_untainted_vars,
            variables_cards=vars_cardinalities,
            exponent=exponent,
            probe_container=probe_container
        )
        metric_value = metric_actd_uc.execute()

    elif scc.value == "energy_avg":
        logging.info('UC: Compute distance-based metric ACED')
        # UC: Compute distance-based metric ACED
        metric_aced_uc = MetricACEDComputeUseCase(
            distance_euclidian=DistanceEuclidian,
            data=data,
            distribution_as_dict=all_distributions_dict,
            nb_tainted_vars=nb_tainted_vars,
            nb_untainted_vars=nb_untainted_vars,
            variables_cards=vars_cardinalities,
            exponent=exponent,
            probe_container=probe_container
        )
        metric_value = metric_aced_uc.execute()

    elif scc.value == "power_avg":
        logging.info('UC: Compute distance-based metric ACPD')
        # UC: Compute distance-based metric ACPD
        metric_acpd_uc = MetricACPDComputeUseCase(
            distance_euclidian=DistanceEuclidian,
            data=data,
            distribution_as_dict=all_distributions_dict,
            nb_tainted_vars=nb_tainted_vars,
            nb_untainted_vars=nb_untainted_vars,
            variables_cards=vars_cardinalities,
            exponent=exponent,
            probe_container=probe_container
        )
        metric_value = metric_acpd_uc.execute()
    # sys.exit(metric_value)
    print(metric_value)


if __name__ == '__main__':
    start_time = time.time()
    main()
    logging.info('------------Execution Ended---------------------')
    print("--- Total execution time: %s seconds ---" % (time.time() - start_time))
