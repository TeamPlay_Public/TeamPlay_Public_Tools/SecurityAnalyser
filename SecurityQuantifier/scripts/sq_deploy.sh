#!/bin/bash

set -e

TOOLS_DIRECTORY=$(pwd)

export CONDA_ALWAYS_YES="true"

echo "Enter the SecurityAnalyser installation folder path:"
read -r SQDIR

# Checking out SQ source code
if [[ $SQDIR ]]; then

	# If the given folder exists
  if [ -d "$SQDIR" ]; then

		# cd to the SQ installation folder
		cd "$SQDIR"

		# Remove the virtual env created by the script with conda: In case of previous failed install
		conda env remove --name secquantvenv
    # Checkout the SecurityQuantifier directory only from the Gitlab adapter
		echo $'\n[CHECKING OUT SQ SOURCE CODE FROM GITLAB]'
		git init
		git remote add -f origin https://gitlab.inria.fr/TeamPlay/TeamPlay_Development/TeamPlay_Tools
		git config core.sparseCheckout true
		echo "SecurityAnalyser/SecurityQuantifier" >> .git/info/sparse-checkout
		git pull origin master
	else
	        echo "Invalid argument. Installation aborted."
        	exit 1
	fi
else
        echo "Invalid argument. Installation aborted."
        exit 1
fi

# Creating Conda Virtual Env
echo $'\n[CREATING CONDA VIRTUAL ENV]'
module load anaconda/3-2020.11 # for TUHH server
# otherwise: module load anaconda
conda create --name secquantvenv
conda install -n secquantvenv pandas numpy h5py pytables


# Installing SQCommon lib
echo $'\n[INSTALLING SQCOMMON LIB]'
conda install  -n secquantvenv "$SQDIR"/SecurityAnalyser/SecurityQuantifier/lib/conda/sqcommon-1.0.2-py38_0.tar.bz2

#End
unset CONDA_ALWAYS_YES
echo $'\n[INSTALLATION COMPLETE]'
