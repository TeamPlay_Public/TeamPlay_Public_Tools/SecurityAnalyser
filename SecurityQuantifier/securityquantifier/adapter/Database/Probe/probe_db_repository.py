"""
    Module defining the ProbeDatabaseRepository
"""

from securityquantifier.adapter.probe_repository import ProbeRepository
from securityquantifier.domain.Data.probe_data import ProbeData

from securityquantifier.interface.domain.Storage.storage_interface import StorageInterface


class ProbeDatabaseRepository(ProbeRepository):
    """ ProbeDatabaseRepository class defines the Repository to save data gathered by the probe into a database """

    @staticmethod
    def store(data: ProbeData,
              storage: StorageInterface):
        """
            Method saving data gathered by the probe into a database
        """
        # TODO: Implement the storing of the data in a database
        return True

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
