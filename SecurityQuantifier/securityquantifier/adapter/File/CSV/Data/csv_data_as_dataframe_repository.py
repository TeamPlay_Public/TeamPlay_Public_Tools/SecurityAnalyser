"""
    Module defining the Repository used to get a DataFrame out of a CSV file
"""
from pathlib import Path

from pandas import read_csv

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.interface.adapter.Data.data_repository_interface import DataRepositoryInterface
from sqcommon.interface.domain.Data.data_interface import DataInterface


class CSVDataAsDataFrameGetRepository(DataRepositoryInterface):
    """
        CSVDataAsDataFrameGetRepository class defines the Repository to create a DataFrame from a CSV file
    """

    @staticmethod
    def find(input_path: Path) -> DataInterface:
        """
            Create a DataFrame from a CSV file

            Parameters
            ----------
            input_path : Path
                the csv file containing the data

            Returns
            -------
            data_df: DataFrame
                Panda DataFrame containing the data
        """
        # Reading only the columns name
        col_names = read_csv(input_path, skiprows=2, nrows=0).columns
        # Defining the three last columns as int
        types_dict = {'C': int, 'D': int, 'E': int}
        # The rest of the columns (aka the first and the second) will be strings
        types_dict.update({col: str for col in col_names if col not in types_dict})
        # Reading the rows from the CSV
        df = read_csv(input_path, skiprows=3, header=0, dtype=types_dict)

        data_df = DataAsDataFrame(df)

        return data_df

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
