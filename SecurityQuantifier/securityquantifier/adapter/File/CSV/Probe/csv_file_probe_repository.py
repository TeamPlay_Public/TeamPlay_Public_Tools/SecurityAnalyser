"""
    Module defining the CSVFileProbeRepository to store probe data into a CSV file
"""

from securityquantifier.adapter.probe_repository import ProbeRepository
from securityquantifier.domain.Data.probe_data import ProbeData

from securityquantifier.interface.domain.Storage.storage_interface import StorageInterface


class CSVFileProbeRepository(ProbeRepository):
    """
        CSVFileProbeRepository class defines the generic Repository to save data
        gathered by the probe into a CSV file
    """

    @staticmethod
    def store(data: ProbeData,
              storage: StorageInterface):
        """
            Method saving data gathered by the probe into a CSV file
        """
        with open(storage.name, 'a') as storage_file:
            for item in CSVFileProbeRepository.recorder:
                line = item.value1 + ', ' + item.value2 + ', ' + item.current_distance + ', ' + item.total_distance
                storage_file.write(line)

        return True

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
