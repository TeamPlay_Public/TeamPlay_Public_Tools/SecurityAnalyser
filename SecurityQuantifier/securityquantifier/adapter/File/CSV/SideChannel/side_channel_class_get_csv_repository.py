"""
    Module defining the Repository used to extract the Side Channel Class from a CSV file
"""
from csv import reader

from pathlib import Path

from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.enums.enum_side_channel_class import SideChannelClasses
from sqcommon.interface.adapter.SideChannel.side_channel_class_repository_interface import \
    SideChannelClassRepositoryInterface
from sqcommon.interface.domain.SideChannel.side_channel_class_interface import SideChannelClassInterface


class SideChannelClassGetCSVRepository(SideChannelClassRepositoryInterface):
    """
        SideChannelClassGetCSVRepository class defines the Repository to get the Side Channel Class from a CSV file

    """
    @staticmethod
    def find(input_path: Path) -> SideChannelClassInterface:
        """
            Find the side channel class associated with the CSV file

            Parameters
            ----------
            input_path : Path
                the csv file containing the data

            Returns
            -------
            side_channel: SideChannelClass
                SideChannelClass containing the side channel class associated with the CSV file
        """
        # Get the side channel class from the file
        try:
            with open(input_path, newline='') as csvfile:
                vars_reader = reader(csvfile, delimiter=',')

                # Get the line with indexes
                for i, row in enumerate(vars_reader):
                    # the fourth row  is the one containing the side channel class
                    if i == 3:
                        # the last element of the row is the side channel class
                        side_channel_name = SideChannelClasses(row[-1]).name
                        side_channel_value = str(row[-1])
                        break
        except FileNotFoundError as e:
            print(e.errno)

        # Construct the SideChannelClass object
        side_channel = SideChannelClass(side_channel_name, side_channel_value)

        return side_channel

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
