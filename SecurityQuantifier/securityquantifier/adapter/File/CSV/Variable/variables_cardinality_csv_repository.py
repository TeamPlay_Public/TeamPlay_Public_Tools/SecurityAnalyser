"""
    Module defining the Repository used to return the cardinality of the variables
"""
from pathlib import Path
from typing import List

from pandas import read_csv


class VariablesCardinalityCSVRepository:
    """
        VariablesCardinalityRepository class returns the cardinality of the variables

    """

    @staticmethod
    def find_all(input_file: Path,
                 nb_tainted_vars: int,
                 nb_untainted_vars: int) -> List[int]:
        """
            Gather the cardinality of each variable in var_names

            Parameters
            ----------
            input_file : Path
                The CSV file
            nb_tainted_vars: int
                Number of tainted variables in the CSV file
            nb_untainted_vars: int
                Number of untainted variables in the CSV file

            Returns
            -------
            cards.toList(): List
                the list of the cardinalities
         """
        nb_columns = list(range(1, nb_tainted_vars + nb_untainted_vars+1))

        cards_dataframe = read_csv(input_file, skiprows=1, nrows=1, usecols=nb_columns)

        cards = cards_dataframe.iloc[0].values[0:3]

        return cards.tolist()

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
