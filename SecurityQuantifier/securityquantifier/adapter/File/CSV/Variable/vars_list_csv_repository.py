"""
    Module defining the Repository used to extract variables list out of a CSV file
"""
from csv import reader

from pathlib import Path

from sqcommon.enums.enum_var_types import VariableTypes
from sqcommon.interface.domain.Variable.variables_list_interface import VariablesListInterface

from securityquantifier.domain.Variable.tainted_variable import TaintedVariable
from securityquantifier.domain.Variable.untainted_variable import UntaintedVariable
from sqcommon.domain.Variable.variables_list import VariablesList
from securityquantifier.interface.adapter.Variable.vars_list_repository_interface import \
    VariablesListRepositoryInterface


class CSVVariablesListRepository(VariablesListRepositoryInterface):
    """
        CSVVariablesListRepository class defines the Repository to extract the variables names list,
         of type var_type, out of a CSV file
    """
    @staticmethod
    def find_by_type(var_type: VariableTypes, input_path: Path) -> VariablesListInterface:
        """
            List variables names of type var_type from a CSV file

            Parameters
            ----------
            var_type: VariableTypes
                Type of the variables to extract
            input_path : Path
                the csv file containing the data

            Returns
            -------
            vars_list: VariablesList
                VariablesList containing the list of variables names of type var_type
        """
        vars_list = VariablesList()

        # Get the variables of type var_type from the csv file
        try:
            with open(input_path, newline='') as csvfile:
                vars_reader = reader(csvfile, delimiter=',')

                # Read the rows
                for row in vars_reader:
                    # If the first token of the row matches to type of variables requested
                    if row[0] == var_type.value:
                        # In the rest of the tokens of the row
                        for var in row[1:]:
                            # Create a variable of the matching type
                            if var_type.value == VariableTypes.TAINTED.value:
                                vr = TaintedVariable(value=var)
                            if var_type.value == VariableTypes.UNTAINTED.value:
                                vr = UntaintedVariable(value=var)
                            # And add it to the list
                            vars_list.lst.append(vr)
                        break
        except FileNotFoundError as e:
            print(e.errno)

        return vars_list

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
