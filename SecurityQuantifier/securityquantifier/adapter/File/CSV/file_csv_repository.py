"""
    Module defining the Repository used to extract a DataFrame out of a CSV file
"""
from os.path import isfile
from pathlib import Path

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.domain.Variable.variables_list import VariablesList

from securityquantifier.domain.Input.input_dataframe import InputCSVDataFrame
from securityquantifier.interface.adapter.Input.input_repository_interface import InputRepositoryInterface
from securityquantifier.interface.domain.Input.input_csv_interface import InputCSVInterface


class FileRepository(InputRepositoryInterface):
    """
        Class defining the Repository used to extract a DataFrame out of a CSV file
    """

    @staticmethod
    def find_by_path(input_path: Path,
                     tainted_vars: VariablesList,
                     untainted_vars: VariablesList,
                     side_channel_class: SideChannelClass,
                     data=DataAsDataFrame) -> InputCSVInterface:
        """
            Static method extracting a DataFrame out of a CSV file
        """
        if not isfile(input_path):
            raise FileNotFoundError('"File %s does not exist", str(input_path)')
        else:
            return InputCSVDataFrame(path=input_path,
                                     tainted_vars=VariablesList,
                                     untainted_vars=VariablesList,
                                     side_channel_class=SideChannelClass,
                                     data=DataAsDataFrame
                                     )

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
