"""
    Module defining the ProbeFileRepository
"""

import abc

from securityquantifier.domain.Data.probe_data import ProbeData
from securityquantifier.interface.adapter.Probe.probe_repository_interface import ProbeRepositoryInterface
from securityquantifier.interface.domain.Storage.storage_interface import StorageInterface


class ProbeRepository(ProbeRepositoryInterface):
    """ ProbeFileRepository class defines the generic Repository to save data gathered by the probe """

    recorder = []

    @staticmethod
    def record(data: ProbeData):
        """
            record(...) buffers the data in a Python data structure, before store it using the store(...) method
        """

        ProbeRepository.recorder.append(data)

    @staticmethod
    @abc.abstractmethod
    def store(data: ProbeData,
              storage: StorageInterface
              ):
        """
            store(...) saves the values and their respective distance
        """
        pass

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
