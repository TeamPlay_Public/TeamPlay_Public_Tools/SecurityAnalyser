"""
    ProbeData module specifies an implementation of DataInterface
    which encapsulates data related to the probe
"""


class ProbeData:
    """
        ProbeData class encapsulates data related to the probe

        Attribute
        ----------
        value1: float
            First value involved in the computation of the current distance

        value2: float
            Second value involved in the computation of the current distance

        current_distance: float
            Computational result of the distance using value 1 and value2

        total_distance: float
            Value of the distance summing all the previous current_distance until now
    """

    def __init__(
        self,
        value1: str,
        value2: str,
        current_distance: str,
        total_distance: str
    ):
        """ Constructor for the TextFileStorage class """
        self._value1 = value1
        self._value2 = value2
        self._current_distance = current_distance
        self._total_distance = total_distance

    @property
    def value1(self) -> str:
        """ Getter for the value1 attribute """
        return self._value1

    @value1.setter
    def value1(self, value1):
        """ Setter for the value1 attribute """
        self._value1 = value1

    @property
    def value2(self) -> str:
        """ Getter for the value2 attribute """
        return self._value2

    @value2.setter
    def value2(self, value2):
        """ Setter for the value2 attribute """
        self._value2 = value2
        
    @property
    def current_distance(self) -> str:
        """ Getter for the current_distance attribute """
        return self._current_distance

    @current_distance.setter
    def current_distance(self, current_distance):
        """ Setter for the current_distance attribute """
        self._current_distance = current_distance

    @property
    def total_distance(self) -> str:
        """ Getter for the total_distance attribute """
        return self._total_distance

    @total_distance.setter
    def total_distance(self, total_distance):
        """ Setter for the total_distance attribute """
        self._total_distance = total_distance
