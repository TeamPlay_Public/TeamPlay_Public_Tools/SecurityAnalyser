"""
    SEFileAsDict module specifies the SEFileAsDict domain
    which is a container for the Dictionary populated by data extracted from the SEFileAsDict
"""
from typing import Dict


class SEFileAsDict:
    """
        SEFileAsDict class encapsulates data extracted from the Security Exchange file

        Attribute
        ----------
        data: Dict
            Data extracted from the Security Exchange file and recorded in a Dict
    """

    def __init__(
        self,
        data: Dict
    ):
        """ Constructor for the SEFileAsDict class """
        self._data = data

    @property
    def data(self) -> Dict:
        """ Getter for the data attribute """
        return self._data

    @data.setter
    def data(self, data):
        """ Setter for the data attribute """
        self._data = data
