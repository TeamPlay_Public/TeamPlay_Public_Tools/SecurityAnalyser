""" Dendrogram module specifies the Dendrogram class """
from typing import List


class Dendrogram:
    """
        Dendrogram class specifies Dendrogram attributes and methods.

        The Dendrogram is a domain object that contains all the necessary values to run the Dendrogram algorithm.

        It has the following inputs :
            leakage_tuples: List[float]
                The list of leakage values to on which to run the Dendrogram algorithm.
                This list is first sorted while keeping their original indices and clustered
                so that each element of the list is in its own cluster
            public_marginal_dist_for_current_public_tuple: List[List[float]]
                The public marginal distribution associated to the public tuple
                for which we run the Dendrogram algorithm
            related_secret_tuples: List[float]
                The secret tuple values related to the leakage tuples in leakage_tuples
            related_public_tuple: List[float]
                The public tuple for which we run the Dendrogram algorithm
            given_resolution: int
                The termination criteria resolution provided by the developer. This value is optional.
                When provided the algorithm will stop once the resolution criteria is met.
            given_nb_clusters: int
                The termination criteria nb_clusters provided by the developer.
                If this value is not provided, a default value is computed.
                The algorithm will stop once the nb_clusters criteria is met.
    """

    _original_position = []
    _distances = []
    _initial_distances = []

    def __init__(
            self,
            leakage_tuples: List[float],
            secret_public_joint_dist_for_current_public_tuple: List[List[float]],
            related_secret_tuples: List[float],
            related_public_tuple: str,
            given_resolution: int,
            given_nb_clusters: int
    ):
        """ Constructor for the Dendrogram class """
        self._initial_leakage_clustering = self._sort_and_clusterize_leakages(leakage_tuples)
        self._secret_public_joint_dist_for_current_public_tuple = secret_public_joint_dist_for_current_public_tuple
        self._related_secret_tuples = self._match_reordered_obs(related_secret_tuples)
        self._related_public_tuple = related_public_tuple
        self._given_resolution = given_resolution
        self._given_nb_clusters = given_nb_clusters
        self._compute_distances()

    def _sort_and_clusterize_leakages(self,
                                      leakages: List[float]):
        """
            From the leakages given in the constructor,
                sort the element in the list while keeping their original indices;
                then return a list containing each element as its own cluster.
        """
        # First sort while keeping the initial indices
        self.original_position, sorted_obs = _sort_obs(leakages)

        # Return the sorted_obs as a list
        return list(sorted_obs)

    def _match_reordered_obs(self,
                             related_secrets: List[float]):
        """
            Reorders the related_secret given in the constructor following the sorting of the leakages.
            Uses the self.original_position.
        """
        if len(self._initial_leakage_clustering) != len(related_secrets):
            raise ValueError("self._initial_clustering and related_secrets must have the same number of elements")

        if len(self._secret_public_joint_dist_for_current_public_tuple) != len(related_secrets):
            raise ValueError("The lists of related_secrets and its related marginal distribution must have the same "
                             "number of elements")

        # for each leakage with indexX in self._initial_clustering,
        # Step 1: get the original_position given by self.original_position at indexX,
        # then get the secret at this index
        # Step 2: Do the same reordering for the distribution for the secret variable
        returned_list_of_secrets = []
        returned_list_of_distributions = []
        for index, _ in enumerate(self._initial_leakage_clustering):
            returned_list_of_secrets.append(related_secrets[list(self._original_position)[index]])
            returned_list_of_distributions.append(
                self._secret_public_joint_dist_for_current_public_tuple[list(self._original_position)[index]])

        self._secret_public_joint_dist_for_current_public_tuple = returned_list_of_distributions
        return returned_list_of_secrets

    def _compute_distances(self):
        """
            Compute distances D between leakages L such as
                D(i) = L(i+1) - L(i)

        """
        self._distances.clear()

        for i in range(0, len(self._initial_leakage_clustering) - 1):
            self._distances.append(
                int(self._initial_leakage_clustering[i + 1][0])
                - int(self._initial_leakage_clustering[i][0]))

        self._initial_distances = self._distances
        self._distances = sorted(set(self._distances))

    def initial_leakage_clustering(self) -> List[List[float]]:
        """ Getter for the initial_leakage_clustering attribute """
        return self._initial_leakage_clustering

    def related_secret_tuples(self) -> List[float]:
        """ Getter for the related_secret_tuples attribute"""
        return self._related_secret_tuples

    def secret_public_joint_dist_for_current_public_tuple(self) -> List[List[float]]:
        """ Getter for the secret_public_joint_dist_for_current_public_tuple attribute"""
        return self._secret_public_joint_dist_for_current_public_tuple

    def related_public_tuple(self) -> str:
        """ Getter for the related_public_tuple attribute"""
        return self._related_public_tuple

    @property
    def given_resolution(self) -> int:
        """ Getter for the given_resolution attribute """
        return self._given_resolution

    @property
    def given_nb_clusters(self) -> int:
        """ Getter for the given_nb_clusters attribute """
        return self._given_nb_clusters

    @property
    def original_position(self) -> List[int]:
        """ Getter for the original_position attribute.
            Contains the indices of the leakages.
         """
        return self._original_position

    @original_position.setter
    def original_position(self, original_position):
        """ Setter for the original_position attribute.
            Contains the indices of the leakages.
         """
        self._original_position = original_position

    def distances(self) -> List[float]:
        """ Getter for the distances attribute.
            Contains a list of distances between elements of the initial cluster.
            Sorted and without duplicates
        """
        return self._distances

    def initial_distances(self) -> List[float]:
        """
            Getter for the initial_distances attribute.
            Contains a list of distances between elements of the initial cluster
        """
        return self._initial_distances


def _sort_obs(leakages: List[float]):
    """
        Sorts a list of float by value and returns two lists:
            - a list of the original indices
            - the sorted list of values
    """
    return zip(*sorted(enumerate(leakages), key=lambda i: i[1]))


def _cluster(lst: List[float]):
    """
        Turns a list into a list of lists.
        Ex:
            Input : [101, 202, 303, 404, 505]
            Output : [[101], [202], [303], [404], [505]]
    """
    return list(map(lambda el: [el], lst))
