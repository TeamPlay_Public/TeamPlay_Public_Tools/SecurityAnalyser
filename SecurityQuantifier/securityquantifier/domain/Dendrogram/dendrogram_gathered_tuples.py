""" DendrogramGatheredTuples module specifies the DendrogramGatheredTuples class """
from typing import List, Dict


class DendrogramGatheredTuples:
    """
        DendrogramGatheredTuples class specifies DendrogramGatheredTuples attributes and methods.
        It contains :
            dict_tuples_occurrences: Dict
            tuples_list: List[float]
     """

    def __init__(
            self,
            dict_tuples_occurrences: Dict,
            tuples_list: List[float]
    ):
        """ Constructor for the Dendrogram class """
        self._dict_tuples_occurrences = dict_tuples_occurrences
        self._tuples_list = tuples_list

    @property
    def dict_tuples_occurrences(self) -> Dict:
        """ Getter for the dict_tuples_occurrences attribute """
        return self._dict_tuples_occurrences

    @property
    def tuples_list(self) -> List[float]:
        """ Getter for the tuples_list attribute """
        return self._tuples_list

    @dict_tuples_occurrences.setter
    def dict_tuples_occurrences(self, dict_tuples_occurrences):
        """ Getter for the dict_tuples_occurrences attribute """
        self._dict_tuples_occurrences = dict_tuples_occurrences

    @tuples_list.setter
    def tuples_list(self, tuples_list):
        """ Getter for the tuples_list attribute """
        self._tuples_list = tuples_list
