""" DendrogramResultsElement module specifies the DendrogramResult class """
import logging
from typing import List

from securityquantifier.enums.enum_termination_criteria import EnumTerminationCriteria


class DendrogramResult:
    """
        DendrogramResult class specifies DendrogramResult attributes and methods.

        This class content is associated to one public tuple which related information was used by
         the Dendrogram algorithm to cluster the leakages and their related secret, distribution values.

        It contains :
            related_public_tuple: str,
                The public tuple related to the leakages, the secret tuples and the joint distribution used by
                the Dendrogram algorithm to get the final clustering.
            final_leakage_clustering: List[List[float]]
                The final leakage clustering, obtained when one termination criteria is met
                (either nb_clusters or resolution) or when the algorithm ends if no termination criteria is met.
                In the latter case, the final leakage clustering is one unique cluster that contains every leakage.
            final_secret_clustering: List[List[float]]
                The related secret clustering where each secret value is associated to the leakage
                 in final_leakage_clustering and clustered the same way
            final_secret_public_distrib: List[List[float]],
                The related joint distribution P(K, X) with K being the secret variable and X the public ones
                and clustered the same way
            final_resolution: int
                The value of the resolution termination criteria when the dendrogram algorithm stopped
                (either when one termination criteria was satisfied or when the algorithm reaches its end)
                The parameter resolution allows interrupting the Dendrogram algorithm
                when the distance between the two current leakages is equal or over the resolution
            nb_clusters: int
                The value of the nb_clusters termination criteria when the dendrogram algorithm stopped
                (either when one termination criteria was satisfied or when the algorithm reaches its end)
                The parameter nb_clusters stops the Dendrogram algorithm
                when the current number of clusters is equal or inferior to nb_clusters
            satisfied_criterium: EnumTerminationCriteria
                The satisfied criteria, either resolution, nb_cluster or default.
                It is the name of the criterium that was satisfied when running the Dendrogram algorithm,
                default if none were satisfied.
     """

    def __init__(
            self,
            related_public_tuple: str,
            final_leakage_clustering: List[List[float]],
            final_secret_clustering: List[float],
            final_public_marginal_dist_for_current_public_tuple: List[List[float]],
            final_resolution: float,
            final_nb_clusters: int,
            satisfied_criterium: EnumTerminationCriteria
    ):
        """ Constructor for the Dendrogram class """
        self._related_public_tuple = related_public_tuple
        self._final_leakage_clustering = final_leakage_clustering
        self._final_secret_clustering = final_secret_clustering
        self._final_public_marginal_dist_for_current_public_tuple = final_public_marginal_dist_for_current_public_tuple
        self._final_resolution = final_resolution
        self._final_nb_clusters = final_nb_clusters
        self._satisfied_criterium = satisfied_criterium
        logging.debug("[%s,%s]" % (str(self._final_nb_clusters), str(self._final_resolution)))

    @property
    def related_public_tuple(self) -> str:
        """ Getter for the related_public_tuple attribute """
        return self._related_public_tuple

    @related_public_tuple.setter
    def related_public_tuple(self, related_public_tuple):
        """ Getter for the related_public_tuple attribute """
        self._related_public_tuple = related_public_tuple

    @property
    def final_leakage_clustering(self) -> List[List[float]]:
        """ Getter for the final_leakage_clustering attribute """
        return self._final_leakage_clustering

    @final_leakage_clustering.setter
    def final_leakage_clustering(self, final_leakage_clustering):
        """ Getter for the final_leakage_clustering attribute """
        self._final_leakage_clustering = final_leakage_clustering

    @property
    def final_secret_clustering(self) -> List[float]:
        """ Getter for the final_secret_clustering attribute """
        return self._final_secret_clustering

    @final_secret_clustering.setter
    def final_secret_clustering(self, final_clustering):
        """ Getter for the final_secret_clustering attribute """
        self._final_secret_clustering = final_clustering

    @property
    def final_public_marginal_dist_for_current_public_tuple(self) -> List[List[float]]:
        """ Getter for the final_secret_public_distrib attribute """
        return self._final_public_marginal_dist_for_current_public_tuple

    @final_public_marginal_dist_for_current_public_tuple.setter
    def final_public_marginal_dist_for_current_public_tuple(self, final_secret_public_distrib):
        """ Getter for the final_public_marginal_dist_for_current_public_tuple attribute """
        self._final_public_marginal_dist_for_current_public_tuple = final_secret_public_distrib

    @property
    def final_resolution(self) -> float:
        """ Getter for the final_resolution attribute """
        return self._final_resolution

    @final_resolution.setter
    def final_resolution(self, final_resolution):
        """ Getter for the final_resolution attribute """
        self._final_resolution = final_resolution

    @property
    def final_nb_clusters(self) -> int:
        """ Getter for the final_nb_clusters attribute """
        return self._final_nb_clusters

    @final_nb_clusters.setter
    def final_nb_clusters(self, final_nb_clusters):
        """ Getter for the final_nb_clusters attribute """
        self._final_nb_clusters = final_nb_clusters

    @property
    def satisfied_criterium(self) -> EnumTerminationCriteria:
        """ Getter for the satisfied_criterium attribute """
        return self._satisfied_criterium

    @satisfied_criterium.setter
    def satisfied_criterium(self, satisfied_criterium):
        """ Getter for the satisfied_criterium attribute """
        self._satisfied_criterium = satisfied_criterium
