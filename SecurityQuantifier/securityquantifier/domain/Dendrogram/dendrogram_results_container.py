""" DendrogramsResultsContainer module specifies the DendrogramsResultsContainer class """
from typing import Dict

from securityquantifier.domain.Dendrogram.dendrogram_result import DendrogramResult


class DendrogramsResultsContainer:
    """
        DendrogramsResultsContainer class specifies DendrogramsResultsContainer attributes and methods.
        It contains a Dictionary of DendrogramResult with:
            - Keys being the public tuple for which the dendrogram algorithm computed the clustering.
            - Values is a DendrogramResult instance

        Each DendrogramResult contains:
            - The final leakage clustering, obtained when one termination criteria is met
            (either nb_clusters or resolution) or when the algorithm ends if no termination criteria is met.
            In the latter case, the final leakage clustering is one unique cluster that contains every leakage.
            - the related secret clustering, that is the secret values related to the leakages
            and clustered the same way
            - The related joint distribution P(K, X) with K being the secret variable and X the public ones.
            - The values of the termination criteria when the dendrogram algorithm stopped
            (either when one termination criteria was satisfied or when the algorithm reaches its end)
     """

    def __init__(
            self,
            results: Dict[str, DendrogramResult]
    ):
        """ Constructor for the DendrogramsResultsContainer class """
        self._results = results

    @property
    def results(self) -> Dict[str, DendrogramResult]:
        """ Getter for the results attribute """
        return self._results
