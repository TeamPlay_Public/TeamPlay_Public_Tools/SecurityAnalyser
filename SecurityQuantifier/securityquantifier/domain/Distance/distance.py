""" Distance module defines the generic Distance class """
from abc import ABC
from typing import List

from securityquantifier.interface.adapter.Probe.probe_repository_interface import ProbeRepositoryInterface
from securityquantifier.interface.domain.Distance.distance_interface import DistanceInterface


class Distance(DistanceInterface, ABC):
    """ Distance class defines the generic Distance object """

    @staticmethod
    def compute_distance(leakages1: List[float],
                         leakages2: List[float],
                         is_active_probe: bool,
                         probe: ProbeRepositoryInterface
                         ) -> float:
        """ Abstract method to be implemented to compute the specific distance """
        pass
