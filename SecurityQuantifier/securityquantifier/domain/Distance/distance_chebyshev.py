""" DistanceChebyshev module specifies the Chebyshev distance class """
from typing import List

from securityquantifier.domain.Data.probe_data import ProbeData
from securityquantifier.interface.adapter.Probe.probe_repository_interface import ProbeRepositoryInterface


class DistanceChebyshev:
    """ DistanceChebyshev class specifies the Chebyshev distance attributes and methods """

    @staticmethod
    def compute_distance(leakages1: List[float],
                         leakages2: List[float],
                         is_active_probe: bool,
                         probe: ProbeRepositoryInterface
                         ) -> float:
        """
            Computes the Chebyshev distance between the two list of leakages leakage 1 and leakage2.
            Depending on boolean is_active_probe, two values and their related distance are recorded by the probe

            Parameters
            ----------
            leakages1 : List[float]
                 A list of leakages related to a tainted variable
            leakages2 : List[float]
                 A list of leakages related to another tainted variable
            is_active_probe: bool
                Boolean indicating whether the probe is active
            probe: ProbeInterface
                The Probe in charge of recording the distances

            Returns
            -------
            float
                The Euclidian distance for the two lists of leakages
        """

        if len(leakages1) == 0:
            raise ValueError(" The list of leakages leakages1 has a length equal to zero.")

        if len(leakages2) == 0:
            raise ValueError(" The list of leakages leakages2 has a length equal to zero.")

        if len(leakages1) != len(leakages2):
            raise ValueError(" The two lists of leakages do not have the same length.")

        chebyshev_dist = 0.0

        # Loop over the first list of leakage
        for index, value1 in enumerate(leakages1):
            # Get the leakage at the same index in the second list
            value2 = leakages2[index]

            # Compute the current distance
            current_distance = abs(value1 - value2)

            # Compute the chebyshev distance
            chebyshev_dist = max(chebyshev_dist, current_distance)

            # Probing: When active the probe stores data for further analysis
            if bool(is_active_probe):
                probe.record(ProbeData(str(value1), str(value2), str(current_distance), str(chebyshev_dist) + '\n'))

        return chebyshev_dist
