""" DistanceEuclidian module specifies the Euclidian distance class """
from math import pow
from typing import List

from securityquantifier.domain.Data.probe_data import ProbeData
from sqcommon.exception.distribution_exception import DistributionDataException
from securityquantifier.interface.adapter.Probe.probe_repository_interface import ProbeRepositoryInterface


class DistanceEuclidian:
    """ DistanceEuclidian class specifies the Euclidian distance attributes and methods """

    @staticmethod
    def compute_distance(leakages1: List[float],
                         leakages2: List[float],
                         public_marginal_dist: List[float],
                         exponent: int,
                         is_active_probe: bool,
                         probe: ProbeRepositoryInterface
                         ) -> float:
        """
             Computes the Euclidian distance between the two list of leakages leakage 1 and leakage2.
             Depending on boolean is_active_probe, two values and their related distance are recorded by the probe

             Parameters
             ----------
             leakages1 : List[float]
                  A list of leakages related to a tainted variable
             leakages2 : List[float]
                  A list of leakages related to another tainted variable
             public_marginal_dist: List[float]
                  A list containing the marginal distribution for the public tuples
             exponent: int
                 The exponent to use to compute the distance. For Euclidian-based distance only
             is_active_probe: bool
                 Boolean indicating whether the probe is active
             probe: ProbeInterface
                 The Probe in charge of recording the distances

             Returns
             -------
             float
                 The Euclidian distance for the two lists of leakages
         """

        if len(leakages1) == 0:
            raise ValueError(" The list of leakages leakages1 has a length equal to zero.")

        if len(leakages2) == 0:
            raise ValueError(" The list of leakages leakages2 has a length equal to zero.")

        if len(leakages1) != len(leakages2):
            raise ValueError(" The two lists of leakages do not have the same length.")

        eucl_dist = 0.0

        # If we have an empty public_marginal_dist, raise an exception
        if not public_marginal_dist:
            raise DistributionDataException(
                "The marginal distribution for public and secret does not contain values",
                "Provide a non-uniform distribution file, if none a uniform distribution will be computed")

        # Loop over the first list of leakage
        for index, value1 in enumerate(leakages1):
            # Get the leakage at the same index in the second list
            value2 = leakages2[index]

            # We extract from the public marginal distribution the value related to the current public tuple
            # (or here the current leakage, as both are linked to the same distribution value)
            current_weight = public_marginal_dist[index]

            # We compute the weighted Euclidean distance
            current_distance = pow(abs(value1 - value2), exponent) * current_weight

            eucl_dist += current_distance

        # Average, so we divide per the number of leakages used for the computation of the distance, len(leakages1)
        eucl_dist = eucl_dist ** (1 / exponent)


        # Probing: When active the probe stores data for further analysis
        if bool(is_active_probe):
            probe.record(ProbeData(str(value1), str(value2), str(current_distance), str(eucl_dist) + '\n'))

        return eucl_dist
