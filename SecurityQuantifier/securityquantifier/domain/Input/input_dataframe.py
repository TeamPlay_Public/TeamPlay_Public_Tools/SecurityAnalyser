""" InputCSVDataFrame module specifies an implementation of InputInterface
    in which data from CSV is stored in a DataAsDataFrame
"""
from pathlib import Path

from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.domain.Variable.variables_list import VariablesList
from securityquantifier.interface.domain.Input.input_csv_interface import InputCSVInterface


class InputPathError(Exception):
    """ InputPathError Exception """
    pass


class InputCSVDataFrame(InputCSVInterface):
    """
        InputCSVDataFrame class specifies an implementation of InputInterface
        in which data from CSV is stored in a DataAsDataFrame
    """

    def __init__(
            self,
            path: Path,
            tainted_vars: VariablesList,
            untainted_vars: VariablesList,
            side_channel_class: SideChannelClass,
            data: DataAsDataFrame
    ):
        """ Constructor for the InputDataFrame class """
        self._path = path
        self._tainted_vars = tainted_vars
        self._untainted_vars = untainted_vars
        self._side_channel_class = side_channel_class
        self._data = data

    @property
    def path(self) -> Path:
        """ Getter for the path attribute """
        return self._path

    @path.setter
    def path(self, path):
        """ Setter for the path attribute """
        self._path = path

    @property
    def tainted_vars(self) -> VariablesList:
        """ Getter for the tainted_vars attribute """
        return self._tainted_vars

    @tainted_vars.setter
    def tainted_vars(self, tainted_vars):
        """ Setter for the tainted_vars attribute """
        self._tainted_vars = tainted_vars

    @property
    def untainted_vars(self) -> VariablesList:
        """ Getter for the untainted_vars attribute """
        return self._untainted_vars

    @untainted_vars.setter
    def untainted_vars(self, untainted_vars):
        """ Setter for the untainted_vars attribute """
        self._untainted_vars = untainted_vars

    @property
    def side_channel_class(self) -> SideChannelClass:
        """ Getter for the side_channel_class attribute """
        return self._side_channel_class

    @side_channel_class.setter
    def side_channel_class(self, side_channel_class):
        """ Setter for the side_channel_class attribute """
        self.side_channel_class = side_channel_class

    @property
    def data(self) -> DataAsDataFrame:
        """ Getter for the data attribute """
        return self._data

    @data.setter
    def data(self, data):
        """ Setter for the data attribute """
        self._data = data
