""" InputHDF5 module specifies an implementation of InputInterface
    which stores the HDF5 file in an object
"""
from pathlib import Path

from pandas import DataFrame
from sqcommon.interface.domain.Data.data_interface import DataInterface

from securityquantifier.interface.domain.Input.input_hdf5_interface import InputHDF5Interface


class InputHDF5(InputHDF5Interface, DataInterface):
    """ InputHDF5 class specifies Security Exchange attributes and methods """

    def __init__(
            self,
            path: Path,
            data: DataFrame
    ):
        """ Constructor for the InputHDF5 class """
        self._path = path
        self._data = data

    @property
    def path(self) -> Path:
        """ Getter for the path attribute """
        return self._path

    @path.setter
    def path(self, path):
        """ Setter for the path attribute """
        self._path = path

    @property
    def data(self) -> DataFrame:
        """ Getter for the data attribute """
        return self._data

    @data.setter
    def data(self, data):
        """ Setter for the data attribute """
        self._data = data
