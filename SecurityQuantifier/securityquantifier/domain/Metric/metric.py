""" Metric module defines the generic Metric class """
import logging
from abc import ABC
from typing import List

from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples
from sqcommon.helper.variables_helper import VariableHelper

from securityquantifier.domain.Probe.probe_container import ProbeContainer
from securityquantifier.interface.domain.Distance.distance_interface import DistanceInterface
from securityquantifier.interface.domain.Metric.metric_interface import MetricInterface


class Metric(MetricInterface, ABC):
    """ Metric class defines the generic Metric object """

    def __init__(self,
                 data,
                 distance: DistanceInterface,
                 nb_tainted_vars: int,
                 nb_untainted_vars: int,
                 variables_card: VariablesList,
                 probe_container: ProbeContainer
                 ):
        """ Constructor for the Metric class """
        self._data = data
        self._distance = distance
        self._nb_tainted_vars = nb_tainted_vars
        self._nb_untainted_vars = nb_untainted_vars
        self._variables_card = variables_card
        self._is_active_probe = probe_container.is_active_probe
        self._probe = probe_container.probe
        self._storage = probe_container.storage
        # Extracting needed information about the tuples
        tuples_information = VariableHelper.get_tuples_information(
            self.data.data.columns.tolist(),
            self._nb_tainted_vars,
            self._variables_card)
        self._total_nb_tuples = tuples_information.total_nb_tuples
        self._nb_public_tuples = tuples_information.nb_public_tuples
        self._nb_secret_tuples = tuples_information.nb_secret_tuples

    def compute_metric(self):
        """ Abstract method to compute the required metric """
        pass

    def _get_final_weights(self) -> List[float]:
        logging.info('Metric: Getting final weights.')
        marginal_distribution_for_secret = \
            self._marginal_distributions_as_dict.data[EnumDistributionTuples.SECRET.value]

        # Compute and stock each weight :
        computed_weights = []
        for index1 in range(0, self._nb_secret_tuples - 1, 1):
            for index2 in range(index1 + 1,  self._nb_secret_tuples, 1):
                computed_weights.append(marginal_distribution_for_secret[index1] *
                                        marginal_distribution_for_secret[index2])
        # Sum up the computed weights
        sum_computed_weight = sum(computed_weights)
        # Divide each computed weights by the previous sum

        final_weights = [i / sum_computed_weight for i in computed_weights]

        return final_weights

    @property
    def data(self):
        """ Getter for the data attribute """
        return self._data

    @data.setter
    def data(self, data):
        """" Setter for the data attribute """
        self._data = data

    @property
    def distance(self):
        """ Getter for the distance attribute """
        return self._distance

    @distance.setter
    def distance(self, distance):
        """" Setter for the distance attribute """
        self._distance = distance
        
    @property
    def nb_tainted_vars(self):
        """ Getter for the nb_tainted_vars attribute """
        return self._nb_tainted_vars

    @nb_tainted_vars.setter
    def nb_tainted_vars(self, nb_tainted_vars):
        """ Setter for the nb_tainted_vars attribute """
        self._nb_tainted_vars = nb_tainted_vars

    @property
    def nb_untainted_vars(self):
        """ Getter for the nb_untainted_vars attribute """
        return self._nb_untainted_vars

    @nb_untainted_vars.setter
    def nb_untainted_vars(self, nb_untainted_vars):
        """ Setter for the nb_untainted_vars attribute """
        self._nb_untainted_vars = nb_untainted_vars

    @property
    def variables_card(self):
        """ Getter for the variables_card attribute """
        return self._variables_card

    @variables_card.setter
    def variables_card(self, variables_card):
        """" Setter for the variables_card attribute """
        self._variables_card = variables_card

    @property
    def total_nb_tuples(self):
        """ Getter for the total_nb_tuples attribute """
        return self._total_nb_tuples

    @total_nb_tuples.setter
    def total_nb_tuples(self, total_nb_tuples):
        """" Setter for the total_nb_tuples attribute """
        self._total_nb_tuples = total_nb_tuples

    @property
    def nb_public_tuples(self):
        """ Getter for the nb_public_tuples attribute """
        return self._nb_public_tuples

    @nb_public_tuples.setter
    def nb_public_tuples(self, nb_public_tuples):
        """" Setter for the nb_public_tuples attribute """
        self._nb_public_tuples = nb_public_tuples
        
    @property
    def nb_secret_tuples(self):
        """ Getter for the nb_secret_tuples attribute """
        return self._nb_secret_tuples

    @nb_secret_tuples.setter
    def nb_secret_tuples(self, nb_secret_tuples):
        """" Setter for the nb_secret_tuples attribute """
        self._nb_secret_tuples = nb_secret_tuples

    @property
    def is_active_probe(self):
        """ Getter for the is_active_probe attribute """
        return self._is_active_probe

    @is_active_probe.setter
    def is_active_probe(self, is_active_probe):
        """" Setter for the is_active_probe attribute """
        self._is_active_probe = is_active_probe

    @property
    def probe(self):
        """ Getter for the probe attribute """
        return self._probe

    @probe.setter
    def probe(self, probe):
        """" Setter for the probe attribute """
        self._probe = probe

    @property
    def storage(self):
        """ Getter for the storage attribute """
        return self._storage

    @storage.setter
    def storage(self, storage):
        """" Setter for the storage attribute """
        self._storage = storage
