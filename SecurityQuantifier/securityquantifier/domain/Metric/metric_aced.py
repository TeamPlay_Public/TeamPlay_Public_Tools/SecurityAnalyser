"""" MetricACED module specifies the ACED Metric class """
import logging
import math

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples

from sqcommon.exception.distribution_exception import DistributionDataException
from sqcommon.exception.final_weights_not_equal_to_one_exception import FinalWeightsNotEqualToOneException

from securityquantifier.domain.Metric.metric import Metric
from securityquantifier.domain.Probe.probe_container import ProbeContainer

from securityquantifier.interface.domain.Distance.distance_interface import DistanceInterface


class MetricACED(Metric):
    """" MetricACED class specifies the ACED Metric attributes and methods """

    def __init__(self,
                 data: DataAsDataFrame,
                 marginal_distributions_as_dict: DistributionAsDict,
                 distance: DistanceInterface,
                 nb_tainted_vars: int,
                 nb_untainted_vars: int,
                 variables_card: VariablesList,
                 exponent: int,
                 probe_container: ProbeContainer
                 ):
        """ Constructor for the MetricACED class """

        self._marginal_distributions_as_dict = marginal_distributions_as_dict
        self._exponent = exponent

        super().__init__(data=data,
                         distance=distance,
                         nb_tainted_vars=nb_tainted_vars,
                         nb_untainted_vars=nb_untainted_vars,
                         variables_card=variables_card,
                         probe_container=probe_container)

    def compute_metric(self):
        """
        Computes the Average Case Time Distinguishability (ACED) associated with a Security Exchange Input

        Returns
        -------
        float
            The computed ACED metric for the given data and distribution
        """
        aced = 0.0

        # Counter storing how many distances are computed
        distances_counter = 0

        logging.info('MetricACED: Computing the metric ACED')
        # Check that we have the two marginal distributions.
        # If so, we compute the uniform weights:
        if not self._marginal_distributions_as_dict.data[EnumDistributionTuples.PUBLIC.value]:
            raise DistributionDataException(
                "The marginal distribution for the public variable does not contain any value",
                "Provide a joint distribution file using arguments -obd and -bd together. Otherwise use -ud to "
                "request a uniform  distribution")
        elif not self._marginal_distributions_as_dict.data[EnumDistributionTuples.SECRET.value]:
            raise DistributionDataException(
                "The marginal distribution for the secret variable does not contain any value",
                "Provide a joint distribution file using arguments -obd and -bd together. Otherwise use -ud to "
                "request a uniform  distribution")
        else:
            # Get the final weights from the marginal distributions for the secret variable
            final_weights = self._get_final_weights()

            if not math.isclose(sum(final_weights), 1, rel_tol=0.0000001):
                raise FinalWeightsNotEqualToOneException("The sum of the computed final weights is not equal to 1",
                                                         "Check that the provided joint distribution is correct")

            # We get the marginal distributions for public
            public_marginal_distribution_list = \
                self._marginal_distributions_as_dict.data[EnumDistributionTuples.PUBLIC.value]
            # and initialize the counter used to scroll the marginal distributions for secret
            dist_counter = 0

        logging.info('MetricACED: Computing Euclidian distances')
        # First loop gets a secret tuple with its related public tuples
        for index1 in range(0, self._total_nb_tuples - self._nb_public_tuples - 1, self._nb_public_tuples):
            # Second loop gets another secret tuple with its related public tuples
            for index2 in range(index1 + self._nb_public_tuples, self._total_nb_tuples - self._nb_public_tuples + 1,
                                self._nb_public_tuples):
                # Each series contains nb_untainted_tuples rows, so we get the data between indexX and
                # indexX+nb_untainted_tuples

                # We extract the current distribution
                current_distrib = final_weights[dist_counter]

                aced += self._distance.compute_distance(
                    # List of leakages for one given secret variable
                    leakages1=self._data.data.iloc[index1:index1 + self._nb_public_tuples, -1].tolist(),
                    # List of leakages for another given secret variable
                    leakages2=self._data.data.iloc[index2:index2 + self._nb_public_tuples, -1].tolist(),
                    public_marginal_dist=public_marginal_distribution_list,
                    exponent=self._exponent,
                    is_active_probe=self._is_active_probe,
                    probe=self._probe) * current_distrib

                # Next index in the final_weights
                dist_counter += 1

        if bool(self._is_active_probe):
            logging.info('MetricACED: Probe is active. Saving data')
            self._probe.store(data=self._probe.recorder,
                              storage=self._storage)

        return aced
