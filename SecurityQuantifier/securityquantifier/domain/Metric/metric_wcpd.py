""" MetricWCPD module specifies the WCPD Metric class """
from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.domain.Variable.variables_list import VariablesList

from securityquantifier.domain.Distance.distance_chebyshev import DistanceChebyshev
from securityquantifier.domain.Metric.metric import Metric
from securityquantifier.domain.Probe.probe_container import ProbeContainer


class MetricWCPD(Metric):
    """ MetricWCPD class specifies the WCPD Metric attributes and methods """

    def __init__(self,
                 data: DataAsDataFrame,
                 distance: DistanceChebyshev,
                 nb_tainted_vars: int,
                 nb_untainted_vars: int,
                 variables_card: VariablesList,
                 probe_container: ProbeContainer
                 ):
        """ Constructor for the MetricWCPD class """
        self._variables_card = variables_card
        super().__init__(data=data,
                         distance=distance,
                         nb_tainted_vars=nb_tainted_vars,
                         nb_untainted_vars=nb_untainted_vars,
                         variables_card=variables_card,
                         probe_container=probe_container)

    def compute_metric(self) -> float:
        """
        Computes the Worst Case Time Distinguishability (WCPD) associated with a Security Exchange Input

        Returns
        -------
        float
            The WCPD associated with data
        """

        wcpd = 0.0

        # Get all the tuples for one given tainted variable
        for index1 in range(0, self._total_nb_tuples - self._nb_public_tuples - 1, self._nb_public_tuples):
            # Get all the tuples for another given tainted variable
            for index2 in range(index1 + self._nb_public_tuples, self._total_nb_tuples - self._nb_public_tuples + 1, self._nb_public_tuples):
                # Measuring the Chebyshev distance between the leakages associated to those two tainted variables
                wcpd = max(wcpd, self._distance.compute_distance(
                    # List of leakages for one given tainted variable
                    leakages1=self._data.data.iloc[index1:index1 + self._nb_public_tuples, -1].tolist(),
                    # List of leakages for another given tainted variable
                    leakages2=self._data.data.iloc[index2:index2 + self._nb_public_tuples, -1].tolist(),
                    is_active_probe=self._is_active_probe,
                    probe=self._probe
                ))

        if bool(self._is_active_probe):
            self._probe.store(data=self._probe.recorder,
                              storage=self._storage)

        return wcpd
