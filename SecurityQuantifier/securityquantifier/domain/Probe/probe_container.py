"""
    Module defining the ProbeContainer, a simple container gathering Probe-related classes
"""

from securityquantifier.interface.adapter.Probe.probe_repository_interface import ProbeRepositoryInterface
from securityquantifier.interface.domain.Storage.storage_interface import StorageInterface


class ProbeContainer:
    """ ProbeContainer class defines a simple container gathering Probe-related classes

        Attribute
        ----------
            is_active_probe: bool
                Boolean to know whether the probe is active

            probe: ProbeRepositoryInterface
                The probe, to save data in a storage support

            storage: StorageInterface
                Information related to the storage support
    """

    def __init__(
            self,
            active_probe: bool,
            probe: ProbeRepositoryInterface,
            storage: StorageInterface):
        """ Constructor for the ProbeContainer class """
        self._is_active_probe = active_probe
        self._probe = probe
        self._storage = storage

    @property
    def is_active_probe(self) -> bool:
        """ Getter for the is_active_probe attribute """
        return self._is_active_probe

    @is_active_probe.setter
    def is_active_probe(self, is_active_probe):
        """ Setter for the is_active_probe attribute """
        self._is_active_probe = is_active_probe

    @property
    def probe(self) -> ProbeRepositoryInterface:
        """ Getter for the probe attribute """
        return self._probe

    @probe.setter
    def probe(self, probe):
        """ Setter for the probe attribute """
        self._probe = probe

    @property
    def storage(self) -> StorageInterface:
        """ Getter for the storage attribute """
        return self._storage

    @storage.setter
    def storage(self, storage):
        """ Setter for the storage attribute """
        self._storage = storage
        
    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
