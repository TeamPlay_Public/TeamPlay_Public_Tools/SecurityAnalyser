"""
    DatabaseStorage module specifies an implementation of StorageInterface
    which encapsulates data related to the storage of the output in a database
"""
from securityquantifier.interface.domain.Storage.storage_interface import StorageInterface


class DatabaseStorage(StorageInterface):
    """
        DatabaseStorage class encapsulates data related to the storage of the output in a database

        Attribute
        ----------
        name: str
            name of the database where to store the output of the program
    """

    def __init__(
        self,
        name: str,
        user: str,
        passwd: str,
        host: str,
        port: int
    ):
        """ Constructor for the DatabaseStorage class """
        self._name = name
        self._user = user
        self._passwd = passwd
        self._host = host
        self._port = port

    @property
    def name(self) -> str:
        """ Getter for the name attribute """
        return self._name

    @name.setter
    def name(self, name):
        """ Setter for the name attribute """
        self._name = name
        
    @property
    def user(self) -> str:
        """ Getter for the user attribute """
        return self._user

    @user.setter
    def user(self, user):
        """ Setter for the user attribute """
        self._user = user
        
    @property
    def passwd(self) -> str:
        """ Getter for the passwd attribute """
        return self._passwd

    @passwd.setter
    def passwd(self, passwd):
        """ Setter for the passwd attribute """
        self._passwd = passwd
        
    @property
    def host(self) -> str:
        """ Getter for the host attribute """
        return self._host

    @host.setter
    def host(self, host):
        """ Setter for the host attribute """
        self._host = host
        
    @property
    def port(self) -> int:
        """ Getter for the port attribute """
        return self._port

    @port.setter
    def port(self, port):
        """ Setter for the port attribute """
        self._port = port
