"""
    TextFileStorage module specifies an implementation of StorageInterface
    which encapsulates data related to the storage of the output as a text file
"""
import os
from pathlib import Path

from securityquantifier.interface.domain.Storage.storage_interface import StorageInterface


class TextFileStorage(StorageInterface):
    """
        TextFileStorage class encapsulates data related to the storage of the output as a text file

        Attribute
        ----------
        name: str
            name of the text file where to store the output of the program
    """

    def __init__(
        self,
        name: Path
    ):
        """ Constructor for the TextFileStorage class """
        # Empty the file if it already exists or create it otherwise
        open(name, "w").close
        self._name = name

    @property
    def name(self) -> Path:
        """ Getter for the name attribute """
        return self._name

    @name.setter
    def name(self, name):
        """ Setter for the name attribute """
        self._name = name
