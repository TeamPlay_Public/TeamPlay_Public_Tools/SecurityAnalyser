""" TaintedVariable module specifies the Tainted Variable class """
from sqcommon.enums.enum_var_types import VariableTypes

from sqcommon.domain.Variable.variable import Variable


class TaintedVariable(Variable):
    """ TaintedVariable class specifies Tainted Variable attributes and methods """

    def __init__(
            self,
            value: str
    ):
        """ Constructor for the TaintedVariable class """
        self._var_type = VariableTypes.TAINTED
        super().__init__(value, 0)

    @property
    def value(self):
        """ Getter for the value attribute """
        return self._value

    @value.setter
    def value(self, value):
        """ Setter for the value attribute """
        self._value = value

    @property
    def var_type(self):
        """ Getter for the var_type attribute """
        return self._var_type
