""" TypedVariablesList module specifies a list of typed VariableList """
from sqcommon.enums.enum_var_types import VariableTypes

from sqcommon.domain.Variable.variables_list import VariablesList


class TypedVariablesList(VariablesList):
    """ TypedVariablesList class specifies attributes and methods for a list of VariableInterface """

    def __init__(self,
                 vtype: VariableTypes
                 ):
        """ Constructor for the VariablesList class
            vtype: type of the Variable, either Tainted or Untainted
        """
        self._vtype = vtype
        super().__init__()

    @property
    def vtype(self):
        """ Getter for the vtype attribute """
        return self._vtype

    @vtype.setter
    def vtype(self, vtype):
        """ Setter for the vtype attribute """
        self._vtype = vtype
