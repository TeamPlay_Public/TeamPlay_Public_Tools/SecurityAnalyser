""" UntaintedVariable module specifies the Untainted Variable class """
from sqcommon.enums.enum_var_types import VariableTypes

from sqcommon.domain.Variable.variable import Variable


class UntaintedVariable(Variable):
    """ UntaintedVariable class specifies Untainted Variable attributes and methods """

    def __init__(
            self,
            value: str
    ):
        """ Constructor for the UntaintedVariable class """
        self._var_type = VariableTypes.UNTAINTED
        super().__init__(value=value,
                         cardinality=0)

    @property
    def value(self):
        """ Getter for the value attribute """
        return self._value

    @value.setter
    def value(self, value):
        """ Setter for the value attribute """
        self._value = value

    @property
    def var_type(self):
        """ Getter for the var_type attribute """
        return self._var_type
