"""
    EnumJointMarginalProbability class enumerates the different joint and marginal probability symbols
    used as keys in the MarginalDistributionJSONGetUseCase attribute dist_as_dict   """
from enum import Enum


class EnumJointMarginalProbability(Enum):
    """
        EnumJointMarginalProbability class enumerates the different joint and marginal probability symbols
        used as keys in the MarginalDistributionJSONGetUseCase attribute dist_as_dict
        For each tuple a name and a value is provided.
    """
    JOINT_PROBA_PUBLIC_SECRET = 'P(SECRET_PUBLIC)'
    MARGINAL_PROBA_SECRET = 'P(SECRET)'
    MARGINAL_PROBA_PUBLIC = 'P(PUBLIC)'
