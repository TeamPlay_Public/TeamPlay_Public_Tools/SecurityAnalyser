""" EnumTerminationCriteria class enumerates the different termination criteria used in the dendrogram algorithm  """
from enum import Enum


class EnumTerminationCriteria(Enum):
    """
        EnumTerminationCriteria class defines the different termination criteria used in the dendrogram algorithm .
        For each tuple a name and a value is provided.
    """
    NB_CLUSTERS = 'nb_clusters'
    RESOLUTION = 'resolution'
    DEFAULT = 'default'
