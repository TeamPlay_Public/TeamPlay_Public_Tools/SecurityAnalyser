""" DataframeHelper module defines the tools to deal with Pandas DataFrame """
from typing import List

from pandas import DataFrame


class DataframeHelper:
    """ DataframeHelper class defines tools to deal with Pandas DataFrame """

    @staticmethod
    def find_row_with_tuple(data: DataFrame,
                            nb_tainted_var: int,
                            k: List):
        """
        Finds rows containing tuple k

        Parameters
        ----------
        data : DataFrame
            The DataFrame representing the Security Exchange File
        nb_tainted_var : int
            The number of tainted_var parameters in the Security Exchange File
        k : list
            The tuple to find

        Returns
        -------
        List[int]
            The list of indexes of lines containing the tuple k
        """
        indexes = []

        for index, row in data.iterrows():
            b_contain_tuple = True
            for i in range(nb_tainted_var):
                b_contain_tuple &= row[data.columns[i]] == k[i]
            if b_contain_tuple:
                indexes.append(index)
        return indexes
