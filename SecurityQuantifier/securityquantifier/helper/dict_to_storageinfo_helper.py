""" DictToStorageInfoHelper module defines the tools to initialize a StorageInformation object """
from pathlib import Path

from securityquantifier.domain.Storage.Database.database_storage import DatabaseStorage
from securityquantifier.domain.Storage.TextFile.text_file_storage import TextFileStorage


class DictToStorageInfoHelper:
    """ DictToStorageInfoHelper class defines tools to initialize a DatabaseStorage object """

    @staticmethod
    def dict_to_databasestorage(data: dict):
        """
        Instantiate a StorageInformation object from a dict

        Parameters
        ----------
        data : dict
            Dictionary containing the data necessary to instantiate a StorageInformation object

        Returns
        -------
        StorageInformation
            The instantiated StorageInformation object
        """

        return DatabaseStorage(name=data["db_name"],
                               user=data["db_user"],
                               passwd=data["db_user_passwd"],
                               host=data["db_host"],
                               port=data["db_port"])

    @staticmethod
    def dict_to_filestorage(data: dict):
        """
        Instantiate a TextFileStorage object using the data parameter

        Parameters
        ----------
        data :
            Dictionary containing the data necessary to instantiate a TextFileStorage object

        Returns
        -------
        TextFileStorage
            The instantiated TextFileStorage object
        """

        return TextFileStorage(name=Path(data))
