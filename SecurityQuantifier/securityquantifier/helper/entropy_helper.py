""" DataframeHelper module defines the tools to compute entropy and related concepts """
import logging
import math
import warnings
from math import log
from typing import List

from sqcommon.exception.distribution_exception import DistributionDataException


class EntropyHelper:
    """ EntropyHelper class defines tools to deal with Pandas DataFrame """

    @staticmethod
    def entropy(nb_secret_tuples: int,
                distribution_var: List[float]) -> float:
        """
            Returns the entropy related to a random variable using:
            https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.entropy.html

            Parameters
            ----------
            nb_secret_tuples: int
                The number of secret tuples found in the input of the Security Quantifier.
                Used to check that the computed entropy value is correct
            distribution_var : List[float]
                 the distribution of the random variable for which we want to compute the entropy

            Returns
            -------
            float
                The entropy related to the random variable
        """
        if len(distribution_var) == 0:
            raise DistributionDataException("entropy_shannon: empty distribution",
                                            "A not-empty distribution must be provided to compute the entropy ")

        hk = sum([proba * math.log(1 / proba, 2) for proba in distribution_var])

        if hk >= log(nb_secret_tuples, 2):
            warnings.warn("H(K) should be lower than Log2(card(secret_tuple)). This is not the case.")
            warnings.warn("H(K) =" + str(hk) + " and Log2(card(secret_tuple))=" + str(log(nb_secret_tuples, 2)))

        logging.info('Computed entropy : %s' % hk)

        return hk
