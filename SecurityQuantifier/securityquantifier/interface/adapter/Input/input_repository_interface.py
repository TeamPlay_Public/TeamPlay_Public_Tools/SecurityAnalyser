""" Module defining the interface for InputRepository.
    Interface to be implemented for a class to behave as a InputRepository
"""
import abc
from pathlib import Path

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.domain.Variable.variables_list import VariablesList

from securityquantifier.interface.domain.Input.input_csv_interface import InputCSVInterface


class InputRepositoryInterface(metaclass=abc.ABCMeta):
    """ InputRepositoryInterface interface defines the behavior of a InputRepository """

    @staticmethod
    @abc.abstractmethod
    def find_by_path(input_path: Path,
                     tainted_vars: VariablesList,
                     untainted_vars: VariablesList,
                     side_channel_class: SideChannelClass,
                     data=DataAsDataFrame) -> InputCSVInterface:
        """
            find_by_path(...) retrieves the input using its path
        """
        pass
