""" Module defining the interface for ProbeRepository.
    Interface to be implemented for a class to behave as a ProbeRepository
"""
import abc

from securityquantifier.domain.Data.probe_data import ProbeData
from securityquantifier.interface.domain.Storage.storage_interface import StorageInterface


class ProbeRepositoryInterface(metaclass=abc.ABCMeta):
    """ ProbeRepositoryInterface interface defines the behavior of a ProbeRepository """

    @staticmethod
    @abc.abstractmethod
    def store(data: ProbeData,
              storage: StorageInterface
              ):
        """
            store(...) saves the values and their respective distance
        """
        pass

    @staticmethod
    @abc.abstractmethod
    def record(data: ProbeData):
        """
             record(...) buffers the data in a Python data structure, before storing it using the store(...) method
        """
        pass
