"""
    VariablesListRepositoryInterface module defines the interface
    to be implemented to act as a VariablesListRepository
"""
import abc
from pathlib import Path

from sqcommon.enums.enum_var_types import VariableTypes
from sqcommon.interface.domain.Variable.variables_list_interface import VariablesListInterface


class VariablesListRepositoryInterface(metaclass=abc.ABCMeta):
    """ VariablesListRepositoryInterface class defines a method
        find_by_type(var_type: VariableTypes, input_path: str) -> VariablesListInterface
        to be implemented to act as a VariablesListRepository
    """

    @staticmethod
    @abc.abstractmethod
    def find_by_type(var_type: VariableTypes, input_path: Path) -> VariablesListInterface:
        """
            Retrieves a list of variables of type VariablesList
        """
        pass
