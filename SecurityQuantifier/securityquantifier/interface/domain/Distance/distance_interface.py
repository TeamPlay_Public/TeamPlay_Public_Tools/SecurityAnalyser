"""
    Interface to be implemented by class to behave as Distance
"""
import abc
from typing import List

from securityquantifier.interface.adapter.Probe.probe_repository_interface import ProbeRepositoryInterface


class DistanceInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as Distance
   """

    @staticmethod
    def compute_distance(leakages1: List[float],
                         leakages2: List[float],
                         public_marginal_dist: List[float],
                         exponent: int,
                         is_active_probe: bool,
                         probe: ProbeRepositoryInterface
                         ) -> float:
        """ Interface static method to be implemented to behave as a Distance """
        pass
