"""
    Interface to be implemented by class to behave as Input
"""
import abc

from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.interface.domain.Data.data_interface import DataInterface


class InputCSVInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as Input
   """

    @property
    @abc.abstractmethod
    def path(self) -> str:
        """
            Getter for the path attribute
        """
        pass

    @property
    @abc.abstractmethod
    def tainted_vars(self) -> VariablesList:
        """
            Getter for the tainted_vars attribute
        """
        pass

    @property
    @abc.abstractmethod
    def untainted_vars(self) -> VariablesList:
        """
            Getter for the untainted_vars attribute
        """
        pass

    @property
    @abc.abstractmethod
    def side_channel_class(self) -> SideChannelClass:
        """
            Getter for the side_channel_class attribute
        """
        pass

    @property
    @abc.abstractmethod
    def data(self) -> DataInterface:
        """
            Getter for the data attribute
        """
        pass
