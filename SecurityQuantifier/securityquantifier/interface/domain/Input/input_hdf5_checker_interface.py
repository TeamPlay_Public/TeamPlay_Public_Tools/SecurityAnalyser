"""
    Interface to be implemented by class to behave as InputHDF5Checker
"""
import abc


class InputHDF5CheckerInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as InputHDF5Checker
   """

    @property
    @abc.abstractmethod
    def check_sidechannelclass_exists(self) -> str:
        """
            Check the existence of the side channel class in the HDF5 file
        """
        pass

    @property
    @abc.abstractmethod
    def check_attributes(self) -> str:
        """
            Check the existence of HDF5 attributes in the file
        """
        pass
