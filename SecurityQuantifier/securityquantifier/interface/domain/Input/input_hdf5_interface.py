"""
    Interface to be implemented by class to behave as Input
"""
import abc


class InputHDF5Interface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as Input
   """

    @property
    @abc.abstractmethod
    def path(self) -> str:
        """
            Getter for the path attribute to implement by classes referring to this interface
        """
        pass
