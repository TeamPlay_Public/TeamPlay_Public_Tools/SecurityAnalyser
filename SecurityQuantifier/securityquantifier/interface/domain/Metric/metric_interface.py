"""
    Interface to be implemented by class to behave as Metric
"""
import abc

from securityquantifier.interface.domain.Distance.distance_interface import DistanceInterface


class MetricInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as Metric
   """

    @property
    @abc.abstractmethod
    def data(self):
        """ Getter for the data attribute """
        pass

    @property
    @abc.abstractmethod
    def distance(self) -> DistanceInterface:
        """ Getter for the distance attribute """
        pass
