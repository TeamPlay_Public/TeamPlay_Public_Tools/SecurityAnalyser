"""
    Interface to be implemented by class to behave as SecurityLevel object
"""
import abc


class SecurityLevelInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as SecurityLevel object
   """

    @property
    @abc.abstractmethod
    def annotation(self) -> str:
        """ Getter for the annotation attribute """
        pass

    @property
    @abc.abstractmethod
    def level(self) -> int:
        """ Getter for the level attribute """
        pass
