"""
    Interface to be implemented by class to behave as Storage
"""
import abc


class StorageInterface(metaclass=abc.ABCMeta):
    """
        Interface to be implemented by class to behave as Storage

        Attribute
        ----------
        name: str
            name of the storage (file or database) where to store the output of the program
    """

    @property
    @abc.abstractmethod
    def name(self):
        """ Getter for the name attribute """
        pass
