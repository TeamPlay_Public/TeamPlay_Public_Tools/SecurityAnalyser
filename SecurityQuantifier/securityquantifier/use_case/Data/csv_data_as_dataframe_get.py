""" Module defining the use cases related to Data as a Dataframe """
from pathlib import Path

from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from sqcommon.interface.adapter.Data.data_repository_interface import DataRepositoryInterface


class CSVDataAsDataFrameGetUC(UseCaseInterface):
    """ CSVDataAsDataFrameGetUC class retrieves the data as a Panda DataFrame """

    def __init__(self,
                 vars_list_repo: DataRepositoryInterface,
                 input_path: Path):
        """ Constructor for the DataAsDataFrameGetUC class """
        self._repository = vars_list_repo
        self._input_path = input_path

    def execute(self):
        """ The execute() method calls the adapter to get the data as a Panda DataFrame """
        return self._repository.find(self._input_path)

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
