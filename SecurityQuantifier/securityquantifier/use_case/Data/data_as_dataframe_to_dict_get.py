""" Module defining the use cases related to SEFileAsDict for an input of type DataAsDataframe """

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface


class DataAsDataFrameToDictGetUC(UseCaseInterface):
    """
        DataAsDataFrameToDictGetUC class converts a DataAsDataframe object into a Dict,

     """

    def __init__(self,
                 data_as_dataframe: DataAsDataFrame,
                 nb_tainted_vars: int,
                 nb_untainted_vars: int
                 ):
        """ Constructor for the DataAsDataFrameGetUC class """
        self._data_as_dataframe = data_as_dataframe
        self._nb_tainted_vars = nb_tainted_vars
        self._nb_untainted_vars = nb_untainted_vars

    def execute(self):
        """ The execute() method converts a DataAsDataframe object into a Dict
            with keys being the public tuples 'a,n'
            and values being a list containing the couple [secret, leakage]
         """

        # Create the returned dict
        data_dict = {}

        # Scroll the Dataframe
        # iloc[rows, columns]
        # select all public tuples columns
        public_tuples = self._data_as_dataframe.data.iloc[:, self._nb_tainted_vars:self._nb_tainted_vars+self._nb_untainted_vars]
        # Extract the public tuples as a string 'a,n'
        # Extract the related secret, leakage
        # Put the secret,leakage in a list: [secret,leakage]

        # if NOT public tuples exists in the Dict
        # Create the empty List that will contain the secret, leakage lists
        # Add the [secret,leakage] to the list for key 'a,n'

        return "todo"

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
