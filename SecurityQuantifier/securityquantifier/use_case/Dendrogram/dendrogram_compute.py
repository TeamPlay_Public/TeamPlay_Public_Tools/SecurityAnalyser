""" Module defining the use cases related to the Dendrogram algorithm """
from typing import List
from copy import deepcopy

from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.domain.Dendrogram.dendrogram import Dendrogram
from securityquantifier.domain.Dendrogram.dendrogram_result import DendrogramResult
from securityquantifier.enums.enum_termination_criteria import EnumTerminationCriteria


class DendrogramUseCase(UseCaseInterface):
    """
        DendrogramUseCase class runs the Dendrogram algorithm on the leakage data.

        At each step we merge the closest clusters two by two.
        We proceed until one termination criteria are met.

        If none are met the algorithm will run until every leakage are gathered in one unique cluster.

        We return the one final clustering as DendrogramResult.
    """
    _dendrogram_result = None
    _resolution_criteria_met = False
    _nb_clusters_criteria_met = False

    def __init__(self,
                 dendrogram: Dendrogram):
        """ Constructor for the DendrogramUseCase class """
        self._dendrogram = dendrogram

    def execute(self) -> DendrogramResult:
        """
            The execute() method computes the dendrogram related to the initial cluster,
            given the two constraints resolution and number of clusters
            and return a DendrogramsResult
        """

        # We should have a list of distances that is not empty
        if len(self._dendrogram.initial_distances()) == 0:
            raise ValueError("The list of distances extracted when running the Dendrogram algorithm is empty",
                             "No clustering can be made out of the Security Exchange file data")

        final_nb_clusters = 0

        for dist in self._dendrogram.distances():

            # Get the length of the current clustering
            clusters_length = len(self._dendrogram.initial_leakage_clustering())

            # First check whether we meet the termination criterium
            one_criterium_is_met = self._check_termination_criterium(dist=dist,
                                                                     clusters_length=clusters_length)

            # Get the related indices of the current distance in the initial distances list
            related_ind = _related_indices(dist, self._dendrogram.initial_distances())

            # the length of related_ind gives us how many distances
            # there are for dist (dist in self._dendrogram.distances())
            nb_distances_for_current_distance_value = len(related_ind)

            # Check that at the end of this loop iteration we would not reach a number of clusters
            # lower than nb_clusters (only if  resolution was not provided).
            # If it would be the case then we do not enter the loop.
            # We initialize for the case when the current distance is equal to 0, in that case the leakages
            # should end up in the same cluster, so we authorize the entry into the loop.
            nb_clusters_after_loop_iteration = clusters_length - nb_distances_for_current_distance_value
            if not(self._dendrogram.given_resolution is None
                    and nb_clusters_after_loop_iteration < self._dendrogram.given_nb_clusters
                    and dist != 0):
                if one_criterium_is_met:
                    break
                else:
                    related_ind = _related_indices(dist, self._dendrogram.initial_distances())
                    # Using the list of indices, merge clusters
                    final_nb_clusters = self._merge_clusters(related_ind)
                    # Remove the used distance from self.dendrogram.initial_distances
                    self._delete_used_distances(related_ind)

        # If no termination criterium is reached, we record the last state of the cluster
        # into the dictionary at key EnumTerminationCriteria.DEFAULT
        if not one_criterium_is_met:
            self._dendrogram_result = self._create_dendrogram_result(
                satisfied_criterium=EnumTerminationCriteria.DEFAULT,
                final_nb_clusters=final_nb_clusters,
                final_resolution=dist
            )

        return self._dendrogram_result

    def _merge_clusters(self,
                        indices: List[int]) -> int:
        """
            Using the list of indices, merge clusters in self._initial_clustering
            with indices i and i+1.
            Return the number of clusters obtained at the end of the function
        """
        # For every index i in indices, merge C(i+1) and C(i)
        # with C=self._initial_clustering
        for index in reversed(indices):
            # We merged the two clusters (at indices i and i+1) in i
            self.dendrogram.initial_leakage_clustering()[index] += \
                self.dendrogram.initial_leakage_clustering()[index + 1]
            # We do the same for the related secret clusters
            self.dendrogram.related_secret_tuples()[index] += self.dendrogram.related_secret_tuples()[index + 1]
            # We do the same for the secret distribution
            self.dendrogram.secret_public_joint_dist_for_current_public_tuple()[index] += \
                self.dendrogram.secret_public_joint_dist_for_current_public_tuple()[
                    index + 1]
            # And remove the unneeded cluster at i+1
            del self.dendrogram.initial_leakage_clustering()[index + 1]
            del self.dendrogram.related_secret_tuples()[index + 1]
            del self.dendrogram.secret_public_joint_dist_for_current_public_tuple()[index + 1]

        return len(self.dendrogram.initial_leakage_clustering())

    def _check_termination_criterium(self,
                                     dist: float,
                                     clusters_length: int):
        """
            Check whether one of the termination criteria are satisfied.
            Loop must end as soon as one criterium is met, ie either if:
                - the number of clusters is below a certain given value
                - the distances below a resolution criteria are dealt with
            When one of these two criteria is met, we record the current cluster in the related Object,
            together with the final values of resolution and nb_clusters
            and the termination criterium value
        """
        if self._dendrogram.given_resolution is not None:
            if dist >= self._dendrogram.given_resolution and not self.resolution_criteria_met():
                self._dendrogram_result = self._create_dendrogram_result(
                    satisfied_criterium=EnumTerminationCriteria.RESOLUTION,
                    final_nb_clusters=clusters_length,
                    final_resolution=dist)
                self.set_resolution_criteria_met_to_true()
        else:
            if clusters_length <= self._dendrogram.given_nb_clusters and not self.nb_clusters_criteria_met():
                self._dendrogram_result = self._create_dendrogram_result(
                    satisfied_criterium=EnumTerminationCriteria.NB_CLUSTERS,
                    final_nb_clusters=clusters_length,
                    final_resolution=dist)
                self.set_nb_clusters_criteria_met_to_true()

        return self.resolution_criteria_met() or self.nb_clusters_criteria_met()

    def _delete_used_distances(self,
                               indices: List[int]):
        """
            Delete from self._initial_distances the elements at the indices in parameter
        """
        for index in reversed(indices):
            del self.dendrogram.initial_distances()[index]

    def _create_dendrogram_result(self,
                                  satisfied_criterium: EnumTerminationCriteria,
                                  final_nb_clusters: int,
                                  final_resolution: float
                                  ):
        """
            Record :
                - the final leakage clustering
                - the final resolution value
                - the final nb_clusters value
                - the final_secret_clustering
                - the final_public_marginal_dist_for_current_public_tuple
            into a DendrogramResult then store this DendrogramResult in the DendrogramReturnedContainer
            with key being the public tuple for which all values used in the current Dendrogram algorithm run
             are related
        """
        return DendrogramResult(related_public_tuple=self.dendrogram.related_public_tuple(),
                                final_resolution=final_resolution,
                                final_nb_clusters=final_nb_clusters,
                                final_leakage_clustering=deepcopy(
                                    self.dendrogram.initial_leakage_clustering()),
                                final_secret_clustering=deepcopy(
                                    self.dendrogram.related_secret_tuples()),
                                final_public_marginal_dist_for_current_public_tuple=deepcopy(
                                    self.dendrogram.secret_public_joint_dist_for_current_public_tuple()),
                                satisfied_criterium=satisfied_criterium
                                )

    @property
    def dendrogram(self) -> Dendrogram:
        """ Getter for the dendrogram attribute """
        return self._dendrogram

    def resolution_criteria_met(self):
        """ Getter for the _resolution_criteria_met attribute. """
        return self._resolution_criteria_met

    def set_resolution_criteria_met_to_true(self):
        """ Set the _resolution_criteria_met attribute to True """
        self._resolution_criteria_met = True

    def nb_clusters_criteria_met(self):
        """ Getter for the _nb_clusters_criteria_met attribute. """
        return self._nb_clusters_criteria_met

    def set_nb_clusters_criteria_met_to_true(self):
        """ Set the _nb_clusters_criteria_met attribute to True """
        self._nb_clusters_criteria_met = True


def _related_indices(dist: float,
                     distances: List[float]):
    """
        Get the indices in the list distances where the value is equal to dist
    """
    return [i for [i, v] in enumerate(distances) if (v == dist)]
