""" Module defining the use cases related to the Euclidian distance """
from typing import List

from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.domain.Distance.distance_euclidian import DistanceEuclidian
from securityquantifier.domain.Probe.probe_container import ProbeContainer


class DistanceEuclidianComputeUC(UseCaseInterface):
    """ DistanceEuclidianComputeUC class computes the Euclidian distance
     between two tuples """

    def __init__(self,
                 leakage1: List[float],
                 leakage2: List[float],
                 exponent: int,
                 is_active_probe: bool,
                 probe: ProbeContainer):
        """ Constructor for the DistanceChebyshevComputeUC class """
        self._leakage1 = leakage1
        self._leakage2 = leakage2
        self._exponent = exponent
        self._is_active_probe = is_active_probe
        self._probe = probe

    def execute(self):
        """ The execute() method calls the adapter to get the Euclidian distance """

        euclidian_dist = DistanceEuclidian()

        return euclidian_dist.compute_distance(leakages1=self._leakage1,
                                               leakages2=self._leakage2,
                                               exponent=self._exponent,
                                               is_active_probe=self._is_active_probe,
                                               probe=self._probe.probe)

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
