"""
    Module defining the use cases related to getting all the distributions, ie the joint distribution P(K,X)
    and the marginal distribution P(X) and P(K)
"""
import logging

from sqcommon.adapter.File.JSON.Distribution.distribution_json_repository import DistributionKeyException
from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict
from sqcommon.domain.Tuples.tuples_info import TuplesInformation

from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface


def get_distribution_for_type(dist_as_dict: DistributionAsDict,
                              distribution_type: EnumDistributionTuples):
    """
        Get the distribution of type distribution_type from the dist_as_dict if it exists
    """
    # Test that the distribution dictionary has an entry of type distribution_type
    if distribution_type.value not in dist_as_dict.data:
        logging.info("No {0} distribution was found. Computing it from the provided distribution(s)".format(
            distribution_type.value))
        return None
    else:
        return dist_as_dict.data[distribution_type.value]


class AllDistributionsGetUseCase(UseCaseInterface):
    """
        AllDistributionsGetUseCase class defines the methods to get the the joint distribution P(K,X)
        and the marginal distributions for P(X) and P(K) from the joint distribution P(K,X)
        using the dist_as_dict provided in parameter.
    """

    def __init__(self,
                 dist_as_dict: DistributionAsDict,
                 tuples_information: TuplesInformation
                 ):
        """ Constructor for the MarginalDistributionJSONGetUseCase class """
        self._secret_public_joint_dist_as_list = get_distribution_for_type(
            dist_as_dict, EnumDistributionTuples.SECRET_PUBLIC)
        self._secret_marginal_dist_as_list = get_distribution_for_type(dist_as_dict, EnumDistributionTuples.SECRET)
        self._public_marginal_dist_as_list = get_distribution_for_type(dist_as_dict, EnumDistributionTuples.PUBLIC)
        if self._secret_public_joint_dist_as_list is None and self._secret_marginal_dist_as_list is None \
                and self._public_marginal_dist_as_list is None:
            # No secret public joint distribution is provided.
            # We need both public and secret marginal distributions to compute the secret public distribution
            raise DistributionKeyException(
                "No secret public joint distribution was provided, nor any marginal distribution",
                "Provide a distribution file with either a secret public joint distribution or both marginal "
                "distributions")

        self._tuples_information = tuples_information

    def execute(self):
        """
            The execute(..) method computes the indices related to :
                - each X
                - each K
            and uses these indices to extract the related joint distribution from the JSON distribution file
        """

        dist_dict = {EnumDistributionTuples.SECRET_PUBLIC.value: [],
                     EnumDistributionTuples.SECRET.value: [],
                     EnumDistributionTuples.PUBLIC.value: []}

        # TODO: Works only when we have a dataframe in which all possible tuples are present,
        #  ie nb_tuples=k*a*n. Here we have the distribution file which contains the same number of tuples
        #  as the dataframe but the idea is the same, it works only when all possible tuples are present.
        #  Code to adapt for cases when not all possible tuples are present. In that case the computation nb_tuples
        #  does not work
        nb_tuples = self._tuples_information.total_nb_tuples
        nb_public_tuples = self._tuples_information.nb_public_tuples
        nb_secret_tuples = self._tuples_information.nb_secret_tuples

        # Variable gathering the marginal distributions
        current_marginal_dist = 0

        # Compute the joint distribution P(SECRET_PUBLIC)
        # from the given marginal distributions secret and public
        # if it was not provided.
        if self._secret_public_joint_dist_as_list is None:
            # For each public tuple
            for index1 in range(0, nb_secret_tuples, 1):
                # For each public tuple related to the current secret variable
                for index2 in range(0, nb_public_tuples, 1):
                    current_joint_dist = \
                        self._public_marginal_dist_as_list[index2] * self._secret_marginal_dist_as_list[index1]
                    # We computed a joint distribution for a secret variable,
                    # we add it into the dictionary with key P(SECRET_PUBLIC)
                    dist_dict[EnumDistributionTuples.SECRET_PUBLIC.value].append(current_joint_dist)
        else:
            dist_dict[EnumDistributionTuples.SECRET_PUBLIC.value].extend(
                self._secret_public_joint_dist_as_list)

        # Compute the marginal distribution P(SECRET)
        # from the given joint distribution
        # if it was not provided.
        if self._secret_marginal_dist_as_list is None:
            # For each public tuple
            for index1 in range(0, nb_tuples - nb_public_tuples + 1, nb_public_tuples):
                # For each public tuple related to the current secret variable
                for index2 in range(index1, index1 + nb_public_tuples, 1):
                    current_marginal_dist += self._secret_public_joint_dist_as_list[index2]
                # We computed a joint distribution for a secret variable,
                # we add it into the dictionary with key P(SECRET)
                dist_dict[EnumDistributionTuples.SECRET.value].append(current_marginal_dist)
                current_marginal_dist = 0
        else:
            dist_dict[EnumDistributionTuples.SECRET.value].extend(self._secret_marginal_dist_as_list)

        # Compute the marginal distribution P(PUBLIC)
        # from the given joint distribution
        # if it was not provided.
        if self._public_marginal_dist_as_list is None:
            # For each public tuple
            for index1 in range(0, nb_public_tuples, 1):
                # For each public tuple related to the current public variable
                for index2 in range(index1, nb_tuples, nb_public_tuples):
                    current_marginal_dist += self._secret_public_joint_dist_as_list[index2]
                # We computed a joint distribution for a public variable,
                # we add it into the dictionary with key P(PUBLIC)
                dist_dict[EnumDistributionTuples.PUBLIC.value].append(current_marginal_dist)
                current_marginal_dist = 0
        else:
            dist_dict[EnumDistributionTuples.PUBLIC.value].extend(self._public_marginal_dist_as_list)

        return DistributionAsDict(data=dist_dict)
