"""
    Module defining the use cases related to generating a joint distribution P(K,X) for secret and public variables
"""

from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict
from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface


class UniformJointDistributionComputeUseCase(UseCaseInterface):
    """
        UniformJointDistributionComputeUseCase class defines the methods to generate a joint distribution P(K,X)
        for secret and public variables
    """

    def __init__(self,
                 total_nb_tuples: int
                 ):
        """ Constructor for the UniformJointDistributionComputeUseCase class """
        self._total_nb_tuples = total_nb_tuples

    def execute(self) -> DistributionAsDict:
        """
            The execute(..) method computes a joint distribution P(K,X) for secret and public variables
            and stores it in a DistributionAsDict
        """
        uniform_dist = [1 / self._total_nb_tuples for _ in range(self._total_nb_tuples)]
        secret_public_distribution = {
            EnumDistributionTuples.SECRET_PUBLIC.value: uniform_dist
        }

        return DistributionAsDict(data=secret_public_distribution)
