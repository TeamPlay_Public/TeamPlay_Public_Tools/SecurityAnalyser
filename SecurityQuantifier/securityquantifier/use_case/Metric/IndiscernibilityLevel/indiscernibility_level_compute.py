"""
    Module defining the use cases related to the Indiscernability Level Metric Computation
    used by the different side-channels
"""
import logging
import time
import warnings

from math import log
from typing import List, Dict

from sqcommon.enums.enum_distribution_tuples import EnumDistributionTuples

from securityquantifier.domain.Dendrogram.dendrogram_results_container import DendrogramsResultsContainer
from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict
from sqcommon.domain.Tuples.tuples_info import TuplesInformation

from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.domain.Dendrogram.dendrogram import Dendrogram
from securityquantifier.domain.Dendrogram.dendrogram_result import DendrogramResult

from securityquantifier.use_case.Dendrogram.dendrogram_compute import DendrogramUseCase


class IndiscernabilityLevelComputeUseCase(UseCaseInterface):
    """
        IndiscernabilityLevelComputeUseCase class defines
        the methods used by the different Side Channels Time, Energy and Power
        which all have their own implementations that inherit from this class.
    """

    def __init__(self,
                 data: DataAsDataFrame,
                 all_distributions_as_dict: DistributionAsDict,
                 nb_tainted_vars: int,
                 nb_untainted_vars: int,
                 tuples_information: TuplesInformation,
                 nb_clusters: int,
                 resolution: int):
        """ Constructor for the IndiscernabilityLevelTimeComputeUseCase class """
        self._data = data
        if not all_distributions_as_dict:
            raise ValueError("The dictionary providing the distributions is empty. Aborting.")
        self._all_distributions_as_dict = all_distributions_as_dict
        self._nb_tainted_vars = nb_tainted_vars
        self._nb_untainted_vars = nb_untainted_vars
        self._tuples_information = tuples_information
        self._nb_clusters = nb_clusters
        self._resolution = resolution

    def execute(self):
        """
            Implemented in the subclasses
        """
        pass

    def get_dendrogram_result(self,
                              leakage_tuples: List[float],
                              public_tuples: List[str],
                              secret_public_joint_dist_for_current_public_tuple: List[List[float]],
                              secret_tuples: List[float]) -> DendrogramResult:
        """
            From the gathered information for one public tuple:
                - Generate an instance of Dendrogram for leakage
                  using nb_clusters (and potentially resolution).
                - Run the dendrogram algorithm and get the clustering(s)
        """
        start_time = time.time()
        # Now that we got all the needed information for one public tuple:
        # 1- Generate an instance of Dendrogram for leakage
        # using nb_clusters (and potentially resolution).
        # Nb: nb_cluster and resolution are optional arguments.
        # However, nb_clusters has a computed default value when not provided, unlike resolution
        dendrogram = Dendrogram(
            leakage_tuples=leakage_tuples,
            secret_public_joint_dist_for_current_public_tuple=secret_public_joint_dist_for_current_public_tuple,
            related_secret_tuples=secret_tuples,
            related_public_tuple=public_tuples[-1],
            given_resolution=self._resolution,
            given_nb_clusters=self._nb_clusters)

        # 2- Run the dendrogram algorithm and get the clustering(s).
        # We get one clustering in return.
        # The clustering is returned when the first satisfied criteria is met.
        # If no criteria is met, the dendrogram algorithm will run
        # until all the leakages are in the same (unique) cluster. This unique cluster will then be returned
        dendrogram_uc = DendrogramUseCase(dendrogram=dendrogram)
        dendrogram_result_for_current_public_tuple = dendrogram_uc.execute()

        logging.debug('Execution time for method get_dendrogram_result: %s seconds ---'
                      % (time.time() - start_time))
        return dendrogram_result_for_current_public_tuple

    def compute_iir_for_each_public_tuple(self,
                                          dendrogram_results_container: DendrogramsResultsContainer,
                                          hk: float):
        """
            For each public tuple, we compute the IIR using the unique clustering related to the public tuple.

            dendrogram_results_container: DendrogramsResultsContainer
                Container of DendrogramsResult gathering all the information for each public tuple
                (one DendrogramsResult per public tuple)
            hk: float
                The computed entropy for the secret variable
        """
        start_time = time.time()
        indiscernability_level = 0
        for result in dendrogram_results_container.results.values():
            # Using the clustering, compute the II (aka, H(K | L,X) ) for the current public tuple
            current_ii_metric_value = self.indiscernability_level_compute(dendrogram_result=result)
            # Save it into indiscernability_level
            indiscernability_level += current_ii_metric_value

        if indiscernability_level > hk:
            warnings.warn("H(K | L,X) should be lower than H(K). This is not the case.")
            warnings.warn("H(K | L,X) =" + str(indiscernability_level) + " and H(K)=" + str(hk))

        # We computed the II for a criteria, we divide it by the entropy for k, H(K), to get the IIR
        indiscernability_ratio = indiscernability_level / hk

        logging.info('Execution time for method compute_iir_for_each_public_tuple: %s seconds ---'
                      % (time.time() - start_time))
        return indiscernability_ratio

    def indiscernability_level_compute(self,
                                       dendrogram_result: DendrogramResult) -> float:
        """
            Compute the IIR metric for the current public tuple and its related clustering

             Parameters
            ----------
            dendrogram_result: DendrogramResult
                 The data related to a public tuple, of type DendrogramResult,
                 used to compute the II (aka H(K | L,X) ) for this public tuple.

            Returns
            -------
            H(K | L,X): float
                The IIR for the given public tuple
        """
        # H(K | L,X)
        hklx = 0

        # Compute the entropy H(K | L, X)
        # for each cluster in final_secret_public_distrib with length > 1
        for cluster in dendrogram_result.final_public_marginal_dist_for_current_public_tuple:
            if len(cluster) > 1:
                # sum up all element in the current cluster = sum
                dist_sum = sum(cluster)

                cluster_length = len(cluster)
                #print("--- Cluster has ", cluster_length, " elements")
                for_element_start_time = time.time()
                total_one_log = 0

                for element in cluster:
                    # compute eli * log(sum/eli)
                    if element == 0:
                        raise ZeroDivisionError(
                            "The distribution for clustering " + str(dendrogram_result.final_leakage_clustering)[
                                                                 1:-1] + " is equal to zero. ")
                    log_start_time = time.time()
                    hklx += element * log(dist_sum / element, 2)
                    one_log_time = time.time() - log_start_time
                    total_one_log += one_log_time
                    #print("--- hklx log time for one log: ", one_log_time)
                total_log_time = time.time() - for_element_start_time
                #print("--- Total time : for element in cluster ---", total_log_time)
                #print("Compared to adding log one by one: ", total_one_log)

        return hklx

    def collect_secret_leakage_distribution_data(self,
                                                 index1: int,
                                                 leakage_indices: List[int],
                                                 leakage_tuples: List[float],
                                                 total_nb_tuples: int,
                                                 nb_public_tuples: int,
                                                 secret_marginal_dist_dict: Dict,
                                                 secret_indices: List[int],
                                                 secret_public_joint_dist_for_current_public_tuple: List[float],
                                                 secret_tuples: List[float]):
        """
            Collects secret values, leakage values, distribution values as lists for the current public tuple.
            Sums up the distribution for each secret values and returns it as dict.

            Parameters:
                index1: int
                    Index of the current public tuple
                leakage_indices: List[int]
                    Index of the current leakage
                leakage_tuples: List[float]
                    List where to store the leakage tuple
                total_nb_tuples: int
                    Total number of tuples in the input
                nb_public_tuples: int
                    Number of public tuples
                secret_marginal_dist_dict: Dict
                    Dict where to store the secret marginal distribution
                secret_indices: List[int]
                    Indices of the secret tuples
                secret_public_joint_dist_for_current_public_tuple: List[float]
                    List where to store the secret public joint distribution
                secret_tuples: List[float]
                    List where to store the secret tuples

        """
        start_time = time.time()

        for index2 in range(index1, total_nb_tuples - nb_public_tuples + index1 + 1, nb_public_tuples):
            # Collect and turn secret values into a list
            secret_t = self._data.data.iloc[[index2], secret_indices].values[0]
            secret_tuples.append(secret_t.tolist())

            # Collect and turn leakage values into a list
            leakage_t = self._data.data.iloc[[index2], leakage_indices].values[0]
            leakage_tuples.append(leakage_t.tolist())

            # Collect related distribution for the current [secret,public]
            joint_distribution_value_for_current_secret = \
                self._all_distributions_as_dict.data[EnumDistributionTuples.SECRET_PUBLIC.value][index2]
            secret_public_joint_dist_for_current_public_tuple.append([joint_distribution_value_for_current_secret])

            # Sum up the distribution for each secret value
            # and store it into a dictionary
            if secret_t[0] not in secret_marginal_dist_dict:
                secret_marginal_dist_dict[secret_t[0]] = joint_distribution_value_for_current_secret
            else:
                secret_marginal_dist_dict[secret_t[0]] += joint_distribution_value_for_current_secret

        logging.debug('Execution time for method collect_secret_leakage_distribution_data: %s seconds ---'
                      % (time.time() - start_time))

    @property
    def data(self):
        """ Getter for the data attribute """
        return self._data

    @property
    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
