"""
    Module defining the use cases related to the Indiscernability Level Metric Computation for Power
"""
import logging

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict
from sqcommon.domain.Tuples.tuples_info import TuplesInformation

from securityquantifier.domain.Dendrogram.dendrogram_results_container import DendrogramsResultsContainer
from securityquantifier.helper.entropy_helper import EntropyHelper
from securityquantifier.use_case.Metric.IndiscernibilityLevel.indiscernibility_level_compute import \
    IndiscernabilityLevelComputeUseCase


class IndiscernabilityLevelPowerComputeUseCase(IndiscernabilityLevelComputeUseCase):
    """
        IndiscernabilityLevelPowerComputeUseCase class defines
        the Indiscernability Level Metric computation for Power methods
    """

    def __init__(self,
                 data: DataAsDataFrame,
                 all_distributions_as_dict: DistributionAsDict,
                 nb_tainted_vars: int,
                 nb_untainted_vars: int,
                 tuples_information: TuplesInformation,
                 nb_clusters: int,
                 resolution: int):
        """ Constructor for the IndiscernabilityLevelPowerComputeUseCase class """
        super().__init__(data=data,
                         all_distributions_as_dict=all_distributions_as_dict,
                         nb_tainted_vars=nb_tainted_vars,
                         nb_untainted_vars=nb_untainted_vars,
                         tuples_information=tuples_information,
                         nb_clusters=nb_clusters,
                         resolution=resolution)

    def execute(self) -> float:
        """
            The execute() method computes the Indiscernability Level for power,
            using DistributionComputeUseCase to compute the needed distributions
         """
        # Lists storing the tuples and distribution for each public tuple
        secret_tuples = []
        leakage_tuples = []
        public_tuples = []
        public_marginal_dist_for_current_public_tuple = []

        # Dictionary storing the marginal distribution for each secret value
        secret_marginal_dist_dict = {}

        # Final container holding the one DendrogramResultsElement associated to each public tuple
        dendrogram_results_container = DendrogramsResultsContainer(results={})

        # Get the column indices of the secret tuples in a dataframe row
        secret_indices = [i for i in range(0, self._nb_tainted_vars)]

        # Get the column indices of the leakage tuples in a dataframe row
        leakage_indices = [i for i in range(self._nb_tainted_vars + self._nb_untainted_vars,
                                            self._nb_tainted_vars + self._nb_untainted_vars + 1)]

        # Get the column indices of the public tuples in a dataframe row
        public_indices = [i for i in range(self._nb_tainted_vars, self._nb_untainted_vars + 1)]

        # Scroll in the dataframe, by public tuples.
        # For each public tuple get the related secret and leakage variables,
        # along with the distribution for each secret value.
        for index1 in range(0, self._tuples_information.nb_public_tuples, 1):
            # Collect and turn [secret, public] values into a list
            public_t = self._data.data.iloc[[index1], public_indices].values[0]
            # We convert it to string as it will be used as a key in the Dictionary DendrogramsResultsContainer
            public_tuples.append(str(public_t.tolist()))

            # For each row in the Security Exchange file that has the current public tuple value
            # Collect its related leakage and marginal distribution values.
            logging.info('Collecting related values for public tuple %s' % str(public_t.tolist()))
            self.collect_secret_leakage_distribution_data(
                index1=index1,
                leakage_indices=leakage_indices,
                leakage_tuples=leakage_tuples,
                total_nb_tuples=self._tuples_information.total_nb_tuples,
                nb_public_tuples=self._tuples_information.nb_public_tuples,
                secret_marginal_dist_dict=secret_marginal_dist_dict,
                secret_indices=secret_indices,
                secret_public_joint_dist_for_current_public_tuple=public_marginal_dist_for_current_public_tuple,
                secret_tuples=secret_tuples)

            # Now that we got all the needed information for one public tuple.

            # We generate an instance of Dendrogram for leakage and
            # run the dendrogram algorithm and get the clustering(s).
            logging.info('Running Dendrogram algorithm')
            dendrogram_result = self.get_dendrogram_result(
                leakage_tuples=leakage_tuples,
                public_tuples=public_tuples,
                secret_public_joint_dist_for_current_public_tuple=public_marginal_dist_for_current_public_tuple,
                secret_tuples=secret_tuples)

            # Append the DendrogramsResults into the container
            dendrogram_results_container.results[str(public_tuples[-1])] = dendrogram_result

            # Emptying the lists
            secret_tuples.clear()
            leakage_tuples.clear()
            public_marginal_dist_for_current_public_tuple.clear()

        # Compute the entropy for the secret variable
        logging.info('Computing the entropy for the secret variable')
        hk = EntropyHelper.entropy(nb_secret_tuples=self._tuples_information.nb_secret_tuples,
                                   distribution_var=list(secret_marginal_dist_dict.values()))

        # Dendrogram algorithm has been ran for each public tuples,
        # now we compute the IIR using for each public tuple the one clustering obtained
        logging.info('Computing the iir value')
        final_iir = self.compute_iir_for_each_public_tuple(dendrogram_results_container=dendrogram_results_container,
                                                           hk=hk)

        return final_iir

    @property
    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
