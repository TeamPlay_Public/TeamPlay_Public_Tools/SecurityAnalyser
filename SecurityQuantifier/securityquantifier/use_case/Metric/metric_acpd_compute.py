""" Module defining the use cases related to the ACPD Metric """
from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict

from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.domain.Distance.distance_euclidian import DistanceEuclidian
from securityquantifier.domain.Metric.metric_acpd import MetricACPD
from sqcommon.domain.Variable.variables_list import VariablesList

from securityquantifier.domain.Probe.probe_container import ProbeContainer


class MetricACPDComputeUseCase(UseCaseInterface):
    """ MetricACPDComputeUseCase class defines the ACPD Metric computation methods """

    def __init__(self,
                 distance_euclidian: DistanceEuclidian,
                 data: DataAsDataFrame,
                 distribution_as_dict: DistributionAsDict,
                 nb_tainted_vars: int,
                 nb_untainted_vars: int,
                 variables_cards: VariablesList,
                 exponent: int,
                 probe_container: ProbeContainer):
        """ Constructor for the MetricACPDComputeUC class """
        self._distance_euclidian = distance_euclidian
        self._data = data
        self._distribution_as_dict = distribution_as_dict
        self._nb_tainted_vars = nb_tainted_vars
        self._nb_untainted_vars = nb_untainted_vars
        self._variables_card = variables_cards
        self._exponent = exponent
        self._probe_container = probe_container

    def execute(self):
        """
            The execute() method computes the ACPD metric using the DistanceEuclidian distance
        """
        metric_acpd = MetricACPD(data=self._data,
                                 marginal_distributions_as_dict=self._distribution_as_dict,
                                 distance=self._distance_euclidian,
                                 nb_tainted_vars=self._nb_tainted_vars,
                                 nb_untainted_vars=self._nb_untainted_vars,
                                 variables_card=self._variables_card,
                                 exponent=self._exponent,
                                 probe_container=self._probe_container
                                 )

        return metric_acpd.compute_metric()

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
