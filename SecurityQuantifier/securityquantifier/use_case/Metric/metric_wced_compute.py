""" Module defining the use cases related to the WCED Metric """
from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.domain.Distance.distance_chebyshev import DistanceChebyshev
from securityquantifier.domain.Metric.metric_wced import MetricWCED
from sqcommon.domain.Variable.variables_list import VariablesList

from securityquantifier.domain.Probe.probe_container import ProbeContainer


class MetricWCEDComputeUseCase(UseCaseInterface):
    """ MetricWCEDComputeUseCase class defines the WCED Metric computation methods """

    def __init__(self,
                 distance_chebyshev: DistanceChebyshev,
                 data: DataAsDataFrame,
                 nb_tainted_vars: int,
                 nb_untainted_vars: int,
                 variables_cards: VariablesList,
                 probe_container: ProbeContainer):
        """ Constructor for the MetricWCEDComputeUseCase class """
        self._distance_chebyshev = distance_chebyshev
        self._data = data
        self._nb_tainted_vars = nb_tainted_vars
        self._nb_untainted_vars = nb_untainted_vars
        self._variables_card = variables_cards
        self._probe_container = probe_container

    def execute(self):
        """ The execute() method computes the WCED metric
         using the DistanceChebyshev distance """

        metric_wced = MetricWCED(data=self._data,
                                 distance=self._distance_chebyshev,
                                 nb_tainted_vars=self._nb_tainted_vars,
                                 nb_untainted_vars=self._nb_untainted_vars,
                                 variables_card=self._variables_card,
                                 probe_container=self._probe_container
                                 )

        return metric_wced.compute_metric()

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
