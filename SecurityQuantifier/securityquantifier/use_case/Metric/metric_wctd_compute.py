""" Module defining the use cases related to the WCTD Metric """

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame

from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.domain.Distance.distance_chebyshev import DistanceChebyshev
from securityquantifier.domain.Metric.metric_wctd import MetricWCTD
from sqcommon.domain.Variable.variables_list import VariablesList

from securityquantifier.domain.Probe.probe_container import ProbeContainer


class MetricWCTDComputeUseCase(UseCaseInterface):
    """ MetricWCTDComputeUseCase class defines the WCTD Metric computation methods """

    def __init__(self,
                 distance_chebyshev: DistanceChebyshev,
                 data: DataAsDataFrame,
                 nb_tainted_vars: int,
                 nb_untainted_vars: int,
                 variables_cards: VariablesList,
                 probe_container: ProbeContainer):
        """ Constructor for the MetricWCTDComputeUseCase class """
        self._distance_chebyshev = distance_chebyshev
        self._data = data
        self._nb_tainted_vars = nb_tainted_vars
        self._nb_untainted_vars = nb_untainted_vars
        self._variables_card = variables_cards
        self._probe_container = probe_container

    def execute(self) -> float:
        """ The execute() method computes the WCTD metric
         using the DistanceChebyshev distance """

        metric_wctd = MetricWCTD(data=self._data,
                                 distance=self._distance_chebyshev,
                                 nb_tainted_vars=self._nb_tainted_vars,
                                 nb_untainted_vars=self._nb_untainted_vars,
                                 variables_card=self._variables_card,
                                 probe_container=self._probe_container
                                 )

        return metric_wctd.compute_metric()

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
