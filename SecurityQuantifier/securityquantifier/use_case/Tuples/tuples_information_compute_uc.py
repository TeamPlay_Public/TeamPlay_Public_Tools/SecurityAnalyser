""" Module defining the use cases related to the extraction of tuples information """

from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.domain.Tuples.tuples_info import TuplesInformation
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.helper.variables_helper import VariableHelper
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface


class TuplesInformationHDF5ComputeUseCase(UseCaseInterface):
    """
        TuplesInformationGetUseCase class defines the method to extract the tuples information
        from a HDF5 file.
    """

    def __init__(self,
                 data: DataAsDataFrame,
                 nb_secret_vars: int,
                 vars_cardinalities: VariablesList, ):
        """ Constructor for the TuplesInformationGetUseCase class """
        self._data = data
        self._nb_secret_vars = nb_secret_vars
        self._vars_cardinalities = vars_cardinalities

    def execute(self) -> TuplesInformation:
        """
            The execute() method computes the tuples information from a HDF5 file:
                - The number of occurrences of public tuples in the Security Exchange file
                - The number of occurrences of secret tuples in the Security Exchange file
                - The total number of occurrences of tuples in the Security Exchange file
            and returns them as a container TuplesInformation
        """
        return VariableHelper.get_tuples_information(data_col_names_as_list=self._data.data.columns.tolist(),
                                                     nb_secret_vars=self._nb_secret_vars,
                                                     variables_cardinalities=self._vars_cardinalities)
