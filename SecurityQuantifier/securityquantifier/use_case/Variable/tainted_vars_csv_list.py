""" Module defining the use cases related to the Tainted variables list """
from pathlib import Path

from sqcommon.enums.enum_var_types import VariableTypes
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.interface.adapter.Variable.vars_list_repository_interface \
    import VariablesListRepositoryInterface


class TaintedListUseCase(UseCaseInterface):
    """ TaintedListUseCase class retrieves the Tainted variables list """

    def __init__(self,
                 vars_list_repo: VariablesListRepositoryInterface,
                 input_path: Path):
        """ Constructor for the TaintedListUseCase class """
        self._repository = vars_list_repo
        self._input_path = input_path

    def execute(self):
        """ The execute() method calls the adapter to get the list of tainted variables """
        return self._repository.find_by_type(VariableTypes.TAINTED, self._input_path)
