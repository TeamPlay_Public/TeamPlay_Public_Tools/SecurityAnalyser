""" Module defining the use cases related to the Untainted variables list"""
from pathlib import Path

from sqcommon.enums.enum_var_types import VariableTypes
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.interface.adapter.Variable.vars_list_repository_interface \
    import VariablesListRepositoryInterface


class UntaintedListUseCase(UseCaseInterface):
    """ UntaintedListUseCase class retrieves the Untainted variables list """

    def __init__(self,
                 vars_list_repo: VariablesListRepositoryInterface,
                 input_path: Path):
        """ Constructor for the UntaintedListUseCase class """
        self._repository = vars_list_repo
        self._input_path = input_path

    def execute(self):
        """ The execute() method calls the adapter to get the list of Untainted variables """
        return self._repository.find_by_type(VariableTypes.UNTAINTED, self._input_path)
