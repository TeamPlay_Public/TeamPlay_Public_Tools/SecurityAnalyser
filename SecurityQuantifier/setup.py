#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = []

setup_requirements = []

test_requirements = []

setup(
    author="Bruno LEBON",
    author_email='bruno.lebon@inria.fr',
    python_requires='>=3.7',
    classifiers=[
        'Development Status :: 2 - Beta',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description=
    "Team play module analysing data collected by the Data Collector to provide in return a security level indicator",
    install_requires=requirements,
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='SecurityQuantifier',
    name='SecurityQuantifier',
    packages=find_packages(include=['SecurityQuantifier', 'SecurityQuantifier.*']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url="https://gitlab.inria.fr/TeamPlay/TeamPlay_Development/TeamPlay_Tools/-/tree/master/SecurityAnalyser/SecurityQuantifier",
    version="1.0.0",
    zip_safe=False
)
