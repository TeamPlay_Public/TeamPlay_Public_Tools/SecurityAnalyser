"""
    Test case for the ProbeDatabaseRepository
"""
from securityquantifier.adapter.Database.Probe.probe_db_repository import ProbeDatabaseRepository


def test_probedatabaserepository_defines_method_store():
    """ Test that ProbeDatabaseRepository has method store(...) """

    store = getattr(ProbeDatabaseRepository, "store", None)

    assert callable(store)
