"""
    Testing class related to the adapter CSVDataAsDataFrameGetRepository and containing testing methods
"""

import pytest
from pandas import DataFrame
from sqcommon.domain.Data.data_dataframe import DataAsDataFrame
from sqcommon.interface.adapter.Data.data_repository_interface import DataRepositoryInterface

from securityquantifier.adapter.File.CSV.Data.csv_data_as_dataframe_repository import CSVDataAsDataFrameGetRepository


@pytest.fixture
def csv_dataframe_repo_model_init():
    """ Instantiate the CSVDataAsDataFrameGetRepository """

    csv_dataframe_repo = CSVDataAsDataFrameGetRepository()

    return csv_dataframe_repo


def test_hdf_is_instance_of_hdf5dataframerepo(csv_dataframe_repo_model_init):
    """ Test CSVDataAsDataFrameGetRepository constructor """

    csv_dataframe_repo = csv_dataframe_repo_model_init

    assert isinstance(csv_dataframe_repo, CSVDataAsDataFrameGetRepository)
    assert isinstance(csv_dataframe_repo, DataRepositoryInterface)


def test_repo_defines_method_find():
    """
        Test that the CSVDataAsDataFrameGetRepository has a method named find.
    """

    find = getattr(CSVDataAsDataFrameGetRepository, "find", None)

    assert callable(find)


def test_returned_value_is_instance_of_dataframe(return_input_file_indiscernability_csv_file_for_repository_test):
    """ Test whether CSVDataAsDataFrameGetRepository.find(file) returns a Pandas DataFrame """
    returned_value = CSVDataAsDataFrameGetRepository.find(
                return_input_file_indiscernability_csv_file_for_repository_test)

    assert isinstance(returned_value, DataAsDataFrame)
    assert isinstance(returned_value.data, DataFrame)
