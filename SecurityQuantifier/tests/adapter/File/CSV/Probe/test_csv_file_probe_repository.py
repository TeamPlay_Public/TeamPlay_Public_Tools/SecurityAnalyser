"""
    Testing class related to the adapter CSVFileProbeRepository and containing testing methods
"""

import pytest

from securityquantifier.adapter.File.CSV.Probe.csv_file_probe_repository import CSVFileProbeRepository
from securityquantifier.adapter.probe_repository import ProbeRepository


@pytest.fixture
def csv_probe_repo_model_init():
    """ Instantiate the CSVFileProbeRepository """

    csv_probe_repo = CSVFileProbeRepository()

    return csv_probe_repo


def test_is_instance_of_csvfileproberepo(csv_probe_repo_model_init):
    """
        Test the CSVFileProbeRepository constructor
    """

    vcr = csv_probe_repo_model_init

    assert isinstance(vcr, CSVFileProbeRepository)
    assert isinstance(vcr, ProbeRepository)


def test_repo_defines_method_store():
    """
        Test that the CSVFileProbeRepository has a method named store.
    """

    store = getattr(CSVFileProbeRepository, "store", None)

    assert callable(store)


def test_returned_value_is_instance_of_bool(return_probedata_100_50_first_distance,
                                            return_textfilestorage_csv):
    """ Test whether CSVFileProbeRepository.store(...) returns a bool """
    returned_value = CSVFileProbeRepository.store(data=return_probedata_100_50_first_distance,
                                                  storage=return_textfilestorage_csv)

    assert isinstance(returned_value, bool)
