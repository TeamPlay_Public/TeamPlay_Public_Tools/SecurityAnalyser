"""
    Testing class related to the adapter SideChannelClassGetCSVRepository and containing testing methods
"""

import pytest
from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.interface.adapter.SideChannel.side_channel_class_repository_interface import \
    SideChannelClassRepositoryInterface

from securityquantifier.adapter.File.CSV.SideChannel.side_channel_class_get_csv_repository import \
    SideChannelClassGetCSVRepository


@pytest.fixture
def sdc_get_csv_repo_model_init():
    """ Instantiate the SideChannelClassGetCSVRepository """

    sdc_get_csv_repo = SideChannelClassGetCSVRepository()

    return sdc_get_csv_repo


def test_is_instance_of_sdcgetcsvrepository(sdc_get_csv_repo_model_init):
    """
        Test the SideChannelClassGetCSVRepository constructor
    """

    vcr = sdc_get_csv_repo_model_init

    assert isinstance(vcr, SideChannelClassGetCSVRepository)
    assert isinstance(vcr, SideChannelClassRepositoryInterface)


def test_sdcgetcsvrepository_defines_method_find():
    """ Test that SideChannelClassGetCSVRepository has method find(...) """

    find = getattr(SideChannelClassGetCSVRepository, "find", None)

    assert callable(find)


def test_returned_value_is_instance_of_sidechannelclass(
        return_input_file_indiscernability_csv_file_for_repository_test):
    """ Test whether CSVFileProbeRepository.find(...) returns a SideChannelClass """
    returned_value = SideChannelClassGetCSVRepository.find(
        input_path=return_input_file_indiscernability_csv_file_for_repository_test)

    assert isinstance(returned_value, SideChannelClass)


def test_returned_value_dot_value_is_ok(returned_value_value_is_time_worst,
                                        return_input_file_indiscernability_csv_time_worst_file_for_repository_test):
    """ Test whether SideChannelClassGetCSVRepository.find(file) returned value is ok """

    returned_value = SideChannelClassGetCSVRepository.find(
        return_input_file_indiscernability_csv_time_worst_file_for_repository_test)

    assert returned_value.value == returned_value_value_is_time_worst


def test_returned_value_dot_name_is_ok(return_input_file_indiscernability_csv_time_worst_file_for_repository_test,
                                       returned_value_name_is_time_worst):
    """ Test whether SideChannelClassGetHDF5Repository.find(file) returned name is ok """

    returned_value = SideChannelClassGetCSVRepository.find(
        return_input_file_indiscernability_csv_time_worst_file_for_repository_test)

    assert returned_value.name == returned_value_name_is_time_worst
