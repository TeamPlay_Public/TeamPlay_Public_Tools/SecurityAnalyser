"""
    Testing class related to the adapter VariablesCardinalityCSVRepository and containing testing methods
"""
from typing import List

import pytest

from securityquantifier.adapter.File.CSV.Variable.variables_cardinality_csv_repository import \
    VariablesCardinalityCSVRepository


@pytest.fixture
def var_card_csv_repo_model_init():
    """ Instantiate the VariablesCardinalityCSVRepository """

    var_card_csv_repo = VariablesCardinalityCSVRepository()

    return var_card_csv_repo


def test_is_instance_of_var_card_csv_repo(var_card_csv_repo_model_init):
    """
        Test the VariablesCardinalityCSVRepository constructor
    """

    vcc = var_card_csv_repo_model_init

    assert isinstance(vcc, VariablesCardinalityCSVRepository)


def test_var_card_csv_repo_defines_method_find_all():
    """ Test that VariablesCardinalityCSVRepository has method find_all(...) """

    find_all = getattr(VariablesCardinalityCSVRepository, "find_all", None)

    assert callable(find_all)


def test_returned_value_is_instance_of_list(return_input_file_indiscernability_csv_file_for_repository_test,
                                            return_nb_tainted_equals_1,
                                            return_nb_untainted_equals_2):
    """ Test whether VariablesCardinalityCSVRepository.find_all(...) returns a List """
    returned_value = VariablesCardinalityCSVRepository.find_all(
        input_file=return_input_file_indiscernability_csv_file_for_repository_test,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2)

    assert isinstance(returned_value, List)


def test_returned_value_list_size_is_equal_to_three(
        return_input_file_indiscernability_csv_file_for_repository_test,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2):
    """ Test whether VariablesCardinalityCSVRepository.find_by_type(...) list has a size of 3 """

    returned_value = VariablesCardinalityCSVRepository.find_all(
        input_file=return_input_file_indiscernability_csv_file_for_repository_test,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2)

    assert len(returned_value) == 3


def test_returned_value_list_values_are_3_2_2(return_cardinalities_equal_to_3_2_2,
                                              return_input_file_indiscernability_csv_file_for_repository_test,
                                              return_nb_tainted_equals_1,
                                              return_nb_untainted_equals_2):
    """ Test whether VariablesCardinalityCSVRepository.find_all(file) has values 3, 2 and 2 """

    returned_value = VariablesCardinalityCSVRepository.find_all(
        input_file=return_input_file_indiscernability_csv_file_for_repository_test,
        nb_tainted_vars=return_nb_tainted_equals_1,
        nb_untainted_vars=return_nb_untainted_equals_2)

    assert all(elem in returned_value for elem in return_cardinalities_equal_to_3_2_2)
