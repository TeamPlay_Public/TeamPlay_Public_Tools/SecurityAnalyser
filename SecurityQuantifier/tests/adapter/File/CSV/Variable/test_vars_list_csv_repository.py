"""
    Testing class related to the adapter CSVVariablesListRepository and containing testing methods
"""

import pytest
from sqcommon.domain.Variable.variables_list import VariablesList

from securityquantifier.adapter.File.CSV.Variable.vars_list_csv_repository import CSVVariablesListRepository
from securityquantifier.interface.adapter.Variable.vars_list_repository_interface import \
    VariablesListRepositoryInterface


@pytest.fixture
def csv_var_list_repo_model_init():
    """ Instantiate the CSVVariablesListRepository """

    csv_var_list_repo = CSVVariablesListRepository()

    return csv_var_list_repo


def test_is_instance_of_csv_var_listrepository(csv_var_list_repo_model_init):
    """
        Test the CSVVariablesListRepository constructor
    """

    cvl = csv_var_list_repo_model_init

    assert isinstance(cvl, CSVVariablesListRepository)
    assert isinstance(cvl, VariablesListRepositoryInterface)


def test_csv_var_listrepository_defines_method_find_by_type():
    """ Test that CSVVariablesListRepository has method find_by_type(...) """

    find_by_type = getattr(CSVVariablesListRepository, "find_by_type", None)

    assert callable(find_by_type)


def test_returned_value_is_instance_of_variableslist(return_variabletypes_with_value_tainted,
                                                     return_input_file_indiscernability_csv_file_for_repository_test):
    """ Test whether CSVVariablesListRepository.find_by_type(...) returns a VariablesList """
    returned_value = CSVVariablesListRepository.find_by_type(
        var_type=return_variabletypes_with_value_tainted,
        input_path=return_input_file_indiscernability_csv_file_for_repository_test)

    assert isinstance(returned_value, VariablesList)


def test_returned_value_list_size_is_equal_to_one_for_tainted_var_type(
        return_variabletypes_with_value_tainted,
        return_input_file_indiscernability_csv_file_for_repository_test):
    """ Test whether CSVVariablesListRepository.find_by_type(file) list has a size of 1 """

    returned_value = CSVVariablesListRepository.find_by_type(
        var_type=return_variabletypes_with_value_tainted,
        input_path=return_input_file_indiscernability_csv_file_for_repository_test)

    assert len(returned_value.lst) == 1


def test_returned_value_list_dot_cardinality_is_equal_to_zero(
        return_variabletypes_with_value_tainted,
        return_input_file_indiscernability_csv_file_for_repository_test,
        return_cardinality_equals_0):
    """ Test whether CSVVariablesListRepository.find_by_type(file) cardinality value = zero """

    returned_value = CSVVariablesListRepository.find_by_type(
        var_type=return_variabletypes_with_value_tainted,
        input_path=return_input_file_indiscernability_csv_file_for_repository_test)

    assert returned_value.lst[0].cardinality == return_cardinality_equals_0


def test_returned_value_list_dot_value_is_equal_to_secret(
        return_variabletypes_with_value_tainted,
        return_input_file_indiscernability_csv_file_for_repository_test,
        return_value_equals_secret):
    """ Test whether CSVVariablesListRepository.find_by_type(file) value = secret """

    returned_value = CSVVariablesListRepository.find_by_type(
        var_type=return_variabletypes_with_value_tainted,
        input_path=return_input_file_indiscernability_csv_file_for_repository_test)

    assert returned_value.lst[0].value == return_value_equals_secret


def test_returned_value_list_dot_var_type_is_equal_to_tainted(
        return_variabletypes_with_value_tainted,
        return_input_file_indiscernability_csv_file_for_repository_test):
    """ Test whether CSVVariablesListRepository.find_by_type(file) var_type = VariableTypes.TAINTED """

    returned_value = CSVVariablesListRepository.find_by_type(
        var_type=return_variabletypes_with_value_tainted,
        input_path=return_input_file_indiscernability_csv_file_for_repository_test)

    assert returned_value.lst[0].var_type == return_variabletypes_with_value_tainted


def test_returned_value_list_size_is_equal_to_two_for_untainted_var_type(
        return_variabletypes_with_value_untainted,
        return_input_file_indiscernability_csv_file_for_repository_test):
    """ Test whether CSVVariablesListRepository.find_by_type(file) list has a size of 2 """

    returned_value = CSVVariablesListRepository.find_by_type(
        var_type=return_variabletypes_with_value_untainted,
        input_path=return_input_file_indiscernability_csv_file_for_repository_test)

    assert len(returned_value.lst) == 2


def test_returned_value_list_dot_cardinalities_are_equal_to_zero(
        return_variabletypes_with_value_untainted,
        return_input_file_indiscernability_csv_file_for_repository_test,
        return_cardinality_equals_0):
    """ Test whether CSVVariablesListRepository.find_by_type(file) both cardinalities value = zero """

    returned_value = CSVVariablesListRepository.find_by_type(
        var_type=return_variabletypes_with_value_untainted,
        input_path=return_input_file_indiscernability_csv_file_for_repository_test)

    for var in returned_value.lst:
        if var.value == "a" or var.value == "n":
            assert int(var.cardinality) == 0


def test_returned_value_list_dot_value_is_equal_to_a_and_n(
        return_variabletypes_with_value_untainted,
        return_input_file_indiscernability_csv_file_for_repository_test,
        return_value_equals_secret):
    """ Test whether CSVVariablesListRepository.find_by_type(file) value = a and n """

    returned_value = CSVVariablesListRepository.find_by_type(
        var_type=return_variabletypes_with_value_untainted,
        input_path=return_input_file_indiscernability_csv_file_for_repository_test)

    # Gathering values from VariablesList in a simple list
    values = set(var.value for var in returned_value.lst)

    # then check that we have the expected values
    assert "a" in values
    assert "n" in values


def test_returned_value_list_dot_var_type_is_equal_to_untainted(
        return_variabletypes_with_value_untainted,
        return_input_file_indiscernability_csv_file_for_repository_test):
    """ Test whether CSVVariablesListRepository.find_by_type(file) var_type = VariableTypes.UNTAINTED
        for both element in the list
    """

    returned_value = CSVVariablesListRepository.find_by_type(
        var_type=return_variabletypes_with_value_untainted,
        input_path=return_input_file_indiscernability_csv_file_for_repository_test)

    assert returned_value.lst[0].var_type == return_variabletypes_with_value_untainted
    assert returned_value.lst[1].var_type == return_variabletypes_with_value_untainted
