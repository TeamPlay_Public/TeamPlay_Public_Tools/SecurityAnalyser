"""
    Testing class related to the adapter FileRepository and containing testing methods
"""

import pytest

from securityquantifier.adapter.File.CSV.file_csv_repository import FileRepository
from securityquantifier.domain.Input.input_dataframe import InputCSVDataFrame
from securityquantifier.interface.adapter.Input.input_repository_interface import InputRepositoryInterface
from securityquantifier.interface.domain.Input.input_csv_interface import InputCSVInterface


@pytest.fixture
def file_repo_model_init():
    """ Instantiate the FileRepository """

    file_repo = FileRepository()

    return file_repo


def test_is_instance_of_csv_var_listrepository(file_repo_model_init):
    """
        Test the FileRepository constructor
    """

    file_repo = file_repo_model_init

    assert isinstance(file_repo, FileRepository)
    assert isinstance(file_repo, InputRepositoryInterface)


def test_filerepository_defines_method_find_by_path():
    """ Test that FileRepository has method find_by_path(...) """

    find_by_path = getattr(FileRepository, "find_by_path", None)

    assert callable(find_by_path)


def test_returned_value_is_instance_of_variableslist(
        return_input_file_indiscernability_csv_file_for_file_repository_test,
        return_variables_list_untainted_a2_n2,
        return_variables_list_tainted_secret3,
        return_sidechannel_with_value_time_worst,
        dataframe_from_indiscernability):
    """ Test whether FileRepository.find_by_path(...) returns a InputCSVDataFrame """
    returned_value = FileRepository.find_by_path(
        input_path=return_input_file_indiscernability_csv_file_for_file_repository_test,
        tainted_vars=return_variables_list_tainted_secret3,
        untainted_vars=return_variables_list_untainted_a2_n2,
        side_channel_class=return_sidechannel_with_value_time_worst,
        data=dataframe_from_indiscernability)

    assert isinstance(returned_value, InputCSVDataFrame)
    assert isinstance(returned_value, InputCSVInterface)
