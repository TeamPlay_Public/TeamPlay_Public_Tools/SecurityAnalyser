"""
    Test case for the ProbeFileRepository
"""
from securityquantifier.adapter.File.CSV.Probe.csv_file_probe_repository import CSVFileProbeRepository


def test_probefilerepository_defines_method_store():
    """ Test that ProbeFileRepository has method store(...) """

    store = getattr(CSVFileProbeRepository, "store", None)

    assert callable(store)


def test_probefilerepository_defines_method_find_record():
    """ Test that ProbeFileRepository has method record(...) """

    record = getattr(CSVFileProbeRepository, "record", None)

    assert callable(record)
