"""
    Testing class related to the domain ProbeData and containing testing methods
"""
import pytest

from securityquantifier.domain.Data.probe_data import ProbeData


@pytest.fixture
def probedata_domain_model_init(return_probedata_value_10,
                                return_probedata_value_20,
                                return_probedata_current_distance_10,
                                return_probedata_total_distance_50):
    """ Instantiate a ProbeData object """

    probedata = ProbeData(value1=return_probedata_value_10,
                          value2=return_probedata_value_20,
                          current_distance=return_probedata_current_distance_10,
                          total_distance=return_probedata_total_distance_50)

    return probedata


def test_probedata_is_instance_of_probedata(probedata_domain_model_init):
    """ Test probedata constructor """

    probedata = probedata_domain_model_init

    assert isinstance(probedata, ProbeData)
