"""
    Testing class related to the domain SEFileAsDict and containing testing methods
"""
from typing import Dict, List

import pytest

from securityquantifier.domain.Data.se_file_as_dict import SEFileAsDict


@pytest.fixture
def se_file_to_dict_domain_model_init(return_dataframe_from_d44_page_73_as_dict,
                                      ):
    """ Instantiate a SEFileAsDict object """

    dataframe_to_dict = SEFileAsDict(data=return_dataframe_from_d44_page_73_as_dict)

    return dataframe_to_dict


def test_data_as_dataframe_to_dict_is_instance_of_dataframe_to_dict(se_file_to_dict_domain_model_init):
    """ Test SEFileAsDict constructor """

    dataframe_to_dict = se_file_to_dict_domain_model_init

    assert isinstance(dataframe_to_dict, SEFileAsDict)


def test_data_as_dataframe_to_dict_return_dict(se_file_to_dict_domain_model_init):
    """
        Test that the property data is of type Dict
    """
    dataframe_to_dict = se_file_to_dict_domain_model_init

    assert isinstance(dataframe_to_dict.data, Dict)


def test_data_as_dataframe_to_dict_has_proper_keys(se_file_to_dict_domain_model_init,
                                                   return_se_file_as_dict_key_2_10,
                                                   return_se_file_as_dict_key_4_15,
                                                   return_se_file_as_dict_key_8_35):
    """
        Test that the Dict contained in the property data has the proper keys:
            2,10
            4,15
            8,35
    """
    dataframe_to_dict = se_file_to_dict_domain_model_init

    data_dict = dataframe_to_dict.data

    # [*data_dict] returns the keys of the dictionary as a list
    assert return_se_file_as_dict_key_2_10 in [*data_dict]
    assert return_se_file_as_dict_key_4_15 in [*data_dict]
    assert return_se_file_as_dict_key_8_35 in [*data_dict]


def test_data_as_dataframe_to_dict_has_proper_values_type(se_file_to_dict_domain_model_init):
    """
        Test that the Dict contained in the property data has the proper type of values List[List]
    """
    dataframe_to_dict = se_file_to_dict_domain_model_init

    for value in dataframe_to_dict.data.values():
        assert isinstance(value, List)
        for elem in value:
            assert isinstance(elem, List)


def test_data_as_dataframe_to_dict_has_proper_values(se_file_to_dict_domain_model_init,
                                                     return_se_file_as_dict_key_2_10,
                                                     return_se_file_as_dict_value_for_2_10,
                                                     return_se_file_as_dict_key_4_15,
                                                     return_se_file_as_dict_value_for_4_15,
                                                     return_se_file_as_dict_key_8_35,
                                                     return_se_file_as_dict_value_for_8_35):
    """
        Test that the Dict contained in the property data has the proper values for keys:
            2,10
            4,15
            8,35
    """
    dataframe_to_dict = se_file_to_dict_domain_model_init

    value = dataframe_to_dict.data.get(return_se_file_as_dict_key_2_10)

    assert value == return_se_file_as_dict_value_for_2_10

    value = dataframe_to_dict.data.get(return_se_file_as_dict_key_4_15)

    assert value == return_se_file_as_dict_value_for_4_15

    value = dataframe_to_dict.data.get(return_se_file_as_dict_key_8_35)

    assert value == return_se_file_as_dict_value_for_8_35


def test_data_as_dataframe_to_dict_has_proper_values_length(se_file_to_dict_domain_model_init,
                                                            return_se_file_as_dict_key_2_10,
                                                            return_se_file_as_dict_value_for_2_10,
                                                            return_se_file_as_dict_key_4_15,
                                                            return_se_file_as_dict_value_for_4_15,
                                                            return_se_file_as_dict_key_8_35,
                                                            return_se_file_as_dict_value_for_8_35):
    """
        Test that the Dict contained in the property data has the proper values length
    """
    dataframe_to_dict = se_file_to_dict_domain_model_init

    value = dataframe_to_dict.data.get(return_se_file_as_dict_key_2_10)

    assert len(value) == 10
    assert len(value[0]) == 2

    value = dataframe_to_dict.data.get(return_se_file_as_dict_key_4_15)

    assert len(value) == 10
    assert len(value[0]) == 2

    value = dataframe_to_dict.data.get(return_se_file_as_dict_key_8_35)

    assert len(value) == 10
    assert len(value[0]) == 2
