"""
    Testing class related to the domain Dendrogram and containing testing methods
"""
import pytest

from securityquantifier.domain.Dendrogram.dendrogram import Dendrogram


# ---------------------------------------------------------------------------------
# Tests for the constructor
# ---------------------------------------------------------------------------------
@pytest.mark.skip(reason="Factorisation. Does not run as is")
def test_dendrogram_is_instance_of_dendrogram_observations(
        dendrogram_model_init_with__observations):
    """
        Test Dendrogram constructor
        Factorisation testing method. Used by the following test cases
    """

    dendrogram = dendrogram_model_init_with__observations

    assert isinstance(dendrogram, Dendrogram)


def test_dendrogram_is_instance_of_dendrogram_with_2_cluster_5_resolutions_observations_a(
        dendrogram_model_init_with_2_cluster_5_resolutions_observations_a):
    """ Test Dendrogram constructor for observations a"""
    test_dendrogram_is_instance_of_dendrogram_observations(
        dendrogram_model_init_with_2_cluster_5_resolutions_observations_a)


def test_dendrogram_is_instance_of_dendrogram_with_2_cluster_5_resolutions_indiscernability(
        dendrogram_model_init_with_2_cluster_5_resolutions_indiscernability):
    """ Test Dendrogram constructor for observations from indiscernability"""
    test_dendrogram_is_instance_of_dendrogram_observations(
        dendrogram_model_init_with_2_cluster_5_resolutions_indiscernability)


def test_dendrogram_is_instance_of_dendrogram_with_4_cluster_3_resolutions_observations_a(
        dendrogram_model_init_with_4_cluster_3_resolutions_observations_a):
    """ Test Dendrogram constructor for observations a"""
    test_dendrogram_is_instance_of_dendrogram_observations(
        dendrogram_model_init_with_4_cluster_3_resolutions_observations_a)


def test_dendrogram_is_instance_of_dendrogram_with_4_cluster_3_resolutions_indiscernability(
        dendrogram_model_init_with_4_cluster_3_resolutions_indiscernability):
    """ Test Dendrogram constructor for observations from indiscernability"""
    test_dendrogram_is_instance_of_dendrogram_observations(
        dendrogram_model_init_with_4_cluster_3_resolutions_indiscernability)


# ---------------------------------------------------------------------------------
# Tests for the initial_clustering() method
# ---------------------------------------------------------------------------------
@pytest.mark.skip(reason="Factorisation. Does not run as is")
def test_dendrogram_sorted_has_same_nb_elements_as_observations(
        dendrogram_model_init_factorisation,
        dendrogram_observations
):
    """
        Test whether the sorted observations has the same number of elements than the initial observations
        Factorisation testing method. Used by the following test cases
    """

    dendrogram = dendrogram_model_init_factorisation

    assert (len(dendrogram.initial_leakage_clustering()) == len(dendrogram_observations))


def test_dendrogram_sorted_has_same_nb_elements_as_observations_with_nb_clusters_2_reso_5_obs_a(
        dendrogram_model_init_with_2_cluster_5_resolutions_observations_a,
        dendrogram_observations_a
):
    """ Test whether the sorted observations has the same number of elements than the initial observations """
    test_dendrogram_sorted_has_same_nb_elements_as_observations(
        dendrogram_model_init_with_2_cluster_5_resolutions_observations_a,
        dendrogram_observations_a)


def test_dendrogram_sorted_has_same_nb_elements_as_observations_with_nb_clusters_2_reso_5_obs_indiscernability(
        dendrogram_model_init_with_2_cluster_5_resolutions_indiscernability,
        dendrogram_observations_from_indiscernability
):
    """ Test whether the sorted observations has the same number of elements than the initial observations """

    dendrogram = dendrogram_model_init_with_2_cluster_5_resolutions_indiscernability

    assert (len(dendrogram.initial_leakage_clustering()) == len(dendrogram_observations_from_indiscernability))


def test_dendrogram_sorted_has_same_nb_elements_as_observations_with_nb_clust_2_reso_5_obs_a(
        dendrogram_model_init_with_4_cluster_3_resolutions_observations_a,
        dendrogram_observations_a
):
    """ Test whether the sorted observations has the same number of elements than the initial observations """
    test_dendrogram_sorted_has_same_nb_elements_as_observations(
        dendrogram_model_init_with_4_cluster_3_resolutions_observations_a,
        dendrogram_observations_a)


def test_dendrogram_sorted_has_same_nb_elements_as_observations_with_nb_cluster_4_reso_3_obs_indiscernability(
        dendrogram_model_init_with_4_cluster_3_resolutions_indiscernability,
        dendrogram_observations_from_indiscernability
):
    """ Test whether the sorted observations has the same number of elements than the initial observations """

    dendrogram = dendrogram_model_init_with_4_cluster_3_resolutions_indiscernability

    assert (len(dendrogram.initial_leakage_clustering()) == len(dendrogram_observations_from_indiscernability))


# ---------------------------------------------------------------------------------
# Tests for the initial_clustering() method: using original_position
# ---------------------------------------------------------------------------------

@pytest.mark.skip(reason="Factorisation. Does not run as is")
def test_dendrogram_sorted_indices_match_with_initial_observations(
        dendrogram_model_init_with_4_cluster_3_resolutions_observations,
        dendrogram_observations):
    """
        Test whether we find all the element of the dendrogram_observations
        in the dendrogram._initial_clustering, using original_position to do the matching

         original_position: a list of indices
         dendrogram_observations_a: a list of float (not sorted)
         dendrogram._initial_clustering: a list of list of one element (sorted)

        Factorisation testing method. Used by the following test cases
    """

    dendrogram = dendrogram_model_init_with_4_cluster_3_resolutions_observations

    original_position = dendrogram.original_position

    # for each index in original_position
    for index in original_position:
        # Get the related value in the sorted list
        val = dendrogram_observations[index]

        # Check it exists in the observation
        for element in dendrogram._initial_leakage_clustering:
            if val in element:
                # Assert val is in the current sublist of dendrogram._initial_clustering
                assert val in element
                break

    # Assert both lists have the same length
    assert (len(dendrogram_observations) == len(dendrogram._initial_leakage_clustering))


def test_dendrogram_sorted_indices_match_with_initial_observation_with_obs_a(
        dendrogram_model_init_with_4_cluster_3_resolutions_observations_a,
        dendrogram_observations_a):
    test_dendrogram_sorted_indices_match_with_initial_observations(
        dendrogram_model_init_with_4_cluster_3_resolutions_observations_a,
        dendrogram_observations_a)


def test_dendrogram_sorted_indices_match_with_initial_observation_with_obs_indiscernability(
        dendrogram_model_init_with_4_cluster_3_resolutions_indiscernability,
        dendrogram_observations_from_indiscernability):
    test_dendrogram_sorted_indices_match_with_initial_observations(
        dendrogram_model_init_with_4_cluster_3_resolutions_indiscernability,
        dendrogram_observations_from_indiscernability)


# ---------------------------------------------------------------------------------
# Tests for the distances
# ---------------------------------------------------------------------------------
@pytest.mark.skip(reason="Factorisation. Does not run as is")
def test_dendrogram_initial_distances_are_correct(dendrogram_model_init_dist,
                                                  initial_distances_for_observations):
    """
        Test the distances we compute using dendrogram.compute_distances are correct.
        Factorisation testing method. Used by the following test cases
    """
    dendrogram = dendrogram_model_init_dist

    assert (dendrogram.initial_distances() == initial_distances_for_observations)


def test_dendrogram_initial_distances_are_correct_with_obs_a(
        dendrogram_model_init_with_4_cluster_3_resolutions_observations_a,
        distances_initial_for_observations_a):
    """
        Test the distances we compute using dendrogram.compute_distances are correct for observations a
    """
    test_dendrogram_initial_distances_are_correct(dendrogram_model_init_with_4_cluster_3_resolutions_observations_a,
                                                  distances_initial_for_observations_a)


def test_dendrogram_initial_distances_are_correct_with_indiscernability(
        dendrogram_model_init_with_4_cluster_3_resolutions_indiscernability,
        distances_initial_for_observations_indiscernability):
    """
        Test the distances we compute using dendrogram.compute_distances are correct for observations indiscernability
    """
    test_dendrogram_initial_distances_are_correct(
        dendrogram_model_init_with_4_cluster_3_resolutions_indiscernability,
        distances_initial_for_observations_indiscernability)


@pytest.mark.skip(reason="Factorisation. Does not run as is")
def test_dendrogram_sorted_no_duplicates_distances_are_correct(
        dendrogram_model_init_with_4_cluster_3_resolutions_observations,
        sorted_no_duplicates_distances_for_observations):
    """
        Test the distances we compute using dendrogram.compute_distances are correct.
        Factorisation testing method. Used by the following test cases
    """
    dendrogram = dendrogram_model_init_with_4_cluster_3_resolutions_observations

    assert (dendrogram.distances() == sorted_no_duplicates_distances_for_observations)


def test_dendrogram_sorted_no_duplicates_distances_are_correct_with_obs_a(
        dendrogram_model_init_with_4_cluster_3_resolutions_observations_a,
        distances_sorted_no_duplicates_for_observations_a):
    """
        Test the distances we compute using dendrogram.compute_distances are correct for observations a
    """
    test_dendrogram_sorted_no_duplicates_distances_are_correct(
        dendrogram_model_init_with_4_cluster_3_resolutions_observations_a,
        distances_sorted_no_duplicates_for_observations_a)


def test_dendrogram_sorted_no_duplicates_distances_are_correct_with_indiscernability(
        dendrogram_model_init_with_4_cluster_3_resolutions_indiscernability,
        distances_sorted_no_duplicates_for_observations_indiscernability):
    """
        Test the distances we compute using dendrogram.compute_distances are correct for observations indiscernability
    """
    test_dendrogram_sorted_no_duplicates_distances_are_correct(
        dendrogram_model_init_with_4_cluster_3_resolutions_indiscernability,
        distances_sorted_no_duplicates_for_observations_indiscernability)


# ---------------------------------------------------------------------------------
# Tests for the _match_reordered_obs method
# ---------------------------------------------------------------------------------
@pytest.mark.skip(reason="Factorisation. Does not run as is")
def test_dendrogram_match_reordered_obs_method(
        dendrogram_model_init_reordered,
        dendrogram_observations):
    """
        Test that the method _match_reordered_obs reorders correctly the related secret values
        following the sorting of its associated observations.
        Factorisation testing method. Used by the following test cases
    """
    dendrogram = dendrogram_model_init_reordered

    assert dendrogram.related_secret_tuples() == dendrogram_observations


def test_dendrogram_match_reordered_obs_method_for_obs_a(
        dendrogram_model_init_with_2_cluster_5_resolutions_observations_a,
        dendrogram_reordered_related_secret_for_observations_a):
    """
        Test that the method _match_reordered_obs reorders correctly the related secret values
        following the sorting of its associated observations
    """
    test_dendrogram_match_reordered_obs_method(
        dendrogram_model_init_with_2_cluster_5_resolutions_observations_a,
        dendrogram_reordered_related_secret_for_observations_a)


def test_dendrogram_match_reordered_obs_method_indiscernability(
        dendrogram_model_init_with_2_cluster_5_resolutions_indiscernability,
        dendrogram_reordered_related_secret_from_indiscernability):
    """
        Test that the method _match_reordered_obs reorders correctly the related secret values
        following the sorting of its associated observations
    """
    test_dendrogram_match_reordered_obs_method(
        dendrogram_model_init_with_2_cluster_5_resolutions_indiscernability,
        dendrogram_reordered_related_secret_from_indiscernability)
