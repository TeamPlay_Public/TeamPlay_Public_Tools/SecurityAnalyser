"""
    Testing class related to the domain DendrogramGatheredTuples and containing testing methods
"""
from securityquantifier.domain.Dendrogram.dendrogram_gathered_tuples import DendrogramGatheredTuples


def test_dendrogram_is_instance_of_dendrogramgatheredtuples(
        dendrogramgatheredtuples_model_init):
    """ Test DendrogramGatheredTuples constructor """

    dendrogramgatheredtuples = dendrogramgatheredtuples_model_init

    assert isinstance(dendrogramgatheredtuples, DendrogramGatheredTuples)


def test_dendrogramgatheredtuples_defines_property_dict_tuples_occurrences():
    """
        Test that DendrogramGatheredTuples has a property named dict_tuples_occurrences.
    """

    dict_tuples_occurrences = getattr(DendrogramGatheredTuples, "dict_tuples_occurrences", None)

    assert isinstance(dict_tuples_occurrences, property)


def test_dendrogramgatheredtuples_defines_property_tuples_list():
    """
        Test that DendrogramGatheredTuples has a property named tuples_list.
    """

    tuples_list = getattr(DendrogramGatheredTuples, "tuples_list", None)

    assert isinstance(tuples_list, property)
