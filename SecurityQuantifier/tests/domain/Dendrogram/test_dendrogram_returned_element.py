"""
    Testing class related to the domain DendrogramResult and containing testing methods
"""
from securityquantifier.domain.Dendrogram.dendrogram_result import DendrogramResult


def test_is_instance_of_dendrogramresult(
        dendrogramresult_model_init_with_4_cluster_1_resolution_indiscernability):
    """ Test DendrogramResult constructor """

    dendrogram_element = dendrogramresult_model_init_with_4_cluster_1_resolution_indiscernability

    assert isinstance(dendrogram_element, DendrogramResult)


def test_dendrogramresult_defines_property_related_public_tuple():
    """
        Test that DendrogramResult has a property named related_public_tuple.
    """

    related_public_tuple = getattr(DendrogramResult, "related_public_tuple", None)

    assert isinstance(related_public_tuple, property)


def test_dendrogramresult_defines_property_final_leakage_clustering():
    """
        Test that DendrogramResult has a property named final_leakage_clustering.
    """

    final_leakage_clustering = getattr(DendrogramResult, "final_leakage_clustering", None)

    assert isinstance(final_leakage_clustering, property)


def test_dendrogramresult_defines_property_final_secret_clustering():
    """
        Test that DendrogramResult has a property named final_secret_clustering.
    """

    final_secret_clustering = getattr(DendrogramResult, "final_secret_clustering", None)

    assert isinstance(final_secret_clustering, property)


def test_dendrogramresult_defines_property_final_resolution():
    """
        Test that DendrogramResult has a property named final_resolution.
    """

    final_resolution = getattr(DendrogramResult, "final_resolution", None)

    assert isinstance(final_resolution, property)


def test_dendrogramresult_defines_property_final_nb_clusters():
    """
        Test that DendrogramResult has a property named final_nb_clusters.
    """

    final_nb_clusters = getattr(DendrogramResult, "final_nb_clusters", None)

    assert isinstance(final_nb_clusters, property)


def test_dendrogramresult_defines_property_satisfied_criterium():
    """
        Test that DendrogramResult has a property named satisfied_criterium.
    """

    satisfied_criterium = getattr(DendrogramResult, "satisfied_criterium", None)

    assert isinstance(satisfied_criterium, property)


def test_dendrogramresult_observations_and_secrets_match(
        dendrogramresult_model_init_with_4_cluster_1_resolution_indiscernability,
        return_all_the_tuples_for_secrets
):
    """
        Test that the secret values correspond to the observations
        and their clustering match
    """
    dendrogram_element = dendrogramresult_model_init_with_4_cluster_1_resolution_indiscernability

    # for each observation get the related k
    assert dendrogram_element.final_secret_clustering == return_all_the_tuples_for_secrets
