"""
    Testing class related to the domain Distance and containing testing methods
"""
import pytest

from securityquantifier.domain.Distance.distance import Distance

from securityquantifier.interface.domain.Distance.distance_interface import DistanceInterface


@pytest.fixture
def distance_model_init():
    """ Generate an instance of Distance """
    distance = Distance()

    return distance


def test_distance_is_instance_of_distance_interface(distance_model_init):
    """ Test whether Variable is an implementation of DistanceInterface """

    distance = distance_model_init

    assert isinstance(distance, DistanceInterface)
    assert isinstance(distance, Distance)


def test_defines_method_compute_distance():
    """ Test that the class has method compute_distance(...) """

    compute_distance = getattr(DistanceInterface, "compute_distance", None)

    assert callable(compute_distance)
