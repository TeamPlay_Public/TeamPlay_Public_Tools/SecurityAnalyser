"""
    Testing class related to the domain DistanceChebyshev and containing testing methods
"""

from securityquantifier.domain.Distance.distance_chebyshev import DistanceChebyshev


def test_distance_model_init():
    """ Test the DistanceChebyshev class constructor """

    distance = DistanceChebyshev()

    return distance


def test_distance_chebyshev_is_instance_of_distance():
    """ Test DistanceChebyshev's constructor """

    de = test_distance_model_init()

    assert isinstance(de, DistanceChebyshev)


def test_distance_chebyshev_result_is_instance_of_float(distance_chebyshev_distance_between_k1_and_k2):
    """ Test that compute_distance returns a float """

    de = distance_chebyshev_distance_between_k1_and_k2

    assert isinstance(de, float)


def test_distance_chebyshev_is_symmetric_naive(sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted):
    """ Test whether the computed distance verifies the symmetry property  """

    # Getting two side channel values needed to compute their relative distance
    s1, s2 = sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted

    # Symmetry property is verified if d(s1, s2) = d(s2, s1)
    assert abs(s1 - s2) == abs(s2 - s2)


def test_distance_chebyshev_is_symmetric_with_implementation(distance_chebyshev_distance_between_k1_and_k2,
                                                             distance_chebyshev_distance_between_k2_and_k1):
    """ Test whether the computed distance verifies the symmetry property  """

    # Symmetry property is verified if d(s1, s2) = d(s2, s1)
    assert distance_chebyshev_distance_between_k1_and_k2 == distance_chebyshev_distance_between_k2_and_k1


def test_distance_chebyshev_identity_of_indiscernible_naive(
        sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted):
    """ Test whether the identity of indiscernible values """

    # Getting two side channel values
    s1, s2 = sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted

    assert abs(s1 - s1) == 0
    assert abs(s2 - s2) == 0


def test_distance_chebyshev_identity_of_indiscernible_with_implementation(
        distance_chebyshev_distance_between_k1_and_k2,
        distance_chebyshev_distance_between_k2_and_k1):
    """ T Test whether the identity of indiscernible values """

    # Identity of indiscernible is verified if d(k1, k1) = 0
    assert distance_chebyshev_distance_between_k1_and_k2 == distance_chebyshev_distance_between_k1_and_k2
    # Identity of indiscernible is verified if d(k2, k2) = 0
    assert distance_chebyshev_distance_between_k2_and_k1 == distance_chebyshev_distance_between_k2_and_k1


def test_distance_chebyshev_is_equal_to_0_naive(sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted):
    """ Test whether the computed distance is equal to zero.
        It should be as the dataframe contains only side channel values equal to 2364 """

    # Getting two side channel values needed to compute their relative distance
    s1, s2 = sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted

    assert abs(s1 - s2) == 0


def test_distance_chebyshev_is_equal_to_0_with_implementation(
        distance_chebyshev_distance_between_k1_and_k2,
        distance_chebyshev_distance_between_k2_and_k1):
    """ Test whether the computed distance is equal to zero.
        It should be as the dataframe contains only side channel values equal to 2364 """

    assert distance_chebyshev_distance_between_k1_and_k2 == 0
    assert distance_chebyshev_distance_between_k2_and_k1 == 0


def test_distance_chebyshev_is_equal_to_40_with_implementation(dataframe_distance_equal_60cheb_20_eucl_init,
                                                               return_nb_tuples_equals_3,
                                                               return_nb_untainted_equals_2,
                                                               return_nb_tainted_equals_1,
                                                               return_sidechannel_with_value_time_worst,
                                                               return_is_active_probe_false,
                                                               return_probe_container_with_probe_not_active):
    """
        Test that the computed chebyshev distance is equal to 40 with a more complex dataframe
    """
    wctd = 0.0

    for index1 in range(0, return_nb_tuples_equals_3 - return_nb_untainted_equals_2 - 1,
                        return_nb_untainted_equals_2):
        for index2 in range(index1 + return_nb_untainted_equals_2,
                            return_nb_tuples_equals_3 - return_nb_untainted_equals_2 + 1,
                            return_nb_untainted_equals_2):
            wctd = max(wctd, DistanceChebyshev.compute_distance(
                leakages1=dataframe_distance_equal_60cheb_20_eucl_init.iloc[
                          index1:index1 + return_nb_untainted_equals_2,
                          return_nb_tainted_equals_1:],
                leakages2=dataframe_distance_equal_60cheb_20_eucl_init.iloc[
                          index2:index2 + return_nb_untainted_equals_2,
                          -1].tolist(),
                is_active_probe=return_is_active_probe_false,
                probe=return_probe_container_with_probe_not_active
            ))

    assert round(wctd, 10) == 0
