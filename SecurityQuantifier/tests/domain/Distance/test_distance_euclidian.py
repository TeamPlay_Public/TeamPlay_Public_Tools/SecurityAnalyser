"""
    Testing class related to the domain DistanceEuclidian and containing testing methods
"""

from securityquantifier.domain.Distance.distance_euclidian import DistanceEuclidian


def test_distance_model_init():
    """ Test the DistanceEuclidian class constructor """

    distance = DistanceEuclidian()

    return distance


def test_distance_euclidian_is_instance_of_distance():
    """ Test DistanceEuclidian-s constructor """

    de = test_distance_model_init()

    assert isinstance(de, DistanceEuclidian)


def test_distance_euclidian_result_is_instance_of_float(domain_distance_euclidian_equal_to_0dot19):
    """ Test that compute_distance returns a float """

    de = domain_distance_euclidian_equal_to_0dot19

    assert isinstance(de, float)


def test_distance_euclidian_is_symmetric_naive(sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted):
    """ Test whether the computed distance verifies the symmetry property  """

    # Getting two side channel values needed to compute their relative distance
    s1, s2 = sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted

    # Symmetry property is verified if d(s1, s2) = d(s2, s1)
    assert abs(s1 - s2) == abs(s2 - s1)


def test_distance_euclidian_is_symmetric_with_implementation(distance_euclidian_distance_between_k1_and_k2,
                                                             distance_euclidian_distance_between_k2_and_k1):
    """ Test whether the computed distance verifies the symmetry property  """

    # Symmetry property is verified if d(s1, s2) = d(s2, s1)
    assert distance_euclidian_distance_between_k1_and_k2 == distance_euclidian_distance_between_k2_and_k1


def test_distance_euclidian_identity_of_indiscernible_naive(
        sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted):
    """ Test whether the identity of indiscernible values """

    # Getting two side channel values
    s1, s2 = sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted

    assert abs(s1 - s1) == 0
    assert abs(s2 - s2) == 0


def test_distance_euclidian_identity_of_indiscernible_with_implementation(
        distance_euclidian_distance_between_k1_and_k2,
        distance_euclidian_distance_between_k2_and_k1):
    """ Test whether the identity of indiscernible values """

    # Identity of indiscernible is verified if d(k1, k1) = 0
    assert distance_euclidian_distance_between_k1_and_k2 == distance_euclidian_distance_between_k1_and_k2
    # Identity of indiscernible is verified if d(k2, k2) = 0
    assert distance_euclidian_distance_between_k2_and_k1 == distance_euclidian_distance_between_k2_and_k1


def test_distance_euclidian_is_equal_to_0_naive(sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted):
    """ Test whether the computed distance is equal to zero.
        It should be as the dataframe contains only side channel values equal to 2364 """

    # Getting two side channel values needed to compute their relative distance
    s1, s2 = sidechannels_from_series_with_distance_0_and_1_tainted_and_2_untainted

    assert abs(s1 - s2) == 0


def test_distance_euclidian_is_equal_to_0_with_implementation(
        distance_euclidian_distance_between_k1_and_k2,
        distance_euclidian_distance_between_k2_and_k1):
    """ Test whether the computed distance is equal to zero.
        It should be as the dataframe contains only side channel values equal to 2364 """

    assert distance_euclidian_distance_between_k1_and_k2 == 0
    assert distance_euclidian_distance_between_k2_and_k1 == 0


def test_distance_euclidian_is_equal_to_xxx_with_implementation(
        distance_euclidian_distance_between_k1_and_k2,
        distance_euclidian_distance_between_k2_and_k1):
    """ Test whether the computed distance is equal to zero.
        It should be as the dataframe contains only side channel values equal to 2364 """

    assert distance_euclidian_distance_between_k1_and_k2 == 0
    assert distance_euclidian_distance_between_k2_and_k1 == 0
