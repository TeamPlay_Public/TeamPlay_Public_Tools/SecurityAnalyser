"""
    Testing class related to the domain InputCSVDataFrame and containing testing methods
"""
import pytest

from securityquantifier.domain.Input.input_dataframe import InputCSVDataFrame
from securityquantifier.interface.domain.Input.input_csv_interface import InputCSVInterface


@pytest.fixture
def inputcsvdataframe_domain_model_init(return_input_file_indiscernability_csv_file_for_file_repository_test,
                                        return_variables_list_tainted_secret3,
                                        return_variables_list_untainted_a2_n2,
                                        return_sidechannel_with_value_time_worst,
                                        data_as_dataframe_from_indiscernability_bl):
    """ Instantiate a InputCSVDataFrame object """

    inputcsvdataframe = InputCSVDataFrame(path=return_input_file_indiscernability_csv_file_for_file_repository_test,
                                          tainted_vars=return_variables_list_tainted_secret3,
                                          untainted_vars=return_variables_list_untainted_a2_n2,
                                          side_channel_class=return_sidechannel_with_value_time_worst,
                                          data=data_as_dataframe_from_indiscernability_bl)

    return inputcsvdataframe


def test_inputcsvdataframe_is_instance_of_inputcsvdataframe(inputcsvdataframe_domain_model_init):
    """ Test InputCSVDataFrame constructor """

    inputcsvdataframe = inputcsvdataframe_domain_model_init

    assert isinstance(inputcsvdataframe, InputCSVDataFrame)
    assert isinstance(inputcsvdataframe, InputCSVInterface)
