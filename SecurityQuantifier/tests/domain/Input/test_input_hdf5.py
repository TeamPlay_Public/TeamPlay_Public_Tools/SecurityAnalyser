"""
    Testing class related to the domain InputHDF5 and containing testing methods
"""
import pytest
from sqcommon.interface.domain.Data.data_interface import DataInterface

from securityquantifier.domain.Input.input_hdf5 import InputHDF5
from securityquantifier.interface.domain.Input.input_hdf5_interface import InputHDF5Interface


@pytest.fixture
def inputhdf5_domain_model_init(return_input_file_indiscernability_csv_file_for_file_repository_test,
                                data_as_dataframe_from_indiscernability_bl):
    """ Instantiate a InputHDF5 object """

    inputhdf5 = InputHDF5(path=return_input_file_indiscernability_csv_file_for_file_repository_test,
                          data=data_as_dataframe_from_indiscernability_bl)

    return inputhdf5


def test_inputhdf5_is_instance_of_inputhdf5(inputhdf5_domain_model_init):
    """ Test InputHDF5 constructor """

    inputhdf5 = inputhdf5_domain_model_init

    assert isinstance(inputhdf5, InputHDF5)
    assert isinstance(inputhdf5, InputHDF5Interface)
    assert isinstance(inputhdf5, DataInterface)
