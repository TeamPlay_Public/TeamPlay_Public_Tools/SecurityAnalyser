"""
    Testing class related to the domain Metric and containing testing methods
"""
from securityquantifier.domain.Metric.metric import Metric


def test_metric_defines_method_compute_metric():
    """ Test that the interface has method compute_metric(...) """

    compute_metric = getattr(Metric, "compute_metric", None)

    assert callable(compute_metric)
