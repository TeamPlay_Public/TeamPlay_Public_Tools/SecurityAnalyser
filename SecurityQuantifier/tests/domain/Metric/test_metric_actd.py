"""
    Testing class related to the domain MetricATCD and containing testing methods
"""
import math

from securityquantifier.domain.Metric.metric import Metric
from securityquantifier.domain.Metric.metric_actd import MetricACTD
from securityquantifier.interface.domain.Metric.metric_interface import MetricInterface


def test_metric_is_instance_of_metric(metric_model_init_with_actd_metric_non_uniform_dist_equal_0):
    """ Test whether MetricACTD is an implementation of MetricInterface """

    metric = metric_model_init_with_actd_metric_non_uniform_dist_equal_0

    assert isinstance(metric, MetricACTD)
    assert isinstance(metric, Metric)
    assert isinstance(metric, MetricInterface)


def test_metric_equals_0_with_uniform_dist(metric_model_init_with_actd_metric_uniform_dist_equal_0,
                                           return_acsd_metric_value_equal_0):
    """
        Test whether the actd metric computed with dataframe metric_model_init_with_metric_equal_0 is equal to 0
        with uniform distribution
    """

    metric = metric_model_init_with_actd_metric_uniform_dist_equal_0

    computed = metric.compute_metric()

    assert computed == return_acsd_metric_value_equal_0
    assert isinstance(computed, float)


def test_metric_equals_0_with_non_uniform_dist(metric_model_init_with_actd_metric_non_uniform_dist_equal_0,
                                               return_acsd_metric_value_equal_0):
    """
        Test whether the actd metric computed with dataframe metric_model_init_with_metric_equal_0 is equal to 0
        with non uniform distribution
    """

    metric = metric_model_init_with_actd_metric_non_uniform_dist_equal_0

    computed = metric.compute_metric()

    assert computed == return_acsd_metric_value_equal_0
    assert isinstance(computed, float)


def test_metric_equals_20_with_uniform_dist(metric_model_init_with_actd_metric_uniform_dist_equal_20,
                                            return_acsd_metric_value_equal_20):
    """
        Test whether the actd metric computed with dataframe
        metric_model_init_with_actd_metric_uniform_dist_equal_cheb_60_eucl_20 is equal to 20
        with a uniform distribution
    """

    metric = metric_model_init_with_actd_metric_uniform_dist_equal_20

    computed = metric.compute_metric()

    assert computed == return_acsd_metric_value_equal_20


def test_metric_equals_1_point_5_with_uniform_dist(metric_model_init_with_actd_metric_uniform_dist_equal_1_point_5,
                                                   return_acsd_metric_value_equal_1_point_5):
    """
        Test whether the actd metric computed with dataframe
        metric_model_init_with_actd_metric_uniform_dist_equal_cheb_60_eucl_20 is equal to 1.5
        with a uniform distribution
    """

    metric = metric_model_init_with_actd_metric_uniform_dist_equal_1_point_5

    computed = metric.compute_metric()

    assert computed == return_acsd_metric_value_equal_1_point_5


def test_metric_equals_2_point_079948_with_non_uniform_dist_12_tuples(
        metric_model_init_with_actd_metric_non_uniform_dist_12_tuples_equal_2_point_079948,
        return_acsd_metric_value_equal_4_point_159896
):
    """
        Test whether the actd metric computed with dataframe
        metric_model_init_with_actd_metric_non_uniform_dist_12_tuples_equal_xxx
        is equal to 42 with a uniform distribution
    """

    metric = metric_model_init_with_actd_metric_non_uniform_dist_12_tuples_equal_2_point_079948

    computed = metric.compute_metric()

    assert math.isclose(computed, return_acsd_metric_value_equal_4_point_159896, rel_tol=0.0000001)
