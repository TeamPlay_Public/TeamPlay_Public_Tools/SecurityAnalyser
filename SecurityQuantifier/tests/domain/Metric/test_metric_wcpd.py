"""
    Testing class related to the domain MetricWCED and containing testing methods
"""
from securityquantifier.domain.Metric.metric import Metric
from securityquantifier.domain.Metric.metric_wcpd import MetricWCPD
from securityquantifier.interface.domain.Metric.metric_interface import MetricInterface


def test_metric_is_instance_of_metric(metric_model_init_with_wcpd_metric_equal_0_with_active_probe):
    """ Test whether MetricWCED is an implementation of MetricInterface """

    metric = metric_model_init_with_wcpd_metric_equal_0_with_active_probe

    assert isinstance(metric, MetricWCPD)
    assert isinstance(metric, Metric)
    assert isinstance(metric, MetricInterface)


def test_metric_equals_0(metric_model_init_with_wcpd_metric_equal_0_with_active_probe):
    """ Test whether the wced metric computed with dataframe metric_model_init_with_metric_equal_0 is equal to 0 """

    metric = metric_model_init_with_wcpd_metric_equal_0_with_active_probe

    computed = metric.compute_metric()

    assert computed == 0
    assert isinstance(computed, float)


def test_metric_equals_60(metric_model_init_with_wcpd_metric_equal_60_with_active_probe):
    """
        Test whether the wcet metric computed with dataframe
        metric_model_init_with_wcpd_metric_equal_cheb_60_eucl_20_with_active_probe is equal to 60
    """

    metric = metric_model_init_with_wcpd_metric_equal_60_with_active_probe

    computed = metric.compute_metric()

    assert computed == 60
