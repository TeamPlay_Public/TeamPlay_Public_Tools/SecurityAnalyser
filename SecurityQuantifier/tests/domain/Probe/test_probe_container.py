"""
    Testing class related to the domain ProbeContainer and containing testing methods
"""
import pytest

from securityquantifier.domain.Probe.probe_container import ProbeContainer


@pytest.fixture
def probecontainer_domain_model_init(return_is_active_probe_true,
                                     return_probe_container_with_probe_active,
                                     return_storage_is_text_file_storage
                                     ):
    """ Instantiate a ProbeContainer object """

    probecontainer = ProbeContainer(active_probe=return_is_active_probe_true,
                                    probe=return_probe_container_with_probe_active,
                                    storage=return_storage_is_text_file_storage
                                    )

    return probecontainer


def test_probecontainer_is_instance_of_probecontainer(probecontainer_domain_model_init):
    """ Test probecontainer constructor"""

    probecontainer = probecontainer_domain_model_init

    assert isinstance(probecontainer, ProbeContainer)
