"""
    Testing class related to the domain DatabaseStorage and containing testing methods
"""
import pytest

from securityquantifier.domain.Storage.Database.database_storage import DatabaseStorage
from securityquantifier.interface.domain.Storage.storage_interface import StorageInterface


@pytest.fixture
def dbstorage_domain_model_init(return_db_name,
                                return_db_user,
                                return_db_user_pwd,
                                return_db_host,
                                return_db_port
                                ):
    """ Instantiate a DatabaseStorage object """

    dbstorage = DatabaseStorage(name=return_db_name,
                                user=return_db_user,
                                passwd=return_db_user_pwd,
                                host=return_db_host,
                                port=return_db_port
                                )

    return dbstorage


def test_dbstorage_is_instance_of_dbstorage(dbstorage_domain_model_init):
    """ Test dbstorage constructor"""

    dbstorage = dbstorage_domain_model_init

    assert isinstance(dbstorage, DatabaseStorage)
    assert isinstance(dbstorage, StorageInterface)
