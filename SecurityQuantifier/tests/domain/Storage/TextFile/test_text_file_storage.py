"""
    Testing class related to the domain TextFileStorage and containing testing methods
"""
import pytest

from securityquantifier.domain.Storage.TextFile.text_file_storage import TextFileStorage
from securityquantifier.interface.domain.Storage.storage_interface import StorageInterface


@pytest.fixture
def textfilestorage_domain_model_init(return_output_file_csv_probing):
    """ Instantiate a TextFileStorage object """

    textfilestorage = TextFileStorage(name=return_output_file_csv_probing)

    return textfilestorage


def test_textfilestorage_is_instance_of_textfilestorage(textfilestorage_domain_model_init):
    """ Test textfilestorage constructor"""

    textfilestorage = textfilestorage_domain_model_init

    assert isinstance(textfilestorage, TextFileStorage)
    assert isinstance(textfilestorage, StorageInterface)
