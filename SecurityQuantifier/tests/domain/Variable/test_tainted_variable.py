"""
    Testing class related to the domain TaintedVariable and containing testing methods
"""
import pytest

from sqcommon.domain.Variable.variable import Variable
from sqcommon.interface.domain.Variable.variable_interface import VariableInterface

from securityquantifier.domain.Variable.tainted_variable import TaintedVariable


@pytest.fixture
def var_model_init():
    """ Generate an instance of TaintedVariable """
    var = TaintedVariable(value="a")

    return var


def test_var_is_instance_of_variable(var_model_init):
    """ Test whether Variable is an implementation of VariableInterface """

    var = var_model_init

    assert isinstance(var, TaintedVariable)
    assert isinstance(var, Variable)
    assert isinstance(var, VariableInterface)


def test_var_value_equals_a(var_model_init):
    """ Test that var contains the right value attribute """

    var = var_model_init

    assert var.value == 'a'


def test_var_cardinality_equals_1(var_model_init):
    """ Test that var contains the right cardinality attribute """

    var = var_model_init

    assert var.cardinality == 0
    assert isinstance(var.cardinality, int)
