import pytest

from sqcommon.domain.Variable.variable import Variable
from sqcommon.domain.Variable.variables_list import VariablesList

from sqcommon.interface.domain.Variable.variables_list_interface import VariablesListInterface

from securityquantifier.domain.Variable.typed_variables_list import TypedVariablesList


@pytest.fixture
def vars_list_model_init(var_model_init):
    """ Generate an instance of TypedVariablesList """
    typed_vars_list = TypedVariablesList(vtype='tainted')

    typed_vars_list.lst = var_model_init

    return typed_vars_list


@pytest.fixture
def var_model_init():
    """ Generate three instances of Variable in a list """
    vars_list = [Variable(cardinality=1, value="a"),
                 Variable(cardinality=2, value="n"),
                 Variable(cardinality=3, value="secret")]

    return vars_list


def test_vars_list_is_instance_of_variableslist(vars_list_model_init):
    """ Test whether VariablesList is an implementation of VariablesListInterface """

    vars_list = vars_list_model_init

    assert isinstance(vars_list, TypedVariablesList)
    assert isinstance(vars_list, VariablesList)
    assert isinstance(vars_list, VariablesListInterface)


def test_vars_in_vars_list_contains_values(vars_list_model_init):
    """ Test that the Variables in vars_list contain the right values attribute """

    vars_list = vars_list_model_init

    # Gathering values from VariablesList in a simple list
    values = set(var.value for var in vars_list.lst)

    # then check that we have the expected values
    assert "a" in values
    assert "secret" in values
    assert "n" in values

    # Check also vtype is correct
    assert 'tainted' == vars_list.vtype


def test_vars_in_vars_list_contains_cardinality(vars_list_model_init):
    """ Test that the Variables in vars_list contain the right cardinality attribute """

    vars_list = vars_list_model_init

    for var in vars_list.lst:
        if var.value == "a":
            assert int(var.cardinality) == 1
        elif var.value == "n":
            assert int(var.cardinality) == 2
        elif var.value == "secret":
            assert int(var.cardinality) == 3
