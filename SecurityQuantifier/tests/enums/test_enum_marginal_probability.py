"""
    Testing class related to the enum EnumMarginalProbability and containing testing methods
"""
from securityquantifier.enums.enum_marginal_probability import EnumJointMarginalProbability


def test_enum_marginal_probability_model_init():
    """ Test the EnumMarginalProbability class constructor """

    return EnumJointMarginalProbability.MARGINAL_PROBA_SECRET


def test_etc_is_instance_of_enum_marginal_probability():
    """ Test whether EnumMarginalProbability is an implementation of EnumMarginalProbability """

    etc = test_enum_marginal_probability_model_init()

    assert isinstance(etc, EnumJointMarginalProbability)


def test_enum_etc_value_equal_time_worst():
    """ Test EnumMarginalProbability enums for value """

    etc_enum = test_enum_marginal_probability_model_init()

    assert etc_enum.value == "P(SECRET)"


def test_enum_etc_name_equal_time_worst():
    """ Test EnumMarginalProbability for name """

    etc_enum = test_enum_marginal_probability_model_init()

    assert etc_enum.name == "MARGINAL_PROBA_SECRET"
