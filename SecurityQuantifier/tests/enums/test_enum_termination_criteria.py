"""
    Testing class related to the enum EnumTerminationCriteria and containing testing methods
"""
from securityquantifier.enums.enum_termination_criteria import EnumTerminationCriteria


def test_enum_termination_criteria_model_init():
    """ Test the EnumTerminationCriteria class constructor """

    return EnumTerminationCriteria.NB_CLUSTERS


def test_etc_is_instance_of_enum_termination_criteria():
    """ Test whether EnumTerminationCriteria is an implementation of EnumTerminationCriteria """

    etc = test_enum_termination_criteria_model_init()

    assert isinstance(etc, EnumTerminationCriteria)


def test_enum_etc_value_equal_time_worst():
    """ Test EnumTerminationCriteria enums for value """

    etc_enum = test_enum_termination_criteria_model_init()

    assert etc_enum.value == "nb_clusters"


def test_enum_etc_name_equal_time_worst():
    """ Test EnumTerminationCriteria for name """

    etc_enum = test_enum_termination_criteria_model_init()

    assert etc_enum.name == "NB_CLUSTERS"
