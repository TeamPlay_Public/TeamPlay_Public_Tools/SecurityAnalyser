"""
    Testing class related to the domain DataframeHelper and containing testing methods
"""
from typing import List

from securityquantifier.helper.dataframe_helper import DataframeHelper


def test_method_find_row_with_tuple(dataframe_distance_equal_60cheb_20_eucl_init,
                                    return_nb_tainted_equals_1,
                                    return_first_tuple_for_dataframe_distance_equal_60cheb_20_eucl_init
                                    ):
    """
        Test static method find_row_with_tuple(data: DataFrame,
                                                nb_tainted_var: int,
                                                k: List)
        The tuple exists and appears in the first row of the Dataframe dataframe_distance_equal_60cheb_20_eucl_init,
        so the result should be [0]
    """
    indexes = DataframeHelper.find_row_with_tuple(data=dataframe_distance_equal_60cheb_20_eucl_init,
                                                  nb_tainted_var=return_nb_tainted_equals_1,
                                                  k=return_first_tuple_for_dataframe_distance_equal_60cheb_20_eucl_init)

    assert isinstance(indexes, List)
    assert indexes == [0]
