"""
    Testing class related to the domain DictToStorageInfoHelper and containing testing methods
"""
from securityquantifier.domain.Storage.Database.database_storage import DatabaseStorage
from securityquantifier.domain.Storage.TextFile.text_file_storage import TextFileStorage
from securityquantifier.helper.dict_to_storageinfo_helper import DictToStorageInfoHelper


def test_method_dict_to_databasestorage(return_db_info_as_dict):
    """
        Test static method dict_to_databasestorage(data: dict)
    """
    storage_info = DictToStorageInfoHelper.dict_to_databasestorage(data=return_db_info_as_dict)

    assert isinstance(storage_info, DatabaseStorage)


def test_method_dict_to_filestorage(return_output_file_csv_probing,
                                    return_textfilestorage_csv):
    """
        Test static method dict_to_filestorage(data: dict)
    """
    storage_info = DictToStorageInfoHelper.dict_to_filestorage(data=return_output_file_csv_probing)

    assert isinstance(storage_info, TextFileStorage)
