"""
    Testing class related to the domain EntropyHelper and containing testing methods
"""
import math

from securityquantifier.helper.entropy_helper import EntropyHelper


def test_method_entropy_fair_coin(return_fair_distribution,
                                  return_entropy_fair_coin,
                                  return_nb_tuples_equals_2
                                  ):
    """
        Test static method entropy for a fair coin
    """
    entropy = EntropyHelper.entropy(distribution_var=return_fair_distribution,
                                    nb_secret_tuples=return_nb_tuples_equals_2)

    assert entropy == return_entropy_fair_coin


def test_method_entropy_biased_coin(return_biased_distribution,
                                    return_entropy_biased_coin,
                                    return_nb_tuples_equals_2
                                    ):
    """
        Test static method entropy for a biased coin
    """
    entropy = EntropyHelper.entropy(distribution_var=return_biased_distribution,
                                    nb_secret_tuples=return_nb_tuples_equals_2)

    assert math.isclose(entropy, return_entropy_biased_coin, rel_tol=0.0000001)
