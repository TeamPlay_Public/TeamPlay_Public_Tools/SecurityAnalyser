"""
    Test case for the InputRepositoryInterface interface
"""
from securityquantifier.interface.adapter.Input.input_repository_interface import InputRepositoryInterface


def test_interface_defines_method_find_by_path():
    """ Test that the interface has method find_by_path(...) """

    find_by_path = getattr(InputRepositoryInterface, "find_by_path", None)

    assert callable(find_by_path)
