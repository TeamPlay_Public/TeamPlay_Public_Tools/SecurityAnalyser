"""
    Test case for the ProbeRepositoryInterface interface
"""
from securityquantifier.interface.adapter.Probe.probe_repository_interface import ProbeRepositoryInterface


def test_interface_defines_method_store():
    """ Test that the interface has method store(...) """

    store = getattr(ProbeRepositoryInterface, "store", None)

    assert callable(store)


def test_interface_defines_method_find_record():
    """ Test that the interface has method record(...) """

    record = getattr(ProbeRepositoryInterface, "record", None)

    assert callable(record)
