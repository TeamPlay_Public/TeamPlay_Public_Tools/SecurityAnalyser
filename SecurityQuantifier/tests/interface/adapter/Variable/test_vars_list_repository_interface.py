"""
    Test case for the VariablesListRepositoryInterface interface
"""
from securityquantifier.interface.adapter.Variable.vars_list_repository_interface import \
    VariablesListRepositoryInterface


def test_interface_defines_method_find_by_type():
    """ Test that the interface has method find_by_type(...) """

    find_by_type = getattr(VariablesListRepositoryInterface, "find_by_type", None)

    assert callable(find_by_type)
