"""
    Test case for the DistanceInterface interface
"""
from securityquantifier.interface.domain.Distance.distance_interface import DistanceInterface


def test_interface_defines_method_compute_distance():
    """ Test that the interface has method compute_distance(...) """

    compute_distance = getattr(DistanceInterface, "compute_distance", None)

    assert callable(compute_distance)
