"""
    Test case for the InputCSVInterface interface
"""
from securityquantifier.interface.domain.Input.input_csv_interface import InputCSVInterface


def test_interface_defines_method_path():
    """
        Test that the interface has a property named path.
    """

    path = getattr(InputCSVInterface, "path", None)

    assert isinstance(path, property)


def test_interface_defines_method_tainted_vars():
    """
        Test that the interface has a property named tainted_vars.
    """

    tainted_vars = getattr(InputCSVInterface, "tainted_vars", None)

    assert isinstance(tainted_vars, property)


def test_interface_defines_method_untainted_vars():
    """
        Test that the interface has a property named untainted_vars.
    """

    untainted_vars = getattr(InputCSVInterface, "untainted_vars", None)

    assert isinstance(untainted_vars, property)


def test_interface_defines_method_side_channel_class():
    """
        Test that the interface has a property named side_channel_class.
    """

    side_channel_class = getattr(InputCSVInterface, "side_channel_class", None)

    assert isinstance(side_channel_class, property)


def test_interface_defines_method_data():
    """
        Test that the interface has a property named data.
    """

    data = getattr(InputCSVInterface, "data", None)

    assert isinstance(data, property)
