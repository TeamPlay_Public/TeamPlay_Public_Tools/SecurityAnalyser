"""
    Test case for the InputHDF5CheckerInterface interface
"""
from securityquantifier.interface.domain.Input.input_hdf5_checker_interface import InputHDF5CheckerInterface


def test_interface_defines_method_check_sidechannelclass_exists():
    """
        Test that the interface has a property named check_sidechannelclass_exists.
    """

    check_sidechannelclass_exists = getattr(InputHDF5CheckerInterface, "check_sidechannelclass_exists", None)

    assert isinstance(check_sidechannelclass_exists, property)


def test_interface_defines_method_check_attributes():
    """
        Test that the interface has a property named check_attributes.
    """

    check_attributes = getattr(InputHDF5CheckerInterface, "check_attributes", None)

    assert isinstance(check_attributes, property)
