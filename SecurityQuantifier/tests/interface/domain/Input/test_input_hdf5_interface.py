"""
    Test case for the InputHDF5Interface interface
"""
from securityquantifier.interface.domain.Input.input_hdf5_interface import InputHDF5Interface


def test_interface_defines_method_path():
    """
        Test that the interface has a property named path.
    """

    path = getattr(InputHDF5Interface, "path", None)

    assert isinstance(path, property)
