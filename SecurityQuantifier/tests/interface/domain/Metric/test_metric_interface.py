"""
    Test case for the MetricInterface interface
"""
from securityquantifier.interface.domain.Metric.metric_interface import MetricInterface


def test_interface_defines_method_data():
    """
        Test that the interface has a property named data.
    """

    data = getattr(MetricInterface, "data", None)

    assert isinstance(data, property)


def test_interface_defines_method_distance():
    """
        Test that the interface has a property named distance.
    """

    distance = getattr(MetricInterface, "distance", None)

    assert isinstance(distance, property)
