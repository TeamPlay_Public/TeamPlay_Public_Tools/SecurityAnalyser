"""
    Test case for the SecurityLevelInterface interface
"""
from securityquantifier.interface.domain.SecurityLevel.security_level_interface import SecurityLevelInterface


def test_interface_defines_method_annotation():
    """
        Test that the interface has a property named annotation.
    """

    annotation = getattr(SecurityLevelInterface, "annotation", None)

    assert isinstance(annotation, property)


def test_interface_defines_method_level():
    """
        Test that the interface has a property named level.
    """

    level = getattr(SecurityLevelInterface, "level", None)

    assert isinstance(level, property)
