#!/usr/bin/env python

"""Tests for `SecurityQuantifier` package."""

from configparser import ConfigParser
from os.path import isfile
from pathlib import Path

from sqcommon.adapter.File.HDF5.Data.hdf5_data_as_dataframe_repository import HDF5DataAsDataFrameGetRepository
from sqcommon.adapter.File.HDF5.SideChannel.side_channel_class_get_hdf5_repository \
    import SideChannelClassGetHDF5Repository
from sqcommon.adapter.File.HDF5.Variable.variables_cardinality_repository \
    import VariablesCardinalityHDF5Repository
from sqcommon.adapter.File.HDF5.Variable.variables_count_hdf5_repository import VariablesCountHDF5Repository
from sqcommon.adapter.File.HDF5.Variable.variables_names_list_hdf5_repository \
    import VariablesNamesHDF5Repository
from sqcommon.enums.enum_hdf5_attributes import HDF5Attributes
from sqcommon.use_case.Data.hdf5_data_as_dataframe_get import HDF5DataAsDataFrameGetUC
from sqcommon.use_case.SideChannel.side_channel_class_get import SideChannelClassGetUC
from sqcommon.use_case.Variable.variables_hdf5_count import VariablesHDF5CountUC
from sqcommon.use_case.Variable.variables_names_hdf5_list import VariablesNamesHDF5UC

from securityquantifier.domain.Distance.distance_chebyshev import DistanceChebyshev
from securityquantifier.use_case.Metric.metric_wced_compute import MetricWCEDComputeUseCase
from securityquantifier.use_case.Metric.metric_wctd_compute import MetricWCTDComputeUseCase
from sqcommon.use_case.Variable.variables_cardinality_hdf5 import VariablesCardHDF5UC


def test_security_quantifier_conf_init():
    """
        Init for the configuration file
    """
    conf_file = "../test_config/securityquantifier.ini"

    input_files = []

    # Initializing exponent
    exponent = 0

    if isfile(conf_file):
        # Configuration file call
        config = ConfigParser()
        config.read(conf_file)

        exponent = int(config['distance']['exponent'])

        input_files.append(config['input']['hdf5file_t_sensitive'])
        input_files.append(config['input']['hdf5file_t_not_sensitive'])
        input_files.append(config['input']['hdf5file_t_protected_by_ladderization'])
        input_files.append(config['input']['hdf5file_e_sensitive'])
        input_files.append(config['input']['hdf5file_e_not_sensitive'])
        input_files.append(config['input']['hdf5file_e_protected_by_ladderization'])

    return input_files, exponent


def test_security_quantifier_message_init():
    """
        Init for messages to display
    """
    messages = [" Demo step 1: WCTD : Sensitive IF Statement",
                " Demo step 2: WCTD : Not Sensitive IF Statement",
                " Demo step 3: WCTD : Protected by Ladderization",
                " Demo step 4: WCED : Sensitive IF Statement",
                " Demo step 5: WCED : Not Sensitive IF Statement",
                " Demo step 6: WCED : Protected by Ladderization"
                ]

    return messages


def test_actd_aced_expected_results():
    """
        Expected values returned by the SecurityQuantifier
    """
    values = [
        150678990,
        11964818,
        0,
        15500101890176,
        553570628516,
        0
    ]

    return values


def test_wctd_wced_with_different_algorithms():
    """
        Three SecurityExchange files from 3 algorithms:
            Sensitive IF statement: We will use the modular exponentiation algorithm
            Not sensitive IF statement. We will use an arbitrary example
            Protected by ladderization. We will use a ladderized exponentiation algorithm
    """
    input_files, exponent = test_security_quantifier_conf_init()
    messages = test_security_quantifier_message_init()
    values = test_actd_aced_expected_results()

    # Initializing metric_wcsd
    metric_wcsd = -1

    # Loop over the different test input files
    for i in range(len(input_files)):

        # Getting the current input file and its related message
        hdf5_f = Path(input_files[i])
        message = messages[i]
        value = values[i]

        print(message)

        # UC: Get the HDF5 file as a InputHDF5 object
        # in a DataFrame
        data_uc = HDF5DataAsDataFrameGetUC(
            data_repo=HDF5DataAsDataFrameGetRepository,
            input_path=hdf5_f
        )
        data = data_uc.execute()

        # UC: Get Side Channel Class from the HDF5 file
        scc_uc = SideChannelClassGetUC(
            side_channel_class_repo=SideChannelClassGetHDF5Repository,
            input_path=hdf5_f
        )
        scc = scc_uc.execute()

        # UC: Get number of tainted variables from the HDF5 file
        nb_tainted_vars_uc = VariablesHDF5CountUC(
            variables_count_repo=VariablesCountHDF5Repository,
            input_path=hdf5_f,
            side_channel_class=scc,
            hdf5_attribute=HDF5Attributes.NB_TAINTED
        )
        nb_tainted_vars = nb_tainted_vars_uc.execute()

        # UC: Get number of untainted variables from the HDF5 file
        nb_untainted_vars_uc = VariablesHDF5CountUC(
            variables_count_repo=VariablesCountHDF5Repository,
            input_path=hdf5_f,
            side_channel_class=scc,
            hdf5_attribute=HDF5Attributes.NB_UNTAINTED
        )
        nb_untainted_vars = nb_untainted_vars_uc.execute()

        # UC: Get the variables names from the HDF5 file
        vars_names_uc = VariablesNamesHDF5UC(
            variables_names_repo=VariablesNamesHDF5Repository,
            input_path=hdf5_f
        )
        vars_names = vars_names_uc.execute()

        # UC: Get the cardinality of each variable
        vars_cardinality_uc = VariablesCardHDF5UC(
            variables_card_repo=VariablesCardinalityHDF5Repository,
            input_path=hdf5_f,
            side_channel_class=scc,
            vars_list=vars_names
        )
        vars_cardinalities = vars_cardinality_uc.execute()

        if scc.value == "time_worst":
            # Compute worst-case metric with cardinality
            metric_wcsd_uc = MetricWCTDComputeUseCase(
                distance_chebyshev=DistanceChebyshev,
                data=data,
                side_channel_class=scc,
                nb_tainted_vars=nb_tainted_vars,
                nb_untainted_vars=nb_untainted_vars,
                variables_cards=vars_cardinalities,
                exponent=exponent
            )
            metric_wcsd = metric_wcsd_uc.execute()

            print("  Worst-case Time Vulnerability level = ")

        elif scc.value == "energy_worst":

            metric_wced_uc = MetricWCEDComputeUseCase(
                distance_chebyshev=DistanceChebyshev,
                data=data,
                side_channel_class=scc,
                nb_tainted_vars=nb_tainted_vars,
                nb_untainted_vars=nb_untainted_vars,
                variables_cards=vars_cardinalities,
                exponent=exponent
            )
            metric_wcsd = metric_wced_uc.execute()

            print("  Worst-case Energy Vulnerability level = ")

        print("                                             ", metric_wcsd)

        assert metric_wcsd == value
