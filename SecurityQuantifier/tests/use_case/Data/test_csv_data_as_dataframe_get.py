"""
    Test case for the CSVDataAsDataFrameGetUC class
"""
from unittest.mock import Mock

from pandas import DataFrame

from securityquantifier.use_case.Data.csv_data_as_dataframe_get import CSVDataAsDataFrameGetUC


def test_uc_hdf5_datadataframe_get(dataframe_from_indiscernability_bl,
                                   return_input_file_indiscernability_csv_file_for_repository_test):
    """ Test the CSVDataAsDataFrameGetUC execute method return value """
    # Creating the Mock
    repo = Mock()

    # Instantiating the UseCase with repo as a mock
    uc = CSVDataAsDataFrameGetUC(vars_list_repo=repo,
                                 input_path=return_input_file_indiscernability_csv_file_for_repository_test)

    # Setting the return value when calling the method find on the mock
    repo.find.return_value = dataframe_from_indiscernability_bl

    # Executing the execute method of the UseCase, which calls the method find() from the mock
    result = uc.execute()
    # Assert find was called with the proper argument
    repo.find.assert_called_with(return_input_file_indiscernability_csv_file_for_repository_test)

    # Check the return value is of correct type
    assert isinstance(result, DataFrame)
