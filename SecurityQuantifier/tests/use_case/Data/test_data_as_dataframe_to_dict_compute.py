"""
    Test case for the DataAsDataFrameToDictGetUC class
"""
from typing import Dict, List

import pytest
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.use_case.Data.data_as_dataframe_to_dict_get import DataAsDataFrameToDictGetUC

@pytest.mark.skip(reason="DataAsDataFrameToDictGetUC is not implemented. Not used. Will it be used ?")
@pytest.fixture
def dad_to_dict_use_case_init(
        data_as_dataframe_from_d4_dot_4_page_73,
        return_nb_tainted_equals_1,
        return_nb_untainted_equals_2):
    """ Generate an instance of DataAsDataframeToDictUC """
    dad_to_dict_uc = DataAsDataFrameToDictGetUC(data_as_dataframe=data_as_dataframe_from_d4_dot_4_page_73,
                                                nb_tainted_vars=return_nb_tainted_equals_1,
                                                nb_untainted_vars=return_nb_untainted_equals_2,
                                                )

    return dad_to_dict_uc

@pytest.mark.skip(reason="DataAsDataFrameToDictGetUC is not implemented. Not used. Will it be used ?")
def test_se_file_use_case_is_instance_of_dendrogram_uc(dad_to_dict_use_case_init):
    """ Test whether DataAsDataframeToDictUC is an implementation of UseCaseInterface """

    dad_to_dict_uc = dad_to_dict_use_case_init

    assert isinstance(dad_to_dict_uc, DataAsDataFrameToDictGetUC)
    assert isinstance(dad_to_dict_uc, UseCaseInterface)

@pytest.mark.skip(reason="DataAsDataFrameToDictGetUC is not implemented. Not used. Will it be used ?")
def test_se_file_use_case_defines_method_execute():
    """
        Test that DataAsDataframeToDictUC has a method named execute.
    """

    execute = getattr(DataAsDataFrameToDictGetUC, "execute", None)

    assert callable(execute)

@pytest.mark.skip(reason="DataAsDataFrameToDictGetUC is not implemented. Not used. Will it be used ?")
def test_se_file_use_case_execute_returns_list(dad_to_dict_use_case_init):
    """
        Test that the execute method of DataAsDataframeToDictUC returns a Dict
    """
    dad_to_dict_uc = dad_to_dict_use_case_init

    dad_as_dict = dad_to_dict_uc.execute()

    assert isinstance(dad_as_dict, Dict)

@pytest.mark.skip(reason="DataAsDataFrameToDictGetUC is not implemented. Not used. Will it be used ?")
def test_se_file_use_case_returns_correct_dict_structure(
        dad_to_dict_use_case_init):
    """
        Test that the execute method returns the correct Dict structure,
        that is a Dict containing a List of length 10, each element being a List of length 2
    """
    dad_to_dict_uc = dad_to_dict_use_case_init

    uc_results = dad_to_dict_uc.execute()

    # It is a Dict
    assert isinstance(uc_results, Dict)

    # A Dict made of List of length 10, each being a List of length 2
    for value in uc_results.values():
        assert isinstance(value, List)
        assert len(value) == 10
        for elem in value:
            assert isinstance(elem, List)
            assert len(elem) == 2

@pytest.mark.skip(reason="DataAsDataFrameToDictGetUC is not implemented. Not used. Will it be used ?")
def test_se_file_use_case_returns_correct_dict_values(
        dad_to_dict_use_case_init,
        return_dataframe_from_d44_page_73_as_dict,
        return_se_file_as_dict_key_2_10,
        return_se_file_as_dict_value_for_2_10,
        return_se_file_as_dict_key_4_15,
        return_se_file_as_dict_value_for_4_15,
        return_se_file_as_dict_key_8_35,
        return_se_file_as_dict_value_for_8_35
):
    """
        Test that the execute method returns the correct Dict in terms of values
    """
    dad_to_dict_uc = dad_to_dict_use_case_init

    expected_dict = return_dataframe_from_d44_page_73_as_dict

    uc_results = dad_to_dict_uc.execute()

    # Check the Dict has the proper keys.
    # [*uc_results] returns the keys of the dictionary as a list
    assert return_se_file_as_dict_key_2_10 in [*uc_results]
    assert return_se_file_as_dict_key_4_15 in [*uc_results]
    assert return_se_file_as_dict_key_8_35 in [*uc_results]

    # Check the Dict has the proper values
    value = uc_results.get(return_se_file_as_dict_key_2_10)

    assert value == return_se_file_as_dict_value_for_2_10

    value = uc_results.get(return_se_file_as_dict_key_4_15)

    assert value == return_se_file_as_dict_value_for_4_15

    value = uc_results.get(return_se_file_as_dict_key_8_35)

    assert value == return_se_file_as_dict_value_for_8_35
