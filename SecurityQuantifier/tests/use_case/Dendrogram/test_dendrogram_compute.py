"""
    Testing class related to the usecase DendrogramUseCase and containing testing methods
"""

import pytest
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.domain.Dendrogram.dendrogram_result import DendrogramResult
from securityquantifier.enums.enum_termination_criteria import EnumTerminationCriteria
from securityquantifier.use_case.Dendrogram.dendrogram_compute import DendrogramUseCase


@pytest.fixture
def dendrogram_use_case_init_with_4_cluster_3_resolutions(
        dendrogram_model_init_with_4_cluster_3_resolutions_observations_a):
    """ Generate an instance of DendrogramUseCase """
    dendrogram_uc = DendrogramUseCase(dendrogram=dendrogram_model_init_with_4_cluster_3_resolutions_observations_a)

    return dendrogram_uc


@pytest.fixture
def dendrogram_use_case_init_with_1_cluster_5_resolutions(
        dendrogram_model_init_with_1_cluster_5_resolutions_with_obs_a):
    """ Generate an instance of DendrogramUseCase """
    dendrogram_uc = DendrogramUseCase(dendrogram=dendrogram_model_init_with_1_cluster_5_resolutions_with_obs_a)

    return dendrogram_uc


@pytest.fixture
def dendrogram_use_case_init_with_4_clusters_5_resolutions(
        dendrogram_model_init_with_4_clusters_5_resolutions_with_obs_a):
    """ Generate an instance of DendrogramUseCase """
    dendrogram_uc = DendrogramUseCase(dendrogram=dendrogram_model_init_with_4_clusters_5_resolutions_with_obs_a)

    return dendrogram_uc


@pytest.fixture
def dendrogram_use_case_init_with_1_cluster_3_resolutions(
        dendrogram_model_init_with_1_cluster_3_resolutions_with_obs_a):
    """ Generate an instance of DendrogramUseCase """
    dendrogram_uc = DendrogramUseCase(dendrogram=dendrogram_model_init_with_1_cluster_3_resolutions_with_obs_a)

    return dendrogram_uc


def test_dendrogram_uc_is_instance_of_dendrogram_uc(dendrogram_use_case_init_with_1_cluster_5_resolutions):
    """ Test whether DendrogramUseCase is an implementation of UseCaseInterface """

    dendrogram_uc = dendrogram_use_case_init_with_1_cluster_5_resolutions

    assert isinstance(dendrogram_uc, DendrogramUseCase)
    assert isinstance(dendrogram_uc, UseCaseInterface)


def test_dendrogram_uc_defines_method_execute():
    """
        Test that DendrogramUseCase has a method named execute.
    """

    execute = getattr(DendrogramUseCase, "execute", None)

    assert callable(execute)


def test_dendrogram_uc_execute_returns_dendrogramresult(dendrogram_use_case_init_with_1_cluster_5_resolutions):
    """
        Test that the execute method returns a DendrogramResult
    """
    dendrogram_uc = dendrogram_use_case_init_with_1_cluster_5_resolutions

    dendrogram = dendrogram_uc.execute()

    assert isinstance(dendrogram, DendrogramResult)


def test_dendrogram_uc_execute_returns_correct_clustering_with_1_cluster_5_resolutions(
        dendrogram_use_case_init_with_1_cluster_5_resolutions,
        return_final_leakage_clustering_for_default_indiscernability_bl):
    """
        Test that the execute method returns the correct clustering.
        With dendrogram_use_case_init_with_1_cluster_5_resolutions,
        the expected clustering is [[1, 2, 6, 8, 9, 12, 14]]

        Because none of the termination criteria are satisfied before the end of the dendrogram algorithm,
        then we should have a DendrogramResult with satisfied_criterium = Enum
        its final_clustering should be made of one cluster containing every observation, ie [[1, 2, 6, 8, 9, 12, 14]]
    """
    dendrogram_uc = dendrogram_use_case_init_with_1_cluster_5_resolutions

    expected_clustering = return_final_leakage_clustering_for_default_indiscernability_bl

    dendrogram_result = dendrogram_uc.execute()

    assert isinstance(dendrogram_result, DendrogramResult)
    assert isinstance(dendrogram_result.satisfied_criterium, EnumTerminationCriteria)

    assert dendrogram_result.final_leakage_clustering == expected_clustering
    assert dendrogram_result.final_resolution == 4
    assert dendrogram_result.final_nb_clusters == 1
    assert dendrogram_result.satisfied_criterium == EnumTerminationCriteria.DEFAULT


def test_dendrogram_uc_execute_returns_correct_clustering_with_4_cluster_3_resolutions(
        dendrogram_use_case_init_with_4_cluster_3_resolutions,
        return_final_leakage_clustering_for_nb_clusters_indiscernability_bl):
    """
        Test that the execute method returns the correct clustering.
        With dendrogram_use_case_init_with_4_cluster_3_resolutions,
        the expected clustering is [[1, 2], [6, 8, 9], [12, 14]]

        Both termination criteria are satisfied at the same index of loop.
        The satisfied_criterium will be resolution because when the criterium resolution is provided,
        then we check termination only wrt this criterium
    """
    dendrogram_uc = dendrogram_use_case_init_with_4_cluster_3_resolutions

    expected_clustering = return_final_leakage_clustering_for_nb_clusters_indiscernability_bl

    dendrogram_result = dendrogram_uc.execute()

    assert dendrogram_result.final_leakage_clustering == expected_clustering
    assert dendrogram_result.final_resolution == 3
    assert dendrogram_result.final_nb_clusters == 3
    assert dendrogram_result.satisfied_criterium == EnumTerminationCriteria.RESOLUTION


def test_dendrogram_uc_execute_returns_correct_clustering_with_4_clusters_5_resolutions(
        dendrogram_use_case_init_with_4_clusters_5_resolutions,
        return_final_leakage_clustering_for_default_indiscernability_bl):
    """
        Test that the execute method returns the correct clustering.
        With dendrogram_use_case_init_with_4_clusters_5_resolutions,
        the expected clustering is [[1, 2], [6, 8, 9], [12, 14]]

        Because only the nb_clusters termination criteria is satisfied before the end of the dendrogram algorithm,
        then we should have a DendrogramResult with satisfied_criterium=EnumTerminationCriteria.NB_CLUSTERS
        with its final_clustering made of three clusters, so final_nb_clusters=3 and final_resolution = 3.
        Nb: the required nb_clusters is 4 but one loop round starts from 5 clusters and ends at 3,
         skipping the state with 4 clusters. This is due to the fact that distance=2 appears twice
         so in one loop round two clusters will be merged.
    """
    dendrogram_uc = dendrogram_use_case_init_with_4_clusters_5_resolutions

    expected_nb_cluster_clustering = return_final_leakage_clustering_for_default_indiscernability_bl

    dendrogram_result = dendrogram_uc.execute()

    assert dendrogram_result.final_leakage_clustering == expected_nb_cluster_clustering
    assert dendrogram_result.final_resolution == 4
    assert dendrogram_result.final_nb_clusters == 1
    assert dendrogram_result.satisfied_criterium == EnumTerminationCriteria.DEFAULT


def test_dendrogram_uc_execute_returns_correct_clustering_with_1_cluster_3_resolutions(
        dendrogram_use_case_init_with_1_cluster_3_resolutions,
        return_final_leakage_clustering_for_resolution_indiscernability_bl,
        return_final_leakage_clustering_for_default_indiscernability_bl
):
    """
        Test that the execute method returns the correct clustering.
        With dendrogram_use_case_init_with_1_cluster_3_resolutions,
        the expected clustering is [[1, 2], [6, 8, 9], [12, 14]]

        Because only the resolution termination criteria is satisfied before the end of the dendrogram algorithm,
        then we should have a DendrogramResult with satisfied_criterium=EnumTerminationCriteria.RESOLUTION
    """
    dendrogram_uc = dendrogram_use_case_init_with_1_cluster_3_resolutions

    expected_resolution_clustering = return_final_leakage_clustering_for_resolution_indiscernability_bl

    dendrogram_result = dendrogram_uc.execute()

    assert dendrogram_result.final_leakage_clustering == expected_resolution_clustering
    assert dendrogram_result.final_resolution == 3
    assert dendrogram_result.final_nb_clusters == 3
    assert dendrogram_result.satisfied_criterium == EnumTerminationCriteria.RESOLUTION
