"""
    Testing class related to the use case DistanceChebyshevComputeUseCase and containing testing methods
"""


def test_distance_chebyshev_non_negativity(domain_distance_chebyshev_equal_zero):
    """ Test non negativity property d(x,y) >= 0 """

    assert domain_distance_chebyshev_equal_zero >= 0


def test_distance_chebyshev_identity(domain_distance_chebyshev_check_identity_1,
                                     domain_distance_chebyshev_check_identity_2,
                                     config_file_epsilon_init):
    """ Test identity property d(x,x) = 0 """

    assert abs(domain_distance_chebyshev_check_identity_1 - 0) < config_file_epsilon_init
    assert abs(domain_distance_chebyshev_check_identity_2 - 0) < config_file_epsilon_init


def test_distance_chebyshev_symmetry(domain_distance_chebyshev_check_symmetry_1,
                                     domain_distance_chebyshev_check_symmetry_2,
                                     config_file_epsilon_init):
    """ Test symmetry property d(x,y) = d(y,x) """

    assert abs(domain_distance_chebyshev_check_symmetry_1 - domain_distance_chebyshev_check_symmetry_2) \
           < config_file_epsilon_init


def test_distance_chebyshev_homogeneity(domain_distance_chebyshev_check_homogeneity_ax,
                                        domain_distance_chebyshev_check_homogeneity_x,
                                        return_homogeneity_property_equals_100):
    """ Test homogeneity property d(a*x,0) = |a|*d(x,0) """

    assert domain_distance_chebyshev_check_homogeneity_ax == abs(
        return_homogeneity_property_equals_100) * domain_distance_chebyshev_check_homogeneity_x


def test_distance_chebyshev_triangle_inequality(domain_distance_chebyshev_check_triangle_inequality_dxz,
                                                domain_distance_chebyshev_check_triangle_inequality_dxy,
                                                domain_distance_chebyshev_check_triangle_inequality_dyz):
    """ Test triangle inequality property d(x,z) <= d(x,y) + d(y,z) """
    assert domain_distance_chebyshev_check_triangle_inequality_dxz \
           <= domain_distance_chebyshev_check_triangle_inequality_dxy \
           + domain_distance_chebyshev_check_triangle_inequality_dyz


def test_distance_chebyshev_result_is_instance_of_float(domain_distance_chebyshev_equal_zero):
    """ Test whether distance's return value is a float """

    assert isinstance(domain_distance_chebyshev_equal_zero, float)


def test_distance_chebyshev_result_equal_0(domain_distance_chebyshev_equal_zero,
                                           config_file_epsilon_init):
    """ Test whether distance's return value is equal to zero """

    assert abs(domain_distance_chebyshev_equal_zero - 0) < config_file_epsilon_init


def test_distance_chebyshev_result_equal_to_zero(domain_distance_chebyshev_equal_zero,
                                                 return_computed_chebyshev_distance_equals_zero,
                                                 config_file_epsilon_init):
    """
        Test whether chebyshev distance's return value is equal to CHEBYSHEV_DISTANCE_COMPUTED_VALUE
        with approximate epsilon
    """

    assert abs(
      domain_distance_chebyshev_equal_zero - return_computed_chebyshev_distance_equals_zero) < config_file_epsilon_init
