"""
    Testing class related to the usecase AllDistributionsGetUseCase and containing testing methods
"""
from numpy.testing import assert_allclose
import pytest

from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.use_case.Distribution.all_distributions_get import AllDistributionsGetUseCase


@pytest.fixture
def marginal_distribution_get_uc_model_init(return_distributionasdict_secret_public_12_tuples,
                                            tuples_information_model_init_for_12tuples):
    """ Generate an instance of AllDistributionsGetUseCase """
    marginal_distribution_uc = AllDistributionsGetUseCase(
        dist_as_dict=return_distributionasdict_secret_public_12_tuples,
        tuples_information=tuples_information_model_init_for_12tuples)

    return marginal_distribution_uc


def test_marginal_distribution_get_uc_is_instance_of_interface(
        marginal_distribution_get_uc_model_init):
    """ Test whether AllDistributionsGetUseCase is an implementation of UseCaseInterface """

    marginal_distribution_uc = marginal_distribution_get_uc_model_init

    assert isinstance(marginal_distribution_uc, AllDistributionsGetUseCase)
    assert isinstance(marginal_distribution_uc, UseCaseInterface)


def test_marginal_distribution_get_uc_defines_method_execute():
    """
        Test that the AllDistributionsGetUseCase has a method named execute.
    """

    execute = getattr(AllDistributionsGetUseCase, "execute", None)

    assert callable(execute)


def test_marginal_distribution_get_uc_method_execute_returns_dict(marginal_distribution_get_uc_model_init):
    """
        Test that the AllDistributionsGetUseCase execute method returns a DistributionAsDict.
    """
    marginal_distribution_uc = marginal_distribution_get_uc_model_init

    returned = marginal_distribution_uc.execute()

    assert isinstance(returned, DistributionAsDict)


def test_marginal_distribution_get_uc_method_execute_returned_dict_has_keys(
        marginal_distribution_get_uc_model_init,
        return_enumdistributiontuples_value_for_secret,
        return_enumdistributiontuples_value_for_public):
    """
        Test that the AllDistributionsGetUseCase execute method returns a DistributionAsDict
        with proper keys.
    """
    marginal_distribution_uc = marginal_distribution_get_uc_model_init

    dist_dict = marginal_distribution_uc.execute()

    # [*dist_dict.data] returns the keys of the dictionary as a list
    assert return_enumdistributiontuples_value_for_secret in [*dist_dict.data]
    assert return_enumdistributiontuples_value_for_public in [*dist_dict.data]


def test_marginal_distribution_get_uc_method_execute_returned_dict_has_values(
        marginal_distribution_get_uc_model_init,
        return_enumdistributiontuples_value_for_secret,
        return_enumdistributiontuples_value_for_public,
        return_marginal_proba_secret_for_12_tuples,
        return_marginal_proba_public_for_12_tuples):
    """
        Test that the AllDistributionsGetUseCase execute method returns a DistributionAsDict
        with proper values.
    """
    marginal_distribution_uc = marginal_distribution_get_uc_model_init

    dist_dict = marginal_distribution_uc.execute()

    assert_allclose(
        dist_dict.data[return_enumdistributiontuples_value_for_secret], return_marginal_proba_secret_for_12_tuples)
    assert_allclose(
        dist_dict.data[return_enumdistributiontuples_value_for_public], return_marginal_proba_public_for_12_tuples)
