"""
    Testing class related to the usecase UniformJointDistributionComputeUseCase and containing testing methods
"""
from numpy.testing import assert_allclose
import pytest

from sqcommon.domain.Distribution.distribution_dict import DistributionAsDict
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.use_case.Distribution.all_distributions_get import AllDistributionsGetUseCase
from securityquantifier.use_case.Distribution.uniform_joint_distribution_compute import \
    UniformJointDistributionComputeUseCase


@pytest.fixture
def uniform_joint_dist_compute_uc_model_init(return_nb_tuples_equals_12):
    """ Generate an instance of UniformJointDistributionComputeUseCase """
    uniform_joint_dist_compute_uc = UniformJointDistributionComputeUseCase(
        total_nb_tuples=return_nb_tuples_equals_12)

    return uniform_joint_dist_compute_uc


def test_uniform_joint_dist_compute_uc_is_instance_of_interface(
        uniform_joint_dist_compute_uc_model_init):
    """ Test whether UniformJointDistributionComputeUseCase is an implementation of UseCaseInterface """

    uniform_joint_dist_compute_uc = uniform_joint_dist_compute_uc_model_init

    assert isinstance(uniform_joint_dist_compute_uc, UniformJointDistributionComputeUseCase)
    assert isinstance(uniform_joint_dist_compute_uc, UseCaseInterface)


def test_uniform_joint_dist_compute_uc_defines_method_execute():
    """
        Test that the UniformJointDistributionComputeUseCase has a method named execute.
    """

    execute = getattr(AllDistributionsGetUseCase, "execute", None)

    assert callable(execute)


def test_uniform_joint_dist_compute_uc_method_execute_returns_dict(uniform_joint_dist_compute_uc_model_init):
    """
        Test that the UniformJointDistributionComputeUseCase execute method returns a DistributionAsDict.
    """
    uniform_joint_dist_compute_uc = uniform_joint_dist_compute_uc_model_init

    returned = uniform_joint_dist_compute_uc.execute()

    assert isinstance(returned, DistributionAsDict)


def test_uniform_joint_dist_compute_uc_method_execute_returned_dict_has_keys(
        uniform_joint_dist_compute_uc_model_init,
        return_enumdistributiontuples_value_for_secretpublic):
    """
        Test that the UniformJointDistributionComputeUseCase execute method returns a DistributionAsDict
        with the proper key.
    """
    uniform_joint_dist_compute_uc = uniform_joint_dist_compute_uc_model_init

    dist_dict = uniform_joint_dist_compute_uc.execute()

    # [*dist_dict.data] returns the keys of the dictionary as a list
    assert return_enumdistributiontuples_value_for_secretpublic in [*dist_dict.data]


def test_uniform_joint_dist_compute_uc_method_execute_returned_dict_has_values(
        uniform_joint_dist_compute_uc_model_init,
        return_uniform_joint_proba_secret_for_12_tuples,
        return_enumdistributiontuples_value_for_secretpublic):
    """
        Test that the UniformJointDistributionComputeUseCase execute method returns a DistributionAsDict
        with proper values.
    """
    uniform_joint_dist_compute_uc = uniform_joint_dist_compute_uc_model_init

    dist_dict = uniform_joint_dist_compute_uc.execute()

    assert_allclose(
        dist_dict.data[return_enumdistributiontuples_value_for_secretpublic],
        return_uniform_joint_proba_secret_for_12_tuples)
