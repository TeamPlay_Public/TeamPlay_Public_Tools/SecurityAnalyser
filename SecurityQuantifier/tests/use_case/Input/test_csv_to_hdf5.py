"""
    Testing class used to generate a HDF5 file from a CSV file
"""
import configparser

from pathlib import Path
from typing import List

from pandas import DataFrame

import h5py
import numpy as np
import pytest
from sqcommon.domain.SideChannel.side_channel_class import SideChannelClass
from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.enums.enum_hdf5_attributes import HDF5Attributes
from sqcommon.use_case.SideChannel.side_channel_class_get import SideChannelClassGetUC
from securityquantifier.adapter.File.CSV.Data.csv_data_as_dataframe_repository import CSVDataAsDataFrameGetRepository
from securityquantifier.adapter.File.CSV.SideChannel.side_channel_class_get_csv_repository \
    import SideChannelClassGetCSVRepository
from securityquantifier.adapter.File.CSV.Variable.variables_cardinality_csv_repository import \
    VariablesCardinalityCSVRepository
from securityquantifier.adapter.File.CSV.Variable.vars_list_csv_repository import CSVVariablesListRepository
from securityquantifier.use_case.Data.csv_data_as_dataframe_get import CSVDataAsDataFrameGetUC
from securityquantifier.use_case.Variable.tainted_vars_csv_list import TaintedListUseCase
from securityquantifier.use_case.Variable.untainted_vars_csv_list import UntaintedListUseCase
from tests.use_case.Variable.variables_cardinality_csv_get import VariablesCardCSVGetUC


@pytest.mark.skip(reason="Not a test per se. Used to generate a HDF5 file from a CSV file")
def test_input_csv_get():
    """
        Used to generate a HDF5 file from a CSV file
    """
    # Configuration file.
    conf_file = '../../test_config/securityquantifier.ini'

    # Configuration file call
    config = configparser.ConfigParser()
    try:
        config.read(conf_file)
    except FileNotFoundError as e:
        print(e.errno)

    csv_f = Path(config['input']['csvfile'])
    hdf5_f = Path(config['output']['hdf5file'])

    # use case: Populate the Input Object with:
    # Variables
    tainted_vars_uc = TaintedListUseCase(
        vars_list_repo=CSVVariablesListRepository,
        input_path=csv_f
    )
    tainted_vars = tainted_vars_uc.execute()

    untainted_vars_uc = UntaintedListUseCase(
        vars_list_repo=CSVVariablesListRepository,
        input_path=csv_f
    )
    untainted_vars = untainted_vars_uc.execute()

    # Side channel class
    side_channel_class_uc = SideChannelClassGetUC(
        side_channel_class_repo=SideChannelClassGetCSVRepository,
        input_path=csv_f
    )
    side_channel_class = side_channel_class_uc.execute()

    # UC: Get the cardinality of each variable
    vars_cardinality_uc = VariablesCardCSVGetUC(
        variables_card_repo=VariablesCardinalityCSVRepository,
        input_path=csv_f,
        tainted_vars=tainted_vars,
        untainted_vars=untainted_vars
    )
    vars_cardinalities = vars_cardinality_uc.execute()

    # data
    data_uc = CSVDataAsDataFrameGetUC(
        vars_list_repo=CSVDataAsDataFrameGetRepository,
        input_path=csv_f
    )
    data = data_uc.execute()

    generate_hdf5(h5_file=hdf5_f,
                  data=data.data,
                  side_channel_class=side_channel_class.value,
                  tainted_vars=tainted_vars,
                  untainted_vars=untainted_vars,
                  vars_cardinalities=vars_cardinalities)


def generate_hdf5(h5_file: Path,
                  data: DataFrame,
                  side_channel_class: SideChannelClass,
                  tainted_vars: VariablesList,
                  untainted_vars: VariablesList,
                  vars_cardinalities: List[int]):
    """
    Generates a Security Exchange File in HDF5 from a Security Exchange File in CSV
    """
    h5_filename = Path(h5_file)
    h5_filename.touch()  # will create file, if it exists will do nothing

    # Generates HDF5
    try:
        with h5py.File(h5_filename, "w") as h5:
            sa, sa_type = df_to_sarray(data)
            print("Type of side_channel_class: ", side_channel_class)
            strscc = str(side_channel_class)
            sstrscc = isinstance(strscc, str)
            print(sstrscc)
            scc = isinstance(side_channel_class, str)
            print(scc)
            dset = h5.create_dataset(side_channel_class, data=sa, dtype=sa_type)

            nbt = HDF5Attributes.NB_TAINTED.value
            istr = isinstance(nbt, str)
            print(istr)

            # Adding HDF5 attributes
            dset.attrs[HDF5Attributes.NB_TAINTED.value] = tainted_vars.nb_vars()
            dset.attrs[HDF5Attributes.NB_UNTAINTED.value] = untainted_vars.nb_vars()
            for nb_tainted in range(0, tainted_vars.nb_vars()):
                dset.attrs[tainted_vars.lst[nb_tainted].value] = vars_cardinalities[nb_tainted]
            for nb_untainted in range(0, untainted_vars.nb_vars()):
                dset.attrs[untainted_vars.lst[nb_untainted].value] = \
                    vars_cardinalities[nb_untainted+tainted_vars.nb_vars()]

            h5.flush()

    except FileNotFoundError as e:
        print(e.errno)

    #
    try:
        with h5py.File(h5_file, "r") as f:
            #     # List all groups
            print("Keys: %s" % f.keys())
    except FileNotFoundError as e:
        print(e.errno)


def df_to_sarray(df):
    """
    Convert a pandas DataFrame object to a numpy structured array.
    Also, for every column of a str type, convert it into
    a 'bytes' str literal of length = max(len(col)).

    :param df: the data frame to convert
    :return: a numpy structured array representation of df
    """

    def make_col_type(col_type, col):
        """
            Manage columns type
        """
        try:
            if 'numpy.object_' in str(col_type.type):
                maxlens = col.dropna().str.len()
                if maxlens.any():
                    maxlen = maxlens.max().astype(int)
                    col_type = ('S%s' % maxlen, 1)
                else:
                    col_type = 'f2'
            return col.name, col_type
        except:
            print(col.name, col_type, col_type.type, type(col))
            raise

    v = df.values
    types = df.dtypes
    numpy_struct_types = [make_col_type(types[col], df.loc[:, col]) for col in df.columns]
    dtype = np.dtype(numpy_struct_types)
    z = np.zeros(v.shape[0], dtype)
    for i, k in enumerate(z.dtype.names):
        # This is in case you have problems with the encoding, remove the if branch if not
        try:
            if dtype[i].str.startswith('|S'):
                z[k] = df[k].str.encode('latin').astype('S')
            else:
                z[k] = v[:, i]
        except:
            print(k, v[:, i])
            raise

    return z, dtype
