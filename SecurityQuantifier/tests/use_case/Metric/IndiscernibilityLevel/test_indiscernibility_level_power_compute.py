"""
    Testing class related to the usecase IndiscernabilityLevelPowerComputeUseCase and containing testing methods
"""
import math

from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.use_case.Metric.IndiscernibilityLevel.indiscernibility_level_compute import \
    IndiscernabilityLevelComputeUseCase
from securityquantifier.use_case.Metric.IndiscernibilityLevel.indiscernibility_level_power_compute import \
    IndiscernabilityLevelPowerComputeUseCase


def test_indiscernabilitylevelpowercompute_uc_is_instance_of_interface(
        indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_5_indiscernability_bl):
    """ Test whether IndiscernabilityLevelPowerComputeUseCase is an implementation of UseCaseInterface """

    indiscernability_uc = indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_5_indiscernability_bl

    assert isinstance(indiscernability_uc, IndiscernabilityLevelPowerComputeUseCase)
    assert isinstance(indiscernability_uc, IndiscernabilityLevelComputeUseCase)
    assert isinstance(indiscernability_uc, UseCaseInterface)


def test_uc_indiscernability_power_compute_execute_returned_value_nb_clusters_2_resolution_5_indiscernability_bl(
        indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_5_indiscernability_bl,
        return_iir_equals_059929):
    """ Test the IndiscernabilityLevelPowerComputeUseCase execute method return value """

    # Instantiating the UseCase
    indiscernability_uc = indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_5_indiscernability_bl

    # Executing the execute method of the UseCase
    result = indiscernability_uc.execute()

    # check we have the correct return values
    assert math.isclose(result, return_iir_equals_059929, rel_tol=0.0000001)


def test_uc_indiscernability_power_compute_execute_returned_value_nb_clusters_2_resolution_999999_indiscernability_bl(
        indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_999999_indiscernability_bl,
        return_iir_equals_09422):
    """ Test the IndiscernabilityLevelComputeUseCase execute method return value """

    # Instantiating the UseCase
    indiscernability_uc = \
        indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_999999_indiscernability_bl

    # Executing the execute method of the UseCase
    result = indiscernability_uc.execute()

    # check we have the correct return values
    assert math.isclose(result, return_iir_equals_09422, rel_tol=0.0000001)


# Functions and methods tests
def test_function_indiscernability_level_compute_for_4_33(
        indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_999999_indiscernability_bl,
        dendrogramresult_model_init_indiscernability_bl_for_public_4_33,
        return_hklx__equals_024273):
    """
        Test the function indiscernability_level_compute for the public tuple [4,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods
    """

    # Instantiating the UseCase
    indiscernability_uc = \
        indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_999999_indiscernability_bl

    hklx = indiscernability_uc.indiscernability_level_compute(
        dendrogramresult_model_init_indiscernability_bl_for_public_4_33)

    assert math.isclose(hklx, return_hklx__equals_024273, rel_tol=0.0000001)


def test_function_indiscernability_level_compute_for_4_35(
        indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_999999_indiscernability_bl,
        dendrogramresult_model_init_indiscernability_bl_for_public_4_35,
        return_hklx__equals_00475):
    """
        Test the function indiscernability_level_compute for the public tuple [4,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods
    """

    # Instantiating the UseCase
    indiscernability_uc = \
        indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_999999_indiscernability_bl

    hklx = indiscernability_uc.indiscernability_level_compute(
        dendrogramresult_model_init_indiscernability_bl_for_public_4_35)

    assert math.isclose(hklx, return_hklx__equals_00475, rel_tol=0.0000001)


def test_function_indiscernability_level_compute_for_6_33(
        indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_999999_indiscernability_bl,
        dendrogramresult_model_init_indiscernability_bl_for_public_6_33,
        return_hklx__equals_01375):
    """
        Test the function indiscernability_level_compute for the public tuple [6,33] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods
    """

    # Instantiating the UseCase
    indiscernability_uc = \
        indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_999999_indiscernability_bl

    hklx = indiscernability_uc.indiscernability_level_compute(
        dendrogramresult_model_init_indiscernability_bl_for_public_6_33)

    assert math.isclose(hklx, return_hklx__equals_01375, rel_tol=0.0000001)


def test_function_indiscernability_level_compute_for_6_35(
        indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_999999_indiscernability_bl,
        dendrogramresult_model_init_indiscernability_bl_for_public_6_35,
        return_hklx__equals_013164):
    """
        Test the function indiscernability_level_compute for the public tuple [6,35] following example documented
        and tested in SecurityQuantifier/docs/tests/indiscernability_testing.ods
    """

    # Instantiating the UseCase
    indiscernability_uc = \
        indiscernabilitylevelpowercompute_uc_model_init_nb_clusters_2_resolution_999999_indiscernability_bl

    hklx = indiscernability_uc.indiscernability_level_compute(
        dendrogramresult_model_init_indiscernability_bl_for_public_6_35)

    assert math.isclose(hklx, return_hklx__equals_013164, rel_tol=0.0000001)
