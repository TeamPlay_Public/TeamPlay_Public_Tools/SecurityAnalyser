"""
    Testing class related to the use case MetricACEDComputeUseCase and containing testing methods
"""
import math

from securityquantifier.use_case.Metric.metric_aced_compute import MetricACEDComputeUseCase


def test_aced_metric_compute_uc_returned_value_equals_20_with_uniform_dist(
        metric_model_init_with_aced_metric_uniform_dist_equal_20,
        return_computed_aced_metric_equals_twenty):
    """ Test the MetricACEDComputeUseCase execute method return value equals 20"""
    metric = metric_model_init_with_aced_metric_uniform_dist_equal_20

    # Executing the execute method of the UseCase
    result = metric.compute_metric()

    # check we have the correct return value
    assert result == return_computed_aced_metric_equals_twenty


def test_aced_metric_compute_uc_returned_value_equals_6_point_4967_with_non_uniform_dist(
        metric_model_init_with_aced_metric_non_uniform_dist_equal_6_point_4967,
        return_computed_aced_metric_equals_6_point_4967):
    """ Test the MetricACEDComputeUseCase execute method return value equals 6.4967 """
    metric = metric_model_init_with_aced_metric_non_uniform_dist_equal_6_point_4967

    # Executing the execute method of the UseCase
    result = metric.compute_metric()

    # check we have the correct return value
    assert math.isclose(result, return_computed_aced_metric_equals_6_point_4967, rel_tol=0.0000001)


def test_aced_metric_compute_uc_returned_value_equals_0_with_uniform_dist(
        metric_model_init_with_aced_metric_uniform_dist_equal_0,
        return_computed_aced_metric_equals_zero):
    """ Test the MetricACEDComputeUseCase execute method return value equals 0"""
    metric = metric_model_init_with_aced_metric_uniform_dist_equal_0

    # Executing the execute method of the UseCase
    result = metric.compute_metric()

    # check we have the correct return value
    assert result == return_computed_aced_metric_equals_zero


def test_aced_metric_compute_uc_returned_value_equals_0_with_non_uniform_dist(
        metric_model_init_with_aced_metric_non_uniform_dist_equal_0,
        return_computed_aced_metric_equals_zero):
    """ Test the MetricACEDComputeUseCase execute method return value equals 0"""
    metric = metric_model_init_with_aced_metric_non_uniform_dist_equal_0

    # Executing the execute method of the UseCase
    result = metric.compute_metric()

    # check we have the correct return value
    assert result == return_computed_aced_metric_equals_zero


def test_aced_metric_compute_uc_defines_method_execute():
    """
        Test that the MetricACEDComputeUseCase has a method named execute.
    """

    execute = getattr(MetricACEDComputeUseCase, "execute", None)

    assert callable(execute)
