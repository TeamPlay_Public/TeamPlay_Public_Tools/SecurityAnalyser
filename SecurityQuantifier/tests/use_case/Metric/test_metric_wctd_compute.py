"""
    Testing class related to the use case MetricWCTDComputeUseCase and containing testing methods
"""
from securityquantifier.use_case.Metric.metric_wctd_compute import MetricWCTDComputeUseCase


def test_wctd_metric_compute_uc_returned_value_equals_60(
        metric_model_init_with_wctd_metric_equal_60_with_active_probe,
        return_computed_wctd_metric_equals_sixty):
    """ Test the MetricWCTDComputeUseCase execute method return value equals 60"""
    metric = metric_model_init_with_wctd_metric_equal_60_with_active_probe

    # Executing the execute method of the UseCase, which calls the method find_by_path() from the mock
    result = metric.compute_metric()

    # check we have the correct return value
    assert result == return_computed_wctd_metric_equals_sixty


def test_wctd_metric_compute_uc_returned_value_equals_0(
        metric_model_init_with_wctd_metric_equal_0_with_active_probe,
        return_computed_wctd_metric_equals_zero):
    """ Test the MetricWCTDComputeUseCase execute method return value equals 0"""
    metric = metric_model_init_with_wctd_metric_equal_0_with_active_probe

    # Executing the execute method of the UseCase, which calls the method find_by_path() from the mock
    result = metric.compute_metric()

    # check we have the correct return value
    assert result == return_computed_wctd_metric_equals_zero


def test_wctd_metric_compute_uc_defines_method_execute():
    """
        Test that the MetricWCTDComputeUseCase has a method named execute.
    """

    execute = getattr(MetricWCTDComputeUseCase, "execute", None)

    assert callable(execute)
