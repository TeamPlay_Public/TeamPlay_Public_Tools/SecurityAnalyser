"""
    Testing class related to the usecase TuplesInformationHDF5ComputeUseCase and containing testing methods
"""

from sqcommon.domain.Tuples.tuples_info import TuplesInformation

from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.use_case.Tuples.tuples_information_compute_uc import TuplesInformationHDF5ComputeUseCase


def test_tuples_information_compute_uc_is_instance_of_interface(
        tuples_information_compute_uc_model_init):
    """ Test whether TuplesInformationHDF5ComputeUseCase is an implementation of UseCaseInterface """

    tuples_information_uc = tuples_information_compute_uc_model_init

    assert isinstance(tuples_information_uc, TuplesInformationHDF5ComputeUseCase)
    assert isinstance(tuples_information_uc, UseCaseInterface)


def test_tuples_information_compute_uc_defines_method_execute():
    """
        Test that the TuplesInformationHDF5ComputeUseCase has a method named execute.
    """

    execute = getattr(TuplesInformationHDF5ComputeUseCase, "execute", None)

    assert callable(execute)


def test_tuples_information_compute_uc_method_execute_returns_dict(tuples_information_compute_uc_model_init):
    """
        Test that the MarginalDistributionJSONGetUseCase execute method returns a TuplesInformation.
    """
    tuples_information_uc = tuples_information_compute_uc_model_init

    returned = tuples_information_uc.execute()

    assert isinstance(returned, TuplesInformation)


def test_tuples_information_compute_uc_method_execute_returns_proper_nb_secret_tuples(
        tuples_information_compute_uc_model_init,
        return_nb_tuples_equals_3):
    """
        Test that the MarginalDistributionJSONGetUseCase execute method returns a TuplesInformation
        with proper number of secret tuples.
    """
    tuples_information_uc = tuples_information_compute_uc_model_init

    returned = tuples_information_uc.execute()

    assert returned.nb_secret_tuples == return_nb_tuples_equals_3


def test_tuples_information_compute_uc_method_execute_returns_proper_nb_public_tuples(
        tuples_information_compute_uc_model_init,
        return_nb_tuples_equals_4):
    """
        Test that the MarginalDistributionJSONGetUseCase execute method returns a TuplesInformation
        with proper number of public tuples.
    """
    tuples_information_uc = tuples_information_compute_uc_model_init

    returned = tuples_information_uc.execute()

    assert returned.nb_public_tuples == return_nb_tuples_equals_4


def test_tuples_information_compute_uc_method_execute_returns_proper_total_nb_tuples(
        tuples_information_compute_uc_model_init,
        return_nb_tuples_equals_12):
    """
        Test that the MarginalDistributionJSONGetUseCase execute method returns a TuplesInformation
        with proper total number of tuples.
    """
    tuples_information_uc = tuples_information_compute_uc_model_init

    returned = tuples_information_uc.execute()

    assert returned.total_nb_tuples == return_nb_tuples_equals_12
