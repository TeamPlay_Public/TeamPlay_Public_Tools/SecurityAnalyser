"""
    Testing class related to the use case TaintedListUseCase and containing testing methods
"""
from pathlib import Path
from unittest import mock

import pytest
from sqcommon.enums.enum_var_types import VariableTypes

from securityquantifier.use_case.Variable import tainted_vars_csv_list as uc


@pytest.fixture
def domain_tainted_variables():
    """ Fixture method to return a list of tainted variables """
    tainted = "k"

    return tainted


@pytest.fixture
def input_path():
    """ Fixture method to return an input path """

    return Path("../data/card/cardinality_k4_a4_n4.h5")


def test_list_tainted_vars(domain_tainted_variables):
    """ Test the TaintedListUseCase execute method return value """
    repo = mock.Mock()
    repo.find_by_type.return_value = domain_tainted_variables

    tainted_vars_list_uc = uc.TaintedListUseCase(vars_list_repo=repo, input_path=input_path)
    result = tainted_vars_list_uc.execute()

    repo.find_by_type.assert_called_with(VariableTypes.TAINTED, input_path)
    assert result == domain_tainted_variables
