"""
    Testing class related to the use case UntaintedListUseCase and containing testing methods
"""
from unittest import mock

import pytest
from sqcommon.enums.enum_var_types import VariableTypes

from securityquantifier.use_case.Variable import untainted_vars_csv_list as uc


@pytest.fixture
def domain_untainted_variables():
    """ Fixture method to return a list of untainted variables """
    untainted1 = "a"

    untainted2 = "n"

    return [untainted1, untainted2]


@pytest.fixture
def input_path():
    """ Fixture method to return an input path """

    return "../data/card/cardinality_k4_a4_n4.h5"


def test_list_untainted_vars(domain_untainted_variables):
    """ Test the UntaintedListUseCase execute method return value """
    repo = mock.Mock()
    repo.find_by_type.return_value = domain_untainted_variables

    input_path = mock.Mock()
    input_path = input_path()

    untainted_vars_list_uc = uc.UntaintedListUseCase(vars_list_repo=repo, input_path=input_path)
    result = untainted_vars_list_uc.execute()

    repo.find_by_type.assert_called_with(VariableTypes.UNTAINTED, input_path)
    assert result == domain_untainted_variables
