"""
    Testing class related to the use case VariablesCardCSVGetUC and containing testing methods
"""
from pathlib import Path
from unittest import mock

import pytest

from tests.use_case.Variable import variables_cardinality_csv_get as uc


@pytest.fixture
def input_path():
    """ Fixture method to return an input path """

    return Path("../data/card/cardinality_k4_a4_n4.h5")


def test_list_tainted_vars(return_cardinalities_equal_to_3_2_2,
                           return_variables_list_secret3,
                           return_variables_list_a2_n2,
                           return_nb_tainted_equals_1,
                           return_nb_untainted_equals_2):
    """ Test the VariablesCardCSVGetUC execute method return value """
    repo = mock.Mock()
    repo.find_all.return_value = return_cardinalities_equal_to_3_2_2

    vars_cards_uc = uc.VariablesCardCSVGetUC(variables_card_repo=repo,
                                             input_path=input_path,
                                             tainted_vars=return_variables_list_secret3,
                                             untainted_vars=return_variables_list_a2_n2)
    result = vars_cards_uc.execute()

    repo.find_all.assert_called_with(input_file=input_path,
                                     nb_tainted_vars=return_nb_tainted_equals_1,
                                     nb_untainted_vars=return_nb_untainted_equals_2)
    assert result == return_cardinalities_equal_to_3_2_2
