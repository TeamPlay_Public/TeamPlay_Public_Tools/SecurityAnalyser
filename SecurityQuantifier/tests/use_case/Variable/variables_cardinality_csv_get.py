""" Module defining the uses cases related to the Variables """
from pathlib import Path

from sqcommon.domain.Variable.variables_list import VariablesList
from sqcommon.interface.use_case.use_case_interface import UseCaseInterface

from securityquantifier.adapter.File.CSV.Variable.variables_cardinality_csv_repository import \
    VariablesCardinalityCSVRepository


class VariablesCardCSVGetUC(UseCaseInterface):
    """ VariablesCardCSVGetUC class retrieves the cardinality of each variable from the CSV file """

    def __init__(self,
                 variables_card_repo: VariablesCardinalityCSVRepository,
                 input_path: Path,
                 tainted_vars: VariablesList,
                 untainted_vars: VariablesList):
        """ Constructor for the VariablesCardCSVGetUC class """
        self._repository = variables_card_repo
        self._input_path = input_path
        self.tainted_vars_list = tainted_vars.nb_vars()
        self.untainted_vars_list = untainted_vars.nb_vars()

    def execute(self):
        """
            The execute() method calls the adapter to get for each variables in vars_list its cardinality
        """
        return self._repository.find_all(input_file=self._input_path,
                                         nb_tainted_vars=self.tainted_vars_list,
                                         nb_untainted_vars=self.untainted_vars_list)

    def __str__(self):
        """
            Returns the class name as a string
        """
        return self.__class__.__name__
